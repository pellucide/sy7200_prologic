# Project: SY7000-SDK Message Display Only - Spieler

CPP  = /tools/powerpc-88x-linux-gnu/bin/g++ -D__TA7700_TERMINAL__ -D__CYGWIN__ -Dclient_prologic
#CPP  = g++ -D__DEBUG__
CC   = /tools/powerpc-88x-linux-gnu/bin/gcc -D__TA7700_TERMINAL__ -D__CYGWIN__ -Dclient_prologic
#CC   = gcc -D__DEBUG__
STRIP = /tools/powerpc-88x-linux-gnu/bin/strip
#STRIP = strip
WINDRES = windres.exe
RES  =
OBJ  = msgonly/Spieler.o msgonly/common.o msgonly/FPOperation.o msgonly/UserBarcode.o msgonly/UserInput.o msgonly/TAString.o msgonly/User.o msgonly/UserAudible.o msgonly/UserVisual.o msgonly/UserDigital.o msgonly/StdInput.o msgonly/KeyboardReader.o msgonly/LCDHandler.o msgonly/LCDUtil.o msgonly/LCDWEPHandler.o msgonly/LegacyTrans.o msgonly/TransFIFO.o msgonly/UtilityBufferOutput.o msgonly/ErrorSystem.o msgonly/InterfaceAccessControlVector.o msgonly/InterfaceBellScheduleVector.o msgonly/InterfaceBufferDays.o msgonly/InterfaceBufferEnum.o msgonly/InterfaceBufferFlags.o msgonly/InterfaceBufferGeneric.o msgonly/InterfaceElement.o msgonly/InterfaceElementBase.o msgonly/InterfaceElementExtend.o msgonly/InterfaceElementSystem.o msgonly/InterfaceMessageBadgeVector.o msgonly/InterfaceProfileAssignVector.o msgonly/InterfaceValidateEntryVector.o msgonly/LegacySetup.o msgonly/SerialIOName.o msgonly/SetupItem.o msgonly/SetupTags.o msgonly/StrBlank.o msgonly/StrClip.o msgonly/TA7000Init.o msgonly/UtilityBuffer.o msgonly/Trans.o msgonly/TransUtil.o msgonly/Operation.o msgonly/OperationValidation.o msgonly/OperationMessage.o msgonly/OperationHelper.o msgonly/OperationUtil.o msgonly/OperationDisplayDriver.o msgonly/AccessConfig.o msgonly/DateTimeConfig.o msgonly/UtilConfig.o msgonly/TagHandler.o msgonly/XMLProperties.o $(RES)
LINKOBJ  = msgonly/Spieler.o msgonly/common.o msgonly/FPOperation.o msgonly/UserBarcode.o msgonly/UserInput.o msgonly/TAString.o msgonly/User.o msgonly/UserAudible.o msgonly/UserVisual.o msgonly/UserDigital.o msgonly/StdInput.o msgonly/KeyboardReader.o msgonly/LCDHandler.o msgonly/LCDUtil.o msgonly/LCDWEPHandler.o msgonly/LegacyTrans.o msgonly/TransFIFO.o msgonly/UtilityBufferOutput.o msgonly/ErrorSystem.o msgonly/InterfaceAccessControlVector.o msgonly/InterfaceBellScheduleVector.o msgonly/InterfaceBufferDays.o msgonly/InterfaceBufferEnum.o msgonly/InterfaceBufferFlags.o msgonly/InterfaceBufferGeneric.o msgonly/InterfaceElement.o msgonly/InterfaceElementBase.o msgonly/InterfaceElementExtend.o msgonly/InterfaceElementSystem.o msgonly/InterfaceMessageBadgeVector.o msgonly/InterfaceProfileAssignVector.o msgonly/InterfaceValidateEntryVector.o msgonly/LegacySetup.o msgonly/SerialIOName.o msgonly/SetupItem.o msgonly/SetupTags.o msgonly/StrBlank.o msgonly/StrClip.o msgonly/TA7000Init.o msgonly/UtilityBuffer.o msgonly/Trans.o msgonly/TransUtil.o msgonly/Operation.o msgonly/OperationValidation.o msgonly/OperationMessage.o msgonly/OperationHelper.o msgonly/OperationUtil.o msgonly/OperationDisplayDriver.o msgonly/DateTimeConfig.o  msgonly/TagHandler.o msgonly/XMLProperties.o $(RES)
LIBS =  -L"/cygdrive/c/cygwin/tools/powerpc-88x-linux-gnu/lib" -L"/cygdrive/c/cygwin/usr/local/lib" -L"lib" lib/libexpat.a lib/libuf.a lib/libct.so -lpthread -g3 -lrt
#LIBS =  -L"C:/cygwin/lib" -lpthread -lstdc++ -static -g3
INCS =  -I"/cygdrive/c/cygwin/tools/powerpc-88x-linux-gnu/include" -I"/cygdrive/c/cygwin/usr/local/include"  -I"src/Include"  -I"src/logger/include" -I"src/Database/Include" -I"src/UserIO/Include" -I"src/setup/Include"  -I"src/Trans/Include"  -I"src/Operation/Include" -I"src/Util" -I"include" -I"include/ufp" -I"include/freetds" -I"src/Database/Include"
CXXINCS = -I"/cygdrive/c/cygwin/tools/powerpc-88x-linux-gnu/include" -I"/cygdrive/c/cygwin/usr/local/include"  -I"src/Include"  -I"src/logger/include" -I"src/Database/Include" -I"src/UserIO/Include" -I"src/setup/Include"  -I"src/Trans/Include"  -I"src/Operation/Include" -I"src/Util" -I"include" -I"include/ufp" -I"include/freetds" -I"src/Database/Include"

BIN  = Spieler

CXXFLAGS = $(CXXINCS) -pthread -D_REENTRANT -fomit-frame-pointer -D_GNU_SOURCE -Wall -g3
CFLAGS = $(INCS) -lcurl -pthread -D_REENTRANT -fomit-frame-pointer -D_GNU_SOURCE -Wall -g3
.PHONY: all all-before all-after clean clean-custom

all: all-before strip all-after


clean: clean-custom
	rm -f $(OBJ) $(BIN)

strip: $(BIN)
ifdef msgonly
	echo $(BIN)
else
	$(STRIP) $(BIN)
endif

$(BIN): $(OBJ)
	$(CPP) $(LINKOBJ) -o $(BIN) $(LIBS)

msgonly/Base64.o: src/Util/Base64.cpp
	$(CPP) -c src/Util/Base64.cpp -o msgonly/Base64.o $(CXXFLAGS)

msgonly/TAString.o: src/Util/TAString.cpp
	$(CPP) -c src/Util/TAString.cpp -o msgonly/TAString.o $(CXXFLAGS)

msgonly/logger.o : src/logger/logger.cpp
	$(CPP) -c src/logger/logger.cpp -o msgonly/logger.o $(CXXFLAGS)

msgonly/fileappender.o : src/logger/fileappender.cpp
	$(CPP) -c src/logger/fileappender.cpp -o msgonly/fileappender.o $(CXXFLAGS)

msgonly/UserInput.o: src/UserIO/UserInput.cpp
	$(CPP) -c src/UserIO/UserInput.cpp -o msgonly/UserInput.o $(CXXFLAGS)

msgonly/TCPIPConfig.o: src/Operation/TCPIPConfig.cpp
	$(CPP) -c src/Operation/TCPIPConfig.cpp -o msgonly/TCPIPConfig.o $(CXXFLAGS)

msgonly/User.o: src/UserIO/User.c
	$(CC) -c src/UserIO/User.c -o msgonly/User.o $(CXXFLAGS)

msgonly/UserAudible.o: src/UserIO/UserAudible.c
	$(CC) -c src/UserIO/UserAudible.c -o msgonly/UserAudible.o $(CXXFLAGS)

msgonly/UserBarcode.o: src/UserIO/UserBarcode.c
	$(CC) -c src/UserIO/UserBarcode.c -o msgonly/UserBarcode.o $(CXXFLAGS)

msgonly/UserVisual.o: src/UserIO/UserVisual.c
	$(CC) -c src/UserIO/UserVisual.c -o msgonly/UserVisual.o $(CXXFLAGS)

msgonly/UserDigital.o: src/UserIO/UserDigital.c
	$(CC) -c src/UserIO/UserDigital.c -o msgonly/UserDigital.o $(CXXFLAGS)

msgonly/StdInput.o: src/UserIO/StdInput.cpp
	$(CPP) -c src/UserIO/StdInput.cpp -o msgonly/StdInput.o $(CXXFLAGS)

msgonly/KeyboardReader.o: src/UserIO/KeyboardReader.cpp
	$(CPP) -c src/UserIO/KeyboardReader.cpp -o msgonly/KeyboardReader.o $(CXXFLAGS)

msgonly/LegacyTrans.o: src/Trans/LegacyTrans.cpp
	$(CPP) -c src/Trans/LegacyTrans.cpp -o msgonly/LegacyTrans.o $(CXXFLAGS)

msgonly/TransFIFO.o: src/Trans/TransFIFO.cpp
	$(CPP) -c src/Trans/TransFIFO.cpp -o msgonly/TransFIFO.o $(CXXFLAGS)

msgonly/UtilityBufferOutput.o: src/setup/UtilityBufferOutput.cpp
	$(CPP) -c src/setup/UtilityBufferOutput.cpp -o msgonly/UtilityBufferOutput.o $(CXXFLAGS)

msgonly/ErrorSystem.o: src/setup/ErrorSystem.cpp
	$(CPP) -c src/setup/ErrorSystem.cpp -o msgonly/ErrorSystem.o $(CXXFLAGS)

msgonly/InterfaceAccessControlVector.o: src/setup/InterfaceAccessControlVector.cpp
	$(CPP) -c src/setup/InterfaceAccessControlVector.cpp -o msgonly/InterfaceAccessControlVector.o $(CXXFLAGS)

msgonly/InterfaceBellScheduleVector.o: src/setup/InterfaceBellScheduleVector.cpp
	$(CPP) -c src/setup/InterfaceBellScheduleVector.cpp -o msgonly/InterfaceBellScheduleVector.o $(CXXFLAGS)

msgonly/InterfaceBufferDays.o: src/setup/InterfaceBufferDays.cpp
	$(CPP) -c src/setup/InterfaceBufferDays.cpp -o msgonly/InterfaceBufferDays.o $(CXXFLAGS)

msgonly/InterfaceBufferEnum.o: src/setup/InterfaceBufferEnum.cpp
	$(CPP) -c src/setup/InterfaceBufferEnum.cpp -o msgonly/InterfaceBufferEnum.o $(CXXFLAGS)

msgonly/InterfaceBufferFlags.o: src/setup/InterfaceBufferFlags.cpp
	$(CPP) -c src/setup/InterfaceBufferFlags.cpp -o msgonly/InterfaceBufferFlags.o $(CXXFLAGS)

msgonly/InterfaceBufferGeneric.o: src/setup/InterfaceBufferGeneric.cpp
	$(CPP) -c src/setup/InterfaceBufferGeneric.cpp -o msgonly/InterfaceBufferGeneric.o $(CXXFLAGS)

msgonly/InterfaceElement.o: src/setup/InterfaceElement.cpp
	$(CPP) -c src/setup/InterfaceElement.cpp -o msgonly/InterfaceElement.o $(CXXFLAGS)

msgonly/InterfaceElementBase.o: src/setup/InterfaceElementBase.cpp
	$(CPP) -c src/setup/InterfaceElementBase.cpp -o msgonly/InterfaceElementBase.o $(CXXFLAGS)

msgonly/InterfaceElementExtend.o: src/setup/InterfaceElementExtend.cpp
	$(CPP) -c src/setup/InterfaceElementExtend.cpp -o msgonly/InterfaceElementExtend.o $(CXXFLAGS)

msgonly/InterfaceElementSystem.o: src/setup/InterfaceElementSystem.cpp
	$(CPP) -c src/setup/InterfaceElementSystem.cpp -o msgonly/InterfaceElementSystem.o $(CXXFLAGS)

msgonly/InterfaceMessageBadgeVector.o: src/setup/InterfaceMessageBadgeVector.cpp
	$(CPP) -c src/setup/InterfaceMessageBadgeVector.cpp -o msgonly/InterfaceMessageBadgeVector.o $(CXXFLAGS)

msgonly/InterfaceProfileAssignVector.o: src/setup/InterfaceProfileAssignVector.cpp
	$(CPP) -c src/setup/InterfaceProfileAssignVector.cpp -o msgonly/InterfaceProfileAssignVector.o $(CXXFLAGS)

msgonly/InterfaceValidateEntryVector.o: src/setup/InterfaceValidateEntryVector.cpp
	$(CPP) -c src/setup/InterfaceValidateEntryVector.cpp -o msgonly/InterfaceValidateEntryVector.o $(CXXFLAGS)

msgonly/LegacySetup.o: src/setup/LegacySetup.cpp
	$(CPP) -c src/setup/LegacySetup.cpp -o msgonly/LegacySetup.o $(CXXFLAGS)

msgonly/SerialIOName.o: src/setup/SerialIOName.cpp
	$(CPP) -c src/setup/SerialIOName.cpp -o msgonly/SerialIOName.o $(CXXFLAGS)

msgonly/SetupItem.o: src/setup/SetupItem.cpp
	$(CPP) -c src/setup/SetupItem.cpp -o msgonly/SetupItem.o $(CXXFLAGS)

msgonly/SetupTags.o: src/setup/SetupTags.cpp
	$(CPP) -c src/setup/SetupTags.cpp -o msgonly/SetupTags.o $(CXXFLAGS)

msgonly/StrBlank.o: src/setup/StrBlank.c
	$(CPP) -c src/setup/StrBlank.c -o msgonly/StrBlank.o $(CXXFLAGS)

msgonly/StrClip.o: src/setup/StrClip.c
	$(CPP) -c src/setup/StrClip.c -o msgonly/StrClip.o $(CXXFLAGS)

msgonly/Spieler.o: Spieler.cpp
	$(CPP) -c Spieler.cpp -o msgonly/Spieler.o $(CXXFLAGS)

msgonly/UtilityBuffer.o: src/setup/UtilityBuffer.cpp
	$(CPP) -c src/setup/UtilityBuffer.cpp -o msgonly/UtilityBuffer.o $(CXXFLAGS)

msgonly/Trans.o: src/Trans/Trans.cpp
	$(CPP) -c src/Trans/Trans.cpp -o msgonly/Trans.o $(CXXFLAGS)

msgonly/TransUtil.o: src/Trans/TransUtil.cpp
	$(CPP) -c src/Trans/TransUtil.cpp -o msgonly/TransUtil.o $(CXXFLAGS)

msgonly/Operation.o: src/Operation/Operation.cpp
	$(CPP) -c src/Operation/Operation.cpp -o msgonly/Operation.o $(CXXFLAGS)

msgonly/OperationValidation.o: src/Operation/OperationValidation.cpp
	$(CPP) -c src/Operation/OperationValidation.cpp -o msgonly/OperationValidation.o $(CXXFLAGS)

msgonly/OperationMessage.o: src/Operation/OperationMessage.cpp
	$(CPP) -c src/Operation/OperationMessage.cpp -o msgonly/OperationMessage.o $(CXXFLAGS)

msgonly/OperationHelper.o: src/Operation/OperationHelper.cpp
	$(CPP) -c src/Operation/OperationHelper.cpp -o msgonly/OperationHelper.o $(CXXFLAGS)

msgonly/OperationUtil.o: src/Operation/OperationUtil.cpp
	$(CPP) -c src/Operation/OperationUtil.cpp -o msgonly/OperationUtil.o $(CXXFLAGS)

msgonly/OperationDisplayDriver.o: src/Operation/OperationDisplayDriver.cpp
	$(CPP) -c src/Operation/OperationDisplayDriver.cpp -o msgonly/OperationDisplayDriver.o $(CXXFLAGS)

msgonly/DateTimeConfig.o : src/Operation/DateTimeConfig.cpp
	$(CPP) -c src/Operation/DateTimeConfig.cpp -o msgonly/DateTimeConfig.o  $(CXXFLAGS)

msgonly/AccessConfig.o : src/Operation/AccessConfig.cpp
	$(CPP) -c src/Operation/AccessConfig.cpp -o msgonly/AccessConfig.o  $(CXXFLAGS)

msgonly/UtilConfig.o : src/Operation/UtilConfig.cpp
	$(CPP) -c src/Operation/UtilConfig.cpp -o msgonly/UtilConfig.o  $(CXXFLAGS)

msgonly/ReaderConfig.o : src/Operation/ReaderConfig.cpp
	$(CPP) -c src/Operation/ReaderConfig.cpp -o msgonly/ReaderConfig.o  $(CXXFLAGS)

msgonly/ConfigMgr.o : src/Operation/ConfigMgr.cpp
	$(CPP) -c src/Operation/ConfigMgr.cpp -o msgonly/ConfigMgr.o  $(CXXFLAGS)

msgonly/TagHandler.o : src/Operation/TagHandler.cpp
	$(CPP) -c src/Operation/TagHandler.cpp -o msgonly/TagHandler.o $(CXXFLAGS)

msgonly/TA7000Init.o : src/Operation/TA7000Init.cpp
	$(CPP) -c src/Operation/TA7000Init.cpp -o msgonly/TA7000Init.o $(CXXFLAGS)

msgonly/FPOperation.o : src/Operation/FPOperation.cpp
	$(CPP) -c src/Operation/FPOperation.cpp -o msgonly/FPOperation.o $(CXXFLAGS)

msgonly/LCDHandler.o : src/UserIO/LCDHandler.cpp
	$(CPP) -c src/UserIO/LCDHandler.cpp -o msgonly/LCDHandler.o $(CXXFLAGS)

msgonly/LCDWEPHandler.o : src/UserIO/LCDWEPHandler.cpp
	$(CPP) -c src/UserIO/LCDWEPHandler.cpp -o msgonly/LCDWEPHandler.o $(CXXFLAGS)

msgonly/LCDUtil.o : src/UserIO/LCDUtil.cpp
	$(CPP) -c src/UserIO/LCDUtil.cpp -o msgonly/LCDUtil.o $(CXXFLAGS)

msgonly/XMLProperties.o : src/Util/XMLProperties.cpp
	$(CPP) -c src/Util/XMLProperties.cpp -o msgonly/XMLProperties.o $(CXXFLAGS)

msgonly/FPRemoteManagement.o : src/Operation/FPRemoteManagement.cpp
	$(CPP) -c src/Operation/FPRemoteManagement.cpp -o msgonly/FPRemoteManagement.o $(CXXFLAGS)

msgonly/SoapMessageGen.o : src/Util/SoapMessageGen.cpp
	$(CPP) -c src/Util/SoapMessageGen.cpp -o msgonly/SoapMessageGen.o $(CXXFLAGS)

msgonly/HttpClient.o : src/Util/HttpClient.cpp
	$(CPP) -c src/Util/HttpClient.cpp -o msgonly/HttpClient.o $(CXXFLAGS)

msgonly/sqlitedb.o : src/Database/sqlitedb.cpp
	$(CPP) -c src/Database/sqlitedb.cpp -o msgonly/sqlitedb.o $(CXXFLAGS)

msgonly/ProjectTasksTable.o : src/Database/ProjectTasksTable.cpp
	$(CPP) -c src/Database/ProjectTasksTable.cpp -o msgonly/ProjectTasksTable.o $(CXXFLAGS)

msgonly/ActivitiesTable.o : src/Database/ActivitiesTable.cpp
	$(CPP) -c src/Database/ActivitiesTable.cpp -o msgonly/ActivitiesTable.o $(CXXFLAGS)

msgonly/OffLineTranTable.o : src/Database/OffLineTranTable.cpp
	$(CPP) -c src/Database/OffLineTranTable.cpp -o msgonly/OffLineTranTable.o $(CXXFLAGS)

msgonly/common.o: src/Database/common.c
	$(CC) -c src/Database/common.c -o msgonly/common.o $(CXXFLAGS)

msgonly/ServerDataBaseConnection.o: src/Database/ServerDataBaseConnection.cpp
	$(CC) -c src/Database/ServerDataBaseConnection.cpp -o msgonly/ServerDataBaseConnection.o $(CXXFLAGS)

# DO NOT DELETE
