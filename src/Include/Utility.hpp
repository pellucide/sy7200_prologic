/*
	Utility.h:

	General data definitions and function prototypes for utility
modules.

	Copyright (c) 1991,1995-2004 - Alcom Communications
	Copyright (c) 2004 - Time America, Inc.

	Written by:	  Alan J. Luse
	Written on:	  16 Dec 2002
	  Based on:	  Based on Msg_Def.h originally written on 15 Aug 1991
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   06 Jul 2004 21:21:30  $
	     $Date: 2005/09/01 22:27:58 $

*/


#if ! defined UTILITY_HPP
#define UTILITY_HPP


/*** Parameter types enumeration and definitions. ***/

/* Named parameter data types. */
typedef UBin8 Type_t;
enum Type_e {
	TYPE_NONE = 0,
	TYPE_String,
	TYPE_StringPtr,
	TYPE_Char,
	TYPE_CharNum,			// Character numeric index: 'A'=1,'B'=2,...
	TYPE_Boolean,
	TYPE_Bit8,
	TYPE_Bit16,
	TYPE_SBin7,
	TYPE_UBin8,
	TYPE_SBin15,
	TYPE_UBin16,
	TYPE_SBin31,
	TYPE_UBin32,
	TYPE_FixSBin7,
	TYPE_FixUBin8,
	TYPE_FixSBin15,
	TYPE_FixUBin16,
	TYPE_FixSBin31,
	TYPE_FixUBin32,
	TYPE_FixScale,
	TYPE_FixMeas,
	TYPE_EnumCharUBin8,
	TYPE_EnumCharUBin16,
	TYPE_EnumCharUBin32,
	TYPE_EnumLabelUBin8,
	TYPE_EnumLabelUBin16,
	TYPE_EnumLabelUBin32,
	TYPE_StrChar,			// Character to string: 'A'="All",'B'="Backup",...
	TYPE_FlagBit8,
	TYPE_FlagBit16,
	TYPE_FlagBit32,
	TYPE_Status,
	TYPE_Mode,
	TYPE_Delay,
	TYPE_ElapsedHMS,
	TYPE_ElapsedHM,
	TYPE_ElapsedH,
	TYPE_ElapsedFunctionHMS,
	TYPE_ElapsedFunctionHM,
	TYPE_ElapsedFunctionH,
	TYPE_ElapsedSec,
	TYPE_ElapsedMin,
	TYPE_ElapsedHour,
	TYPE_ElapsedFunctionSec,
	TYPE_ElapsedFunctionMin,
	TYPE_ElapsedFunctionHour,
	MAX_TYPE
};

/* Map alternate parameter type names. */
#define TYPE_Byte TYPE_Bit8
#define TYPE_EnumUBin8 TYPE_EnumCharUBin8
#define TYPE_EnumUBin16 TYPE_EnumCharUBin16
#define TYPE_EnumUBin32 TYPE_EnumCharUBin32


/* Data buffer structure. */
class Buffer {

protected:
	size_t size;
	Byte * data;

public:
				Buffer (size_t length = 32)
					: size (0), max_size (length)
					{ data = new Byte [length]; *data = '\0'; };
				~Buffer (void) { delete [] data; };
	void		Reset (void) { size = 0; *data = '\0'; };
	size_t		MaxSize (void) const { return (max_size); };
	size_t		Length (void) const { return (size); };
	size_t		AddData (const Byte * new_data, size_t length);
	size_t		AddString (const char * string, const char * separator = NULL);
	size_t		AddChar (char new_char)
					{ AddData ((Byte *)&new_char, 1); AddString (""); return (1); };
	void		OutputBytes (FILE * stream) const;
	void		OutputString (FILE * stream) const;
	void		OutputLog (FILE * stream) const;

private:
	size_t max_size;
};


/*** Mode types and definitions. ***/

/* Three way option enumeration and override value structure. */
typedef enum {
	Default = 0,
	ForceOff = -1,
	ForceOn = 1
	}
Mode3Way_e;

/* Three way option and override integer value structure. */
typedef struct {
	Mode3Way_e s;
	int v;
	}
Mode3WayValue_t;

/* Three way option and override integer value structure. */
typedef struct {
	Mode3Way_e s;
	char * v;
	}
Mode3WayString_t;

/* Three way option and override delay value structure. */
#if defined _TIME_DEF || defined _TIMER_HPP
typedef struct {
	Mode3Way_e s;
	TimerInterval_t v;
	}
Mode3WayTime_t;
#endif

/* Macro functions to process 3 way mode override options. */
#define _do_3way(m,b) if(m!=Default){if(m==ForceOn)b=ON;if(m==ForceOff)b=OFF;}
#define _do_3wayvalue(m,val) if(m.s!=Default){if(m.s==ForceOn)val=m.v;if(m.s==ForceOff)val=0;}

#endif

