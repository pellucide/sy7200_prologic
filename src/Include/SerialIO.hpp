/*
	SerialIO.hpp:

	Data definitions and function prototypes for serial I/O module.

	Copyright (c) 1991-1992,1995-2004 - Alcom Communications
	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   19 Aug 2004
	  Based on:   Sio_Def.H originally written on 30 Oct 1989
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   23 May 2004 16:56:34  $
		 $Date: 2005/09/01 22:27:58 $

*/


#if ! defined SERIALIO_HPP

#include<stdio.h>

#define SERIALIO_HPP

/* Serial I/O baud rates. */
#if ! defined _BAUD_RATES
#define _BAUD_RATES
typedef enum {
	BAUD_75,
	BAUD_150,
	BAUD_300,
	BAUD_600,
	BAUD_1200,
	BAUD_2400,
	BAUD_4800,
	BAUD_9600,
	BAUD_19200,
	BAUD_38400,
	BAUD_76800,

	BAUD_50,
	BAUD_110,
	BAUD_134,
	BAUD_200,
	BAUD_1050,
	BAUD_2000,

	BAUD_900,
	BAUD_1800,
	BAUD_3600,
	BAUD_7200,
	BAUD_14400,
	BAUD_28800,
	BAUD_57600,
	BAUD_115200,
	MAX_BAUD,
	BAUD_NONE
	}
BaudRate_e;
#endif


/* Serial I/O parity and data length settings. */
#define Parity8BitMask 0x80
typedef enum {
	PARITY_NONE,
	PARITY_ODD,
	PARITY_EVEN,
	PARITY_ONE,
	PARITY_ZERO,

	PARITY_NONE_8 = Parity8BitMask,
	PARITY_ODD_8,
	PARITY_EVEN_8,
	PARITY_ONE_8,
	PARITY_ZERO_8,

	PARITY_NO_CHANGE = 0xFF,

	MAX_PARITY
	}
Parity_e;

/* Alternate names for force parity bit settings. */
#define PARITY_MARK		PARITY_ONE
#define PARITY_SPACE	PARITY_ZERO


/* Hardware interface type definitions. */
typedef UBin8 SIOInterface_t;
typedef enum {
	SIO_RS232,
	SIO_RS422,
	SIO_Modem,
	MAX_SIO}
SIOInterface_e;


/* Global baud rate table. */
extern const UBin32 SioBaudTable [MAX_BAUD+2];

/* Global baud rate names. */
extern const char * const SioNameBaud [];
extern const char * const SioNameBaudShort [];


/* Serial I/O checksum & CRC class. */
class SerialIOCheck {

public:
				SerialIOCheck (UBin16 seed = 0) : check (seed) {}
	UBin16		CalculateSum (const Byte * data_p, size_t length);
	UBin16		CalculateCRC16 (const Byte * data_p, size_t length);
	UBin16		Value16 (void) { return (check); }
	UBin8		Value8 (void) { return (check & 0xFF); }

private:
	UBin16 check;
};

#endif

