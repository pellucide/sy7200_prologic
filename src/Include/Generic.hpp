/*
	Generic.hpp:

	Generally useful, system independent declarations, definitions and
types for C++.

	Copyright (c) 1991-1993,1995-2001,2004 - Alcom Communications
	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   27 Dec 1999
	  Based on:   Gen_Def.h originally written on 04 Sep 1989
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   30 May 2001 17:33:32  $
	     $Date: 2005/09/01 22:27:58 $

*/


#if ! defined GENERIC_HPP
#define GENERIC_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

/* Boolean type and values. */
typedef bool Boolean;
enum Boolean_e {
#if !defined TRUE
	TRUE = 1,
	FALSE = 0,
#endif
	YES = TRUE,
	NO = FALSE,
	ON = TRUE,
	OFF = FALSE,
};

/* General type definitions. */
typedef unsigned int Bit;
typedef signed char SChar;
typedef unsigned char UChar;
typedef unsigned char Byte;
typedef unsigned char Bit8;
typedef unsigned short Bit16;
typedef unsigned long Bit32;
typedef unsigned short Count;

/* Numeric type definitions. */
typedef signed char SBin7;
typedef unsigned char UBin8;
typedef signed short SBin15;
typedef unsigned short UBin16;
typedef signed long SBin31;
typedef unsigned long UBin32;

/* Named logical operators. */
#define NOT	!
#define AND	&&
#define OR	||

/* Array size (# elements) macro. */
#define array_size(array) (sizeof array / sizeof array[0])

/* Define ANSI standard exit function values if not previously defined. */
#if ! defined (EXIT_SUCCESS)
#define EXIT_SUCCESS 0
#endif
#if ! defined (EXIT_FAILURE)
#define EXIT_FAILURE 1
#endif

/* Define additional exit values. */
#define EXIT_ABORT	2			/* Program abort. */			
#define EXIT_INTR	3			/* User interrupt (^C). */			

/* Anonymous handle type and various Null definitions. */
#define NULL_PTR ((void *) NULL)
typedef void * Handle;			/* Abstract data type handle. */
#define NULL_HANDLE ((Handle) NULL)
typedef void (* Entry) (void);	/* General pointer to function. */
#define NULL_ENTRY ((Entry) NULL)

/* Null reference macro to satisfy different compilers. */
#define NULL_REF(parm) (parm = parm)

/* Cross-Sectional Array referencing macros. */
#define csa_step(array, span) (void *)((char *)array + span)
#define csa_index(array, index, span) (void *)((char *)array + (index)*span)
#define csa_bound(array, index, lbound, span) (void *)((char *)array + ((index)-(lbound))*span)

/* Class or structure offset class. */
typedef unsigned short Offset_t;
class Offset {

public:
	Offset_t offset;
	static const Offset_t OffsetInvalid = ~0;

public:
				Offset (Offset_t offset = OffsetInvalid) : offset (offset) {};
				Offset (const void * member, const void * base)
					: offset ((Byte *)member - (Byte *)base) {};
	void *		Get (void * base) const
					{ return ((Byte *)base + offset); };
	const void *
				Get (const void * base) const
					{ return ((const Byte *)base + offset); };
	Offset_t	GetOffset (void) const
					{ return (offset); };
	Boolean		Valid (void) const
					{ return ((offset == OffsetInvalid) ? NO : YES); };
};

/* System status code type definition. */
typedef SBin15 Code;
enum StatusCode {
	SC_FAILURE		=      -1,
	SC_OKAY			=       0,
	MAX_SC
};

/* Program and data signature values. */
const UBin16 SignProg = 0xAA55;
const UBin16 SignData = 0x55AA;

#endif


