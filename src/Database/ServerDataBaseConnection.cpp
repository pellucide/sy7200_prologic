#include "ServerDataBaseConnection.h"


ServerDataBaseConnection::ServerDataBaseConnection(char* configFile)
{
    int verbose = 0;
    connectionResult = try_ctlogin(configFile,&ctx, &conn, &cmd, verbose);
    if (connectionResult != CS_SUCCEED) 
    {
      fprintf(stderr, "Data Base Server Login failed\n");
    }  
}

ServerDataBaseConnection::~ServerDataBaseConnection()
{
   int verbose = 0;
   CS_RETCODE ret = try_ctlogout(ctx, conn, cmd, verbose);
   if (ret != CS_SUCCEED)
	 fprintf(stderr, "DB Server Logout failed\n");
}

CS_RETCODE ServerDataBaseConnection::RunQueryWithoutResult(char* query)
{
  
  return run_command(cmd,query);
}

CS_RETCODE ServerDataBaseConnection::RunQueryWithResult(char* query)
{ 
	CS_RETCODE ret;
  	ret = ct_command(cmd, CS_LANG_CMD,query, CS_NULLTERM, CS_UNUSED);
	if (ret != CS_SUCCEED) 
	{
		fprintf(stderr, "ct_command() failed %s %d: %s\n" , __FILE__ , __LINE__ , __FUNCTION__ );
		return ret;
	}

    ret = ct_send(cmd);
	if (ret != CS_SUCCEED) 
	{
		fprintf(stderr, "ct_send(): failed %s %d: %s\n" , __FILE__ , __LINE__ , __FUNCTION__ );
		return ret;
	}
	return ret;
}

CS_RETCODE ServerDataBaseConnection::GetRowResult(int& row_count , int& num_cols)
{

	 CS_INT count;
	 CS_RETCODE ret;
	 CS_RETCODE results_ret;
	 int is_return_status=0;
	 CS_INT result_type;
	 int i;
     int is_status_result=0;
	if ((results_ret = ct_results(cmd, &result_type)) == CS_SUCCEED) 
	{
		is_status_result = 0;
		switch ((int) result_type) 
		{
			case CS_CMD_SUCCEED:
				break;
			case CS_CMD_DONE:
				return result_type;
				break;
			case CS_CMD_FAIL:
				fprintf(stderr, "ct_results() result_type CS_CMD_FAIL. %s %d: %s\n" , 
				__FILE__ , __LINE__ , __FUNCTION__);
				return results_ret;
			case CS_STATUS_RESULT:
				fprintf(stdout, "ct_results: CS_STATUS_RESULT detected for sp_who %s %d: %s\n" ,
				__FILE__ , __LINE__ , __FUNCTION__);
				is_status_result = 1;
				/* fall through */
			case CS_ROW_RESULT:
				ret = ct_res_info(cmd, CS_NUMDATA, &num_cols, CS_UNUSED, NULL);
				if (ret != CS_SUCCEED || num_cols > maxcol) 
				{
					fprintf(stderr, "ct_res_info() failed %s %d: %s\n" , 
					__FILE__ , __LINE__ , __FUNCTION__);
					return ret;
				}
				if (num_cols <= 0) 
				{
					fprintf(stderr, "ct_res_info() return strange values %s %d: %s\n" , 
					__FILE__ , __LINE__ , __FUNCTION__);
					return ret;
				}
				if (is_status_result && num_cols != 1) 
				{
					fprintf(stderr, "CS_STATUS_RESULT return more than 1 column %s %d: %s\n" ,
					__FILE__ , __LINE__ , __FUNCTION__);
					return ret;
				}
				for (i=0; i < num_cols; i++) 
				{
					 /* here we can finally test for the return status column */
					ret = ct_describe(cmd, i+1, &col[i].datafmt);
					if (ret != CS_SUCCEED) 
					{
						fprintf(stderr, "ct_describe() failed for column %d %s %d: %s\n", i,
						__FILE__ , __LINE__ , __FUNCTION__);
						return -1;
					}

					if (col[i].datafmt.status & CS_RETURN) 
					{
						fprintf(stdout, "ct_describe() indicates a return code in column %d for sp_who %s %d: %s\n",
						i,__FILE__ , __LINE__ , __FUNCTION__ );
						is_return_status = i+1;
						/*
						* other possible values:
						 * CS_CANBENULL
						 * CS_HIDDEN
						* CS_IDENTITY
						* CS_KEY
						* CS_VERSION_KEY
						* CS_TIMESTAMP
						* CS_UPDATABLE
						* CS_UPDATECOL
						 */
					}
					col[i].datafmt.datatype = CS_CHAR_TYPE;
					col[i].datafmt.format = CS_FMT_NULLTERM;
					col[i].datafmt.maxlength = colsize;
					col[i].datafmt.count = 1;
					col[i].datafmt.locale = NULL;

					ret = ct_bind(cmd, i+1, &col[i].datafmt, &col[i].data, &col[i].datalength, &col[i].ind);
					if (ret != CS_SUCCEED) 
					{
						fprintf(stderr, "ct_bind() failed %s %d: %s \n",__FILE__ , __LINE__ , __FUNCTION__ );
						return ret;
					}
				}	
		}
	}
    
	 if ((ret = ct_fetch(cmd, CS_UNUSED, CS_UNUSED, CS_UNUSED, &count)) == CS_SUCCEED) 
	 {
		for (i=0; i < num_cols; i++) 
		{ /* data */
			if ((result_type == CS_STATUS_RESULT) && strcmp(col[i].data,"0")) 
			{
				fprintf(stderr, "CS_STATUS_RESULT should return 0 as result %s %d: %s\n",
					__FILE__ , __LINE__ , __FUNCTION__);
				return ret;
			}
		}	
		row_count = row_count + count;
	 }
	 else
	 {
		if ((result_type == CS_STATUS_RESULT) && row_count != 1) 
		{
			fprintf(stderr, "CS_STATUS_RESULT should return a row %s %d: %s\n",__FILE__ , __LINE__ , __FUNCTION__);
			return ret;
		}
            
		switch ((int) ret) 
		{
			case CS_END_DATA:
				fprintf(stdout, "ct_results fetched %d rows.\n", row_count);
				return ret;
			 case CS_ROW_FAIL:
				fprintf(stderr, "ct_fetch() CS_ROW_FAIL on row %d.\n", row_count);
				return ret;
			 case CS_FAIL:
				fprintf(stderr, "ct_fetch() returned CS_FAIL.\n");
				return ret;
			 default:
				fprintf(stderr, "ct_fetch() unexpected return.\n");
				return ret;
		}
	 }

}
