#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "OffLineTranTable.h"
#include "sqlitedb.h"


extern sqlite3 *db;

OffLineTranRecord::OffLineTranRecord(const char* Tran, const char* T, const char* EmpId, const char * Gro, const char * Pro, const char * Ta, const char * Act,const char * PSwitch)
{
    int len;
	if(Tran){len = strlen ( Tran );Transaction =(char*) malloc(len+1);strcpy(Transaction,Tran);Transaction[len]= '\0';}
	else Transaction = NULL;
	if(T){len = strlen ( T );Time =(char*) malloc(len+1);strcpy(Time,T);Time[len]= '\0';}
	else Time = NULL;
	if(EmpId){len = strlen ( EmpId);EmployeeId = (char*) malloc(len+1);strcpy(EmployeeId,EmpId);EmployeeId[len]= '\0';}
	else EmployeeId = NULL;
	if(Gro){len = strlen ( Gro );Group = (char*) malloc(len+1);strcpy(Group,Gro);Group[len]= '\0';}
    else Group = NULL;
	if(Pro){len = strlen ( Pro );Project = (char*) malloc(len+1);strcpy(Project,Pro);Project[len]= '\0';}
    else Project = NULL;
	if(Ta){len = strlen ( Ta );Task = (char*) malloc(len+1);strcpy(Task,Ta);Task[len]= '\0';}
    else Task = NULL;
	if(Act){len = strlen ( Act );Activity = (char*) malloc(len+1);strcpy(Activity,Act);Activity[len]= '\0';}
    else Activity = NULL;
    if(PSwitch)
	{
	  len = strlen ( PSwitch );ProjectSwitch = (char*) malloc(len+1);strcpy(ProjectSwitch,PSwitch);
	  ProjectSwitch[len]= '\0';
	}
    else ProjectSwitch = NULL;
}

OffLineTranRecord::~OffLineTranRecord()
{
   if(Transaction) free(Transaction);
   if(Time) free(Time);
   if(EmployeeId) free(EmployeeId);
   if(Group) free(Group);
   if(Project) free(Project);
   if(Task) free(Task);
   if(Activity) free(Activity);
   if(ProjectSwitch) free(ProjectSwitch);

}
void OffLineTranRecord::SetTransaction(const char* Tran)
{
    int len;
	if(Tran){len = strlen ( Tran );Transaction =(char*) realloc(Transaction,len+1);strcpy(Transaction,Tran);Transaction[len]= '\0';}
	else Transaction = NULL;
}
void OffLineTranRecord::SetTime(const char* T)
{
    int len;
	if(T){len = strlen ( T );Time =(char*) realloc(Time,len+1);strcpy(Time,T);Time[len]= '\0';}
	else Time = NULL;
}
void OffLineTranRecord::SetEmployeeId(const char* EmpId)
{
    int len;
	if(EmpId){len = strlen ( EmpId);EmployeeId = (char*) realloc(EmployeeId,len+1);strcpy(EmployeeId,EmpId);EmployeeId[len]= '\0';}
	else EmployeeId = NULL;
}

void OffLineTranRecord::SetGroup(const char * Gro)
{
    int len;
	if(Gro){len = strlen ( Gro );Group = (char*) realloc(Group,len+1);strcpy(Group,Gro);Group[len]= '\0';}
    else Group = NULL;
}

void OffLineTranRecord::SetProject(const char * Pro)
{
    int len;
	if(Pro){len = strlen ( Pro );Project = (char*) realloc(Project,len+1);strcpy(Project,Pro);Project[len]= '\0';}
    else Project = NULL;
}

void OffLineTranRecord::SetTask(const char * Ta)
{
    int len;
	if(Ta){len = strlen ( Ta );Task = (char*) realloc(Task,len+1);strcpy(Task,Ta);Task[len]= '\0';}
    else Task = NULL;
}

void OffLineTranRecord::SetActivity(const char * Act)
{
    int len;
	if(Act){len = strlen ( Act );Activity = (char*) realloc(Activity,len+1);strcpy(Activity,Act);Activity[len]= '\0';}
    else Activity = NULL;
}

void OffLineTranRecord::SetProjectSwitch(const char * PSwitch)
{
    int len;
    if(PSwitch)
	{
	  len = strlen ( PSwitch );ProjectSwitch = (char*) realloc(ProjectSwitch,len+1);strcpy(ProjectSwitch,PSwitch);
	  ProjectSwitch[len]= '\0';
	}
    else ProjectSwitch = NULL;
}
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int OffLineTranTable::CreateTable()
{
  char* sql_stmt = NULL;
  rc = SQLITE_OK;

  //sprintf(sql_stmt,"CREATE TABLE IF NOT EXISTS FINGERDATA(EXTERNALID CHAR(10), FINGERINDEX INTEGER, PRIVILEGELEVEL INTEGER,  SECURITYLEVEL INTEGER, TEMPLATE CHAR(512),  PRIMARY KEY(EXTERNALID, FINGERINDEX))");
  sql_stmt = (char*) realloc(sql_stmt,strlen("CREATE TABLE IF NOT EXISTS OFFLINETRANTABLE (TRANSACTIONS TEXT, TIME TEXT, EMPLOYEEID TEXT, GROUPS TEXT, PROJECTS TEXT, TASKS TEXT,  ACTIVITIES TEXT , PROJECTSWITCH TEXT )")+1);
  sprintf(sql_stmt,"CREATE TABLE IF NOT EXISTS OFFLINETRANTABLE (TRANSACTIONS TEXT, TIME TEXT, EMPLOYEEID TEXT, GROUPS TEXT, PROJECTS TEXT, TASKS TEXT,  ACTIVITIES TEXT , PROJECTSWITCH TEXT )");

  char* zErrMsg ;
  rc = sqlite_prot_exec(db, sql_stmt, NULL, NULL, &zErrMsg );
  if( rc != SQLITE_OK ){
    fprintf(stderr, "Can't create table OFFLINETRANTABLE : %s\n", sqlite3_errmsg(db));
    sqlite3_free(zErrMsg);
    if(sql_stmt)free(sql_stmt);
	return rc;
  }
  if(sql_stmt)free(sql_stmt);
  return rc;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int OffLineTranTable::DeleteTable()
{
   //Drop the table
  char* sql_stmt = NULL;
  sql_stmt =(char*) realloc(sql_stmt,strlen("DROP TABLE IF EXISTS OFFLINETRANTABLE")+1);
  sprintf(sql_stmt,"DROP TABLE IF EXISTS OFFLINETRANTABLE");
  rc = sqlite_prot_exec(db, sql_stmt, NULL, NULL, &zErrMsg );
  if( rc != SQLITE_OK ){
    fprintf(stderr, "Can't drop table OFFLINETRANTABLE: %s\n", sqlite3_errmsg(db));
    sqlite3_free(zErrMsg);
    if(sql_stmt)free(sql_stmt);
	return rc;
  }

  if(sql_stmt)free(sql_stmt);
  return rc;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int OffLineTranTable::FindRowCount ( const char* where_clause ,int* frc ){
  int num_rows ;
  int num_cols ;
  char** result ;
  char sql [ 1000 ] ;
  if ( 0 == where_clause ){
    strcpy ( sql , const_sql_cnt_offlinetran );
  } else {
    sprintf ( sql , "%s WHERE %s" , const_sql_cnt_offlinetran , where_clause );
  }
  *frc = sqlite_prot_get_table( db , sql , & result , & num_rows , & num_cols , 0 );
  if ( SQLITE_OK != *frc){
    return 0 ;
  }

  if ( num_rows < 1 ){
    return 0 ;
  }

  if ( 0 == result ){
    return 0 ;
  }

  int ix = num_rows * num_cols ;
  int ret = atoi ( result [ ix ] ) ;
  sqlite3_free_table( result );

  return ret ;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int OffLineTranTable::InsertRow(const OffLineTranRecord* p_row)
{
  const char * Tran;
  const char * T;
  const char * EmpId;
  const char * Gro;
  const char * Pro;
  const char * Ta;
  const char * Act;
  const char * PSwitch;
  char sql_stmt_d[1000];
  char sql_stmt_I[1000];
  Tran = p_row->GetTransaction();
  T = p_row->GetTime();
  EmpId = p_row->GetEmployeeId();
  Gro = p_row->GetGroup();
  Pro = p_row->GetProject();
  Ta = p_row->GetTask();
  Act = p_row->GetActivity();
  PSwitch = p_row->GetProjectSwitch();
  rc = SQLITE_ERROR;
  //int perform = 0;

  debugFX(100,"Tran = %s T= %s EmpId = %s Gro = %s Pro = %s Ta = %s Act = %s\n",Tran,T,EmpId,Gro,Pro,Ta,Act);
  if(strcmp("ClockIn",Tran) == 0)
  {
    //sprintf ( sql_stmt_d , "DELETE FROM OFFLINETRANTABLE WHERE TRANSACTIONS = '%s' AND TIME = '%s'AND EMPLOYEEID = '%s' AND GROUPS = '%s' AND PROJECTS = '%s' AND TASKS = '%s' AND ACTIVITIES = '%s'" ,Tran ,T,EmpId,Gro,Pro,Ta,Act);
    sprintf ( sql_stmt_d ,"TRANSACTIONS = '%s' AND TIME = '%s'AND EMPLOYEEID = '%s' AND GROUPS = '%s' AND PROJECTS = '%s' AND TASKS = '%s' AND ACTIVITIES = '%s' AND PROJECTSWITCH = '%s'" ,Tran ,T,EmpId,Gro,Pro,Ta,Act,PSwitch);
	sprintf(sql_stmt_I, "INSERT INTO OFFLINETRANTABLE (TRANSACTIONS,TIME,EMPLOYEEID,GROUPS,PROJECTS,TASKS,ACTIVITIES,PROJECTSWITCH) VALUES ('%s', '%s', '%s', '%s','%s', '%s','%s','%s')",Tran ,T,EmpId,Gro,Pro,Ta,Act,PSwitch);
  }
  if((strcmp("GetActivities",Tran) == 0) || (strcmp("GetGroups",Tran) == 0) || (strcmp("GetProjectTasks",Tran) == 0))
  {
    //sprintf ( sql_stmt_d , "DELETE FROM OFFLINETRANTABLE WHERE TRANSACTIONS = '%s'" ,Tran);
    sprintf ( sql_stmt_d , "TRANSACTIONS = '%s'" ,Tran);
    sprintf(sql_stmt_I, "INSERT INTO OFFLINETRANTABLE (TRANSACTIONS) VALUES ('%s')",Tran);
  }
  if(strcmp("InPunch",Tran) == 0)
  {
    //sprintf ( sql_stmt_d , "DELETE FROM OFFLINETRANTABLE WHERE TRANSACTIONS = '%s' AND TIME = '%s'AND EMPLOYEEID = '%s' AND GROUPS = '%s'" ,Tran ,T,EmpId,Gro);
    sprintf ( sql_stmt_d , "TRANSACTIONS = '%s' AND TIME = '%s'AND EMPLOYEEID = '%s' AND GROUPS = '%s'" ,Tran ,T,EmpId,Gro);
	sprintf(sql_stmt_I, "INSERT INTO OFFLINETRANTABLE (TRANSACTIONS,TIME,EMPLOYEEID,GROUPS) VALUES ('%s', '%s', '%s', '%s')",Tran ,T,EmpId,Gro);
  }
  if(strcmp("OutPunch",Tran) == 0)
  {
    //sprintf ( sql_stmt_d , "DELETE FROM OFFLINETRANTABLE WHERE TRANSACTIONS = '%s' AND TIME = '%s'AND EMPLOYEEID = '%s'" ,Tran ,T,EmpId);
    sprintf ( sql_stmt_d , "TRANSACTIONS = '%s' AND TIME = '%s'AND EMPLOYEEID = '%s'" ,Tran ,T,EmpId);
	sprintf(sql_stmt_I, "INSERT INTO OFFLINETRANTABLE (TRANSACTIONS,TIME,EMPLOYEEID) VALUES ('%s', '%s', '%s')",Tran ,T,EmpId);
  }

  if(strcmp("SwipePunch",Tran) == 0)
  {
    //sprintf ( sql_stmt_d , "DELETE FROM OFFLINETRANTABLE WHERE TRANSACTIONS = '%s' AND TIME = '%s'AND EMPLOYEEID = '%s'" ,Tran ,T,EmpId);
	sprintf ( sql_stmt_d , "TRANSACTIONS = '%s' AND TIME = '%s'AND EMPLOYEEID = '%s'" ,Tran ,T,EmpId);
	sprintf(sql_stmt_I, "INSERT INTO OFFLINETRANTABLE (TRANSACTIONS,TIME,EMPLOYEEID) VALUES ('%s', '%s', '%s')",Tran ,T,EmpId);
  }

  if(strcmp("Validate",Tran) == 0)
  {
    //sprintf ( sql_stmt_d , "DELETE FROM OFFLINETRANTABLE WHERE TRANSACTIONS = '%s' AND EMPLOYEEID = '%s'" ,Tran ,EmpId);
    sprintf ( sql_stmt_d , "TRANSACTIONS = '%s' AND EMPLOYEEID = '%s'" ,Tran ,EmpId);
    sprintf(sql_stmt_I, "INSERT INTO OFFLINETRANTABLE (TRANSACTIONS,EMPLOYEEID) VALUES ('%s', '%s')",Tran ,EmpId);
  }

  debugFX(100,"sql_stmt_d = %s\n",sql_stmt_d);
  debugFX(100,"sql_stmt_I = %s\n",sql_stmt_I);

  /*rc = sqlite_prot_exec ( db , sql_stmt_d , NULL , NULL , & zErrMsg );
  if ( rc != SQLITE_OK ){
     fprintf(stderr, "Can't delete record in OFFLINETRANTABLE: %s\n", sqlite3_errmsg(db));
	 sqlite3_free ( zErrMsg );
	 return rc;
  }*/
  int frc;
  if((!FindRowCount(sql_stmt_d,&frc)) && frc == SQLITE_OK)
  {
	debugFX(100,"Add Row \n");
	//printf("Add Row frc = %d \n" ,frc);
	rc = sqlite_prot_exec(db, sql_stmt_I, NULL, NULL, &zErrMsg );
	if( rc != SQLITE_OK ){
		fprintf(stderr, "Can't insert record in OFFLINETRANTABLE: %s\n", sqlite3_errmsg(db));
		sqlite3_free(zErrMsg);
		return rc ;
	}
	return rc;
  }
  else{
   //printf("row exist frc = %d \n" ,frc);
   return frc;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

char** OffLineTranTable::SelectTable(const char* where_clause , unsigned int limit ,int* num_rows , int* num_cols)
{
  char sql [ 1000 ] ;
  if(tableValidation)
  {
   sqlite3_free_table(resultTable);
   tableValidation = 0;
  }

  if ( NULL == where_clause ){
    sprintf ( sql ,"%s LIMIT %d" ,const_sql_all_offlinetran,limit);
  } else {
    sprintf ( sql , "%s WHERE %s LIMIT %d" , const_sql_all_offlinetran , where_clause,limit );
  }
  if ( SQLITE_OK != sqlite_prot_get_table( db , sql , & resultTable , num_rows , num_cols , & zErrMsg )){
     fprintf(stderr, "Can't get table in OFFLINETRANTABLE: %s\n", sqlite3_errmsg(db));
	 sqlite3_free ( zErrMsg );
	 return NULL ;
  }
  tableValidation = 1;
  return resultTable;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int OffLineTranTable::DeletRows(const OffLineTranRecord* p_row)
{
  const char * Tran;
  const char * T;
  const char * EmpId;
  const char * Gro;
  const char * Pro;
  const char * Ta;
  const char * Act;
  const char * PSwitch;
  char sql_stmt_d[1000];
  Tran = p_row->GetTransaction();
  T = p_row->GetTime();
  EmpId = p_row->GetEmployeeId();
  Gro = p_row->GetGroup();
  Pro = p_row->GetProject();
  Ta = p_row->GetTask();
  Act = p_row->GetActivity();
  PSwitch = p_row->GetProjectSwitch();
  rc = SQLITE_OK;

  debugFX(100,"Tran = %s T= %s EmpId = %s Gro = %s Pro = %s Ta = %s Act = %s\n",Tran,T,EmpId,Gro,Pro,Ta,Act);
  if(strcmp("ClockIn",Tran) == 0)
	sprintf ( sql_stmt_d , "DELETE FROM OFFLINETRANTABLE WHERE TRANSACTIONS = '%s' AND TIME = '%s'AND EMPLOYEEID = '%s' AND GROUPS = '%s' AND PROJECTS = '%s' AND TASKS = '%s'AND ACTIVITIES = '%s' AND PROJECTSWITCH = '%s' " ,Tran ,T,EmpId,Gro,Pro,Ta,Act,PSwitch);
  if((strcmp("GetActivities",Tran) == 0) || (strcmp("GetGroups",Tran) == 0) || (strcmp("GetProjectTasks",Tran) == 0))
    sprintf ( sql_stmt_d , "DELETE FROM OFFLINETRANTABLE WHERE TRANSACTIONS = '%s'" ,Tran);
  if(strcmp("InPunch",Tran) == 0)
   sprintf ( sql_stmt_d , "DELETE FROM OFFLINETRANTABLE WHERE TRANSACTIONS = '%s' AND TIME = '%s'AND EMPLOYEEID = '%s' AND GROUPS = '%s'" ,Tran ,T,EmpId,Gro);
  if(strcmp("OutPunch",Tran) == 0)
   sprintf ( sql_stmt_d , "DELETE FROM OFFLINETRANTABLE WHERE TRANSACTIONS = '%s' AND TIME = '%s'AND EMPLOYEEID = '%s'" ,Tran ,T,EmpId);
  if(strcmp("SwipePunch",Tran) == 0)
   sprintf ( sql_stmt_d , "DELETE FROM OFFLINETRANTABLE WHERE TRANSACTIONS = '%s' AND TIME = '%s'AND EMPLOYEEID = '%s'" ,Tran ,T,EmpId);
  if(strcmp("Validate",Tran) == 0)
   sprintf ( sql_stmt_d , "DELETE FROM OFFLINETRANTABLE WHERE TRANSACTIONS = '%s' AND EMPLOYEEID = '%s'" ,Tran ,EmpId);

  debugFX(100,"sql_stmt_d = %s\n",sql_stmt_d);
  rc = sqlite_prot_exec ( db , sql_stmt_d , NULL , NULL , & zErrMsg );
  if ( rc != SQLITE_OK ){
     fprintf(stderr, "Can't delete record in OFFLINETRANTABLE: %s\n", sqlite3_errmsg(db));
	 sqlite3_free ( zErrMsg );
	 return rc;
  }
  return rc;
}
//
