#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "ActivitiesTable.h"
//#include "fpmodule.h"
#include "sqlitedb.h"
//#include "TAString.h"
//#include "Str_Def.h"

extern sqlite3 *db;

ActivityRecord::ActivityRecord(const char* AName, const char* ACode)
{
    int len = strlen ( AName );
	ActivityName =(char*) malloc(len+1);
	strcpy(ActivityName,AName);
	ActivityName[len]= '\0';
	len = strlen ( ACode );
	ActivityCode =(char*) malloc(len+1);
	strcpy(ActivityCode,ACode);
	ActivityCode[len]= '\0';
}
ActivityRecord::~ActivityRecord()
{
   if(ActivityName) free(ActivityName);
   if(ActivityCode) free(ActivityCode);
}

void ActivityRecord::SetActivityName(const char* AName)
{
  ActivityName = (char*) realloc(ActivityName,strlen(AName)+1);
  strcpy(ActivityName,AName);
}
void ActivityRecord::SetActivityCode(const char* ACode)
{
  ActivityCode = (char*) realloc(ActivityCode,strlen(ACode)+1);
  strcpy(ActivityCode,ACode);
}

 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int ActivitiesTable::CreateTable()
{
  char* sql_stmt = NULL;
  rc = SQLITE_OK;

  //sprintf(sql_stmt,"CREATE TABLE IF NOT EXISTS FINGERDATA(EXTERNALID CHAR(10), FINGERINDEX INTEGER, PRIVILEGELEVEL INTEGER,  SECURITYLEVEL INTEGER, TEMPLATE CHAR(512),  PRIMARY KEY(EXTERNALID, FINGERINDEX))");
  sql_stmt = (char*) realloc(sql_stmt,strlen("CREATE TABLE IF NOT EXISTS ACTIVITIESTABLE(ID INT,ACTIVITYNAME TEXT, ACTIVITYCODE TEXT )")+1);
  sprintf(sql_stmt,"CREATE TABLE IF NOT EXISTS ACTIVITIESTABLE(ID INT,ACTIVITYNAME TEXT, ACTIVITYCODE TEXT )");

  char* zErrMsg ;
  rc = sqlite_prot_exec(db, sql_stmt, NULL, NULL, &zErrMsg );
  if( rc != SQLITE_OK ){
    fprintf(stderr, "Can't create table ACTIVITIESTABLE : %s\n", sqlite3_errmsg(db));
    sqlite3_free(zErrMsg);
    if(sql_stmt)free(sql_stmt);
	return rc;
  }
  if(sql_stmt)free(sql_stmt);
  return rc;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int ActivitiesTable::DeleteTable()
{
   //Drop the table
  char* sql_stmt = NULL;
  sql_stmt =(char*) realloc(sql_stmt,strlen("DROP TABLE IF EXISTS ACTIVITIESTABLE")+1);
  sprintf(sql_stmt,"DROP TABLE IF EXISTS ACTIVITIESTABLE");
  rc = sqlite_prot_exec(db, sql_stmt, NULL, NULL, &zErrMsg );
  if( rc != SQLITE_OK ){
    fprintf(stderr, "Can't drop table ACTIVITIESTABLE: %s\n", sqlite3_errmsg(db));
    sqlite3_free(zErrMsg);
    if(sql_stmt)free(sql_stmt);
	return rc;
  }

  if(sql_stmt)free(sql_stmt);
  return rc;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int ActivitiesTable::InsertRow(const ActivityRecord* a_row,int * row_id)
{

  int num_rows = 0 ;
  int num_cols = 0 ;
  char** result = NULL ;

  const char * aName;
  const char * aCode;
  char sql_stmt[512];
  aName = a_row->GetActivityName();
  aCode = a_row->GetActivityCode();

  rc = SQLITE_OK;

  *row_id = getID(a_row);
  if(*row_id >= 0)
  {
    //debugFX(100,"the row exist\n");
	return SQLITE_OK;
  }
  else
  {

	sprintf(sql_stmt,"select MAX(ID) from ACTIVITIESTABLE");
	//debugFX(100,"sql_stmt = %s\n",sql_stmt);
	rc = sqlite_prot_get_table( db , sql_stmt , & result , & num_rows , & num_cols , 0 );
	if ( SQLITE_OK != rc )
	{
		fprintf(stderr, "sqlite_prot_get_table FAILED In InsertRow Function \n");
		//debugFX(100,"sqlite_prot_get_table FAILED In InsertRow Function \n");
		sqlite3_free_table( result );
		return rc ;
	}
	if ( num_rows < 1 || num_cols < 1 || result == NULL)
	{
        fprintf(stderr, "sqlite_prot_get_table FAILED In num_rows < 1 || num_cols < 1 ||  0 == result \n");
		//debugFX(100,"sqlite_prot_get_table FAILED num_rows < 1 || num_cols < 1 ||  0 == result \n");
		sqlite3_free_table( result );
		return rc ;
	}

	int ix = num_rows * num_cols ;
	int id;
	if(result [ ix ] == NULL)
		id = 0;
	else
	    id = atoi ( result [ ix ] );
	id++;

    sprintf(sql_stmt, "INSERT INTO ACTIVITIESTABLE(ID,ACTIVITYNAME,ACTIVITYCODE) VALUES (%d,'%s','%s')",id,aName, aCode);
	//debugFX(100,"sql_stmt = %s\n",sql_stmt);
	rc = sqlite_prot_exec(db, sql_stmt, NULL, NULL, &zErrMsg );
    if( rc != SQLITE_OK ){
	    fprintf(stderr, "Can't insert record in ACTIVITIESTABLE: %s\n", sqlite3_errmsg(db));
       sqlite3_free(zErrMsg);
       sqlite3_free_table( result );
       *row_id = -1;
	   return rc ;
	}
	sqlite3_free_table( result );
	*row_id = id;
	return rc;

  }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int ActivitiesTable::DeleteRow(int ID)
{
    char sql_stmt[1000];

	rc = SQLITE_OK;
	sprintf ( sql_stmt , "ID = %d" ,ID);
	//debugFX(100,"sql_stmt = %s\n",sql_stmt);
	if(FindRowCount (sql_stmt))
	{

		sprintf ( sql_stmt ,"delete from ACTIVITIESTABLE where ID = %d" ,ID);
        rc = sqlite_prot_exec(db, sql_stmt, NULL, NULL, &zErrMsg );
		if( rc != SQLITE_OK )
		{
			fprintf(stderr, "delete from ACTIVITIESTABLE : Can't delete row in ACTIVITIESTABLE: %s\n", sqlite3_errmsg(db));
			sqlite3_free(zErrMsg);
			return rc ;
		}
		sprintf ( sql_stmt ,"update ACTIVITIESTABLE SET ID = %d where ID = (select MAX(ID) from ACTIVITIESTABLE) AND ID > %d",ID,ID);
		rc = sqlite_prot_exec(db, sql_stmt, NULL, NULL, &zErrMsg );
		if( rc != SQLITE_OK )
		{
			fprintf(stderr, "update ACTIVITIESTABLE : Can't delete row in ACTIVITIESTABLE: %s\n", sqlite3_errmsg(db));
			sqlite3_free(zErrMsg);
			return rc ;
		}
        return rc;
	}
	else
     fprintf(stderr, "ID row in ACTIVITIESTABLE Out Off Range\n");
	return rc;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int ActivitiesTable::getMaxID()
{
  int num_rows = 0 ;
  int num_cols = 0 ;
  char** result = NULL ;
  char sql_stmt[100];

  sprintf(sql_stmt,"select MAX(ID) from ACTIVITIESTABLE");
  //debugFX(100,"sql_stmt = %s\n",sql_stmt);
  rc = sqlite_prot_get_table( db , sql_stmt , & result , & num_rows , & num_cols , 0 );
  if ( SQLITE_OK != rc )
  {
	fprintf(stderr, "sqlite_prot_get_table FAILED In getMaxID Function \n");
	//debugFX(100,"sqlite_prot_get_table FAILED\n");
	sqlite3_free_table( result );
	return -1 ;
  }
  if ( num_rows < 1 || num_cols < 1 || result == NULL)
  {
    fprintf(stderr, "sqlite_prot_get_table FAILED In num_rows < 1 || num_cols < 1 ||  0 == result \n");
	//debugFX(100,"sqlite_prot_get_table FAILED num_rows < 1 || num_cols < 1 ||  0 == result \n");
	sqlite3_free_table( result );
	return -1 ;
  }
	
	int ix = num_rows * num_cols ;
	int id;
	if(result [ ix ] == NULL)
		id = 0;
	else
	    id = atoi ( result [ ix ] );
	sqlite3_free_table( result );
	return id;
  
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int ActivitiesTable::getID(const ActivityRecord* a_row)
{
  int num_rows = 0 ;
  int num_cols = 0 ;
  char** result = NULL ;

  const char * aCode;
  char sql_stmt[512];
  aCode = a_row->GetActivityCode();
  rc = SQLITE_OK;
  
  sprintf(sql_stmt,"select ID from ACTIVITIESTABLE where ACTIVITYCODE = '%s'" ,aCode);
  //debugFX(100,"sql_stmt = %s\n",sql_stmt);
  rc = sqlite_prot_get_table( db , sql_stmt , & result , & num_rows , & num_cols , 0 );
  if ( SQLITE_OK != rc )
  {
		fprintf(stderr, "sqlite_prot_get_table FAILED In getID Function \n");
		//debugFX(100,"sqlite_prot_get_table FAILED\n");
		sqlite3_free_table( result );
		return -1 ;
  }
  if ( num_rows < 1 || num_cols < 1 || result == NULL)
  {
		//fprintf(stderr, "sqlite_prot_get_table FAILED In num_rows < 1 || num_cols < 1 ||  0 == result \n");
		//debugFX(100,"sqlite_prot_get_table FAILED num_rows < 1 || num_cols < 1 ||  0 == result \n");
		sqlite3_free_table( result );
		return -1 ;
  }
	
  int ix = num_rows * num_cols ;
  int id;
  if(result [ ix ] == NULL)
		id = -1;
  else
	    id = atoi ( result [ ix ] );
  sqlite3_free_table( result );
  return id;
}
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int ActivitiesTable::FindRowCount ( const char* where_clause )
{
  int num_rows ;
  int num_cols ;
  char** result ;
  char sql [ 1000 ] ;
  if ( 0 == where_clause ){
    strcpy ( sql , const_sql_cnt_activities );
  } else {
    sprintf ( sql , "%s WHERE %s" , const_sql_cnt_activities , where_clause );
	//debugFX(100,"sql = %s\n",sql);
  }
  if ( SQLITE_OK != sqlite_prot_get_table( db , sql , & result , & num_rows , & num_cols , 0 )){
     //debugFX(100,"sqlite_prot_get_table FAILED \n");
	 return 0 ;
  }
  debugX(100,"num_rows = %d num_cols = %d \n ",num_rows,num_cols);
  
  if ( num_rows < 1 ){
	  return 0 ;
  }

  if ( 0 == result ){
	  return 0 ;
  }

  int ix = num_rows * num_cols ;
  int ret = atoi ( result [ ix ] ) ;
  sqlite3_free_table( result );

  return ret ;
}


