/**
 * Simple XML Properties object.
 * It can read properties from xml and and write properties to an xml file.
 * This supports only one level of xml, meaning, the root's
 * children are all leaves. It cannot handle multiple levels
 */


// all values read in from GetProperty are stored into 'static value[64]', unless keyvalue is passed in
// characterdatahandler also uses a 'static value[64]' --- 64 characters is not enough ???!!!???


#include "XMLProperties.h"

#include <mcheck.h>

// define the maximum size of a property string value and/or xml element character data (<stag>data<etag>)
#define ELEMENT_DATA_SIZE	640



XMLProperties::XMLProperties(const char * filename)
{
	// Registry File
	this->filename = filename;
	// Create the parser
	parser = XML_ParserCreate(NULL);
	if (! parser)
	{
		#ifdef DEBUG
		printf("Couldn't allocate memory for parser\n");
		#endif
	}
	buffern = NULL;
}

XMLProperties::XMLProperties()
{
	parser = XML_ParserCreate(NULL);
		if (! parser)
		{
			#ifdef DEBUG
			printf("Couldn't allocate memory for parser\n");
			#endif
		}
		buffern = NULL;
}
XMLProperties::~XMLProperties()
{
	if(parser) XML_ParserFree(parser);
	if(buffern) {free(buffern);buffern=NULL;}
}

char * XMLProperties::GetProperty(const char * key, char* keyValue)
{
	FILE * fp ;
	int hd ;
	struct stat file_info;
	//static char value[64];
	static char value[ELEMENT_DATA_SIZE];

	hd = open(filename, O_RDONLY) ;
    fstat(hd, &file_info);
    close(hd);

	fp = fopen(filename, "r");
	if( !fp )
	{
		#ifdef DEBUG
		printf("Couldn't open the Properties file\n");
		#endif
		return NULL;;
	}

	// Reset and reuse the parser
	XML_ParserReset(parser, NULL);
	XML_SetElementHandler(parser, startElementHandler, endElementHandler);
	XML_SetCharacterDataHandler(parser, characterDataHandler);


	value[0] = '\0';

	PROP_P prop = (PROP_P) malloc( sizeof(PROP) );
	prop->key = const_cast<char *>(key);
//printf("getpropKV: key is %s\n", key);
	if(keyValue)
	{
//printf("getpropKV: varg len is %d\n", strlen(keyValue));
	   prop->value = keyValue;
	}
	else
	{
//printf("getpropKV: static value len is %d\n", strlen(value));
       prop->value = value;
	}
	prop->listKey = NULL;
	prop -> foundList = NO;
	prop->command = FINDKEY;
	prop->found = NO;

	XML_SetUserData(parser, prop);


	buffern =(char*)malloc(file_info.st_size + 1);
	bzero(buffern, file_info.st_size+1);
	for (;;)
	{
		int done;
		int len;

		fread(buffern, 1, file_info.st_size+1, fp);

		if (ferror(fp))
		{
			#ifdef DEBUG
			printf("File Read error\n");
			#endif
            if(prop)free( prop );
			if(buffern){free(buffern);buffern=NULL;}
			return NULL;
    	}
    	len = strlen(buffern);
    	done = feof(fp);
    	if (XML_Parse(parser, buffern, len, done) == XML_STATUS_ERROR)
		{
			#ifdef DEBUG
			printf("GetProperty(k,kv) Parse error at line %d:\n%s\n",
					XML_GetCurrentLineNumber(parser), XML_ErrorString(XML_GetErrorCode(parser)));
			printf("GetProperty args are %s and %s\n", key, prop->value);
			#endif
			if(buffern){free(buffern);buffern=NULL;}
			if(prop)free( prop );
			return NULL;
		}
    	if(done) break;
	}

	// Cleanup
	if(fp)
	{
		fclose(fp);
	}

	if(prop)free( prop );

	if( strlen(value) != 0 ) {
		if(buffern)
		{
			free(buffern);
			buffern=NULL;
		}
		return value;
	}

	if(buffern){free(buffern);buffern=NULL;}
	return NULL;
}

char * XMLProperties::GetProperty(const char *xmlBuffer , const char * key , char* keyValue)
{
    // Reset and reuse the parser
	XML_ParserReset(parser, NULL);
	XML_SetElementHandler(parser, startElementHandler, endElementHandler);
	XML_SetCharacterDataHandler(parser, characterDataHandler);
	//static char value[64];
	static char value[ELEMENT_DATA_SIZE];
	value[0] = '\0';

	PROP_P prop = (PROP_P) malloc( sizeof(PROP) );
	prop->key = const_cast<char *>(key);
	if(keyValue)
		prop->value = keyValue;
	else
		prop->value = value;
	prop->command = FINDKEY;
	prop->listKey = NULL;
	prop -> foundList = NO;
	prop->found = NO;
	XML_SetUserData(parser, prop);

	int done = 0;
	int len = 0;
	len = strlen(xmlBuffer);

    if (XML_Parse(parser,xmlBuffer, len, done) == XML_STATUS_ERROR)
	{
		#ifdef DEBUG
		//printf("GetProperty(buf,key,kv): Parse error at line %d:\n%s\n",
		//XML_GetCurrentLineNumber(parser), XML_ErrorString(XML_GetErrorCode(parser)));
		//printf("GetProperty args are %s and %s\n", key, prop->value);
		//printf("GetProperty buffer is \n%s\n", xmlBuffer);

		#endif
        if(prop)free( prop );
		return NULL;
	}

	if(prop)free( prop );

	if( strlen(value) != 0 ){return value;}

	return NULL;

}

list<ElmData>& XMLProperties::GetListElements(const char *xmlBuffer,const char * listKey,const char * elementKey,int from,int to)
{
    // Reset and reuse the parser
	XML_ParserReset(parser, NULL);
	XML_SetElementHandler(parser, startElementHandler, endElementHandler);
	XML_SetCharacterDataHandler(parser, characterDataHandler);

    PROP_P prop = (PROP_P) malloc( sizeof(PROP) );
	prop->key = const_cast<char *>(elementKey);
	prop->listKey = const_cast<char *>(listKey);
	prop->command = FINDLISTKEY;
	prop->found = NO;
    prop->foundList = NO;
	prop->foundElm = NO;
	prop->dataElmFound = NO;
	prop->listElmNum = 0 ;
	prop->elmFrom = from ;
	prop->elmTo = to;
	prop->dataElmNum = 0;
	prop->elmNumInRange = 0;

    //static char value[64];
	static char value[ELEMENT_DATA_SIZE];
	value[0] = '\0';
    prop->value = value;

	//erase all the old elements from the list
	list<ElmData>::iterator it1,it2;
	//it1 = listElmData.begin();
	//it2 =  listElmData.end();
    //listElmData.erase(it1,it2);
	listElmData.clear();

	prop->listElmData = &listElmData;
	prop->level = 0;
	prop->listLevel = 0;

	XML_SetUserData(parser, prop);

	int done = 0;
	int len = 0;
	len = strlen(xmlBuffer);
    if (XML_Parse(parser,xmlBuffer, len, done) == XML_STATUS_ERROR)
	{
		#ifdef DEBUG
		printf("GetListElements: Parse error at line %d:\n%s\n",
		XML_GetCurrentLineNumber(parser), XML_ErrorString(XML_GetErrorCode(parser)));
		printf("GetListElements args: %s and %s\n", listKey, elementKey);
		printf("GetListElements bufr: %s\n", xmlBuffer);
//GetListElements(const char *xmlBuffer,const char * listKey,const char * elementKey,int from,int to)
		#endif
        if(prop)free( prop );
		it1 = listElmData.begin();
		it2 =  listElmData.end();
		listElmData.erase(it1,it2);
		return listElmData;
	}
	if(prop)free( prop );
    return listElmData;
}

bool XMLProperties::SetProperty(const char * key, const char * value)
{
	FILE * fp ;
	int hd ;
	struct stat file_info;

	hd = open(filename, O_RDONLY) ;
    fstat(hd, &file_info);
    close(hd);

    fp = fopen(filename, "r");
	if( !fp )
	{
		#ifdef DEBUG
		printf("Couldn't open the Properties file\n");
		#endif
		return false;
	}

	const char * tmpfile = "propertiestmp.xml";

	FILE * fw = fopen(tmpfile, "w");
	if( !fw )
	{
		#ifdef DEBUG
		printf("Couldn't open the file for write\n");
		#endif
		if( fp ) fclose( fp );
		return false;
	}

	if( fputs(XML_HEADER, fw) == EOF )
	{
		if( fp ) fclose( fp );
		if( fw ) fclose( fw );
		return false;
	}

	if( fputs("\n", fw) == EOF )
	{
		if( fp ) fclose( fp );
		if( fw ) fclose( fw );
		return false;
	}

	// Reset and reuse the parser
	XML_ParserReset(parser, NULL);
	XML_SetElementHandler(parser, startElementHandler, endElementHandler);
	XML_SetCharacterDataHandler(parser, characterDataHandler);

	PROP_P prop = (PROP_P) malloc( sizeof(PROP) );
	prop->key = const_cast<char *>(key);
	prop->value = const_cast<char *>(value);
	prop->command = SETKEY;
	prop->listKey = NULL;
	prop -> foundList = NO;
	prop->found = NO;
	prop->fw = fw;
	prop->status = CMD_STATUS_DEFAULT;

	XML_SetUserData(parser, prop);

	//bzero(buffer, BUFFERSIZE);
	buffern =(char*)malloc(file_info.st_size + 1);
	bzero(buffern, file_info.st_size+1);

	for (;;)
	{
		int done=0;
		int len;

		fread(buffern, 1, file_info.st_size+1, fp);

		if (ferror(fp))
		{
			#ifdef DEBUG
			printf("File Read error\n");
			#endif
			prop->status = CMD_STATUS_DEFAULT;
			break;
    	}
    	len = strlen(buffern);
    	done = feof(fp);

    	if (XML_Parse(parser, buffern, len, done) == XML_STATUS_ERROR)
		{
			#ifdef DEBUG
			printf("Parse error at line %d:\n%s\n",
					XML_GetCurrentLineNumber(parser), XML_ErrorString(XML_GetErrorCode(parser)));
			#endif
			prop->status = CMD_STATUS_DEFAULT;
			break;
		}
    	if(done) break;
	}

	// Cleanup
	if(fp)
	{
		fclose(fp);
	}
	if(fw)
	{
		fclose(fw);
	}

	// Rename the File
	if( prop->status == CMD_SETKEY_SUCCESS )
	{
		rename(tmpfile, filename);
		if(prop)free( prop );
		if(buffern){free(buffern);buffern=NULL;}
		return true;
	}

	if(prop)free( prop );
	if(buffern){free(buffern);buffern=NULL;}
	return false;
}

/**
 * Start Element Handler
 */
void XMLCALL startElementHandler(void * userData, const char * name, const char ** atts)
{
	if( userData != NULL )
	{
		PROP_P prop = (PROP_P)userData;
		(prop->level)++; // Increase The Level
		if( (prop->key) && strcmp( name, prop->key ) == 0 )
			prop->found = YES;
		if(prop->foundElm)
		  {prop->dataElmFound = YES; strcpy(prop->dataElmKey,name);}
		if(prop->foundList && prop->command == FINDLISTKEY && strcmp( name, prop->key ) == 0 && (prop->listLevel+1) == (int)prop->level )
		{
		  if((prop->elmFrom <= prop->listElmNum+1 &&  prop->listElmNum+1 <= prop->elmTo))
		  {prop->foundElm = YES; prop->elmNumInRange++;}
		  else
		    prop->foundElm = NO;
		  prop->listElmNum++;
		}
		if((prop->listKey) && prop->command == FINDLISTKEY && strcmp( name, prop->listKey ) == 0 )
		  { prop->foundList = YES; prop -> listLevel = prop->level; }
		//printf("key = %s foundList = %d level = %d foundElm = %d dataelm = %d \n",name,prop->foundList,prop->level,prop->foundElm,prop->dataElmNum);
		if( prop->command == SETKEY )
		{
			if( prop->status == CMD_FILE_WRITE_FAIL ) return;
			/*for(int i = 1; i < prop->level; i++)
			{
				if( fputs("Y", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
			}*/
			if( fputs("<", prop->fw) == EOF )
			{
				prop->status = CMD_FILE_WRITE_FAIL;
				return;
			}
			if( fputs(name, prop->fw) == EOF )
			{
				prop->status = CMD_FILE_WRITE_FAIL;
				return;
			}
			if( prop->level == 1 )
			{
				if( fputs(">\n", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
			}
			else
			{
				if( fputs(">", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
			}
		}
	}
}

/**
 * End Element Handler
 */
void XMLCALL endElementHandler(void * userData, const char * name)
{
	if( userData != NULL )
	{
		PROP_P prop = (PROP_P)userData;
		(prop->level)--;
		if( prop->command == FINDKEY )
		{
			if( strcmp( name, prop->key ) == 0 )
			{
				prop->found = NO;
			}
		}

		if(prop->command == FINDLISTKEY)
		{
		   if(strcmp(name,prop->dataElmKey) == 0)
			   prop->dataElmFound = NO;
		   if((prop->listKey) && strcmp( name, prop->listKey ) == 0 )
			{ prop->foundList = NO;}
		   if(prop->foundList && strcmp( name, prop->key ) == 0 && (prop->listLevel) == (int)prop->level )
		   { prop->foundElm = NO; prop->dataElmNum = 0;}
		}

		if( prop->command == SETKEY )
		{
			if( prop->status == CMD_FILE_WRITE_FAIL ) return;

			// Handle the case where in the key is not present in the xml file
			if( prop->level == 0 && prop->status != CMD_SETKEY_SUCCESS )
			{
				if( fputs(" <", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs(prop->key, prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs(">", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs(prop->value, prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs("</", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs(prop->key, prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs(">\n", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				prop->status = CMD_SETKEY_SUCCESS;
			}

			// Handle the case where in the element is empty
			if( prop->found == YES )
			{
				if( prop->status != CMD_SETKEY_SUCCESS )
				{
					if( fputs(prop->value, prop->fw) == EOF )
					{
						prop->status = CMD_FILE_WRITE_FAIL;
						prop->found = NO;
						return;
					}
					prop->status = CMD_SETKEY_SUCCESS;
				}
				prop->found = NO;
			}

			if( fputs("</", prop->fw) == EOF )
			{
				prop->status = CMD_FILE_WRITE_FAIL;
				return;
			}
			if( fputs(name, prop->fw) == EOF )
			{
				prop->status = CMD_FILE_WRITE_FAIL;
				return;
			}
			if( fputs(">\n", prop->fw) == EOF )
			{
				prop->status = CMD_FILE_WRITE_FAIL;
				return;
			}
		}
	}
}

/**
 * Character Data Handler
 */
void XMLCALL characterDataHandler(void * userData, const XML_Char * s, int len)
{
	if( userData != NULL )
	{
		PROP_P prop = (PROP_P)userData;
		if( prop->command == FINDKEY )
		{
			if( prop->found == YES )
			{
//TODO: review: if prop->value is passed in, what if it's not large enough         // <-------------
				// len(prop->value) < len ??????
				// prop->value is internal value (length == 64)
				// prop->value is key_value (length == ??)
				// what if len >= 64 ???
				strncpy(prop->value, s, len); 	// <--- unsafe ????!!!!  need to know length of prop->value
				prop->value[len] = '\0';		// assumes prop->value points to buffer longer than len
				//printf("%s\n",prop->value);
			}
		}
		else if( prop->command == SETKEY )
		{
			if( prop->status == CMD_FILE_WRITE_FAIL ) return;
			// ignore tabs and newlines
			if(*s != '\n' && *s != '\t')
			{
				if( prop->found == YES )
				{
					if( fputs(prop->value, prop->fw) == EOF )
					{
						prop->status = CMD_FILE_WRITE_FAIL;
						return;
					}
					prop->status = CMD_SETKEY_SUCCESS;
				}
				else
				{
					char data[len + 1];
					strncpy(data, s, len);
					data[len] = '\0';						// overflow? no. alloc(len+1), len is last char
					if( fputs(data, prop->fw) == EOF )
					{
						prop->status = CMD_FILE_WRITE_FAIL;
					}
				}
			}
		}

		else if(prop->command == FINDLISTKEY)
		{
		    if(prop->foundList && prop->foundElm && prop->dataElmFound)
			{
		      prop->dataElmNum++;
			  ElmData elmData;

              strcpy(elmData.key,prop->dataElmKey);
			  if(len > 599) len = 599; // limit the len data we can write to the data buffer 
			  strncpy(elmData.data,s,len);						// <--- unsafe?  note that if len > length of .data, !boom!
			  elmData.data[len] = '\0';                 // overflow? assumes prop->value points to buffer longer than len

			  elmData.elmIndexInList = prop->listElmNum;
			  elmData.dataIndexInElm = prop->dataElmNum;

              prop->listElmData->push_back(elmData);

			   /*if(prop->dataElmFound == YES)
			   {
				strncpy(prop->value, s, len);
				prop->value[len] = '\0';
				printf("%s -> %s listElmNum = %d dataElmNum = %d \n",prop->dataElmKey,prop->value,prop->listElmNum,prop->dataElmNum);
			   }*/
			   //printf("%s -> ",prop->elmTable[prop->elmNumInRange-1].key);
			   //printf("%s ",prop->elmTable[prop->elmNumInRange-1].key);
               //printf("elmIndexInList = %s",prop->elmTable[prop->elmNumInRange-1].elmIndexInList);
               //printf("dataIndexInElm = %s\n\n",prop->elmTable[prop->elmNumInRange-1].dataIndexInElm);
			}
		}
	}
}

