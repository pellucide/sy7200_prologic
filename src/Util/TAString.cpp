/**
 * A Simple String class
 */

#include "TAString.h"

TAString::TAString(const char * src)
{
	value = new char[strlen(src) + 1];
	strcpy(value, src);
}

/**
 * Copy Constructor
 */
TAString::TAString(const TAString& str)
{
	value = new char[strlen(str.value)+1];
	strcpy(value, str.value);
}

/**
 * Overload Assignment operator
 */
void TAString::operator=(const char * src)
{
	delete[] value;
	value = new char[strlen(src) + 1];
	strcpy(value, src);
}

/**
 * Overload Assignment operator
 */
void TAString::operator=(const TAString & src)
{
	delete[] value;
	value = new char[strlen(src.value) + 1];
	strcpy(value, src.value);
}

TAString::~TAString()
{
	delete[] value;
}

/**
 * return Substring
 */
TAString TAString::substr(size_t index, size_t len)
{
	if( strlen(value) < (index + len) )
	{
		return NULL;
	}
	int num = 0;
	if(len == 0) 
	{
		num = strlen(value) - index;
	}
	else
	{
		num = len;
	}
	char str[num + 1];
	memcpy(str, value+index, num);
	str[num] = '\0';
	return TAString(str);
}

const char * TAString::c_str()
{
	return value;	
}
