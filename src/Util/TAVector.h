#ifndef _TAVector_H_
#define _TAVector_H_

#include <stddef.h>

template <typename Val>
class TAVector
{
public:
  TAVector() : sizeofvect ( 0 ) , maxsize ( 0 ) , valPtr ( 0 ){}
  TAVector(int);
  virtual ~TAVector() { retirePtr ();}

  void pop_back() { resize ( size () - 1 );}
  Val * back();
  Val * front();
  void push_back(const Val& v ) { resize ( size () + 1 ); valPtr [ size () - 1 ] = v ;}
  void clear (){ retirePtr ();}
  bool erase(int index);
  int size() const ;
  Val* operator[](int index);

private:
  //do not allow copy or assign!
  TAVector ( const TAVector& );
  TAVector& operator = ( const TAVector& );

  void resize ( int s );
  void retirePtr () { if ( 0 != valPtr ){ delete [] valPtr ;} valPtr = 0 ; maxsize = 0 ; sizeofvect = 0 ;}

  int sizeofvect;
  int maxsize;
  Val* valPtr;
};

template <typename Val>
TAVector<Val>::TAVector(int s)
{
	if(s > 0)
	{
		valPtr = new Val[s];
		maxsize = s;
		sizeofvect = 0;	
	}
	else
	{
		this();
	}
}

template <typename Val>
int TAVector<Val>::size() const 
{
	return sizeofvect;
}

template <typename Val>
Val* TAVector<Val>::back()
{
	if(sizeofvect == 0) return NULL;
	return &valPtr[sizeofvect-1];
}

template <typename Val>
Val* TAVector<Val>::front()
{
	if(sizeofvect == 0) return NULL;
	return &valPtr[0];
}

template <typename Val>
bool TAVector<Val>::erase(int index)
{
	if(index < 0 || index >= sizeofvect) return false;
	for(int i = index; i<sizeofvect-1; i++)
	{
		valPtr[i] = valPtr[i + 1];
	}
	resize ( size () - 1 );
	return true;
}

template <typename Val>
Val* TAVector<Val>::operator[](int index)
{
	if(index < 0 || index >= sizeofvect) return NULL;
	return &valPtr[index];
}

template <typename Val>
void TAVector<Val> ::resize ( int v ){
  if ( v < 0 ){
    return ;
  }

  if ( v > maxsize ){
    //new one and copy content
    Val* tmp = new Val [ v ];
    for ( int i = 0 ; i != sizeofvect ; i ++ ){
      tmp [ i ] = valPtr [ i ];
    }
    retirePtr ();
    valPtr = tmp ;
    maxsize = v ;
  }
  sizeofvect = v ;
}

#endif //_TAVector_H_
