#ifndef _HTTPCLIENT_H_
#define _HTTPCLIENT_H_

#include <stdio.h>
#include <string>
#include <curl/curl.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include "SoapMessageGen.h"
#include "XMLProperties.h"
#include "SystemGlobals.h"
#include "debugprint.h"

#define URL_MAX_LEN 100

struct MemoryStruct {
  char *memory;
  size_t size;
};




class HttpClient
{
 public:
	 HttpClient();
	 HttpClient(char* U , long timeOut,const bool useProxy = false,const char* proxy=NULL,const char* proxyPort=NULL,        const bool useProxyAuth=false,const char* username=NULL,const char* password=NULL,const bool useSAH = false,const char* SAH= NULL );
     ~HttpClient();
	 char* ClockIn(char* ClockId ,char* Account,char* empId,char* utcTime,char* projectSwitch,char* group,char* project,				   char* task,char* activity,char* soapVer , CURLcode* res);
	 //char* InPunch(char* ClockId ,char* Account,char* empId,char* utcTime,char* group,char* soapVer,CURLcode* res);
	 //char* OutPunch(char* ClockId ,char* Account,char* empId,char* utcTime,char* soapVer,CURLcode* res);
	 char* HeartBeat(char* ClockId ,char* Account ,char* utcTime , char* soapVer,CURLcode* res);
	 char* HeartBeatFile(char* ClockId ,char* Account ,char* utcTime,char* soapVer , CURLcode* res,FILE *fOut);
     //char* GetProjectTasks(char* ClockId ,char* Account , char* soapVer , CURLcode* res);
     char* GetActivities(char* ClockId ,char* Account , char* soapVer , CURLcode* res);
     char* SwipePunch(char* ClockId ,char* Account,char* empId,char* utcTime,char* soapVer , CURLcode* res);
	 char* LocalTime(char* ClockId ,char* Account,char* soapVer,CURLcode* res);

     char* SwipePunchWithActivityCode(char* ClockId ,char* Account,char* empId, char * activityCode, char* utcTime, char* soapVer , CURLcode* res);
	 char* IsValidEmployee(char* ClockId ,char* Account,char* empId, char * utcTime, char* soapVer,CURLcode* res);
	 //
	 char* EmployeeUsesPin(char* ClockId ,char* Account,char* empId, char * utcTime, char* soapVer,CURLcode* res);
	 char* ValidEmployeePin(char* ClockId ,char* Account,char* empId, char* empPin, char * utcTime, char* soapVer,CURLcode* res);
	 char* GetPinTableFromFile(char* ClockId ,char* Account , char* soapVer , CURLcode* res, FILE * fout);
	 //
	 // fpm
	 //
	 char* SetFPTemplate(char* ClockId ,char* Account,char* empId, char* utcTime, char* templateId, char* fptemplate, char* soapVer,CURLcode* res);

	 // memory processing
	 char* PopulateFPTemplates(char* ClockId ,char* Account, char* soapVer,CURLcode* res);

	 char* GetActivitiesFile(char* ClockId ,char* Account , char* soapVer , CURLcode* res,FILE *fOut,int tout);

	 // file processing
	 char* PopulateFPTemplatesFile(char* ClockId ,char* Account, char* soapVer,CURLcode* res, FILE *fOut,int tout);

	 char* TemplatesAvailable(char* ClockId ,char* Account, char* utcTime,char* soapVer,CURLcode* res);
	 //
	 char* GetFPTemplate(char* ClockId ,char* Account,char* empId, char* utcTime, char* soapVer,CURLcode* res);



	 void setURL(char* U);
	 char* getURL(){return URL;};
	 long getTimeOut(){return this->timeOut;};
	 void setTimeOut(long newTimeOut){curl_easy_setopt(curl,CURLOPT_TIMEOUT,newTimeOut);};
	 void resetHttp();
     struct MemoryStruct chunk;
     SoapMessageGen* smg;
	 int count ;
private:

	char URL[URL_MAX_LEN];
    CURL *curl;
	long timeOut;
    bool useProxy;
	char* proxy;
	char* proxyPort;
	bool useProxyAuthentication;
	char* proxyUserName;
	char* proxyPassword;
	bool useSoapActionHeader;
	char* soapActionHeader;
    struct curl_slist *headers;

	void ResetMemory();
    void Request(char* soapVer,struct curl_slist *headers,CURLcode* res);


	bool TraceHeartbeat;
	bool TraceSwipePunch;
	bool TraceGetActivities;
	bool TraceLocalTime;
	bool TraceIsValidEmployee;
	bool TraceSetFPTemplate;
	bool TraceGetFPTemplate;
	bool TracePopulateFPTemplates;
	bool TraceTemplatesAvailable;
	bool TraceEmployeeUsesPin;
	bool TraceValidEmployeePin;
	bool TraceGetPinTable;


	bool VerifySSL;

};

#endif //_HTTPCLIENT_H_
