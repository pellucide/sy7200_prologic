#ifdef DEBUGPRINT
/* just a helper for code location */
#define LOC std::cout << "debug:" << __FILE__ << ":" << __LINE__ << " ";

/* macro using var args */
//#define DP(fmt,...) LOC printf(fmt,__VA_ARGS__);
//#define DP(fmt,...) printf(fmt , ##__VA_ARGS__);

/* macro for general debug print statements. */
#define DVAL(text) LOC std::cout << text << std::endl;

/* macro that dumps a variable name and its actual value */
#define DVAR(text) LOC std::cout << (#text) << "=" << text << std::endl;


// definitions to display messages to the default log file
//
// note: not printf style. do an sprintf beforehand, pass that string
//

#ifdef _LOGGER_H_


//#define DLOGD(msg) {logger->ldebug(msg);printf(msg);printf("\n");}
//#define DLOGI(msg) {logger->linfo(msg);printf(msg);printf("\n");}
//#define DLOGW(msg) {logger->lwarn(msg);printf(msg);printf("\n");}
//#define DLOGE(msg) {logger->lerror(msg);printf(msg);printf("\n");}
//#define DLOGF(msg) {logger->lfatal(msg);printf(msg);printf("\n");}

// compiles ok /execute not liking these:

#define DP(fmt,...)    {static char msg[128]; snprintf(msg, sizeof(msg), fmt , ##__VA_ARGS__); logger->linfo(msg);/*printf("%s\n",msg);*/}
#define DLOGE(fmt,...) {static char msg[128]; snprintf(msg, sizeof(msg), fmt , ##__VA_ARGS__); logger->lerror(msg);/*printf("%s\n",msg);*/}
#define DLOGD(fmt,...) {static char msg[128]; snprintf(msg, sizeof(msg), fmt , ##__VA_ARGS__); logger->ldebug(msg);/*printf("%s\n",msg);*/}
#define DLOGI(fmt,...) {static char msg[128]; snprintf(msg, sizeof(msg), fmt , ##__VA_ARGS__); logger->linfo(msg);/*printf("%s\n",msg);*/}
#define DLOGW(fmt,...) {static char msg[128]; snprintf(msg, sizeof(msg), fmt , ##__VA_ARGS__); logger->lwarn(msg);/*printf("%s\n",msg);*/}
#define DLOGF(fmt,...) {static char msg[128]; snprintf(msg, sizeof(msg), fmt , ##__VA_ARGS__); logger->lfatal(msg);/*printf("%s\n",msg);*/}




#endif


#else

/* when debug isn't defined all the macro calls do absolutely nothing */
#define DP(fmt,...)
#define DVAL(text)
#define DVAR(text)


// definitions to display messages to the default log file
//
// note: not printf style. do an sprintf beforehand, pass that string
//

#ifdef _LOGGER_H_

//#define DLOGE(fmt,...) {static char msg[128]; snprintf(msg, sizeof(msg), fmt , ##__VA_ARGS__); logger->lerror(msg);/*printf("%s\n",msg);*/}
//#define DLOGD(fmt,...) {static char msg[128]; snprintf(msg, sizeof(msg), fmt , ##__VA_ARGS__); logger->ldebug(msg);/*printf("%s\n",msg);*/}
//#define DLOGI(fmt,...) {static char msg[512]; snprintf(msg, sizeof(msg), fmt , ##__VA_ARGS__); logger->linfo(msg);/*printf("%s\n",msg);*/}
//#define DLOGW(fmt,...) {static char msg[128]; snprintf(msg, sizeof(msg), fmt , ##__VA_ARGS__); logger->lwarn(msg);/*printf("%s\n",msg);*/}
//#define DLOGF(fmt,...) {static char msg[128]; snprintf(msg, sizeof(msg), fmt , ##__VA_ARGS__); logger->lfatal(msg);/*printf("%s\n",msg);*/}
//#define DLOGN(fmt,...){static char msg[128]; snprintf(msg, sizeof(msg), fmt , ##__VA_ARGS__); logger->linfo(msg);/*printf("%s\n",msg);*/}

#define DLOGD(fmt,...)
#define DLOGI(fmt,...)
#define DLOGW(fmt,...)
#define DLOGE(fmt,...)
#define DLOGF(fmt,...)
#define DLOGN(fmt,...){static char msg[128]; snprintf(msg, sizeof(msg), fmt , ##__VA_ARGS__); logger->linfo(msg);/*printf("%s\n",msg);*/}


#else

#define DLOGD(msg)
#define DLOGI(msg)
#define DLOGW(msg)
#define DLOGE(msg)
#define DLOGF(msg)

#endif


// definitions to display messages to a specific log file
//
// TODO:

#endif // DEBUGPRINT

