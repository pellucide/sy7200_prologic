#ifndef _XMLPROPERTIES_H_
#define _XMLPROPERTIES_H_

/**
 * Simple XML Properties object.
 * It can read properties from xml and and write properties to an xml file.
 * This supports only one level of xml, meaning, the root's
 * children are all leaves. It cannot handle multiple levels
 */


#include <stdio.h>
#include <string.h>
#include <expat.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <list>

#ifdef USE_MEMCHECK
#include <memcheck.h>
#endif


using namespace std;



#ifndef YES
#define YES 		0x01
#endif
#ifndef NO
#define NO	 		0x00
#endif

#define XML_HEADER  "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
//#define DEBUG 1
enum COMMAND
{
	FINDKEY = 0x01,
	SETKEY = 0x02,
	FINDLISTKEY

};

enum CMD_STATUS
{
	CMD_STATUS_DEFAULT = 0x00,
	CMD_FINDKEY_SUCCESS = 0x01,
	CMD_SETKEY_SUCCESS = 0x02,
	// Errors
	CMD_FILE_WRITE_FAIL = 0x03
};

class ElmData
{
public:
	char key[100];
	char data[600];  									// <---- this may need to change according to expected data lengths found
	int elmIndexInList;
	int dataIndexInElm;
	ElmData():elmIndexInList(0),dataIndexInElm(0){}
    ~ElmData(){};
};

typedef ElmData* ElmData_P;

typedef struct prop
{
	char * key;
	char * elm;
	char * listKey;
	char * value;
	COMMAND command;
	unsigned char found;
    unsigned char foundList;
    unsigned char foundElm;
	unsigned char dataElmFound;
	unsigned int level;
	char dataElmKey[100];
    list<ElmData>* listElmData;
	int dataElmNum;
	int listElmNum;
	int elmNumInRange;
	int listLevel;
	int elmFrom;
	int elmTo;
	FILE * fw;
	CMD_STATUS status;
} PROP, *PROP_P;


// XML Handlers
void XMLCALL startElementHandler(void * userData, const char * name, const char ** atts);
void XMLCALL endElementHandler(void * userData, const char * name);
void XMLCALL characterDataHandler(void * userData, const XML_Char * s, int len);

class XMLProperties
{
public:
	XMLProperties(const char * filename);
	XMLProperties();
	virtual ~XMLProperties();

	char * GetProperty(const char * key,char* keyValue = NULL);
	char * GetProperty(const char *xmlBuffer , const char * key,char* keyValue = NULL);
	bool SetProperty(const char * key, const char * value);
    list<ElmData>& GetListElements(const char *xmlBuffer,const char * listKey,const char * elementKey,int from,int to);
	list<ElmData>& getListElementData(){return listElmData;}

private:
	const char * filename;
	XML_Parser parser;
	char* buffern;
	list<ElmData> listElmData;
};

#endif //_XMLPROPERTIES_H_
