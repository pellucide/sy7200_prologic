#include "Config.h"

/******************************************************************
 * *************** Date & Time Menu *******************************
 * ***************************************************************/
static const char * DT_MENU_HDNG 			= "      OPTIONS:      ";
static const char * DT_MENU_SETDATEANDTIME 	= "1-SET DATE AND TIME";
static const char * DT_MENU_DAYLIGHTSAVINGS = "2-DAYLIGHT SAVINGS";

/******************************************************************
 * *************** Configure Date *********************************
 * ***************************************************************/
static const char * CFG_DATE_LINE1 = " CONFIGURE THE DATE ";
static const char * CFG_DATE_LINE2 = "     (MMDDYYYY)     ";
/******************************************************************
 * *************** Configure Time *********************************
 * ***************************************************************/
static const char * CFG_TIME_LINE1 = " CONFIGURE THE TIME ";
static const char * CFG_TIME_LINE2 = "       (HHMM)       ";
static const char * CFG_TIME_LINE3 = "    (00:00-23:59)   ";
/******************************************************************
 * *************** Daylight Savings *******************************
 * ***************************************************************/
static const char * CFG_DAYSAV_HDNG     = "  DAYLIGHT SAVINGS  ";
static const char * CFG_DAYSAV_DONOTUSE = "1-DO NOT USE";
static const char * CFG_DAYSAV_STDDATES = "2-STANDARD DATES";


OperationStatusCode DateTimeConfig::ShowDateAndTimeMenuScreen()
{
	OperationStatusCode code = SC_Success;
	int timeout = oper.GetDataTimeout();
	int cursorloc = 3;
	char input;
	
	for(;;)
	{
		cursorloc = 3;
		code = SC_Success;
		user_output_clear();
		user_output_write(1, 1, DT_MENU_HDNG);
		user_output_write(3, 2, DT_MENU_SETDATEANDTIME);
		user_output_write(4, 2, DT_MENU_DAYLIGHTSAVINGS);
		user_output_write(cursorloc, 1, CURSOR); 
		
		while(true)
		{
			int tm = 0;
			while (!user_input_ready () && tm < timeout) 
			{
				usleep(10000);
				tm += 20; 
			}
			if(tm >= timeout) 
			{
				oper.Beep(TYPE_TimeOut);
				return SC_TIMEOUT;
			}
			oper.Beep(TYPE_KeyPress);
			input = user_input_get ();
			switch(input)
			{
				case '1': // Set Date and Time
					code = ShowConfigureDateScreen();
					if(code != SC_Clear) return code;
					break;
				case '2': // Daylight Savings
					code = ShowDaylightSavingsScreen();
					if(code != SC_Clear) return code;
					break;
				case '-': // Down Arrow
					if(cursorloc == 3) 
					{
						user_output_write(cursorloc++, 1, BLANKSPACE); 
						user_output_write(cursorloc, 1, CURSOR);
					}
					break;
				case '+': // Up Arrow
					if(cursorloc == 4) 
					{
						user_output_write(cursorloc--, 1, BLANKSPACE);
						user_output_write(cursorloc, 1, CURSOR);
					}
					break;
				case '\n': 
					if(cursorloc == 3)
					{
						code = ShowConfigureDateScreen();
						if(code != SC_Clear) return code;
					}
					else if(cursorloc == 4) 
					{
						code = ShowDaylightSavingsScreen();
						if(code != SC_Clear) return code;
					}
					break;
				case ESC:
					return SC_Clear;
			}
			if(code == SC_Clear) break;
		}	
	}
	
	return code;
}

/**
 * Reads the Date from the User
 * Need to move this code to LCD Handler
 */
OperationStatusCode DateTimeConfig::ShowConfigureDateScreen()
{
	OperationStatusCode code = SC_Success;
	user_output_clear();
	user_output_write(1, 1, CFG_DATE_LINE1);
	user_output_write(2, 1, CFG_DATE_LINE2);
	
	int timeout = oper.GetDataTimeout();
	
	static char sysdate[9];
	static char disp[2];
	disp[1] = '\0';
	time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );
	strftime(sysdate, 9, "%m%d%Y", optimeinfo);
	user_output_write(4, 7, sysdate);
	
	user_output_cursor(USER_CURSOR_BOTH);
	user_output_write(4, 6, BLANKSPACE);
	
	int numOfChars = 0;
	char input;
	while(true) // 8 Characters in Date Format MMDDYYYY
	{
		int tm = 0;
		while (!user_input_ready () && tm < timeout) 
		{
			usleep(10000);
			tm += 20; 
		}
		if(tm >= timeout) 
		{
			user_output_cursor(USER_CURSOR_OFF);
			oper.Beep(TYPE_TimeOut);
			return SC_TIMEOUT;
		}
		input = user_input_get ();
		if( input == '\n' ) 
		{
			oper.Beep(TYPE_KeyPress);
			break;
		}
		if( input == ESC ) 
		{
			oper.Beep(TYPE_KeyPress);
			user_output_cursor(USER_CURSOR_OFF);
			return SC_Clear;
		}
		switch(input)
		{
			case '>':
				oper.Beep(TYPE_KeyPress);
				if( numOfChars < 7 )
				{
					disp[0] = sysdate[numOfChars];
					user_output_write(4, numOfChars + 7, disp);
					numOfChars++;
				}
				continue;
			case '<':
				oper.Beep(TYPE_KeyPress);
				if( numOfChars > 0 ) 
				{
					disp[0] = sysdate[numOfChars];
					user_output_write(4, numOfChars + 7, disp);
					if(numOfChars == 1)
					{
						user_output_write(4, 6, BLANKSPACE);
					}
					else
					{
						disp[0] = sysdate[numOfChars - 2];
						user_output_write(4, numOfChars + 5, disp);
					}
					numOfChars--;
				}
				continue;
			default:
				break;
		}
		if(input < '0' || input > '9') 
		{
			oper.Beep(150, TYPE_Reject);
			continue;
		}
		
		/**
		 * Validate the Date that the User enters
		 */
		switch(numOfChars)
		{
			case 0:
				if(input != '0' && input != '1') 
				{
					oper.Beep(150, TYPE_Reject);
					continue;
				}
				break;
			case 1:
				if( sysdate[0] == '1' && ( input != '0' && input != '1' && input != '2') ) 
				{
					oper.Beep(150, TYPE_Reject);
					continue;
				}
				break;
			case 2:
				if(input >= '4') 
				{
					oper.Beep(150, TYPE_Reject);
					continue;
				}
				break;
			case 3:
				if( sysdate[2] == '3' && ( input != '0' && input != '1') ) 
				{
					oper.Beep(150, TYPE_Reject);
					continue;
				}
				break;
			case 4:
				if( input != '2') 
				{
					oper.Beep(150, TYPE_Reject);
					continue;
				}
				break;
			case 5:
				if( input != '0') 
				{
					oper.Beep(150, TYPE_Reject);
					continue;
				}
				break;
		}
		oper.Beep(TYPE_KeyPress);
		sysdate[numOfChars] = input;
		disp[0] = input;
		user_output_write(4, numOfChars + 7, disp);
		if(numOfChars == 7)
		{
			disp[0] = sysdate[6];
			user_output_write(4, numOfChars + 6, disp);
		}
		if( numOfChars < 7 ) numOfChars++;
	}
	
	// Show the Set Time Screen
	code = ShowConfigureTimeScreen(sysdate);
	user_output_cursor(USER_CURSOR_OFF);
	return code;
}

/**
 * Reads the Time from the User
 * Need to move this code to LCD Handler
 */
OperationStatusCode DateTimeConfig::ShowConfigureTimeScreen(char * date)
{
	OperationStatusCode code = SC_Success;
	user_output_clear();
	user_output_write(1, 1, CFG_TIME_LINE1);
	user_output_write(2, 1, CFG_TIME_LINE2);
	user_output_write(3, 1, CFG_TIME_LINE3);
	
	int timeout = oper.GetDataTimeout();
	
	static char systime[5];
	static char disp[2];
	disp[1] = '\0';
	time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );
	strftime(systime, 5, "%H%M", optimeinfo);
	user_output_write(4, 9, systime);
	
	user_output_cursor(USER_CURSOR_BOTH);
	user_output_write(4, 8, BLANKSPACE);
	
	int numOfChars = 0;
	char input;
	while(true) // 8 Characters in Time Format HHMM
	{
		int tm = 0;
		while (!user_input_ready () && tm < timeout) 
		{
			usleep(10000);
			tm += 20;
		}
		if(tm >= timeout) 
		{
			user_output_cursor(USER_CURSOR_OFF);
			oper.Beep(TYPE_TimeOut);
			return SC_TIMEOUT;
		}
		
		input = user_input_get ();
		if( input == '\n' ) 
		{
			oper.Beep(TYPE_KeyPress);
			break;
		}
		if( input == ESC ) 
		{
			oper.Beep(TYPE_KeyPress);
			user_output_cursor(USER_CURSOR_OFF);
			return SC_Clear;
		}
		
		switch(input)
		{
			case '>':
				oper.Beep(TYPE_KeyPress);
				if( numOfChars < 3 ) 
				{
					disp[0] = systime[numOfChars];
					user_output_write(4, numOfChars + 9, disp);
					numOfChars++;
				}
				continue;
			case '<':
				oper.Beep(TYPE_KeyPress);
				if( numOfChars > 0 ) 
				{
					disp[0] = systime[numOfChars];
					user_output_write(4, numOfChars + 9, disp);
					if(numOfChars == 1)
					{
						user_output_write(4, 8, BLANKSPACE);
					}
					else
					{
						disp[0] = systime[numOfChars - 2];
						user_output_write(4, numOfChars + 7, disp);
					}
					numOfChars--;
				}
				continue;
			default:
				break;
		}
		
		if(input < '0' || input > '9') 
		{
			oper.Beep(150, TYPE_Reject);
			continue;
		}
		
		/**
		 * Validate the Date that the User enters
		 */
		switch(numOfChars)
		{
			case 0:
				if(input != '0' && input != '1' && input != '2') 
				{
					oper.Beep(150, TYPE_Reject);
					continue;
				}
				break;
			case 1:
				if( systime[0] == '2' && ( input != '0' && input != '1' && input != '2' && input != '3') ) 
				{
					oper.Beep(150, TYPE_Reject);
					continue;
				}
				break;
			case 2:
				if(input >= '6' ) 
				{
					oper.Beep(150, TYPE_Reject);
					continue;
				}
				break;
		}
		oper.Beep(150, TYPE_KeyPress);
		systime[numOfChars] = input;
		disp[0] = input;
		user_output_write(4, numOfChars + 9, disp);
		if(numOfChars == 3)
		{
			disp[0] = systime[2];
			user_output_write(4, numOfChars + 8, disp);
		}
		if( numOfChars < 3 ) numOfChars++;
	}
	// Set The Time
	static char datecmd[21];
	//Build the date command
	sprintf(datecmd, "date ");
	strncat(datecmd, date, 4);
	strncat(datecmd, systime, 4);
	strncat(datecmd, date+4, 4);
	system(datecmd);
	system("hwclock -w");
	oper.CreateDateTimeChangeTransaction();
	user_output_cursor(USER_CURSOR_OFF);
	return code;
}

OperationStatusCode DateTimeConfig::ShowDaylightSavingsScreen()
{
	OperationStatusCode code = SC_Success;
start:	user_output_clear();
	user_output_write(1, 1, CFG_DAYSAV_HDNG);
	user_output_write(3, 2, CFG_DAYSAV_DONOTUSE);
	user_output_write(4, 2, CFG_DAYSAV_STDDATES);
	
	int timeout = oper.GetDataTimeout();
	
	int cursorloc = 3;
	user_output_write(cursorloc, 1, CURSOR); 
	char input;
	
	while(true)
	{
		int tm = 0;
		while (!user_input_ready () && tm < timeout) 
		{
			usleep(10000);
			tm += 20; 
		}
		if(tm >= timeout) return SC_TIMEOUT;
		input = user_input_get ();
		switch(input)
		{
			case '1': // Do Not Use
				oper.Beep(50, TYPE_KeyPress);
				#ifdef DEBUG
				printf("Do Not Use\n");
				#endif
				break;
			case '2': // Std Dates
				oper.Beep(50, TYPE_KeyPress);
				#ifdef DEBUG
				printf("STD DATES\n");
				#endif
				break;
			case '-': // Down Arrow
				if(cursorloc == 3) 
				{
					oper.Beep(50, TYPE_KeyPress);
					user_output_write(cursorloc++, 1, BLANKSPACE); 
					user_output_write(cursorloc, 1, CURSOR);
				}
				else
				  oper.Beep(50,TYPE_Reject); 
				break;
			case '+': // Up Arrow
				if(cursorloc == 4) 
				{
					oper.Beep(50, TYPE_KeyPress);
					user_output_write(cursorloc--, 1, BLANKSPACE);
					user_output_write(cursorloc, 1, CURSOR);
				}
				else
				    oper.Beep(50,TYPE_Reject);
				break;
			case '\n': 
				if(cursorloc == 3) 
				{
					oper.Beep(50, TYPE_KeyPress);
					#ifdef DEBUG
					printf("DO NOT USE\n");
					#endif
				}
				else if(cursorloc == 4) 
				{
					oper.Beep(50, TYPE_KeyPress);
					#ifdef DEBUG
					printf("STD DATES\n");
					#endif
				}
			break;
			case ESC:
				return SC_Clear;
			break;	
		    default:
		        oper.Beep(150,TYPE_Reject);
                oper.displayErrMessage("   NOT AVAILABLE    ");
                goto start; 
            break;    
		        	
		}
		if(code == SC_TIMEOUT) break;
	}
	
	return code;
}

