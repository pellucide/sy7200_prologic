#include "MSQLServer.h"


CS_RETCODE MSQLServer::login()
{
  CS_CONTEXT *ctx;
  CS_CONNECTION *conn;
  CS_COMMAND *cmd;
  CS_RETCODE ret;
  int verbose = 0;

  if(connected)
  {
	debugFX(100,"INFO:The Terminal already connected\n");
	return CS_SUCCEED
  }
  ret = try_ctlogin(&ctx, &conn, &cmd, verbose);
  if (ret != CS_SUCCEED)
  {
	debugFX(100,"ERR:MSQLS Login Failed RET=%d\n",ret);
	return ret;
  }
  connected = true;
  return ret;
}
CS_RETCODE MSQLServer::logout()
{
  CS_CONTEXT *ctx;
  CS_CONNECTION *conn;
  CS_COMMAND *cmd;
  CS_RETCODE ret;
  int verbose = 0;

  if(connected)
  {
	ret = try_ctlogout(ctx, conn, cmd, verbose);
	if (ret != CS_SUCCEED)
	{
		debugFX(100,"ERR:MSQLS Logout Failed RET=%d\n",ret);
		return ret;
	}
	connected = false;
	return ret;
  }
  debugFX(100,"INFO:MSQLS Already Disconnected \n");
  return CS_SUCCEED;
}


CS_RETCODE MSQLServer::get_query_data(string query)
{
    CS_CONTEXT *ctx;
	CS_CONNECTION *conn;
	CS_COMMAND *cmd;
	
	CS_INT num_cols;
	CS_INT count, row_count = 0;
	CS_INT result_type;
	CS_RETCODE ret;
	CS_RETCODE results_ret;
	int i, is_return_status=0;
	int is_status_result=0;
    int verbose = 0;

	if(!connected)
	{
	  debugFX(100,"ERR:MSQLS Not Connected\n");
	  return CS_FAIL;
	}
    ret = ct_command(cmd, CS_LANG_CMD,query, CS_NULLTERM, CS_UNUSED);
	if (ret != CS_SUCCEED) 
	{
		debugFX(100,"ERR:MSQLS Run Command Failed\n");
		return ret;
	}
    ret = ct_send(cmd);
    if (ret != CS_SUCCEED) 
	{
      debugFX(100,"ERR:MSQLS Send Command Failed\n");
      return ret;
	}

	fetch_query_data(cmd);
	


}

CS_RETCODE MSQLServer::fetch_query_data(CS_COMMAND *cmd)
{
    //CS_RETCODE ret;
	CS_RETCODE results_ret;
	CS_INT num_cols;
	int i, is_return_status=0;
	bool is_status_result=false;

	while ((results_ret = ct_results(cmd, &result_type)) == CS_SUCCEED) 
	{
		is_status_result = 0;
		switch ((int) result_type) 
		{
			case CS_CMD_SUCCEED:
				debugFX(100,"INFO:ct_results() result_type CS_CMD_SUCCEED.\n");
				break;
			case CS_CMD_DONE:
				debugFX(100,"INFO:ct_results() result_type CS_CMD_DONE.\n");
				break;
			case CS_CMD_FAIL:
				 debugFX(100,"ERR:ct_results() result_type CS_CMD_FAIL.\n");
				 return CS_CMD_FAIL;
			case CS_STATUS_RESULT:
				debugFX(100, "INFO:ct_results: CS_STATUS_RESULT detected \n");
				is_status_result = true;
				/* fall through */
            case CS_ROW_RESULT:
				 get_check_query_column(cmd,&num_cols,is_status_result);
				 bind_query_data(cmd,num_cols,)



}

CS_RETCODE get_check_query_column(CS_COMMAND *cmd,CS_INT *num_cols,bool is_status_result)
{
	CS_RETCODE ret = ct_res_info(cmd, CS_NUMDATA,num_cols, CS_UNUSED, NULL);
    
    if (ret != CS_SUCCEED || *num_cols > maxcol) {
	   debugFX(stderr, "ct_res_info() failed\n");
	   return ret;
	}

	if (*num_cols <= 0) {
		debugFX(stderr, "ct_res_info() in return strange values of number column\n");
		return CS_CMD_FAIL;
	}

	if (is_status_result && *num_cols != 1) {
		debugFX(stderr, "CS_STATUS_RESULT return more than 1 column\n");
		return CS_CMD_FAIL;
	}
	return ret;
}

CS_RETCODE bind_query_data(CS_COMMAND *cmd,CS_INT num_cols,CS_INT datatype,CS_INT format,CS_INT maxlength,CS_INT count,CS_LOCALE *locale)
{
   CS_RETCODE ret;
   int i;

   for (i=0; i < num_cols; i++) 
   {
	 /* here we can finally test for the return status column */
	 ret = ct_describe(cmd, i+1, &col[i].datafmt);
	 if (ret != CS_SUCCEED)
	 {
		debugFX(stderr, "ct_describe() failed for column %d\n", i);
		return ret;
	 }
     if (col[i].datafmt.status & CS_RETURN) 
	 {
		fprintf(stdout, "ct_describe() indicates a return code in column %d for sp_who\n", i);
		is_return_status = i+1;
		/*
		 * other possible values:
		 * CS_CANBENULL
		 * CS_HIDDEN
		 * CS_IDENTITY
		 * CS_KEY
		 * CS_VERSION_KEY
		 * CS_TIMESTAMP
		 * CS_UPDATABLE
		 * CS_UPDATECOL
		*/
	 }
   
}