#include <ctype.h>
#include "Operation.hpp"

/**
 * Check if the string is Alpha
 */
Boolean Operation::CheckInputIsAlpha(char * input)
{
	SysFieldLength len = strlen(input);
	for(SysFieldLength i=0; i<len; i++)
	{
		if(isalpha(*(input + i)) == 0)
		{
			return false;	
		}
	}
	return true;
}

void Operation::PadAlpha(char * input, SysFieldLength min)
{
	SysFieldLength len = strlen(input);
	/**
	 * Check if we have to pad with blank
	 */
	if(len < min)
	{
		for(SysFieldLength i=0; i < min - len; i++)
		{
			*(input + len + i) = ' ';
		}
		*(input + min) = '\0';	
	}
}

/**
 * Check if the string is Numeric
 */
Boolean Operation::CheckInputIsNumeric(char * input)
{
	SysFieldLength len = strlen(input);
	for(SysFieldLength i=0; i<len; i++)
	{
		if(isdigit(*(input + i)) == 0)
		{
			return false;	
		}
	}
	
	return true;
}

void Operation::PadNumeric(char * input, SysFieldLength min)
{
	SysFieldLength len = strlen(input);
	// Pad with leading zeroes the length is less than what is needed
	if( len < min )
	{
		char pad[min];
		for( int i=0; i<min - len; i++)
		{
			pad[i] = '0';
		}
		pad[min - len] = '\0';
		strcat(pad, input);	
		strcpy(input, pad);
	}
}

/**
 * Check if the string is AlphaNumeric
 */
Boolean Operation::CheckInputIsAlphaNumeric(char * input)
{
	SysFieldLength len = strlen(input);
	for(SysFieldLength i=0; i<len; i++)
	{
		if(isalnum(*(input + i)) == 0)
		{
			return false;	
		}
	}
	return true;
}

Boolean Operation::CheckInputIsDecimal(char * input, SysDP numdec)
{
	SysFieldLength len = strlen(input);
	for(SysFieldLength i=0; i<len; i++)
	{
		if( isdigit( *(input + i) ) == 0 && *(input + i) != '.')
		{
			return false;	
		}
	}
	return true;
}
