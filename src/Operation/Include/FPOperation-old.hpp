#ifndef _FP_H_
#define _FP_H_

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <typeinfo>
#include <errno.h>
#include <time.h>

#include "Str_Def.h"
#include "StdInput.h"
#include "asm/TA7000_IO.h"
#include "Operation.hpp"
#include "LCDHandler.h"
#include <pthread.h>

extern "C" {
#include "uf_api.h"
#include "uf_serial.h"
#include "uf_module_log.h"
#include "uf_log.h"
#include "uf_packet.h"
#include "uf_command.h"

}

#define FP_TEMPLATESIZE 384
#define ENCODED_TEMPLATESIZE 512
#define PACKET_LEN 13

const int mTimeout =5000;   //timeout = 3000 ms

enum FingerPrintOperation
{
	Verify_Operation=0,
	Identify_Operation,
	Ignore_Operation
};


class FPOperation
{
public:
	FPOperation();
	~FPOperation();
    LCDHandler lcdHandler;
	Operation oper;
	FingerPrintOperation fpOperation;
	UF_UINT32 FpUserID;
	bool FpScanValid ;
    bool EnableFpUnit;
    UF_RET_CODE ScanResult;
	int ScanCount;
	static pthread_mutex_t finger_print_mutex;
	static pthread_cond_t  fprint_cond_flag;

	OperationStatusCode Enroll(unsigned long int UserId , int timeOut);
    OperationStatusCode Verification(unsigned long int UserId ,int timeOut,UF_RET_CODE* Result );
    OperationStatusCode Identification(unsigned long int* UserId,int timeOut,UF_RET_CODE* Result);
    OperationStatusCode ScanTemplate(UF_UINT32* templ_size,UF_BYTE* templ_data ,int timeOut,UF_RET_CODE* Result);
	OperationStatusCode ScanTemplateScore(UF_UINT32* templ_size,UF_BYTE* templ_data , int timeOut ,UF_RET_CODE* Result, UF_UINT32* p_iqs);
	OperationStatusCode IdentifyByTemplate(UF_BYTE* templ_data, UF_UINT32* templ_size, UF_UINT32* UserId ,int timeOut,UF_RET_CODE* Result);
    OperationStatusCode VerifyByTemplate(UF_BYTE* templ_data, UF_UINT32* templ_size, UF_UINT32 UserId ,int timeOut,UF_RET_CODE* Result);
	OperationStatusCode DeleteUser(unsigned long int UserId ,UF_RET_CODE* Result );
	OperationStatusCode DeleteAllUsers(UF_RET_CODE* Result );
	OperationStatusCode AddUser(UF_UINT32 UserId ,UF_BYTE* templ,UF_UINT32 templ_size,UF_RET_CODE* Result);
	UF_RET_CODE WriteSystemParameter( UF_SYS_PARAM param_id, UF_UINT32 param_value );
	UF_RET_CODE FPU_Initialize();
    UF_RET_CODE FPU_Finalize();
	UF_RET_CODE FPU_Reset();
};

#endif //_FP_H_
