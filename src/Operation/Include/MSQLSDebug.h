/***************************************************************************
*                                                                             
*                                LEGAL NOTICES                                
*                                --------------------------                                
*                                                                             
*    COPYRIGHT (C) SYNEL INC. ALL RIGHTS RESERVED.                        
*                                                                             
*    ANY USE OF THIS IMPLEMENTATION REQUIRES AN 
*    APPROPRIATE LICENSE FROM SYNEL INC.        
*
***************************************************************************/
/***************************************************************************
*                                                                             
*                                MSQLSDebug.h                      
*                                ------------                                
* 
*    Header file for debug functions
*     
***************************************************************************/

#ifndef _MSQLSDEBUG_H_
#define _MSQLSDEBUG_H_

#define MSQLSDEBUG                   0

/* Inline and macro functions for SY7000Demo. */
#if defined (MSQLSDEBUG) && MSQLSDEBUG > 0
	#include <stdio.h>
	#define debug(fmt, args...) printf(fmt, ## args)
	#define debugF(fmt, args...) printf("%s: " fmt, __func__, ## args)
	#define debugX(lvl, fmt, args...) if (DEBUG >= lvl) debug(fmt, ## args);
	#define debugFX(lvl, fmt, args...) if (DEBUG >= lvl) debugF(fmt, ## args);
#else
	#define debug(fmt, args...)
	#define debugF(fmt, args...)
	#define debugX(lvl, fmt, args...)
	#define debugFX(lvl, fmt, args...)
#endif

#endif //_NE7000DEBUG_H_