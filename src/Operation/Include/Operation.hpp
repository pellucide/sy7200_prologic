#ifndef OPERATION_HPP
#define OPERATION_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif


#include "System.hpp"
#include <time.h>
#include <pthread.h>


enum BadgeType
{
    TYPE_Supervisor = 0,
    TYPE_Employee,
    TYPE_Configuration,
    TYPE_Swipe_And_Go,
    TYPE_Invalid
};

enum SupervisorType
{
	TYPE_Supervisor_None = 0,
	TYPE_Supervisor_Entry,
	TYPE_Supervisor_Recall
};

enum SourceType
{
    TYPE_SupervisorInputSource = 0,
    TYPE_SupervisorEmployeeInputSource,
    TYPE_EmployeeInputSource,
    TYPE_ConfigurationSource,
    TYPE_FunctionSource,
    TYPE_DateSource,
	TYPE_TimeSource,
	TYPE_Unknown
};

enum PromptType
{
	TYPE_Idle = 0,
	TYPE_InvalidSource,
	TYPE_InvalidBadge,
	TYPE_Try_Again,
	TYPE_EnterFunction
};

enum BeepType
{
	TYPE_Accept = 0,
	TYPE_Reject,
	TYPE_Error,
	TYPE_KeyPress,
	TYPE_TimeOut,
	TYPE_TurnOff
};

enum OperationStatusCode
{
	SC_Access_Denied=0,
	SC_Invalid_Badge,
	SC_Invalid_Function,
	SC_Supervisor_Function,
	SC_Invalid_Source,
	SC_Invalid_Time,
	SC_Invalid_Date,
	SC_Allow,
	SC_Allow_Access,
	SC_Profile_Denied,
	SC_TIMEOUT,
	SC_Clear,
	SC_Success,
	SC_OutOfMemory,
	SC_Fail,
	SC_Scan_Ready,
	SC_Try_Again,
	SC_ActivityCodePrompt,
	SC_ShowAppVer

};


enum FPReaderStatus
{
	FP_DISABLED = 0,
	FP_ENABLED
};

enum FPIDMode
{
	VERIFY_MODE = 0,
	IDENTIFY_MODE
};

enum WatchDogStatus
{
	WATCH_DISABLED = 0,
	WATCH_ENABLED
};

/**
 * Class Operations
 * Holds all the Operation Logic
 */
class Operation
{
    public:
        Operation(){accessCode[0] = '\0'; resetAccessCode[0] = '\0';};
        ~Operation(){};

        /**
         * Time Functions
         */
        static pthread_mutex_t localtime_mutex;
        struct tm localtimeinfo;
        void GetTimeAsString(char *);
        const char * GetFormattedTime();
        const char * GetTimeForIdleDisplay();
        const char * GetDateForIdleDisplay();
        const char * GetSystemTime();
        const char * GetSystemDate();
        int GetDayOfMonth();

		void ReadBadgeOffset(char *);
#ifdef client_prologic
		void ReadKeypadOffset(char *);
#endif
        BadgeType GetBadgeType(const char *);
        SupervisorType GetSupervisorType(const char *);
        int GetMaxBadgeLen();
        ///Not being Used Right now
        void SetFPReaderStatus(int status)
        {
	        if(status == 1) fp_reader_status = FP_ENABLED;
	        else fp_reader_status = FP_DISABLED;
	    };

	    FPReaderStatus GetFPReaderStatus()  {  return fp_reader_status;	};
	 	///

	 	FPIDMode GetFPIDMode();

	 	int GetSecurityLevel();

        void InitWatchDog();
        void SetWatchOption(int status);
        void SetAutoRestart(int option)
        {
	        if (option ==1)
        		auto_restart = true;
        	else
        		auto_restart = false;
        }

	    WatchDogStatus GetWatchOption()   {  return watchdogstatus;		};

	 	bool AutoRestartEnabled()	{	return auto_restart;	}

        /**
         * Bell Module
         */
        void CheckBellSchedule();

        /**
         * Access Window Check
         */
		bool CheckIfInAccessWindow();

        char GetSwipeAndGoFunction();
        char GetTerminalId();
        void SetTerminalId(char id);
        void UpdateClockFile();
        void SetTCPPort(int port);
        int GetTCPPort();
        char * GetAccessCode();
        void SetAccessCode(const char * code);
//
        char * GetResetAccessCode();
        void SetResetAccessCode(const char * code);

        /**
         * Validation
         */
        Code ValidateBadge(char *);
#ifdef client_prologic
//  copy ValidateBadge here as ValidateKeypadBadge
		Code ValidateKeypadBadge(char *);
#endif
        Code ValidateSource(const SysInputSource, SourceType);
        OperationStatusCode ValidateFunction(const char *, const SysFunctionKey, const BadgeType, SysLevelData *, Bit8 *);
        Code ValidateDate(const char *);
        Code ValidateTime(const char *);

        /**
         * Messages
         */
        int GetEmployeeMessage(const char * badge,char * buff,int buffSize);
        int GetEmployeeMessage(const char *, const SysFunctionKey,char * buff,int buffSize);
        int GetPromptMessage(PromptType,char * buff,int buffSize);
        const char * GetFunctionMessage(const SysFunctionKey);
        const char * GetFunctionMessageForLevel(const SysFunctionKey key, const SysFunctionLevel level);

        /**
         * Transactions
         */
		Boolean ReportAccesDeniedTransaction();
		void SetAccesDeniedTransFlag(OperationStatusCode, Bit8 &);
		OperationStatusCode CreateDateTimeChangeTransaction();
		OperationStatusCode ClearClockTransactions();

		/**
		 * Timeouts
		 */
		int GetDataTimeout();
		int GetFunctionTimeout();
		int GetMessageTimeout();
		int GetSupervisorModeTimeout();
		int GetErrorMsgTimeout();

		/**
		 * Access
		 */
		int GetAccessDuration();

		/**
		 * Sound
		 */
		void Beep(int, BeepType);
		void Beep(BeepType type)
		{
			Beep(0, type);
		}

		/**
		 * LEDs
		 */
		void TurnOnLEDRed();
		void TurnOffLEDRed();
		void TurnOnLEDYellow();
		void TurnOffLEDYellow();
		void TurnOnLEDGreen();
		void TurnOffLEDGreen();
        void displayErrMessage(char* msg);

	private:
	//	bool FpEnabled;
		static FPReaderStatus fp_reader_status;
		static WatchDogStatus watchdogstatus;
		static bool auto_restart;

        SysDayOfWeek GetSysDayOfWeek(int day);
        OperationStatusCode CheckProfileForBadge(const char *, Bit8 &, Bit8 &,Bit8 &);
        OperationStatusCode CheckAccessForBadge(const char *, Bit8 &, Bit8 &);
        OperationStatusCode CheckLevelsForFunction(const SysFunctionKeyItem *,const char *, SysLevelData *);
        int GetDayOfWeek();
        SysTime GetTime();
        OperationStatusCode CheckProfileMessageFlags(const SysProfileMessageItem *, Bit8 &, Bit8 &);
        Boolean ValidateTable(const char *, char *, SysInputTable);

        Boolean CheckInputIsAlpha(char *);
        Boolean CheckInputIsNumeric(char *);
        Boolean CheckInputIsAlphaNumeric(char *);
        Boolean CheckInputIsDecimal(char *, SysDP);


        void PadAlpha(char *, SysFieldLength);
        void PadNumeric(char *, SysFieldLength);

		char accessCode[21];
		char resetAccessCode[21];

	public:
		char lastFun[20];

		static int iTimeZone;
		static char strDaylightSavingsStart[8]; // MM/DD
		static char strDaylightSavingsEnd[8];
		static void SYClock_GetTime(struct tm *);


};

inline const size_t const_thread_stack_size () { return 100000 ;}

#endif // #endif // OPERATION_HPP

