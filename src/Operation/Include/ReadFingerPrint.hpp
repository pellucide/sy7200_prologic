//Copyright (C) 2005 TimeAmerica, Inc. All Rights Reserved
//20051109 wjm
//$Id: ReadFingerPrint.hpp,v 1.4 2007/01/06 03:04:20 walterm Exp $

#ifndef READFINGERPRINT_HPP
#define READFINGERPRINT_HPP

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

#include <pthread.h> 

#include "fpmodule.h"

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

class ReadFingerPrint{

public:
  ReadFingerPrint( int fd_wr );
  virtual ~ReadFingerPrint(){};

  static const char* Identify ( const char* t , char* out_badgenr , int* ix );
  static void EnrollDefault ( const char* t );
  static bool Verify ( const char* t );

private:
  //copy and assignment are NOT allowed!
  ReadFingerPrint ( const ReadFingerPrint& );
  ReadFingerPrint& operator = ( const ReadFingerPrint& );

private:
  static void * FingerThread(void *arg);
  static bool initFpSdk();

};

//***********************************************************************
#endif //READFINGERPRINT_HPP
