#include "Config.h"

/******************************************************************
 * ******************* Readers Menu *****************************
 * ***************************************************************/

/**
 * Menu Strings
 */
static const char * READERS_HDNG		= "     READERS      ";
static const char * READERS_FP	= "1- FINGERPRINT RDR";

static const char * FP_HDNG			= " FINGERPRINT READER ";
static const char * FP_ENABLE		= "1- ENROLLMENT ON";
static const char * FP_DISABLE		= "2- ENROLLMENT OFF";


OperationStatusCode ReaderConfig::ShowReadersScreen()
{	
	int timeout = oper.GetSupervisorModeTimeout();
	int tm = 0;
	char input;
	
	for(;;)
	{
		user_output_clear();
		user_output_write(1, 1, READERS_HDNG);
		user_output_write(2, 2, READERS_FP);
		user_output_write(2, 1, CURSOR);
		
		tm = 0;
		while(true)
		{
			if (user_input_ready ()) 
			{
				oper.Beep(50, TYPE_KeyPress);
				input = user_input_get ();
				if( input == ESC ) return SC_Clear;
				else if( input == '1' || input == '\n' )
				{
					OperationStatusCode code = ShowFPReaderScreen();
					if(code == SC_Clear) break;
					else return code;
				}
			}
			else
			{
				usleep(10000);
				tm += 20;
				if(tm >= timeout)
				{
					return SC_TIMEOUT;
				}
			}
		}	
	}
	return SC_Success;
}

OperationStatusCode ReaderConfig::ShowFPReaderScreen()
{
	user_output_clear();
	user_output_write(1, 1, FP_HDNG);
	user_output_write(2, 2, FP_ENABLE);
	user_output_write(2, 1, CURSOR);
	user_output_write(3, 2, FP_DISABLE);
	
	int timeout = oper.GetSupervisorModeTimeout();
	int cursorloc = 2;
	char input;
	
	while(true)
	{
		int tm = 0;
		while (!user_input_ready () && tm < timeout) 
		{
			usleep(10000);
			tm += 20; 
		}
		if(tm >= timeout) return SC_TIMEOUT;
		oper.Beep(50, TYPE_KeyPress);
		input = user_input_get();
		switch(input)
		{
			case '1': // Enable telnet
				return EnableFPEnroll();
				break;
			case '2': // Network Status
				return DisableFPEnroll();
				break;
			case '-': // Down Arrow
				if(cursorloc < 3)
				{
					user_output_write(cursorloc++, 1, BLANKSPACE);
				}
				user_output_write(cursorloc, 1, CURSOR);
				break;
			case '+': // Up Arrow
				if(cursorloc > 2)
				{
					user_output_write(cursorloc--, 1, BLANKSPACE);
				}
				user_output_write(cursorloc, 1, CURSOR);
				break;
			case '\n':
				if(cursorloc == 2)
				{
					return EnableFPEnroll();
				}
				else if(cursorloc == 3)
				{
					return DisableFPEnroll();
				}
				break;	
			case ESC:
				return SC_Clear;
		}
	}
	
	return SC_Success;
}

OperationStatusCode ReaderConfig::EnableFPEnroll()
{
	//do uf_initialise here?
	//set some flag to indicate its enabled
	oper.SetFPReaderStatus(1);
	user_output_clear();
	user_output_write(2, 2, "ENROLLMENT ENABLED");
	usleep(500000);
	return SC_Success;
}

OperationStatusCode ReaderConfig::DisableFPEnroll()
{
	//do uf_finalise here?
	oper.SetFPReaderStatus(0);
	user_output_clear();
	user_output_write(2, 2, "ENROLLMENT DISABLED");
	usleep(500000);
	return SC_Success;
}
