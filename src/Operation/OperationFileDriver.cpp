#include <iostream>
#include <fstream>
#include <time.h>

#include "Str_Def.h"
#include "OperationDriver.hpp"
#include "OperationUtil.hpp"
#include "LegacyTrans.hpp"

using namespace std;

/**
 * Runs the bell schedule check every minute
 */
/*void *RunBellSchedule(void * id)
{
   Operation * oper = new Operation();
   //Check for bell schedules every minute
   while(true)
   {
       oper->CheckBellSchedule(); 
       sleep(10);
   }
}*/

OperationFileDriver::OperationFileDriver()
{
}

OperationFileDriver::~OperationFileDriver()
{   
}

Code OperationFileDriver::Process()
{
    Code code = SC_OKAY;
    char buffer[128];
    SysInputSource source;
    char sourcechar;
    /**
     * flag to check if an employee has swiped or a supervisor is 
     * entering the employee transaction 
     */
    bool superemployee = false;
    ifstream transfile ("Operation.clk");
    if (! transfile.is_open())
    {
        cout << "Error opening file"; 
        return SC_FAILURE;
    }
    
    //Get the Opeartion Time and add to the buffer
    time_t optime;
    struct tm * optimeinfo;
    time ( &optime );
    optimeinfo = localtime ( &optime );
    IntegerPut (TYPE_UBin16, &optimeinfo->tm_hour, NULL, 2);
    IntegerPut (TYPE_UBin16, &optimeinfo->tm_min, NULL, 2);
    IntegerPut (TYPE_UBin16, &optimeinfo->tm_sec, NULL, 2);
    
    if( !transfile.eof() )
    {
        //Read the badge source
        transfile.getline (buffer,100);
        str_clip (buffer);
        //Validate the source
        code = GetSysInputSourceForChar(*buffer, source);
        if( code != SC_OKAY) return code;
    }
    
    if( !transfile.eof() )
    {
        //Read the Badge
        transfile.getline (buffer,100);
        str_clip (buffer);
        //Validate the badge
        code = oper->ValidateBadge(buffer);
        if( code != SC_OKAY) return code; //need to display appropriate message
        oper->ReadBadgeOffset(buffer);
        
        //Check what type of badge it is 
        BadgeType type = oper->GetBadgeType(buffer);
        
        const char * msg = oper->GetEmployeeMessage(buffer);
        if(msg != NULL)
        {
            cout << "Initial Swipe Message : " << msg << endl;
        }
        
        switch(type)
        {
            case TYPE_Configuration:
            {
                break;
            }    
            case TYPE_Invalid:
            {
                break;
            }
            case TYPE_Supervisor:
            {
                /**
                 * Check if the supervisor source is valid
                 */
                code = oper->ValidateSource(source, TYPE_SupervisorInputSource);
                if( code != SC_OKAY) return code;
                /**
                 * Add the source to the operation data
                 */
                code = GetCharForSysInputSource(source, sourcechar);
                if( code != SC_OKAY) return code;
                CharPut(sourcechar);
                
                
                /**
                 * Add the badge to the operation data
                 */
                StringPut(buffer);
                CharPut(TransFieldSeparatorChar);
                CharPut(TransSuperFunctionChar);
                
                //Read the Date source
                if( !transfile.eof() )
                {
                    //Read the badge source
                    transfile.getline (buffer,100);
                    str_clip (buffer);
                    //Validate the source
                    code = GetSysInputSourceForChar(*buffer, source);
                    if( code != SC_OKAY) return code;
                    code = oper->ValidateSource(source, TYPE_DateSource);
                    if( code != SC_OKAY) return code;
                    //Add the source to the opeartion data
                    code = GetCharForSysInputSource(source, sourcechar);
                    if( code != SC_OKAY) return code;
                    CharPut(sourcechar);
                } 
                
                //Read the Date  
                if( !transfile.eof() )
                {
                    //Read the date
                    transfile.getline (buffer,100);
                    str_clip (buffer);
                    //Validate the date
                    code = oper->ValidateDate(buffer);
                    if( code != SC_OKAY) return code;
                    //Add the source to the opeartion data
                    StringPut(buffer);
                }
                
                //Read the Time source
                if( !transfile.eof() )
                {
                    //Read the badge source
                    transfile.getline (buffer,100);
                    str_clip (buffer);
                    //Validate the source
                    code = GetSysInputSourceForChar(*buffer, source);
                    if( code != SC_OKAY) return code;
                    code = oper->ValidateSource(source, TYPE_DateSource);
                    if( code != SC_OKAY) return code;
                    //Add the source to the opeartion data
                    code = GetCharForSysInputSource(source, sourcechar);
                    if( code != SC_OKAY) return code;
                    CharPut(sourcechar);
                }
                
                //Read the Time  
                if( !transfile.eof() )
                {
                    //Read the badge source
                    transfile.getline (buffer,100);
                    str_clip (buffer);
                    //Validate the source
                    code = oper->ValidateTime(buffer);
                    if( code != SC_OKAY) return code;
                    //Add the source to the opeartion data
                    StringPut(buffer);
                }
                
                //Read the Employee Badge source
                if( !transfile.eof() )
                {
                    //Read the badge source
                    transfile.getline (buffer,100);
                    str_clip (buffer);
                    //Validate the source
                    code = GetSysInputSourceForChar(*buffer, source);
                    if( code != SC_OKAY) return code;
                    code = oper->ValidateSource(source, TYPE_SupervisorEmployeeInputSource);
                    if( code != SC_OKAY) return code;
                    //Add the source to the opeartion data
                    code = GetCharForSysInputSource(source, sourcechar);
                    if( code != SC_OKAY) return code;
                    CharPut(sourcechar);
                    superemployee = true;
                }
                
                //Read the Employee Badge  
                if( !transfile.eof() )
                {
                    //Read the badge source
                    transfile.getline (buffer,100);
                    str_clip (buffer);
                    //Validate the source
                    code = oper->ValidateBadge(buffer);
                    if( code != SC_OKAY) return code;
                    oper->ReadBadgeOffset(buffer);
                }
                //Fall through to Read the Employee Stuff  
            }    
            case TYPE_Employee:
            {
                /**
                 * Check if supervisor is entering the employee info
                 */
                 if (!superemployee)
                 {
                    /**
                     * Check if the employee source is valid
                     */
                    code = oper->ValidateSource(source, TYPE_EmployeeInputSource);
                    if( code != SC_OKAY) return code;
                    /**
                     * Add the source to the operation data
                     */
                    code = GetCharForSysInputSource(source, sourcechar);
                    if( code != SC_OKAY) return code;
                    CharPut(sourcechar);
                 }    
                 //Add the badge to the operation data
                 StringPut(buffer);
                 CharPut(TransFieldSeparatorChar);
                 if( !transfile.eof() )
                 {
                     //Read the Function
                     transfile.getline (buffer,100);
                     str_clip (buffer);
                     //Validate the function
                     //code = oper->ValidateFunction(*buffer);
                     if( code != SC_OKAY) return code;
                     //Add the function to the opeartion data
                     CharPut(*buffer);
                    
                     //Need to find out where to get the flags from
                     CharPut('@');
                     CharPut('@');
                     CharPut('@');
                     CharPut('@');
                     CharPut('@');
                 }
                 break;
             }     
             case TYPE_Swipe_And_Go:
             	break;   
        }    
    }    
    
    //Send the data to the Transaction module
    LegacyTransItem * transitem = LegacyTransItem::GetInstance();
    transitem->ProcessData((char *)data, false);
    transfile.close();
    
    //pthread_t thread1;
    //int id = 1;
    //pthread_create( &thread1, NULL, RunBellSchedule, (void *)id);
   
    string input = "Exit";
    const string quit = "quit";
    while( strcmp( input.c_str(), quit.c_str() ) != 0 )
    {
        cout << "\nEnter quit to Exit the Application : ";
        cin >> input;
    }
    
    return code;
}
