#ifndef __WATCHDOG_H__
#define __WATCHDOG_H__

#include <stdio.h>

//	WatchDog();
	void WatchDog_Prologue();
//	~WatchDog();
	void WatchDog_Epilogue();
//	void Run();
	void WatchDog_Run();

//	bool auto_restart;
	typedef enum {false, true}	bool;
	bool auto_restart;


// private: make them static in c file
//	static void RebootClock();
//	static char * getFormattedTime();
//	static char * getFormattedTimeOfWeek();
//	static char * getFormattedTimeOfDay();

//	static bool CompareTimeStamps(const char * tstamp, const char * systime);
//	static bool CompareTimeForRestartWeekly(const char * tstamp, const char * systime);
//	static bool CompareTimeForRestartDaily(const char * tstamp, const char * systime);


#endif

