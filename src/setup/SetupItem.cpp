/*
	SetupElement:  [Class]

	The SetupElement module provides

	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   01 Jan 2000
	 $Revision: 1.2 $
	   $Author: deepali $
	  $Modtime$
	     $Date: 2006/05/12 01:28:21 $

Description:

	This module

*/


/* Compilation options for SetupElement. */

/* Include files for SetupElement. */
#include <string.h>
#include "System.hpp"

#include "Setup.hpp"

/* Constant parameters for SetupElement. */

/* Type and structure definitions for SetupElement. */

/* Internal static data for SetupElement. */

/* Function prototypes for SetupElement. */


/*
	SetupElement::Lookup:

	Lookup a tag name in an array of SetupElement objects.

Description:

	The SetupElement object is assumed to be an array of such objects with a
final element containing an null pointer for the name member.

See also:  SetupElement::LookupLegacy

Arguments:
	tag			Tag name to lookup.

Returns:
	SetupElement *
				Pointer to matching SetupElement object;
				NULL if tag was not found.

*/

		const SetupElement *
SetupElement::Lookup (const char * tag)
	const

{
	const SetupElement * list_p;

	for (list_p = this; ; list_p++) {
		if (NOT list_p->Valid ()) {
			list_p = NULL;
			break;
			}
		if (strcmp (tag, list_p->tag) == 0) break;
		}

	return (list_p);
}


/*
	SetupElement::LookupLegacy:

	Lookup a legacy tag name in an array of SetupElement objects.

Description:

	The SetupElement object is assumed to be an array of such objects with a
final element containing an null pointer for the name member.

See also:  SetupElement::Lookup

Arguments:
	tag			Legacy tag name to lookup.

Returns:
	SetupElement *
				Pointer to matching SetupElement object;
				NULL if tag was not found.

*/

		const SetupElement *
SetupElement::LookupLegacy (const char * tag)
	const

{
	const SetupElement * list_p;

	for (list_p = this; ; list_p++) {
		if (NOT list_p->Valid ()) {
			list_p = NULL;
			break;
			}
		if (strcmp (tag, list_p->legacy) == 0) break;
		}

	return (list_p);
}


/*
	SetupElement::ListLegacy:

	Lookup a legacy tag name in an array of SetupElement objects.

Description:

	The SetupElement object is assumed to be an array of such objects with a
final element containing an null pointer for the name member.

See also:  SetupElement::List

Arguments:
	stream		Stream on which to output formatted information.

	data_p		Pointer to data structure containing items.

	mode		YES include comments; NO tag lines only.

Returns:
	void

*/

		void
SetupElement::ListLegacy (FILE * stream, const void * const data_p,
	Boolean mode) const

{
	const SetupElement * list_p;
		
	InterfaceBuffer buffer;
	SetupElementFlagBits module_flags = SEF_None;

	for (list_p = this; ; list_p++) {

		if (NOT list_p->Valid ()) break;

		if (list_p->flags.all & SEF_NoDisplay) continue;
		
		if (mode) {
			if (module_flags != (list_p->flags.all & SEF_Modules)) {
				fprintf (stream, "\n");
				module_flags = list_p->flags.all & SEF_Modules;
				}
			}

		buffer.Reset ();
		//Check to exclude fingerprint related commands - F1, F2, F3, F4, F5
		const char * tag_p = list_p->legacy;
		if(strstr(tag_p, "F1") != NULL || strstr(tag_p, "F2") != NULL || strstr(tag_p, "F3") != NULL 
			|| strstr(tag_p, "F4") != NULL || strstr(tag_p, "F5") != NULL 
			|| strstr(tag_p, "CT") != NULL || strstr(tag_p, "DF") != NULL )
		{ 
			//exclude these tags. 
		//	printf("\n Exclude the tag %s", tag_p);
			continue;
		}
		//Check End
		fprintf (stream, "%s=", list_p->legacy);
		list_p->FormatData (buffer, data_p);
		buffer.OutputString (stream);
		fprintf (stream, "\n");

		}

	return;
}


