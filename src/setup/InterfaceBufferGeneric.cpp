/*
	InterfaceBuffer:  [Class]

	The InterfaceBuffer module provides functions to get and put generic data
types from a buffer.

	Copyright (c) 2000-2002 - Alcom Communications
	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   18 Aug 2004
	  Based on:   portions of MsgGen*.c originally 27 Nov 2000
	 $Revision: 1.2 $
	   $Author: deepali $
	  $Modtime$
	     $Date: 2006/06/23 23:29:14 $

Description:

	This module provides functions to convert a basic set of generic data
types to and from string format in a data buffer.

*/


/* Compilation options for InterfaceBuffer. */

/* Include files for InterfaceBuffer. */
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "System.hpp"
#include "Interface.hpp"

/* Constant parameters for InterfaceBuffer. */

/* Type and structure definitions for InterfaceBuffer. */

/* Internal static data for InterfaceBuffer. */

/* Function prototypes for InterfaceBuffer. */

/* External global data defined by InterfaceBuffer. */
const InterfaceFormatBoolean InterfaceBooleanLabel10 = {"0", "1"};
const InterfaceFormatBoolean InterfaceBooleanLabelYN = {"N", "Y"};
const InterfaceFormatBoolean InterfaceBooleanLabelTF = {"F", "T"};
const InterfaceFormatBoolean InterfaceBooleanLabelYesNo = {"No", "Yes"};
const InterfaceFormatBoolean InterfaceBooleanLabelTrueFalse = {"False", "True"};
const InterfaceFormatBoolean * InterfaceBooleanLabels [] = {
	&InterfaceBooleanLabel10,
	&InterfaceBooleanLabelYN,
	&InterfaceBooleanLabelTF,
	&InterfaceBooleanLabelYesNo,
	&InterfaceBooleanLabelTrueFalse,
	NULL
};
const InterfaceFormatBoolean * InterfaceBooleanChars [] = {
	&InterfaceBooleanLabelYN,
	&InterfaceBooleanLabelTF,
	&InterfaceBooleanLabel10,
	NULL
};


/*
	InterfaceBuffer::AddPad

	Add padding character to specified width.

Description:

	If current buffer data is less than width add pad characters to that
width.

Arguments:
	width		Desired width of data in buffer.

	pad			Character to use for padding; default is space.
	
Returns:
	void

*/

		void
InterfaceBuffer::AddPad (size_t width, char pad)

{
   // printf("AddPad = %c \n",pad);
	for (; size < width; ) Buffer::AddData ((Byte *)&pad, 1);
	data_p = (char *)data + size;

	return;
}


/*
	InterfaceBuffer::StringGet:

	Get a string from the buffer.

Description:

	Locate a quoted or unquoted string in the buffer.

Notes:

	If the first character of the data field is a double quote character ('"')
then the string returned will consist of everything from the character
following the first double quote up to and including the character just
before the next double quote.

	If the string is not quoted and width is 0 then everything up to the next
field separator or end of the data string is considered to be part of the
string.  If a width is specified an unquoted string will consist of width
characters or to the end of the data whichever is less.

	Upon return the buffer data_p always points past the field parsed; any
field separator following the string is considered part of the field and
the return pointer will be past it.

See also:  InterfaceBuffer::StringPut

Arguments:
	string_p	Pointer to string data.
	
	string_size	Length of storage pointed to by string_p including
				terminating null character.

	width		Maximum number of data bytes to examine;
				0 to examine rest of data buffer.

	separator	Field separator to look for if no width is specified
				and an unquoted string is present.

Returns:
	Code		SC_OKAY if a valid value field was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
InterfaceBuffer::StringGet (char * string_p, size_t string_size, size_t width,
	char separator)

{
	Code code = SC_INTERFACE_BAD_DATA;
	char * orig_dp = data_p;
	const char * str_p = data_p;
	const char * end_p;

	for (;;) {
		size_t suffix = 0;

		/* Terminate target string just in case. */
		*string_p = '\0';

		/* Check for quoted string. */
		if (*str_p == '"') {
			str_p += 1;
			// Search for matching, trailing quote.
			end_p = strchr (str_p, '"');
			// No terminating quote found.
			if (end_p == NULL) break;
			suffix++;
			if (*(end_p+1) == separator) suffix++;
			}

		/* If no width specified scan for field separator or end of string. */
		else if (width == 0) {
			end_p = strchr (str_p, separator);
			if (end_p == NULL) end_p = str_p + strlen (str_p);
			else suffix++;
			}

		/* Fixed field uses rest of data up to width characters. */
		else {
			end_p = str_p + strlen (str_p);
			if ((size_t)(end_p - str_p) > width) end_p = str_p + width;
			}

		/* Move string to target string. */
		size_t length = (size_t)(end_p - str_p);
		if((length == string_size) && (length == 20))
		{
			//do not decrease length - this is fix for prompt string with len = 20 (max)
		}
		else
		{
			if (string_size != 0 AND length > string_size - 1) length = string_size - 1;
		}
		
		strncat (string_p, str_p, length);

		/* Update buffer data pointer and clear status code. */
		
		data_p = (char *)end_p + suffix;
		code = SC_OKAY;
		break;
		}

	if (code != SC_OKAY) data_p = orig_dp;
	return (code);
}


/*
	InterfaceBuffer::StringPut

	Get a string into the buffer.

Description:

	Copy a string into the buffer with or without quotes as indicated by the
mode parameter.


See also:  InterfaceBuffer::StringGet

Arguments:
	string_p	Pointer to null terminated string to add to buffer.

	mode		InterfaceStringMode enumeration value indicating whether the
				string should be quoted or not.

	width		Number of data bytes to output pad with trailing spaces
				to reach width if needed; does not include quotes if requested;
				0 to use length of supplied string.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
InterfaceBuffer::StringPut (const char * string_p, InterfaceStringMode mode,
	size_t width)

{
	//printf("StringPut = %s \n",string_p);
	size_t start_size = size;

	if (mode == InterfaceStringQuote) AddString ("\"");

	if (width != 0 AND strlen (string_p) > width) {
		AddData ((Byte *)string_p, width);
		AddString ("");
		}
	else AddString (string_p);

	/* Pad field to requested width with spaces. */
	if (width != 0) {
		if (mode == InterfaceStringQuote) ++width; // One quote already added.
		AddPad (start_size + width);
		}

	if (mode == InterfaceStringQuote) AddString ("\"");

	return (size - start_size);
}


/*
	InterfaceBuffer::CharGet

	Get a single character from the buffer.

Description:

	Get a optionally validated character in the buffer.

Notes:

	If no character is found or it is not in the validation string return a
null character ('\0').

See also:  InterfaceBuffer::CharPut

Arguments:
	value		Reference to character value to be set.
	
	valid_chars	String of valid characters to accept;
				NULL if any character will be accepted.
	
Returns:
	Code		SC_OKAY if a valid value field was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
InterfaceBuffer::CharGet (char & value, const char * valid_chars)

{
	Code code = SC_INTERFACE_BAD_DATA;
	char * orig_dp = data_p;

	for (;;) {

		if (NOT DataLeft ()) {
			code = SC_INTERFACE_NO_DATA;
			break;
		}

		if (valid_chars != NULL) {
			if (strchr (valid_chars, *data_p) == NULL) break;
			}

		value = *data_p++;

		code = SC_OKAY;
		break;
		}

	if (code != SC_OKAY) data_p = orig_dp;
	return (code);
}


/*
	InterfaceBuffer::ByteGet

	Get a single byte from the buffer.

Description:

	Get an unsigned byte of data from the buffer.

Notes:

	If no byte is found return a 0 and bad data status code.

See also:  InterfaceBuffer::CharPut

Arguments:
	byte		Reference to byte value to be set.
	
Returns:
	Code		SC_OKAY if a valid value field was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
InterfaceBuffer::ByteGet (Byte & byte)

{
	Code code = SC_INTERFACE_NO_DATA;
	char * orig_dp = data_p;

	for (;;) {

		if (NOT DataLeft ()) break;

		byte = *data_p++;

		code = SC_OKAY;
		break;
		}

	if (code != SC_OKAY) data_p = orig_dp;
	return (code);
}


/*
	InterfaceBuffer::BooleanGet:

	Get a Boolean value from the data buffer.

Description:

	Accept one of several alternative values signifying a Boolean value
including "YES:NO", "Y:N", "TRUE:FALSE", "T:F" and "1:0".  A pointer to an
array of pointers to InterfaceFormatBoolean label pairs can be supplied
via to format pointer; if not supplied the default list
InterfaceBooleanLabels is used.

Notes:

	Comparisons are all case insensitive.

See also:  InterfaceBuffer::BooleanPut

Arguments:
	value		Reference to Boolean value to set based on buffer data.
	
	format_p	Pointer to an array of InterfaceFormatBoolean template
				data pointers containing Boolean value names correspondence.

	width		Maximum number of data bytes to examine;
				0 to examine rest of data buffer.

Returns:
	Code		SC_OKAY if a valid value field was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
InterfaceBuffer::BooleanGet (Boolean & value,
	const InterfaceFormatBoolean ** format_p, size_t width)

{
	Code code = SC_INTERFACE_BAD_DATA;
	char * orig_dp = data_p;
	const InterfaceFormatBoolean ** fpp = InterfaceBooleanLabels;
	const InterfaceFormatBoolean * fp = NULL;
	int state = 0;

	/* Use custom state labels if none supplied. */
	if (format_p != NULL) fpp = format_p;
	fp = *fpp;

	/* No data so stop now. */
	if (*data_p == '\0') ;

	else for (; *fpp != NULL; ++fpp) {
		fp = *fpp;

		for (state = 0; state <= 1; ++state) {
			size_t length = strlen (fp->label[state]);
			if (length == 0) continue;
			else if (width != 0) {
				if (width == 1) {
					if (tolower (*data_p) == tolower (*fp->label[state])) {
						value = state ? YES : NO;
						code = SC_OKAY;
						break;
						}
					}
				else if (width == length AND
					(_strincmp (fp->label[state], data_p, width) == 0))
					{
					value = state ? YES : NO;
					code = SC_OKAY;
					break;
					}
				}
			else if (_stricmp (fp->label[state], data_p) == 0) {
				value = state ? YES : NO;
				code = SC_OKAY;
				break;
				}
			}
		if (code == SC_OKAY) break;

		}

	if (code == SC_OKAY AND fp != NULL) data_p += strlen (fp->label[state]);

	if (code != SC_OKAY) data_p = orig_dp;
	return (code);
}


/*
	InterfaceBuffer::BooleanPut:

	Place one of two string values in the buffer based on the Boolean's state.

Description:

	Use the first InterfaceFormatBoolean label pair to display the state of
a Boolean value.

Notes:

	The character strings in the first format data pair are used to
set the message field; no uppercase or lowercase conversion is performed.

See also:  InterfaceBuffer::BooleanGet

Arguments:
	value		Boolean value to be placed in data

	format_p	Pointer to an array of InterfaceFormatBoolean template
				data pointers containing Boolean value names correspondence.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
InterfaceBuffer::BooleanPut (Boolean value,
	const InterfaceFormatBoolean ** format_p)

{
	//printf("BooleanPut = %d \n",value);
	size_t length = 0;

	// Make sure state is 1 or 0 only.
	Boolean state = value ? 1 : 0;
	const InterfaceFormatBoolean * choice_p = *InterfaceBooleanLabels;
	if (format_p != NULL) choice_p = *format_p;
	length = AddString (choice_p->label[state]);

	return (length);
}


/*
	InterfaceBuffer::IndexGet:

	Parse index data value from buffer.

Description:

	Get a single byte from the buffer and adjust it by subtracting a base
value and verifying its range.

Arguments:
	index		Reference to character index value.

	format_p	Pointer to InterfaceFormatIndex format data.

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
InterfaceBuffer::IndexGet (SysIndex & index,
	const InterfaceFormatIndex * format_p)

{
	Code code = SC_INTERFACE_BAD_DATA;
	char * orig_dp = data_p;

	for (;;) {
		/* Use default range and base if not supplied. */
		if (format_p == NULL) format_p = &InterfaceFormatIndexDefault;

		Byte byte = 0;
		code = ByteGet (byte);
		if (code != SC_OKAY) break;

		SysIndex new_index = byte;
		if (new_index >= format_p->base) new_index -= format_p->base;
		else new_index = 0;

		code = SC_INTERFACE_VALUE_RANGE;
		if (new_index < format_p->range.min) break;
		if (new_index > format_p->range.max) break;

		index = new_index;
		code = SC_OKAY;
		break;
	}

	if (code != SC_OKAY) data_p = orig_dp;
	return (code);
}


/*
	InterfaceBuffer::IndexPut:

	Place an index value in the buffer.

Description:

	Place a single byte index value in the buffer by limiting its value to the
specified range then adding a base value.

Arguments:
	index		Character index value value.

	format_p	Pointer to InterfaceFormatIndex format data.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
InterfaceBuffer::IndexPut (SysIndex index,
	const InterfaceFormatIndex * format_p)

{
    //printf("IndexPut = %d \n",index);
	size_t length = 0;

	/* Use default range and base if not supplied. */
	if (format_p == NULL) format_p = &InterfaceFormatIndexDefault;

	/* Limit value to range. */
	if (index < format_p->range.min) index = format_p->range.min;
	if (index > format_p->range.max) index = format_p->range.max;

	length += BytePut (index + format_p->base);

	return (length);
}


/*
	InterfaceBuffer::IntegerGet:

	Get an integer value from the data buffer.

Description:


See also:  InterfaceBuffer::IntegerPut

Arguments:
	type		Integer data type.

	value_p		Pointer to integer value to set based on buffer data.
	
	mm_p		Pointer to range data; NULL for no range checking.

	width		Maximum number of data bytes to examine;
				0 to examine rest of data buffer.

Returns:
	Code		SC_OKAY if a valid value field was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
InterfaceBuffer::IntegerGet (Type_t type, void * value_p,
	const InterfaceMinMax * mm_p, size_t width)

{
	Code code = SC_INTERFACE_BAD_DATA;
	char * orig_dp = data_p;
	char * end_p;
	char * str_p;
	char buffer [width+1];

	if (width != 0) {
		if (DataLeft () < width) width = DataLeft ();
		buffer[0] = '\0';
		strncat (buffer, data_p, width);
		data_p += width;
		str_p = buffer;
		}
	else str_p = data_p;

	switch (type)
		{

	case TYPE_Bit8:
	case TYPE_Bit16:
		{
		unsigned long value = strtoul (str_p, &end_p, 16);
		switch (type)
			{
		case TYPE_Bit8:
			*(Bit8 *)value_p = (Bit8)value;
			break;
		case TYPE_Bit16:
			*(Bit16 *)value_p = (Bit16)value;
			break;
			}
		}

	case TYPE_SBin7:
	case TYPE_SBin15:
	case TYPE_SBin31:
		{
		SBin31 value = strtol (str_p, &end_p, 10);
		switch (type)
			{
		case TYPE_SBin7:
			*(SBin7 *)value_p = (SBin7)value;
			break;
		case TYPE_SBin15:
			*(SBin15 *)value_p = (SBin15)value;
			break;
		case TYPE_SBin31:
			*(SBin31 *)value_p = (SBin31)value;
			break;
			}
		}

	case TYPE_UBin8:
	case TYPE_UBin16:
	case TYPE_UBin32:
		{
		UBin32 value = strtoul (str_p, &end_p, 10);
		switch (type)
			{
		case TYPE_UBin8:
			*(UBin8 *)value_p = (UBin8)value;
			break;
		case TYPE_UBin16:
			*(UBin16 *)value_p = (UBin16)value;
			break;
		case TYPE_UBin32:
			*(UBin32 *)value_p = (UBin32)value;
			break;
			}
		}

		}

	/* If using fixed width all the data field should have been parsed. */
	if (width != 0) {
		if (*end_p == '\0') code = SC_OKAY;
		}
	else {
		data_p = end_p;
		code = SC_OKAY;
		}

	if (code != SC_OKAY) data_p = orig_dp;
	return (code);
}


/*
	InterfaceBuffer::IntegerPut:

	Convert integer value to string.

Description:

	Convert a signed or unsigned integer value to a string and place in output
buffer.  If a width is specified use leading zeroes to fill the value to
the specified width.

See also:  InterfaceBuffer::IntegerGet

Arguments:
	type		Integer data type.

	value_p		Pointer to integer value to be placed in data buffer.

	mm_p		Pointer to range data; NULL for no range checking.

	width		Width of output converted field;
				0 for variable width based on converted value.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
InterfaceBuffer::IntegerPut (Type_t type, const void * value_p,
	const InterfaceMinMax * mm_p, size_t width)

{
	//printf("IntegerPut \n");
	size_t length = 0;
	char buffer [12] = "";
	char * buf_p = buffer;

	if (width != 0)
		{
        switch (type)
			{
		case TYPE_Bit8:
			length = sprintf (buf_p, "%0*x", width,
				(unsigned int)*(Bit8 *)value_p);
			break;
		case TYPE_Bit16:
			length = sprintf (buf_p, "%0*x", width,
				(unsigned int)*(Bit16 *)value_p);
			break;
		case TYPE_SBin7:
			length = sprintf (buf_p, "%0*d", width, *(SBin7 *)value_p);
			break;
		case TYPE_SBin15:
			length = sprintf (buf_p, "%0*d", width, *(SBin15 *)value_p);
			break;
		case TYPE_SBin31:
			length = sprintf (buf_p, "%0*ld", width, *(SBin31 *)value_p);
			break;
		case TYPE_UBin8:
			length = sprintf (buf_p, "%0*u", width, *(UBin8 *)value_p);
			break;
		case TYPE_UBin16:
			length = sprintf (buf_p, "%0*u", width, *(UBin16 *)value_p);
			break;
		case TYPE_UBin32:
			length = sprintf (buf_p, "%0*lu", width, *(UBin32 *)value_p);
			break;
			}
		}

	else {
		switch (type)
			{
		case TYPE_Bit8:
			length = sprintf (buf_p, "%02x", (unsigned int)*(Bit8 *)value_p);
			break;
		case TYPE_Bit16:
			length = sprintf (buf_p, "%04x", (unsigned int)*(Bit16 *)value_p);
			break;
		case TYPE_SBin7:
			length = sprintf (buf_p, "%d", *(SBin7 *)value_p);
			break;
		case TYPE_SBin15:
			length = sprintf (buf_p, "%d", *(SBin15 *)value_p);
			break;
		case TYPE_SBin31:
			length = sprintf (buf_p, "%ld", *(SBin31 *)value_p);
			break;
		case TYPE_UBin8:
			length = sprintf (buf_p, "%u", *(UBin8 *)value_p);
			break;
		case TYPE_UBin16:
			length = sprintf (buf_p, "%u", *(UBin16 *)value_p);
			break;
		case TYPE_UBin32:
			length = sprintf (buf_p, "%lu", *(UBin32 *)value_p);
			break;
			}
		}

	if (length != 0) AddString (buf_p);

	return (length);
}


