/*
	InterfaceElement::System:  [Class]

	The InterfaceElement::System module provides system specific data item
format and parse.

	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   27 Nov 2000
	 $Revision: 1.10 $
	   $Author: walterm $
	  $Modtime:   19 Apr 2002 14:09:20  $
	     $Date: 2007/02/16 15:31:34 $

Description:

	This module provides system specific parse and format member functions
for manipulating InterfaceElement objects.

*/


/* Compilation options for InterfaceElement::System. */

/* Include files for InterfaceElement::System. */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "System.hpp"
#include "Interface.hpp"
#include "Legacy.hpp"
#include "Fields.h"
#include "InterfaceProfileAssignHolder.hpp"
#include "InterfaceMessageBadgeHolder.hpp"
#include "InterfaceValidateEntryHolder.hpp"
#include "InterfaceAccessControlHolder.hpp"
#include "TAVector.h"



using namespace std;

/* Constant parameters for InterfaceElement::System. */

/* Type and structure definitions for InterfaceElement::System. */
extern LegacySetup SystemSetup;
extern bool fpUnitEnabled;
/* Internal static data for InterfaceElement::System. */

/* Function prototypes for InterfaceElement::System. */
Code		_TimeGet (InterfaceBuffer & buffer, SysTime & time);
size_t	_TimePut (InterfaceBuffer & buffer, SysTime time);
Code _TimeSecondsGet (InterfaceBuffer & buffer, SysTimeSecs & time);
size_t _TimeSecondsPut (InterfaceBuffer & buffer, SysTimeSecs & time);
Code _DateGet (InterfaceBuffer & buffer, SysDate & date);
size_t _DatePut (InterfaceBuffer & buffer, SysDate & date);
static Code		_TimeWindowGet (InterfaceBuffer & buffer,
					SysTimeWindow & window);
static size_t	_TimeWindowPut (InterfaceBuffer & buffer,
					const SysTimeWindow & window);
static Code		_AccessWindowGet (InterfaceBuffer & buffer,
					SysAccessWindowItem & window);
static size_t	_AccessWindowPut (InterfaceBuffer & buffer,
					const SysAccessWindowItem & window);
static Code     _BadgeLenTypeGet (InterfaceBuffer & buffer,
					SysBadgeLengths * const bl_p,
					const InterfaceMinMax * const mm_p);
static size_t   _BadgeLenTypePut (InterfaceBuffer & buffer,
					const SysBadgeLengths * const bl_p,
					const InterfaceMinMax * const mm_p);
static Code     _BadgeMaxOffGet (InterfaceBuffer & buffer,
					SysBadgeLengths * const bl_p,
					const InterfaceMinMax * const mm_p);
static size_t   _BadgeMaxOffPut (InterfaceBuffer & buffer,
					const SysBadgeLengths * const bl_p,
					const InterfaceMinMax * const mm_p);
static Code     _HalfHeaderGet (InterfaceBuffer & buffer,
					char * string_p, size_t string_size);
static Code		_MessageBadgeGet (InterfaceBuffer & buffer,
					SysMessageBadgeItem & message,
					const LegacyInterfaceMessageBadge * format_p,
					SysFieldLength badge_length, Boolean & reset);
static size_t	_MessageBadgePut (InterfaceBuffer & buffer,
					const SysMessageBadgeItem & message,
					const LegacyInterfaceMessageBadge * format_p,
					SysFieldLength badge_length, Boolean reset = 0);
static Code		_FunctionKeyGet (InterfaceBuffer & buffer,
					SysFunctionKeyItem & function,
					const LegacyInterfaceFunctionKey * format_p, SysIndex & index);
static size_t	_FunctionKeyPut (InterfaceBuffer & buffer,
					const SysFunctionKeyItem & function,
					const LegacyInterfaceFunctionKey * format_p);
static Code		_FunctionDetailGet (InterfaceBuffer & buffer,
					SysFunctionDetailItem & function,
					const LegacyInterfaceFunctionDetail * format_p, SysIndex & index);
static Code     _GetFunctionKeyIndex( char key, SysIndex& index );
static size_t	_FunctionDetailPut (InterfaceBuffer & buffer,
					const SysFunctionDetailItem & function,
					const LegacyInterfaceFunctionDetail * format_p);
static Code		_BellScheduleGet (InterfaceBuffer & buffer,
					SysBellScheduleItem & bell,
					const LegacyInterfaceBellSchedule * format_p,
					Boolean & reset);
static size_t	_BellSchedulePut (InterfaceBuffer & buffer,
					const SysBellScheduleItem & bell,
					const LegacyInterfaceBellSchedule * format_p,
					Boolean reset = 0);
static Code		_AccessBadgeGet (InterfaceBuffer & buffer,
					SysAccessBadgeItem & access,
					SysFieldLength badge_length, Boolean & reset);
static size_t	_AccessBadgePut (InterfaceBuffer & buffer,
					const SysAccessBadgeItem & access,
					
					SysFieldLength badge_length, Boolean reset = 0);
static Code		_ProfileMessageGet (InterfaceBuffer & buffer,
					SysProfileMessageItem & message,
			const LegacyInterfaceProfileMessage * format_p, SysIndex & index);
static size_t	_ProfileMessagePut (InterfaceBuffer & buffer,
					const SysProfileMessageItem & message,
			const LegacyInterfaceProfileMessage * format_p, SysIndex & index);
static Code     _ProfileEntryGet (InterfaceBuffer & buffer,
					SysProfileEntryItem & entry,
					const LegacyInterfaceProfileEntry * format_p, SysIndex & eidx, SysIndex & tidx, bool & clear);
static size_t   _ProfileEntryPut (InterfaceBuffer & buffer,
					const SysProfileEntryItem & entry,
					const LegacyInterfaceProfileEntry * format_p, SysIndex & eidx, SysIndex & tidx);
static Code     _ProfileAssignGet (InterfaceBuffer & buffer,
					SysProfileAssignItem & assign,
					const LegacyInterfaceProfileAssign * format_p, 
                    SysFieldLength badge_length, Boolean & flag );
static size_t   _ProfileAssignPut (InterfaceBuffer & buffer,
                    const SysProfileAssignItem & assign,
                    const LegacyInterfaceProfileAssign * format_p, 
                    SysFieldLength badge_length, Boolean reset );  
/*static size_t   _AddSysProfileAssignItem( TAVector<SysProfileAssignItem&> & assignVec_p, 
                        SysProfileAssignItem & assign );*/
                                        
static Code     _ValidateLevelGet (InterfaceBuffer & buffer,
					SysValidateLevelItem * level_l,
					const LegacyInterfaceValidateLevel * format_p);
static size_t   _ValidateLevelPut (InterfaceBuffer & buffer,
					const SysValidateLevelItem * const level_l,
					const LegacyInterfaceValidateLevel * format_p);
static Code     _ValidateEntryGet (InterfaceBuffer & buffer,
					SysValidateEntryItem & entry,
					const LegacyInterfaceValidateEntry * format_p,
					const SysValidateData & validate,
					SysFieldLength badge_length, Boolean & reset);
static size_t   _ValidateEntryPut (InterfaceBuffer & buffer,
					const SysValidateEntryItem & entry,
					const LegacyInterfaceValidateEntry * format_p,
					const SysValidateData & validate,
					SysFieldLength badge_length, Boolean reset = 0);

static Code 	_FingerTemplateGet (InterfaceBuffer & buffer,
					SysFingerTemplateData *employee, 
					const LegacyFingerTemplate * format_p);
					
bool PF_clear = false;

/*
	InterfaceElement::SystemParse:

	Parse an InterfaceElement using the InterfaceElement data provided.

Description:

	Convert the supplied item data to the requested InterfaceElement
format using the data type, value, and format data supplied.

See also:  InterfaceElement::SystemPrint

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	obj_p		Pointer to object or region containing data for element.

Returns:
	Code		System status code of SC_OKAY for successful completion;
				SC_INTERFACE_UNKNOWN_TYPE if item type is not known;
				SC_INTERFACE_BAD_DATA if item data could not be parsed;

*/

		Code
InterfaceElement::SystemParse (InterfaceBuffer & buffer, void * const obj_p)
	const

{
	Code code = SC_INTERFACE_BAD_DATA;
	
	switch (type) {
	
	case SYSTYPE_DelayShort:
	case SYSTYPE_Delay:
		if (*buffer.data_p == '\0')	break;
		{
			void * value_p = value_o.Get (obj_p);
			code = buffer.IntegerGet (type, value_p,
				(const InterfaceMinMax *)format_p);
		}
		break;

	case SYSTYPE_Time:
		{
			SysTime * time_p = (SysTime *)value_o.Get (obj_p);
			code = _TimeGet (buffer, *time_p);
			if (code != SC_OKAY) break;
		}
		break;

	case SYSTYPE_TimeWindow:
		{
			SysTimeWindow * window_p = (SysTimeWindow *)value_o.Get (obj_p);
			code = _TimeWindowGet (buffer, *window_p);
		}
		break;
		
	case SYSTYPE_AccessWindow:
		{
			SysAccessWindowItem * window_p = (SysAccessWindowItem *)value_o.Get (obj_p);
			code = _AccessWindowGet (buffer, *window_p);
		}
		break;

	case SYSTYPE_OnOffAuto:
		code = SC_OKAY;
		break;

	case SYSTYPE_KeyPair:
		{
			SysFunctionKeyPair * kp_p =
				(SysFunctionKeyPair *)value_o.Get (obj_p);
			code = buffer.CharGet (kp_p->setup, (const char *)format_p);
			if(code == SC_INTERFACE_NO_DATA) code = SC_OKAY;
			if (code != SC_OKAY) break;
			code = buffer.CharGet (kp_p->execute, (const char *)format_p);
			if(code == SC_INTERFACE_NO_DATA) code = SC_OKAY;
			if (code != SC_OKAY) break;
		}
		break;
		
	case SYSTYPE_SupervisorPrefix:
		{
			SysSuperPrefix *prefix = (SysSuperPrefix *)value_o.Get(obj_p);
			code = buffer.StringGet(prefix->entry,(format_p == NULL) ?
				0 : *(InterfaceFormatStringLength *)(format_p),0,',');
			if (code != SC_OKAY) break;
			code = buffer.StringGet(prefix->recall,(format_p == NULL) ?
				0 : *(InterfaceFormatStringLength *)(format_p),0,',');
			if (code != SC_OKAY) break;
		}
		break;

	case SYSTYPE_LegacyBadgeLenType:
		code = _BadgeLenTypeGet (buffer,
			(SysBadgeLengths *)value_o.Get (obj_p),
			(const InterfaceMinMax *)format_p);
		break;

	case SYSTYPE_LegacyBadgeMaxOff:
		code = _BadgeMaxOffGet (buffer,
			(SysBadgeLengths *)value_o.Get (obj_p),
			(const InterfaceMinMax *)format_p);
		break;

	case SYSTYPE_LegacyHalfHeader:
		{
			char * string_p = (char *)value_o.Get (obj_p);
			code = _HalfHeaderGet (buffer, string_p, (format_p == NULL) ?
				SysFieldMaxHeaderHalf :
				*(InterfaceFormatStringLength *)(format_p));
		}
		break;

	case SYSTYPE_LegacyMessage:
		{
			SysMessageBadgeItem message;
			Boolean reset;
			if (NOT reference_o.Valid ()) {
				code = SC_INTERFACE_MISSING_REF;
				break;
			}
			SysFieldLength badge_length =
				*(SysFieldLength *)reference_o.Get (obj_p);
			code = _MessageBadgeGet (buffer, message,
					   (const LegacyInterfaceMessageBadge *)format_p,
					   badge_length, reset);
			if (code != SC_OKAY) break; 
//			SysMessageBadgeItem * msg_p =
//				*(SysMessageBadgeItem **)value_o.Get (obj_p);
			InterfaceMessageBadgeHolder * holder_p =
				*(InterfaceMessageBadgeHolder **)value_o.Get (obj_p);
			if (holder_p == NULL) break; 
			
			if( reset == 0 )
			{
			   holder_p->add( message );
   			}
   			else if( reset == 1 )
   			{
               holder_p->clear();
			   holder_p->add( message );
            }
		}
		break;

	case SYSTYPE_LegacyFunctionKey:
		{
			SysFunctionKeyItem function;
			SysIndex index = 0;
			code = _FunctionKeyGet (buffer, function,
					   (const LegacyInterfaceFunctionKey *)format_p, index);
			if (code != SC_OKAY) break;
			SysFunctionKeyItem * func_p =
				*(SysFunctionKeyItem **)value_o.Get (obj_p);
			if (func_p == NULL)	break;
			func_p += index;
			*func_p = function;
		}
		break;

	case SYSTYPE_LegacyFunctionDetail:
		{
			SysFunctionDetailItem function;
			SysIndex index = 0;
			code = _FunctionDetailGet (buffer, function,
					   (const LegacyInterfaceFunctionDetail *)format_p, index);
			if (code != SC_OKAY) break;
			SysFunctionDetailItem * func_p =
				*(SysFunctionDetailItem **)value_o.Get (obj_p);
			if (func_p == NULL)	break;
			func_p += index;
			*func_p = function;
		}
		break;

	case SYSTYPE_LegacyBell:
		{
			SysBellScheduleItem function;
			Boolean reset;
			code = _BellScheduleGet (buffer, function,
					   (const LegacyInterfaceBellSchedule *)format_p, reset);
			if (code != SC_OKAY) break;
			
   
            InterfaceBellScheduleHolder * holder_p =
				*(InterfaceBellScheduleHolder **)value_o.Get (obj_p);
			if (holder_p == NULL) break;
			
			if( reset == 0 )
			{
			   holder_p->add( function );
   			}
   			else if( reset == 1 )
   			{
               holder_p->clear();
			   holder_p->add( function );
            }
		}
		break;

	case SYSTYPE_LegacyAccessBadge:
		{
			SysAccessBadgeItem badge;
			Boolean reset;
			if (NOT reference_o.Valid ()) {
				code = SC_INTERFACE_MISSING_REF;
				break;
			}
			SysFieldLength badge_length =
				*(SysFieldLength *)reference_o.Get (obj_p);
			code = _AccessBadgeGet (buffer, badge, badge_length, reset);
			if (code != SC_OKAY) break;
			InterfaceAccessControlHolder * holder_p =
				*(InterfaceAccessControlHolder **)value_o.Get (obj_p);
			if (holder_p == NULL) break;
			if( reset == 0 )
			{
			   holder_p->add( badge );
   			}
   			else if( reset == 1 )
   			{
               holder_p->clear();
			   holder_p->add( badge );
            }
		}
		break;

	case SYSTYPE_LegacyProfileMessage:
		{
			SysProfileMessageItem message;
			SysIndex index = 0;
			code = _ProfileMessageGet (buffer, message,
					   (const LegacyInterfaceProfileMessage *)format_p, index);
            
			if (code != SC_OKAY) break;
			SysProfileMessageItem * msg_p =
				*(SysProfileMessageItem **)value_o.Get (obj_p);
			if (msg_p == NULL)	break;
			
			msg_p += index;
			*msg_p = message;
		}
		break;

	case SYSTYPE_LegacyProfileDefault:
		{
			SysProfileMessageItem message;
			SysIndex index = 0;
			code = _ProfileMessageGet (buffer, message,
					   (const LegacyInterfaceProfileMessage *)format_p, index);
            
			if (code != SC_OKAY) break;
			SysProfileMessageItem * msg_p =
				*(SysProfileMessageItem **)value_o.Get (obj_p);
			
			if (msg_p == NULL)	break;
			
			msg_p += index;			
			*msg_p = message;
		}
		break;

	case SYSTYPE_LegacyProfileEntry:
		{
			SysProfileEntryItem entry;
			SysIndex eidx, tidx = 0;
			bool clear = false;
			code = _ProfileEntryGet (buffer, entry,
					   (const LegacyInterfaceProfileEntry *)format_p, eidx, tidx, clear);
			if (code != SC_OKAY) break;
			
			SysProfileEntryItem * entry_p =
				*(SysProfileEntryItem **)value_o.Get (obj_p);
			if (entry_p == NULL) break;
			
			/**
			 * Index 0 means that we need to clear the entries
			 */
		    if( clear ) 
		   	{
		    	PF_clear = true;
		        //TODO Clear all the entries
		        for( SysIndex i = 0; i < SysProfileEntryMax; i++ )
		        {
		            entry_p = NULL;
		            entry_p++;
		        }
                break;
		    }
			
			entry_p += eidx;
			/**
			 * Check if an entry exits in this location. If so we need to add to the array
			 */
			if(entry_p == NULL)
			{
				entry_p->index = entry.index;
				for(int i=0; i<SysProfileTimeMax; i++)
				{
					entry_p->time[i] = entry.time[i];	
				}
			}
			else
			{
				entry_p->index = entry.index;
				(*entry_p).time[tidx] = entry.time[tidx];
			}
		}
		break;

	case SYSTYPE_LegacyProfileAssign:
		{
			SysProfileAssignItem assign;
			Boolean flag = 0;
			
			SysBadgeLengths lengths =
				*(SysBadgeLengths *)reference_o.Get (obj_p);
			SysFieldLength badge_length = lengths.valid;
		
			code = _ProfileAssignGet (buffer, assign,
					   (const LegacyInterfaceProfileAssign *)format_p, badge_length, flag );
			if (code != SC_OKAY) break;
			
			//If flag is 1 then reset otherwise if it is 0, then add
			InterfaceProfileAssignHolder *assignHld_p = *(InterfaceProfileAssignHolder **)value_o.Get(obj_p);

			if (assignHld_p == NULL) break;

			if( flag == 0 )
			{
			   assignHld_p->add( assign );
   			}
   			else if( flag == 1 )
   			{
               assignHld_p->clear();
			   assignHld_p->add( assign );
            }   
		}
		break;

	case SYSTYPE_LegacyValidLevels:
	{
			SysValidateLevelItem * level_p =
				(SysValidateLevelItem *)value_o.Get (obj_p);
			code = _ValidateLevelGet (buffer, level_p,
					   (const LegacyInterfaceValidateLevel *)format_p);
			//printf("level_width in parss = %d\n",level_p[1].width);
	        //printf("level_width in parss = %d\n",level_p[2].width);
	        //printf("level_width in parss = %d\n",level_p[3].width);
	        //printf("level_width in parss = %d\n",level_p[3].width);
			if (code != SC_OKAY) break;
	}
	break;

	case SYSTYPE_LegacyValidEntry:
		{
			SysValidateEntryItem entry;

			const SysValidateData * valid_p = (const SysValidateData *)value_o.Get (obj_p);
			if (NOT reference_o.Valid ()) 
			{
				code = SC_INTERFACE_MISSING_REF;
				break;
			}
			SysFieldLength badge_length = *(SysFieldLength *)reference_o.Get (obj_p);
			Boolean reset;
		
			code = _ValidateEntryGet (buffer, entry,
					   (const LegacyInterfaceValidateEntry *)format_p,
					   *valid_p, badge_length, reset);
			if (code != SC_OKAY) break; 
			InterfaceValidateEntryHolder * holder_p = 
               (InterfaceValidateEntryHolder*) valid_p->entry;
            //printf("reset = %d\n",reset);
			if (holder_p == NULL) break;
			
			if( reset == 0 )
			{
				holder_p->add( entry );
			}
			else if( reset == 1 )
			{
				//holder_p->clear();
				holder_p->add( entry );
            }
		}
		break;

	case SYSTYPE_ClearTemplates:
		{
			if( ! fpUnitEnabled)
			{
				//DO not parse. Just ignore
				code = SC_OKAY;
				break;
			}
			
			char * value_p = (char *)value_o.Get (obj_p);
			code = buffer.CharGet (*value_p, (const char *)format_p);
			if(code == SC_INTERFACE_NO_DATA) code = SC_OKAY;
			if(code != SC_OKAY) break;
			
			if(*value_p == '1'){
			  //clear all templates 
			}
			break;
		}
		
	case SYSTYPE_DeleteFinger:
		{
			if( ! fpUnitEnabled)
			{
				//DO not parse. Just ignore
				code = SC_OKAY;
				break;
			}
			
			char * value_p = (char *)value_o.Get (obj_p);
			*value_p = '\0';
			
			if (*buffer.data_p == '\0')
			{
				// Empty field yields null string so just exit.
				code = SC_OKAY;
				break;
			}
			
			code = buffer.StringGet (value_p, (format_p == NULL) ?
				0 : *(InterfaceFormatStringLength *)(format_p));
						
			if(code != SC_OKAY)  break; 
			
			const char * badge = value_p;
			//Delete all templates for this badge
			if(badge != NULL)
			{
			
			}
			break;
		}
	case SYSTYPE_FingerTemplateLine1:
		{
			if( ! fpUnitEnabled)
			{
				//DO not parse. Just ignore
				code = SC_OKAY;
				break;
			}
			
			SysFingerTemplateData *finger = (SysFingerTemplateData *)value_o.Get(obj_p);
			
			code = _FingerTemplateGet(buffer, finger, 
							(const LegacyFingerTemplate *)format_p);
			
			if (code != SC_OKAY) break;
			
			break;
		}
	case SYSTYPE_FingerTemplateLine2:
		{
			if( ! fpUnitEnabled)
			{
				//DO not parse. Just ignore
				code = SC_OKAY;
				break;
			}
			
			char * value_p = (char *)value_o.Get (obj_p);
			*value_p = '\0';
			
			if (*buffer.data_p == '\0')
			{
				// Empty field yields null string so just exit.
				code = SC_OKAY;
				break;
			}
			code = buffer.StringGet (value_p, (format_p == NULL) ?
				0 : *(InterfaceFormatStringLength *)(format_p));
						
			if(code != SC_OKAY) { printf("\n code != SC_OKAY"); break; }
			
			//Now read the fptemplate structure, get the data and insert it into database.
			char findex = SystemSetup.fptemplate.finger.fingerindex;
			
			char rt[2];
			rt[0] = SystemSetup.fptemplate.finger.threshhold;
			rt[1] = '\0';
			
			char pl[2];
			pl[0] = SystemSetup.fptemplate.finger.privilegelevel;
			pl[1] = '\0';
			
			const char* bdg = SystemSetup.fptemplate.finger.badge;
			
			char* tdata = SystemSetup.fptemplate.templatestring;
			char templatedata[600];
			strcpy(templatedata, tdata);
			int l1 = strlen(templatedata);
			templatedata[l1] = '\0';
			
			int fingerindex = atoi(&findex);
			int securitylevel = atoi(&rt[0]);
			int privilegelevel = atoi(&pl[0]);
			
			
	

			/*
			  char line[1024];
			  sprintf(line,"'%s',%d,%d,%d,'%s'", bdg, fingerindex, privilegelevel, securitylevel, templatedata);
			
			  FILE * fp = fopen("FP.CLK", "a");
			  if( !fp )
			  {
			  fprintf(stderr, "Couldn't open the FP.CLK file\n");
			  //return;
			  }
			  else
			  {
			  fputs(line, fp);
			  fputs("\n", fp);
			  fclose(fp);
			  }
			*/

			break;
		}
	case SYSTYPE_FingerTemplateLine5:
		{
		/*	printf("\n inside SYSTYPE_FingerTemplateLine5");
			char * value_p = (char *)value_o.Get (obj_p);
			*value_p = '\0';
			
			if (*buffer.data_p == '\0')
			{
				// Empty field yields null string so just exit.
				code = SC_OKAY;
				break;
			}
			printf("\n getting template part 4");
			code = buffer.StringGet (value_p, (format_p == NULL) ?
				0 : *(InterfaceFormatStringLength *)(format_p));
						
			if(code != SC_OKAY) { printf("\n code != SC_OKAY"); break; }
			
			//Now read the fptemplate structure, get the data and insert it into database.
			char findex = SystemSetup.fptemplate.finger.fingerindex;
			printf("\n findex = %c", findex);
			
			char rt[2];
			rt[0] = SystemSetup.fptemplate.finger.threshhold;
			printf("\n rt = %c", rt[0]);  
			rt[1] = '\0';
			
			char pl[2];
			pl[0] = SystemSetup.fptemplate.finger.privilegelevel;
			printf("\n pl = %c", pl[0]);  
			pl[1] = '\0';
			
			const char* bdg = SystemSetup.fptemplate.finger.badge;
			printf("\n bdg = %s", bdg);  
			
			char* TPart1 = SystemSetup.fptemplate.templatepart1;
			if(TPart1 != NULL)
			{
				printf("\n len of TPart1 = %d", strlen(TPart1));
				printf("\n TPart 1 = %s", TPart1);
			}
			
			char* TPart2 = SystemSetup.fptemplate.templatepart2;
			if(TPart2 != NULL)
			{
				printf("\n len of TPart2 = %d", strlen(TPart2));
				printf("\n TPart 2 = %s", TPart2);
			}
			
			char* TPart3 = SystemSetup.fptemplate.templatepart3;
			if(TPart3 != NULL)
			{
				printf("\n len of TPart3 = %d", strlen(TPart3));
				printf("\n TPart 3 = %s", TPart3);
			}
			
			char* TPart4 = SystemSetup.fptemplate.templatepart4;
			if(TPart4 != NULL)
			{
				printf("\n len of TPart4 = %d", strlen(TPart4));
				printf("\n TPart 4 = %s", TPart4);
			}
			
			char templatedata[512];
			strcpy(templatedata, TPart1);
			strcat(templatedata, TPart2);
			strcat(templatedata, TPart3);
			strcat(templatedata, TPart4);
			
			printf("\n templatedata = %s", templatedata);
			printf("\n\n length of templatedata = %d", strlen(templatedata));
			
			//char seclevel = '4';
			//char seclevel = sindex;
			int fingerindex = atoi(&findex);
			int securitylevel = atoi(&rt[0]);
			int privilegelevel = atoi(&pl[0]);
			
			printf("\n fingerindex = %d", fingerindex);
			printf("\n securitylevel = %d", securitylevel);
			printf("\n privilegelevel = %d", privilegelevel);
			
			printf("\n insert the record into the table");
			//get the internal user id for this index from the db
			bool replace_template = false;
			FptemplateTable fptemplatetbl;
			
			int InternalUserID = fptemplatetbl.FindInternalUserId(bdg, fingerindex);
			if(InternalUserID == -1)
			{
				//get the next internal user id
				printf("\n user id not found for external id = %s and fingerindex = %d", bdg, fingerindex);
				InternalUserID = fptemplatetbl.getNextInternalUserID();
			}
			else
				replace_template = true;
			printf("\n InternalUserID = %d", InternalUserID);
				
			FptemplateTable_Row fptemplatetable_row(InternalUserID, bdg, fingerindex, securitylevel, templatedata);
		
			if(replace_template)
			{
				//delete the existing row first
				int ret = fptemplatetbl.DeleteRow_key(InternalUserID);
				if(ret != SQLITE_OK)
				{
					printf("\n record could not be deleted");
					//Code = ?
					break;
				}
				else
					printf("\n record deleted");
			}
				
			//Now insert the record
			if ( fptemplatetbl.InsertRow(fptemplatetable_row) != SQLITE_OK)
			{
				printf("\n row could not be inserted = %s", sqlite3_errmsg(db));
				//Code = ?
				break;
			}
			else
				printf("\n record inserted into the table");
			
			//Update the security level for all templates for this badge in the database. 
			if ( fptemplatetbl.UpdateSecurityLevel(bdg, securitylevel) != SQLITE_OK)
			{
				printf("\n row could not be updated = %s", sqlite3_errmsg(db));
				//Code = ?
				break;
			}
			else
				printf("\n records updated into the table");
				
			//Update the privilege level for all templates for this badge in the database
			if ( fptemplatetbl.UpdatePrivilegeLevel(bdg, privilegelevel) != SQLITE_OK)
			{
				printf("\n row could not be updated = %s", sqlite3_errmsg(db));
				//Code = ?
				break;
			}
			else
				printf("\n records updated into the table");
							
			break;*/
		}
	case SYSTYPE_LegacyFingerTable:
		code = SC_OKAY;
		break;

	case SYSTYPE_Daylight_savings_adjustment: {
	  LegacySetup ::d_s_a_t* p = ( LegacySetup ::d_s_a_t* ) value_o .Get ( obj_p );
	  buffer .data_p = *p = ( char* ) buffer .data_p ;
	  code = *p ? SC_OKAY : SC_FAILURE ;
	  break ;
	}

	default:
		code = SC_INTERFACE_UNKNOWN_TYPE;
		break;
	}

	/* External parse routines may return SC_FAILURE for parsing failures
		so translate to InterfaceElement subsystem status code. */
	if (code == SC_FAILURE)	code = SC_INTERFACE_BAD_DATA;

	return (code);
}


/*
	InterfaceElement::SystemFormat:

	Format an InterfaceElement using the InterfaceElement data provided.

Description:

	Convert the supplied data element to the requested InterfaceElement
format using the data type, value, and format data supplied.

See also:  InterfaceElement::SystemPrint

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	obj_p		Pointer to object or region containing data for element.

Returns:
	Code		System status code of SC_OKAY for successful completion;
				SC_INTERFACE_UNKNOWN_TYPE if item type is not known;

*/

		Code
InterfaceElement::SystemFormat (InterfaceBuffer & buffer,
	const void * const obj_p) const

{
	Code code = SC_OKAY;

	switch (type) {
	
	case SYSTYPE_DelayShort:
	case SYSTYPE_Delay:
		{
			const void * value_p = value_o.Get (obj_p);
			buffer.IntegerPut (type, value_p,
				(const InterfaceMinMax *)format_p);
		}
		break;

	case SYSTYPE_Time:
		_TimePut (buffer, *(SysTime *)value_o.Get (obj_p));
		break;

	case SYSTYPE_TimeWindow:
		{
			SysTimeWindow * window_p = (SysTimeWindow *)value_o.Get (obj_p);
			_TimeWindowPut (buffer, *window_p);
		}
		break;
		
	case SYSTYPE_AccessWindow:
		{
			SysAccessWindowItem * window_p = (SysAccessWindowItem *)value_o.Get (obj_p);
			_AccessWindowPut (buffer, *window_p);
		}
		break;

	case SYSTYPE_OnOffAuto:
		break;

	case SYSTYPE_KeyPair:
		{
			SysFunctionKeyPair * value_p =
				(SysFunctionKeyPair *)value_o.Get (obj_p);
			buffer.CharPut (value_p->setup);
			buffer.CharPut (value_p->execute);
		}
		break;
	case SYSTYPE_SupervisorPrefix:
		{
			SysSuperPrefix * value_p = (SysSuperPrefix *)value_o.Get(obj_p);
			buffer.StringPut(value_p->entry, InterfaceStringNoQuote, 0); 
			if(value_p->recall != NULL){
			buffer.StringPut(",", InterfaceStringNoQuote, 0); 
			buffer.StringPut(value_p->recall,InterfaceStringNoQuote, 0);  }
		}
		break;
	case SYSTYPE_LegacyBadgeLenType:
		_BadgeLenTypePut (buffer,
			(const SysBadgeLengths *)value_o.Get (obj_p),
			(const InterfaceMinMax *)format_p);
		break;

	case SYSTYPE_LegacyBadgeMaxOff:
		_BadgeMaxOffPut (buffer,
			(const SysBadgeLengths *)value_o.Get (obj_p),
			(const InterfaceMinMax *)format_p);
		break;

	case SYSTYPE_LegacyHalfHeader:
		break;

	case SYSTYPE_LegacyMessage:
		{
			
            InterfaceMessageBadgeHolder *holder_p =
				*(InterfaceMessageBadgeHolder **)value_o.Get (obj_p);
			if (holder_p == NULL) break;

	        if (holder_p->size() == 0 ) buffer.AddString ("(null)");
			else
			{
				SysFieldLength badge_length =
				*(SysFieldLength *)reference_o.Get (obj_p);
           
      	        int indx;
      	        for (indx = 0; indx < holder_p->size(); indx++)
      	        {
                    const SysMessageBadgeItem& message = holder_p-> Get(indx);
            
                    if( indx != 0 )
                        buffer.AddString("\nEM=");
                    _MessageBadgePut (buffer, message,
                     (const LegacyInterfaceMessageBadge *)format_p, badge_length);
                }
			}
        }
		break;

		case SYSTYPE_LegacyFunctionKey:
		{
			const SysFunctionKeyItem * func_p =
				*(SysFunctionKeyItem **)value_o.Get (obj_p);
			
			for( SysIndex i = 0; i < SysFunctionKeyMax ;i++ )
			{
			   if (func_p == NULL)	buffer.AddString ("(null)");
			   else if( func_p->Valid() )
			   {
                  if( i != 0 )
                     buffer.AddString("\nFL=");
                  _FunctionKeyPut (buffer, *func_p,
					(const LegacyInterfaceFunctionKey *)format_p);
			   }
			   func_p++;   
			}
		}
		break;

	case SYSTYPE_LegacyFunctionDetail:
		{
			const SysFunctionDetailItem * func_p =
				*(SysFunctionDetailItem **)value_o.Get (obj_p);
			
			for( SysIndex i = 0; i < SysFunctionDetailMax ;i++ )
			{
 			   if (func_p == NULL)	buffer.AddString ("(null)");
 			   else if( func_p->Valid() )
 			   {
    			   if( i != 0 )
                     buffer.AddString("\nFP=");
                    
                   _FunctionDetailPut	(buffer, *func_p,
						(const LegacyInterfaceFunctionDetail *)format_p);
 			   }
 			   func_p++; 
			}
		}
		break;

	case SYSTYPE_LegacyBell:
		{
            InterfaceBellScheduleHolder *holder_p =
				*(InterfaceBellScheduleHolder **)value_o.Get (obj_p);
			if (holder_p == NULL) break;

	        if (holder_p->size() == 0 ) buffer.AddString ("(null)");
			else
			{
      	        int indx;
      	        for (indx = 0; indx < holder_p->size(); indx++)
      	        {
                    const SysBellScheduleItem& function = holder_p-> Get(indx);
            
                    if( indx != 0 )
                        buffer.AddString("\nBS=");
                    _BellSchedulePut (buffer, function,
                     (const LegacyInterfaceBellSchedule *)format_p);
                }
			}
		}
		break;

	case SYSTYPE_LegacyAccessBadge:
		{
		    InterfaceAccessControlHolder *holder_p =
				*(InterfaceAccessControlHolder **)value_o.Get (obj_p);
			if (holder_p == NULL) break;
			if (holder_p->size() == 0 ) buffer.AddString ("(null)");
			else
			{
			    if (NOT reference_o.Valid ()) {
				 code = SC_INTERFACE_MISSING_REF;
				 break;
			    }
			    SysFieldLength badge_length =
				*(SysFieldLength *)reference_o.Get (obj_p);
                int indx;
      	        for (indx = 0; indx < holder_p->size(); indx++)
      	        {
      	            const SysAccessBadgeItem& badge = holder_p-> Get(indx);
      	            if( indx != 0 )
                        buffer.AddString("\nAC=");
       			    _AccessBadgePut (buffer, badge, badge_length);  
      	        }
            }
		}
		break;

	case SYSTYPE_LegacyProfileMessage:
		{
			const SysProfileMessageItem * msg_p =
				*(SysProfileMessageItem **)value_o.Get (obj_p);
				
			for( SysIndex i = 0; i < SysProfileMessageMax ;i++ )
			{
			   if (msg_p == NULL) buffer.AddString ("(null)");
			   else if (msg_p->Valid ()) 
               {
                  if( i != 0 )
                     buffer.AddString("\nPM=");
				  _ProfileMessagePut (buffer, *msg_p,
					(const LegacyInterfaceProfileMessage *)format_p, i);
				}
			   msg_p++;
			}
		}
		break;

	case SYSTYPE_LegacyProfileDefault:
		{
			const SysProfileMessageItem * msg_p =
				*(SysProfileMessageItem **)value_o.Get (obj_p);
				
			for( SysIndex i = 0; i < SysProfileMessageDefaults ;i++ )
			{
			   if (msg_p == NULL) buffer.AddString ("(null)");
			   else if (msg_p->Valid ()) 
               {
                  if( i != 0 )
                     buffer.AddString("\nPX=");
				  _ProfileMessagePut (buffer, *msg_p,
					(const LegacyInterfaceProfileMessage *)format_p, i);
				}
	            msg_p++;
			}
		}
		break;

	case SYSTYPE_LegacyProfileEntry:
		{
			bool clearAdded = false;
			const SysProfileEntryItem * entry_p =
				*(SysProfileEntryItem **)value_o.Get (obj_p);
           
			for( SysIndex i = 0; i < SysProfileEntryMax ;i++ )
            {
                if (entry_p == NULL) buffer.AddString ("(null)");
                else if(PF_clear && !clearAdded) { 
	               buffer.AddString("0"); clearAdded = true; }
			    else 
                {
                    for( SysIndex j = 0; j < SysProfileTimeMax ;j++ )
                    {
                        //Check if ProfileEntry is valid
                        if ( entry_p->time[j].message == 0 && 
                                 entry_p->time[j].window.start == 0 &&
                                 entry_p->time[j].window.stop == 0 )
                        {
                           continue;
                        }
                        if( !( i == 0 && j== 0 ))
                            buffer.AddString("\nPF=");
                        _ProfileEntryPut (buffer, *entry_p,
					        (const LegacyInterfaceProfileEntry *)format_p, i, j);
                    }
			    }
			    entry_p++;
            }
		}
		break;

	case SYSTYPE_LegacyProfileAssign:
		{		
            InterfaceProfileAssignHolder *assignHld_p =
				*(InterfaceProfileAssignHolder **)value_o.Get (obj_p);
			if (assignHld_p == NULL) break;

	        if (assignHld_p->size() == 0 ) buffer.AddString ("(null)");
			else
			{
      			SysBadgeLengths lengths = *(SysBadgeLengths *)reference_o.Get (obj_p);
      			SysFieldLength badge_length = lengths.valid;
           
      	        int indx;
      	        for (indx = 0; indx < assignHld_p->size(); indx++)
      	        {
                    const SysProfileAssignItem& assign = assignHld_p-> Get(indx);
            
                    if( indx != 0 )
                        buffer.AddString("\nPE=");
                    _ProfileAssignPut (buffer, assign,
                         (const LegacyInterfaceProfileAssign *)format_p, 
                         badge_length, false );
                }
			}
		}
		break;


	case SYSTYPE_LegacyValidLevels:
		{
			SysValidateLevelItem * level_p =
				(SysValidateLevelItem *)value_o.Get (obj_p);
			code = _ValidateLevelPut (buffer, level_p,
					   (const LegacyInterfaceValidateLevel *)format_p);
			if (code != SC_OKAY) break;
		}
		break;

	case SYSTYPE_LegacyValidEntry:
		{
			const SysValidateData * valid_p =
				(const SysValidateData *)value_o.Get (obj_p);
			InterfaceValidateEntryHolder * holder_p = 
               (InterfaceValidateEntryHolder*)valid_p->entry;

			if (holder_p == NULL) break;
			if (NOT reference_o.Valid ()) {
				code = SC_INTERFACE_MISSING_REF;
				break;
			}
			SysFieldLength badge_length =
				*(SysFieldLength *)reference_o.Get (obj_p);
	        if (holder_p->size() == 0 ) buffer.AddString ("(null)");
			else
			{
      	        int indx;
      	        for (indx = 0; indx < holder_p->size(); indx++)
      	        {
                    const SysValidateEntryItem& entry = holder_p-> Get(indx);
            
                    if( indx != 0 )
                        buffer.AddString("\nLE=");
                        
                    _ValidateEntryPut (buffer, entry,
					(const LegacyInterfaceValidateEntry *)format_p,
					*valid_p, badge_length);
                }
			}
		}
		break;
		
	case SYSTYPE_ClearTemplates:
		{
			buffer.CharPut (*(char *)value_o.Get (obj_p));
			break;
		}
		
	case SYSTYPE_DeleteFinger:
		{
			buffer.StringPut ((char *)value_o.Get (obj_p), InterfaceStringNoQuote );
			break;
		} 

	case SYSTYPE_LegacyFingerTable:
		break;

	case SYSTYPE_Daylight_savings_adjustment: {
	  LegacySetup ::d_s_a_t* p = ( LegacySetup ::d_s_a_t* ) value_o .Get ( obj_p );
	  char c [ 100 ];
	  p ->as_string ( c , sizeof ( c ));
	  buffer .StringPut ( c );
	}
	  break ;

	default:
		code = SC_INTERFACE_UNKNOWN_TYPE;
		break;
	}

	return (code);
}


/*
	_TimeGet:

	Parse time data from buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	time		Reference to time of day data.

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
_TimeGet (InterfaceBuffer & buffer, SysTime & time)

{
	Code code = SC_INTERFACE_BAD_DATA;
	char * orig_dp = buffer.data_p;

	for (;;) {
		if (buffer.DataLeft () < 4)	break;

		SysTime hours, minutes;
		code = buffer.IntegerGet (TYPE_UBin16, &hours, NULL, 2);
		if (code != SC_OKAY) break;
		code = buffer.IntegerGet (TYPE_UBin16, &minutes, NULL, 2);
		if (code != SC_OKAY) break;

		code = SC_INTERFACE_BAD_DATA;
		if (hours >= HoursPerDay) break;
		if (minutes >= MinutesPerHour) break;

		time = hours * MinutesPerHour + minutes;

		code = SC_OKAY;
		break;
	}

	if (code != SC_OKAY) buffer.data_p = orig_dp;
	return (code);
}


/*
	_TimePut:

	Convert time of day in minutes into hours and minutes for output.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	time		Time of day data value.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
_TimePut (InterfaceBuffer & buffer, SysTime time)

{
	size_t length = 0;
	SysTime minutes, hours;

	hours = time / MinutesPerHour;
	hours = hours % HoursPerDay;
	minutes = time % MinutesPerHour;

	length += buffer.IntegerPut (TYPE_UBin16, &hours, NULL, 2);
	length += buffer.IntegerPut (TYPE_UBin16, &minutes, NULL, 2);

	return (length);
}

/*
	_TimeSecondsGet:

	Parse time data from buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	time		Reference to time of day data.

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
_TimeSecondsGet (InterfaceBuffer & buffer, SysTimeSecs & time)

{
	Code code = SC_INTERFACE_BAD_DATA;
	char * orig_dp = buffer.data_p;

	for (;;) {
		if (buffer.DataLeft () < 6)	break;

		SysTime hours, minutes, seconds;
		code = buffer.IntegerGet (TYPE_UBin16, &hours, NULL, 2);
		if (code != SC_OKAY) break;
		code = buffer.IntegerGet (TYPE_UBin16, &minutes, NULL, 2);
		if (code != SC_OKAY) break;
		code = buffer.IntegerGet (TYPE_UBin16, &seconds, NULL, 2);
		if (code != SC_OKAY) break;

		code = SC_INTERFACE_BAD_DATA;
		if (hours >= HoursPerDay) break;
		if (minutes >= MinutesPerHour) break;
		if (seconds >= SecondsPerMinute) break;

		time = ( hours * MinutesPerHour * SecondsPerMinute ) + ( minutes * SecondsPerMinute ) + seconds;

		code = SC_OKAY;
		break;
	}

	if (code != SC_OKAY) buffer.data_p = orig_dp;
	return (code);
}


/*
	_TimeSecondsPut:

	Convert time of day in seconds into hours, minutes and seconds for output.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	time		Time of day data value.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
_TimeSecondsPut (InterfaceBuffer & buffer, SysTimeSecs time)

{
	size_t length = 0;
	SysTime minutes, hours, seconds;

	hours = ( time / SecondsPerMinute ) / MinutesPerHour ;
	hours = hours % HoursPerDay;
	minutes = ( time / SecondsPerMinute ) % MinutesPerHour;
	seconds = time % SecondsPerMinute;

	length += buffer.IntegerPut (TYPE_UBin16, &hours, NULL, 2);
	length += buffer.IntegerPut (TYPE_UBin16, &minutes, NULL, 2);
	length += buffer.IntegerPut (TYPE_UBin16, &seconds, NULL, 2);

	return (length);
}

/*
	_DateGet:

	Parse date data from buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	date		Reference to date of day data.

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
_DateGet (InterfaceBuffer & buffer, SysDate & time)

{
	Code code = SC_INTERFACE_BAD_DATA;
	char * orig_dp = buffer.data_p;

	for (;;) {
		if (buffer.DataLeft () < 8)	break;
		SysTime year, month, date;
        code = buffer.IntegerGet (TYPE_UBin16, &year, NULL, 4);
        if (code != SC_OKAY) break;
        code = buffer.IntegerGet (TYPE_UBin16, &month, NULL, 2);
        if (code != SC_OKAY) break;
        code = buffer.IntegerGet (TYPE_UBin16, &date, NULL, 2);
        if (code != SC_OKAY) break;
        time.year = year;
        time.month = month;
        time.date = date;
	    code = SC_OKAY;
		break;
	}

	if (code != SC_OKAY) buffer.data_p = orig_dp;
	return (code);
}

/*
	_DatePut:

	Convert to date for output

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	date		Date value

Returns:
	size_t		Number of bytes added to buffer data.

*/
		size_t
_DatePut (InterfaceBuffer & buffer, SysDate & time)

{
	size_t length = 0;
	SysTime year = time.year;
	SysTime month = time.month;
	SysTime date = time.date;

	length += buffer.IntegerPut (TYPE_UBin16, &year, NULL, 4);
	length += buffer.IntegerPut (TYPE_UBin16, &month, NULL, 2);
	length += buffer.IntegerPut (TYPE_UBin16, &date, NULL, 2);

	return (length);
}


/*
	_TimeWindowGet:

	Parse a pair of time of day values from the data buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	window		Reference to time of day window data.

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
_TimeWindowGet (InterfaceBuffer & buffer, SysTimeWindow & window)

{
	Code code;

	for (;;) {

		code = _TimeGet (buffer, window.start);
		if (code != SC_OKAY) break;

		code = _TimeGet (buffer, window.stop);
		if (code != SC_OKAY) break;

		break;
	}

	return (code);
}

/*
	_AccessWindowGet:

	Parse Access Window

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	window		Reference to access window data

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
_AccessWindowGet (InterfaceBuffer & buffer, SysAccessWindowItem & window)

{
	Code code;

	for (;;) 
	{

		code = _TimeGet (buffer, window.time.start);
		if (code != SC_OKAY) break;

		code = _TimeGet (buffer, window.time.stop);
		if (code != SC_OKAY) break;
		
		const InterfaceFormatDays days(InterfaceBooleanChars, 8);
		code = buffer.DaysGet(window.days, &days, 7);
		if (code != SC_OKAY)
		{
			window.days = 0x7F; // All Days
			code = SC_OKAY;
		}
		break;
	}
	return (code);
}


/*
	_TimeWindowPut

	Format a pair of time of day values into the data buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	window		Reference to time of day window data.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
_TimeWindowPut (InterfaceBuffer & buffer, const SysTimeWindow & window)

{
	size_t length = 0;

	length += _TimePut (buffer, window.start);
	length += _TimePut (buffer, window.stop);

	return (length);
}

/*
	_AccessWindowPut

	Format Access Window

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	window		Reference to access window data.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
_AccessWindowPut (InterfaceBuffer & buffer, const SysAccessWindowItem & window)

{
	size_t length = 0;

	length += _TimePut (buffer, window.time.start);
	length += _TimePut (buffer, window.time.stop);
	
	const InterfaceFormatDays days(InterfaceBooleanChars, 8);
	length += buffer.DaysPut (window.days, &days, 7);
	
	return (length);
}


/*
	_BadgeLenTypeGet

	Parse the badge length and optional AcroComm format data type.

See also:  _BadgeLenTypePut

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	bl_p		Pointer to SysBadgeLengths object.

	mm_p		Min/max range for lengths.

Returns:
	Code		System status code of SC_OKAY for successful completion;
				SC_INTERFACE_UNKNOWN_TYPE if item type is not known;
				SC_INTERFACE_BAD_DATA if item data could not be parsed;

*/

		Code
_BadgeLenTypeGet (InterfaceBuffer & buffer,
	SysBadgeLengths * const bl_p, const InterfaceMinMax * const mm_p)

{
	Code code = SC_INTERFACE_BAD_DATA;

	for (;;) {

		code = buffer.IntegerGet (TYPE_UBin8, &bl_p->valid, mm_p);
		if (code != SC_OKAY) break;

		/* If no data left then standard badge length command is done. */
		if (NOT buffer.DataLeft ()) break;
		char sep;
		code = buffer.CharGet (sep);
		if (code != SC_OKAY) break;
		if (sep != InterfaceFieldSeparator) {
			code = SC_INTERFACE_BAD_DATA;
			break;
		}

		/* If type supplied set default type. */
		if (NOT buffer.DataLeft ()) {
			bl_p->type = SysInputTypeDefault;
			break;
		}
		code = buffer.EnumCharGet (bl_p->type, SysInputTypeChars);
		if (code != SC_OKAY) break;

		break;
	}

	return (code);
}


/*
	_BadgeLenTypePut

	Format the badge length and optional AcroComm format data type.

See also:  _BadgeLenTypeGet

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	bl_p		Pointer to SysBadgeLengths object.

	mm_p		Min/max range for lengths.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
_BadgeLenTypePut (InterfaceBuffer & buffer,
	const SysBadgeLengths * const bl_p, const InterfaceMinMax * const mm_p)

{
	size_t length = 0;

	length += buffer.IntegerPut (TYPE_UBin8, &bl_p->valid, mm_p);
	if (bl_p->type != SysInputTypeDefault) {
		length += buffer.CharPut (InterfaceFieldSeparator);
		length += buffer.EnumCharPut (bl_p->type, SysInputTypeChars);
	}

	return (length);
}


/*
	_BadgeMaxOffGet

	Parse the maximum badge length and optional AcroComm format badge offset.

See also:  _BadgeMaxOffPut

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	bl_p		Pointer to SysBadgeLengths object.

	mm_p		Min/max range for lengths.

Returns:
	Code		System status code of SC_OKAY for successful completion;
				SC_INTERFACE_UNKNOWN_TYPE if item type is not known;
				SC_INTERFACE_BAD_DATA if item data could not be parsed;

*/

		Code
_BadgeMaxOffGet (InterfaceBuffer & buffer,
	SysBadgeLengths * const bl_p, const InterfaceMinMax * const mm_p)

{
	Code code = SC_INTERFACE_BAD_DATA;
	
	for (;;) {

		code = buffer.IntegerGet (TYPE_UBin8, &bl_p->maximum, mm_p);
		if (code != SC_OKAY) {
			break;
		}

		if (NOT buffer.DataLeft ()) break;
		char sep;
		code = buffer.CharGet (sep);
		if (code != SC_OKAY) break; 
		if (sep != InterfaceFieldSeparator) {
			code = SC_INTERFACE_BAD_DATA;
			break;
		}

		code = buffer.IntegerGet (TYPE_UBin8, &bl_p->offset, mm_p);
		if (code != SC_OKAY) {
			break;
		}

		break;
	}
	return (code);
}


/*
	_BadgeMaxOffPut

	Format the maximum badge length and optional AcroComm format badge offset.

See also:  _BadgeMaxOffGet

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	bl_p		Pointer to SysBadgeLengths object.

	mm_p		Min/max range for lengths.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
_BadgeMaxOffPut (InterfaceBuffer & buffer,
	const SysBadgeLengths * const bl_p, const InterfaceMinMax * const mm_p)

{
	size_t length = 0;
	
	length += buffer.IntegerPut (TYPE_UBin8, &bl_p->maximum, mm_p);
	return (length);
}


/*
	_HalfHeaderGet

	Parse the badge length and optional AcroComm format data type.

See also:  _BadgeLenTypePut

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	bl_p		Pointer to SysBadgeLengths object.

	mm_p		Min/max range for lengths.

Returns:
	Code		System status code of SC_OKAY for successful completion;
				SC_INTERFACE_UNKNOWN_TYPE if item type is not known;
				SC_INTERFACE_BAD_DATA if item data could not be parsed;

*/

		Code
_HalfHeaderGet (InterfaceBuffer & buffer, char * string_p, size_t string_size)

{
	Code code = SC_INTERFACE_BAD_DATA;

	for (;;) {

		char * half_p = string_p + SysFieldMaxHeaderHalf;
		code = buffer.StringGet (half_p, string_size);
		if (code != SC_OKAY) break;

		size_t length = strlen (string_p);
		if (length < SysFieldMaxHeaderHalf) {
			// Fill space between base string and second half with spaces.
			memset (string_p+length, ' ', SysFieldMaxHeaderHalf-length);
		}

		break;
	}

	return (code);
}


/*
	_MessageBadgeGet:

	Parse message data from buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	message		Reference to message data.

	format_p	Pointer to format data.

	badge_length
				Current badge length.
				
	reset		Reference to reset flag:
				Set to 1 if reset record read; 0 otherwise.				

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
_MessageBadgeGet (InterfaceBuffer & buffer, SysMessageBadgeItem & message,
	const LegacyInterfaceMessageBadge * format_p, SysFieldLength badge_length,
	Boolean & reset)

{
	Code code = SC_INTERFACE_BAD_DATA;

	for (;;) {

		if (format_p == NULL) {
			code = SC_INTERFACE_MISSING_FORMAT;
			break;
		}
		
		code = buffer.BooleanGet (reset, format_p->reset, 1);
		if (code != SC_OKAY)  break;
		
		code = buffer.BooleanGet (message.once, format_p->boolean, 1);
		if (code != SC_OKAY) break;
		code = buffer.BooleanGet (message.initial_swipe, format_p->boolean, 1);
		if (code != SC_OKAY) break;

		if (message.initial_swipe) {
			// Ignore function key field if doing initial swipe.
			buffer.data_p++;
			message.key = ' ';
		}
		else {
			code = buffer.CharGet (message.key, (const char *)format_p->keys);
			if (code != SC_OKAY) break;
		}

		code = buffer.StringGet (message.badge, sizeof message.badge,
				   badge_length);
		if (code != SC_OKAY) break;

		code = buffer.StringGet (message.text, sizeof message.text,
				   LegacyFieldWidthMessage);
		if (code != SC_OKAY) break;

		break;
	}

	return (code);
}


/*
	_MessageBadgePut:

	Place message data in buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	message		Reference to message data.

	format_p	Pointer to format data.

	reset		1 if this is the first record and previous records
					should be discarded;
				0 otherwise.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
_MessageBadgePut (InterfaceBuffer & buffer, const SysMessageBadgeItem & message,
	const LegacyInterfaceMessageBadge * format_p, SysFieldLength badge_length,
	Boolean reset)

{
	size_t length = 0;

	length += buffer.BooleanPut (reset);

	length += buffer.BooleanPut (message.once, format_p->boolean);
	length += buffer.BooleanPut (message.initial_swipe, format_p->boolean);
	length += buffer.AddChar (message.key);
	length += buffer.StringPut (message.badge, InterfaceStringNoQuote,
				  badge_length);
	length += buffer.StringPut (message.text, InterfaceStringNoQuote,
				  LegacyFieldWidthMessage);

	return (length);
}


/*
	_FunctionKeyGet:

	Parse function key data from buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	function	Reference to function key data.

	format_p	Pointer to format data.

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
_FunctionKeyGet (InterfaceBuffer & buffer, SysFunctionKeyItem & function,
	const LegacyInterfaceFunctionKey * format_p, SysIndex & index)

{
	Code code = SC_INTERFACE_BAD_DATA;

	for (;;) {
		Boolean flag;

		if (format_p == NULL) {
			code = SC_INTERFACE_MISSING_FORMAT;
			break;
		}

		code = buffer.CharGet (function.key, (const char *)format_p->keys);
		if (code != SC_OKAY) break;
		code = buffer.EnumCharGet (function.levels, SysFunctionLevelChars);
		if (code != SC_OKAY) break;

		code = _GetFunctionKeyIndex( function.key, index );
		if( code != SC_OKAY ) break;
		
		code = buffer.BooleanGet (flag, format_p->boolean, 1);
		if (code != SC_OKAY) break;
		if (flag) function.flags |= SysFunctionFlagSupervisor;
		else function.flags	&= ~SysFunctionFlagSupervisor;

		code = buffer.BooleanGet (flag, format_p->boolean, 1);
		if (code != SC_OKAY) break;
		if (flag) function.flags |= SysFunctionFlagProfile;
		else function.flags	&= ~SysFunctionFlagProfile;

		code = buffer.BooleanGet (flag, format_p->boolean, 1);
		if (code != SC_OKAY) break;
		if (flag) function.flags |= SysFunctionFlagAccess;
		else function.flags	&= ~SysFunctionFlagAccess;

		code = buffer.BooleanGet (flag, format_p->boolean, 1);
		if (code != SC_OKAY) break;
		if (flag) function.flags |= SysFunctionFlagMessage;
		else function.flags	&= ~SysFunctionFlagMessage;

		/* Older terminals do not use print flag. */
		if (NOT buffer.DataLeft ()) break;

		code = buffer.BooleanGet (flag, format_p->boolean, 1);
		if (code != SC_OKAY) break;
		if (flag) function.flags |= SysFunctionFlagMessage;
		else function.flags	&= ~SysFunctionFlagPrint;

		break;
	}

	return (code);
}

Code _GetFunctionKeyIndex( char key, SysIndex& index )
{
    if( key >= '0' && key <= '9')
    {
        index = key - '0';
    }
    else
    {
            switch( key )
            {
                case '*': 
                    index = 10;
                    break;
                    
                case '#': 
                    index = 11;
                    break;
                    
                case '?': 
                    index = 12;
                    break;
                default:
                    return SC_INTERFACE_BAD_DATA;                                    
		    }
    }
	return SC_OKAY;
}


/*
	_FunctionKeyPut:

	Place function key data in buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	function	Reference to function key data.

	format_p	Pointer to format data.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
_FunctionKeyPut (InterfaceBuffer & buffer, const SysFunctionKeyItem & function,
	const LegacyInterfaceFunctionKey * format_p)

{
	size_t length = 0;

	length += buffer.AddChar (function.key);
	length += buffer.EnumCharPut (function.levels, SysFunctionLevelChars);
	length += buffer.BooleanPut (function.flags & SysFunctionFlagSupervisor,
		format_p->boolean);
	length += buffer.BooleanPut (function.flags & SysFunctionFlagProfile,
		format_p->boolean);
	length += buffer.BooleanPut (function.flags & SysFunctionFlagAccess,
		format_p->boolean);
//	length += buffer.BooleanPut (function.flags & SysFunctionFlagPrint,
//		format_p->boolean);
	length += buffer.BooleanPut (function.flags & SysFunctionFlagMessage,
		format_p->boolean);

	return (length);
}


/*
	_FunctionDetailGet:

	Parse function detail data from buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	function	Reference to function detail data.

	format_p	Pointer to format data.

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
_FunctionDetailGet (InterfaceBuffer & buffer, SysFunctionDetailItem & function,
	const LegacyInterfaceFunctionDetail * format_p, SysIndex & index)

{
	Code code = SC_INTERFACE_BAD_DATA;

	for (;;) {

		if (format_p == NULL) {
			code = SC_INTERFACE_MISSING_FORMAT;
			break;
		}

		code = buffer.CharGet (function.key, (const char *)format_p->keys);
		if (code != SC_OKAY) break;
		
		code = _GetFunctionKeyIndex( function.key, index );
		if( code != SC_OKAY ) break;
		
		code = buffer.EnumCharGet (function.level, SysFunctionLevelChars);
		if (code != SC_OKAY) break;
		
		index = index * SysFunctionKeyLevelsMax + function.level - 1;

		code = buffer.StringGet (function.message, sizeof function.message,
				   LegacyFieldWidthMessage);
		if (code != SC_OKAY) break;

		code = buffer.FlagsBit8Get (function.source, format_p->source, 3);
		if (code != SC_OKAY) break;

		code = buffer.EnumCharGet (function.type, SysInputTypeNums);
		if (code != SC_OKAY) break;
		code = buffer.EnumCharGet (function.decimal, SysInputDecimals);
		if (code != SC_OKAY) break;
		code = buffer.EnumCharGet (function.length.max, SysInputChars);
		if (code != SC_OKAY) break;
		code = buffer.EnumCharGet (function.length.min, SysInputChars);
		if (code != SC_OKAY) break;
		code = buffer.EnumCharGet (function.validation.flags,
				   SysInputValidChars);
		if (code != SC_OKAY) break;
		code = buffer.EnumCharGet (function.validation.table,
				   SysInputTableChars);
		if (code != SC_OKAY) break;

		break;
	}

	return (code);
}


/*
	_FunctionDetailPut:

	Place function detail data in buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	function	Reference to function detail data.

	format_p	Pointer to format data.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
_FunctionDetailPut (InterfaceBuffer & buffer,
	const SysFunctionDetailItem & function,
	const LegacyInterfaceFunctionDetail * format_p)

{
	size_t length = 0;

	length += buffer.AddChar (function.key);
	length += buffer.EnumCharPut (function.level, SysFunctionLevelChars);

	length += buffer.StringPut (function.message, InterfaceStringNoQuote,
		LegacyFieldWidthMessage);

	length += buffer.FlagsBit8Put (function.source, format_p->source, 3);
	length += buffer.EnumCharPut (function.type, SysInputTypeNums);
	length += buffer.EnumCharPut (function.decimal, SysInputDecimals);
	length += buffer.EnumCharPut (function.length.max, SysInputChars);
	length += buffer.EnumCharPut (function.length.min, SysInputChars);
	length += buffer.EnumCharPut (function.validation.flags, SysInputValidChars);
	length += buffer.EnumCharPut (function.validation.table, SysInputTableChars);

	return (length);
}


/*
	_BellScheduleGet:

	Parse bell schedule data from buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	bell		Reference to bell schedule data.

	format_p	Pointer to format data.

	reset		1 if this is the first record and previous records
					should be discarded;
				0 otherwise.

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
_BellScheduleGet (InterfaceBuffer & buffer, SysBellScheduleItem & bell,
	const LegacyInterfaceBellSchedule * format_p, Boolean & reset)

{
	Code code = SC_INTERFACE_BAD_DATA;

	for (;;) {

		if (format_p == NULL) {
			code = SC_INTERFACE_MISSING_FORMAT;
			break;
		}

		code = buffer.BooleanGet (reset, format_p->reset, 1);
		if (code != SC_OKAY) break;

		code = buffer.DaysGet (bell.days, &format_p->days, 7);
		if (code != SC_OKAY) break;
		code = _TimeGet (buffer, bell.time);
		if (code != SC_OKAY) break;
		code = buffer.IntegerGet (SYSTYPE_DelayShort, &bell.duration, NULL, 2);
		if (code != SC_OKAY) break;

		break;
	}

	return (code);
}


/*
	_BellSchedulePut:

	Place bell schedule data in buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	bell		Reference to bell schedule data.

	format_p	Pointer to format data.

	reset		1 if this is the first record and previous records
					should be discarded;
				0 otherwise.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
_BellSchedulePut (InterfaceBuffer & buffer, const SysBellScheduleItem & bell,
	const LegacyInterfaceBellSchedule * format_p, Boolean reset)

{
	size_t length = 0;
	
	//length += buffer.BooleanPut (reset);
	//get the original reset flag. Do not put default 0.
	buffer.BooleanGet (reset, format_p->reset, 1);
	length += buffer.BooleanPut (reset);
	
	length += buffer.DaysPut (bell.days, &format_p->days, 7);
	length += _TimePut (buffer, bell.time);
	length += buffer.IntegerPut (SYSTYPE_DelayShort, &bell.duration, NULL, 2);

	return (length);
}


/*
	_AccessBadgeGet:

	Parse access badge data from buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	access		Reference to access badge data.

	badge_length
				Current badge length.
				
	reset		Reference to reset flag:
					Set to 1 if reset record read; 0 otherwise.				

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
_AccessBadgeGet (InterfaceBuffer & buffer, SysAccessBadgeItem & access,
	SysFieldLength badge_length, Boolean & reset)

{
	Code code = SC_INTERFACE_BAD_DATA;

	for (;;) {

		code = buffer.BooleanGet (reset, NULL, 1);
		if (code != SC_OKAY) break;

		code = buffer.StringGet (access.badge, sizeof access.badge,
				   badge_length);
		if (code != SC_OKAY) break;

		break;
	}

	return (code);
}


/*
	_AccessBadgePut:

	Place access badge data in buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	access		Reference to access badge data.

	badge_length
				Current badge length.
				
	reset		1 if this is the first record and previous records
					should be discarded;
				0 otherwise.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
_AccessBadgePut (InterfaceBuffer & buffer, const SysAccessBadgeItem & access,
	SysFieldLength badge_length, Boolean reset)

{
	size_t length = 0;

	length += buffer.BooleanPut (reset);

	length += buffer.StringPut (access.badge, InterfaceStringNoQuote,
				  badge_length);

	return (length);
}


/*
	_ProfileMessageGet:

	Parse profile message data from buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	message		Reference to profile message data.

	format_p	Pointer to format data.

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
_ProfileMessageGet (InterfaceBuffer & buffer, SysProfileMessageItem & message,
	const LegacyInterfaceProfileMessage * format_p, SysIndex & midx)

{
	Code code = SC_INTERFACE_BAD_DATA;

	for (;;) {
		//SysIndex midx = 0;
		Boolean flag;

		if (format_p == NULL) {
			code = SC_INTERFACE_MISSING_FORMAT;
			break;
		}

		code = buffer.IndexGet (midx, format_p->message);
		if (code != SC_OKAY) break;

		code = buffer.StringGet (message.text, sizeof message.text,
				   LegacyFieldWidthMessage);
		if (code != SC_OKAY) break;

		message.flags = SysProfileFlagDefault;

		code = buffer.BooleanGet (flag, format_p->boolean, 1);
		if (code != SC_OKAY) break;
		if (flag) message.flags |= SysProfileFlagAllow;
		else message.flags	&= ~SysProfileFlagAllow;

		code = buffer.BooleanGet (flag, format_p->boolean, 1);
		if (code != SC_OKAY) break;
		if (flag) message.flags |= SysProfileFlagSupervisor;
		else message.flags	&= ~SysProfileFlagSupervisor;

		SysIndex tone = 0;
		code = buffer.EnumCharGet (tone, format_p->tone);
		if (code != SC_OKAY) break;
		message.flags = field_set (SysProfileFlags, SysProfileFlagToneMask,
								   tone, message.flags);

		code = buffer.IntegerGet (SYSTYPE_DelayShort, &message.timeout);
		if (code != SC_OKAY) break;
		
		message.flags |= SysProfileFlagValid;
		break;
	}

	return (code);
}


/*
	_ProfileMessagePut:

	Place profile message data in buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	message		Reference to profile message data.

	format_p	Pointer to format data.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
_ProfileMessagePut (InterfaceBuffer & buffer,
	const SysProfileMessageItem & message,
	const LegacyInterfaceProfileMessage * format_p, SysIndex & midx)

{
	size_t length = 0;

	length += buffer.IndexPut (midx, format_p->message);

	length += buffer.StringPut (message.text, InterfaceStringNoQuote,
				  LegacyFieldWidthMessage);

	length += buffer.BooleanPut (message.flags & SysProfileFlagAllow,
		format_p->boolean);
	length += buffer.BooleanPut (message.flags & SysProfileFlagSupervisor,
		format_p->boolean);
	length += buffer.EnumCharPut (
		field_get (SysProfileFlags, SysProfileFlagToneMask, message.flags),
		format_p->tone);
	length += buffer.IntegerPut (SYSTYPE_DelayShort, &message.timeout, NULL, 2);

	return (length);
}


/*
	_ProfileEntryGet:

	Parse profile entry data from buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	entry		Reference to profile entry data.

	format_p	Pointer to format data.

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
_ProfileEntryGet (InterfaceBuffer & buffer, SysProfileEntryItem & entry,
	const LegacyInterfaceProfileEntry * format_p, SysIndex & eidx, SysIndex & tidx, bool & clear)

{
	Code code = SC_INTERFACE_BAD_DATA;

	for (;;) {
		//SysIndex eidx = 0;
		//SysIndex tidx = 0;
		
		if (format_p == NULL) {
			code = SC_INTERFACE_MISSING_FORMAT;
			break;
		}

		code = buffer.IndexGet (eidx, format_p->entry);
		if (code != SC_OKAY) break;
		entry.index = eidx;
		
		code = buffer.IndexGet (tidx, format_p->time);
		
		// Check if the entry number is 0. If so we need to clear all the entries
		if( eidx == 0 && code == SC_INTERFACE_NO_DATA )
		{
		    clear = true;
		    code = SC_OKAY;
		    return (code);
		}
		
		if (code != SC_OKAY) break;

		code = buffer.IndexGet (entry.time[tidx].message,
                              format_p->message);
		if (code != SC_OKAY) break;
		code = _TimeWindowGet (buffer, entry.time[tidx].window);
		if (code != SC_OKAY) break;

		break;
	}

	return (code);
}


/*
	_ProfileEntryPut:

	Place profile entry data in buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	entry		Reference to profile entry data.

	format_p	Pointer to format data.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
_ProfileEntryPut (InterfaceBuffer & buffer,
	const SysProfileEntryItem & entry,
	const LegacyInterfaceProfileEntry * format_p,
    SysIndex & eidx, SysIndex & tidx)

{
	size_t length = 0;
	//SysIndex eidx = 0;
	//SysIndex tidx = 0;

	length += buffer.IndexPut (eidx, format_p->entry);
	length += buffer.IndexPut (tidx, format_p->time);

	length += buffer.IndexPut (entry.time[tidx].message,
								  format_p->message);
	length += _TimeWindowPut (buffer, entry.time[tidx].window);

	return (length);
}





/*
	_ProfileAssignGet:

	Parse profile assign data from buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	assign		Reference to profile assign data.

	format_p	Pointer to format data.

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/
		Code
_ProfileAssignGet (InterfaceBuffer & buffer, SysProfileAssignItem & assign,
	const LegacyInterfaceProfileAssign * format_p, SysFieldLength badge_length, 
    Boolean & reset)

{
	Code code = SC_INTERFACE_BAD_DATA;

	for (;;) {

		if (format_p == NULL) {
			code = SC_INTERFACE_MISSING_FORMAT;
			break;
		}

		code = buffer.BooleanGet (reset, format_p->reset, 1);
		if (code != SC_OKAY) break;

		code = buffer.StringGet (assign.badge, sizeof assign.badge,
				   badge_length);
		if (code != SC_OKAY) break;

		for( int i = 0; i < SysDayOfWeekMax-1; i++ ) {
		  code = buffer.IndexGet( assign.entry[i], format_p->entry );
		  if (code != SC_OKAY) break;		
		}		
		break;
	}

	return (code);
}

		size_t
_ProfileAssignPut (InterfaceBuffer & buffer,const SysProfileAssignItem & assign,
	const LegacyInterfaceProfileAssign * format_p, SysFieldLength badge_length,
    Boolean reset )
{
	size_t length = 0;

	length += buffer.BooleanPut (reset, format_p->reset );

	length += buffer.StringPut (assign.badge, InterfaceStringNoQuote, 
         badge_length);
				   
	for( int i = 0; i < SysDayOfWeekMax-1; i++ ) {
	    length += buffer.IndexPut( assign.entry[i], format_p->entry );
	}		
	return (length);
}

/**
 * Inserts an Assignment Item into the Assignment vector
 *
size_t _AddSysProfileAssignItem( vector<SysProfileAssignItem&> assignVec_p, 
                        SysProfileAssignItem assign )
{
}*/

/*
	_ValidateLevelGet:

	Parse validate level data from buffer.

Notes:

	Unlike most program setup command parse functions the validate levels
command always sets both the width and include_badge values for all
levels.  Width defaults to 0 and include_badge defaults NO for all values
that are not explicitly set by the message.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	level_p		Pointer to validate level data array.

	format_p	Pointer to format data.

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
_ValidateLevelGet (InterfaceBuffer & buffer, SysValidateLevelItem * level_l,
	const LegacyInterfaceValidateLevel * format_p)

{
	Code code = SC_OKAY;
	SysValidateLevelItem * level_p;
	SysIndex lidx;

	for (;;) {

		if (format_p == NULL) {
			code = SC_INTERFACE_MISSING_FORMAT;
			break;
		}

		for (lidx = 0, level_p = level_l;
			 lidx < SysValidateLevelMax;
			 ++lidx, ++level_p)
			{

			if (code == SC_FAILURE) level_p->width = 0;
			else {
				
				//printf("buffer =   %s ",buffer.data_p);
				code = buffer.IntegerGet (TYPE_UBin8, &level_p->width);
				
				//printf("&level_p->width = %d poit= %d\n",level_p->width,&level_p->width);
				if (code != SC_OKAY) code = SC_FAILURE;
	
				char temp;
				code = buffer.CharGet (temp, ",");
				if (code != SC_OKAY) code = SC_FAILURE;
			}

		}

		for (lidx = 0, level_p = level_l;
			 lidx < SysValidateLevelMax;
			 ++lidx, ++level_p)
			{

			if (code == SC_FAILURE) level_p->include_badge = NO;
			else 
			{
				code = buffer.BooleanGet (level_p->include_badge,
						   format_p->boolean, 1);
				if (code != SC_OKAY) code = SC_FAILURE;
			}

		}

		break;
	}

	/* Clear local status code value. */
	if (code == SC_FAILURE) code = SC_OKAY;

	return (code);
}


/*
	_ValidateLevelPut:

	Place validate level data in buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	level_p		Pointer to validate level data array.

	format_p	Pointer to format data.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
_ValidateLevelPut (InterfaceBuffer & buffer,
	const SysValidateLevelItem * const level_l,
	const LegacyInterfaceValidateLevel * format_p)

{
	size_t length = 0;
	const SysValidateLevelItem * level_p;
	SysIndex lidx;

	for (lidx = 0, level_p = level_l;
		 lidx < SysValidateLevelMax;
		 ++lidx, ++level_p)
	{
		length += buffer.IntegerPut (TYPE_UBin8, &level_p->width, NULL, 2);
		length += buffer.AddChar (',');
	}

	for (lidx = 0, level_p = level_l;
		 lidx < SysValidateLevelMax;
		 ++lidx, ++level_p)
	{
		length += buffer.BooleanPut (level_p->include_badge, format_p->boolean);
	}

	return (length);
}


/*
	_ValidateEntryGet:

	Parse validate entry from buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	entry		Reference to validate entry.

	format_p	Pointer to format data.

	badge_length
				Current badge length.
				
	reset		Reference to reset flag:
				Set to 1 if reset record read; 0 otherwise.				

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
_ValidateEntryGet (InterfaceBuffer & buffer, SysValidateEntryItem & entry,
	const LegacyInterfaceValidateEntry * format_p,
	const SysValidateData & validate, SysFieldLength badge_length,
	Boolean & reset)

{
	Code code = SC_INTERFACE_BAD_DATA;

	for (;;) {

		if (format_p == NULL) {
			code = SC_INTERFACE_MISSING_FORMAT;
			break;
		}
		//printf("buffer =   %s ",buffer.data_p);
		code = buffer.EnumCharGet (entry.level, format_p->levels);
		if (code != SC_OKAY) break;
		code = buffer.BooleanGet (reset, format_p->reset, 1);
		if (code != SC_OKAY) break;

		SysFieldLength data_len = validate.level[entry.level].width;
		//printf("width =   %d ",data_len);
		if (validate.level[entry.level].include_badge) data_len += badge_length;
		code = buffer.StringGet (entry.data, sizeof entry.data, data_len);
		if (code != SC_OKAY) break;

		code = buffer.StringGet (entry.message, sizeof entry.message,
				   LegacyFieldWidthMessage);
		if (code != SC_OKAY) break;

		break;
	}

	return (code);
}


/*
	_ValidateEntryPut:

	Place validate entry in buffer.

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted
				data is stored.

	entry		Reference to validate entry.

	format_p	Pointer to format data.

	reset		1 if this is the first record and previous records
					should be discarded;
				0 otherwise.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
_ValidateEntryPut (InterfaceBuffer & buffer, const SysValidateEntryItem & entry,
	const LegacyInterfaceValidateEntry * format_p,
	const SysValidateData & validate, SysFieldLength badge_length,
	Boolean reset)

{
	size_t length = 0;

	length += buffer.EnumCharPut (entry.level, format_p->levels);

	//length += buffer.BooleanPut (reset);
	//reset is default 0. instead, get this value from structure
	buffer.BooleanGet (reset, format_p->reset, 1);
	length += buffer.BooleanPut (reset);
	
	SysFieldLength data_len = validate.level[entry.level].width;
	if (validate.level[entry.level].include_badge) data_len += badge_length;
	length += buffer.StringPut (entry.data, InterfaceStringNoQuote, data_len);
	length += buffer.StringPut (entry.message, InterfaceStringNoQuote,
				  LegacyFieldWidthMessage);

	return (length);
}

Code
_FingerTemplateGet(InterfaceBuffer & buffer, SysFingerTemplateData *finger, 
							const LegacyFingerTemplate * format_p)
{
	Code code = SC_INTERFACE_BAD_DATA;
	
	for (;;) {
		code = buffer.StringGet(finger->badge,(format_p == NULL) ?
				0 : *(InterfaceFormatStringLength *)(format_p),0,',');
		if (code != SC_OKAY) break;
		//format_p - based on function detail parsing "FP"
		code = buffer.CharGet(finger->fingerindex,(const char *)format_p->f_index);
		if (code != SC_OKAY) break;
		
		char sep;
		char bad_char;
		bool get_sep = true;
		
		code = buffer.CharGet (sep, ",");
		if (code != SC_OKAY) break;
		
		code = buffer.CharGet(finger->threshhold,(const char *)format_p->r_threshhold);
		if (code != SC_OKAY) 
		{
			//break;
			//The error must be because of the threshhold value being different from the template (1 to 7). 
			//Do not return error. In this case we consider the threshhold as 4 (default)
			code = buffer.CharGet(bad_char); //remove the bad char from buffer. Otherwise it will not be able to parse the next separator, as it will consider this bad char as separator. 
			if (code != SC_OKAY) {printf("\n failure- bad char"); break; }
			if(bad_char == ',')
			{
				//this is a separator, that means the threashhold field was actually empty. 
				//So do not parse the separator again
				get_sep = false;
			}
			
			finger->threshhold = '4';
			code = SC_OKAY;
		}
		
		if(get_sep)
		{
			code = buffer.CharGet (sep, ",");
			if (code != SC_OKAY) {printf("\n failure- separator"); break; }
			get_sep = true;
		}
		code = buffer.CharGet(finger->privilegelevel,(const char *)format_p->f_priv_level);
		if (code != SC_OKAY) 
		{
			//break;
			//The error must be because of the privilege value being different from the template (0 to 2). 
			//Do not return error. In this case we consider the privilege as 0 (default)
			code = buffer.CharGet(bad_char); //remove the bad char from buffer. Otherwise it will not be able to parse the next separator, as it will consider this bad char as separator. 
			if (code != SC_OKAY) {printf("\n failure- bad char"); break; }
			if(bad_char == ',')
			{   //For Future Use
				//this is a separator, that means the priv level field was actually empty. 
				//So do not parse the separator again 
				get_sep = false;
			}
			
			finger->privilegelevel = '0';
			code = SC_OKAY;
		}
				
		break;
	}
	return(code);
	
}
