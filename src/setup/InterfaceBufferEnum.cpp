/*
	InterfaceBuffer::Enum:  [Module]

	Interface buffer subsystem enumeration data type functions.

	Copyright (c) 2003-2004 - Alcom Communications
	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   19 Aug 2004
	  Based on:   MsgGenE.c originally written on 21 Apr 2003
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   20 Apr 2004 22:27:00  $
	     $Date: 2005/09/01 22:27:58 $

Description:

	The InterfaceBuffer::Enum module provides functions to map label
characters and strings to and from enumeration states.

*/


/* Include files for InterfaceBuffer::Enum. */
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "System.hpp"
#include "Interface.hpp"

/* Constant parameters for InterfaceBuffer::Enum. */
#define ENUM_NUMERIC	'#'
#define ENUM_CHARACTER	'*'

/* Type and structure definitions for InterfaceBuffer::Enum. */

/* Internal static data for InterfaceBuffer::Enum. */

/* External global data for InterfaceBuffer::Enum. */

/* Function prototypes for InterfaceBuffer::Enum. */
static Boolean  _get_mode (const char * & template_p);


/*
	InterfaceBuffer::EnumCharGet:

	Get an enumerated state value from the message data.

Description:

	A single character in the message data field is translated into an
enumeration value.

	The resulting enumeration value can be either a 0 based numerical value
or a character value.  The leading character of the template string
determines the mode:  A lead character of '#' forces numeric mode while a
'*' character forces character mode.  If neither character is found a
default of numeric is used.

Examples:

	If the enumeration character template is "xyz" then a message field
of "y" will be returned as the value 1.  A template of "*ABC" with a
message field of "c" yields a value of 'C'.

Notes:

	If the message character does not match the enumeration character
template the message data is considered bad and reported without
making any value update.

	Any positions in the status template that contain a space character
are considered to be invalid enumeration values and will return a bad
data status code.

	The case of characters in the template string has no effect on this
function.

See also:  InterfaceBuffer::EnumCharPut, InterfaceBuffer::EnumLabelGet

Arguments:
	state		Reference to enumeration value to set based on buffer data.
	
	template_p	Pointer to a string of characters to use as the valid states.

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
InterfaceBuffer::EnumCharGet (InterfaceEnum_t & state, const char * template_p)

{
	Code code = SC_INTERFACE_BAD_DATA;
	InterfaceEnum_t new_state = state;

	for (;;) {

		/* No data so stop now. */
		if (*data_p == '\0') break;

		Boolean numeric = _get_mode (template_p);

		/* Check if character (ignoring case) is in template_p. */
		const char * sp;
		if (((sp = strchr (template_p, tolower (*data_p))) == NULL) AND
			((sp = strchr (template_p, toupper (*data_p))) == NULL)) {
			code = SC_INTERFACE_VALUE_RANGE;
			break;
			}
		++data_p;

		/* Numeric mode so index to char is state. */
		if (numeric) new_state = (InterfaceEnum_t)(sp - template_p);
		/* Character mode so state is template character. */
		else new_state = template_p[sp-template_p];
		code = SC_OKAY;

		break;
		}

	/* Set new state value if everything's okay. */
	if (code == SC_OKAY) state = new_state;

	return (code);
}


/*
	InterfaceBuffer::EnumCharPut:

	Put a enumeraton state character code field into the message data.

Description:

	If the supplied enumeration state value is valid a single character
code corresponding to that state is placed in the message data.  If
an invalid enumeration value is supplied no field is placed in the
string and a length of zero is returned.

	An enumeration value can be either a 0 based numerical value
or a character value.  The leading character of the template string
determines the mode:  A lead character of '#' forces numeric mode while a
'*' character forces character mode.  If neither character is found a
default of numeric is used.

Examples:

	If the enumeration character template_p is "xyz" and a value of 2 is
supplied then a "z" will be placed in the message field.  A template of
"*ABC" with a value of 'C' yields a message field of "C".

Notes:

	Any positions in the state template_p that contain a space character
are considered invalid states as are values that exceed the length of
the template_p string.

	Characters in the template_p string are used directly to set the
message field; no uppercase or lowercase conversion is performed.

See also:  InterfaceBuffer::EnumCharGet, InterfaceBuffer::EnumLabelPut

Arguments:
	state		Enumeration value to use to place label in data buffer.

	template_p	Pointer to a string of characters to use as
				the valid states.


Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
InterfaceBuffer::EnumCharPut (InterfaceEnum_t state, const char * template_p)

{
	size_t length = size;

	for (;;) {
		Boolean numeric = _get_mode (template_p);

		if (numeric) {

			/* Be sure value is within template_p. */
			if (state >= (InterfaceEnum_t)strlen (template_p)) break;

			/* Be sure value's template_p character is
				not a space (invalid code). */
			if (template_p[state] == ' ') break;

			/* Move state character into message. */
			AddChar (template_p[state]);

			}

		else AddChar (state);

		break;
		}

	return (size - length);
}


/*
	InterfaceBuffer::EnumLabelGet:

	Parse an enumeration label from a data string.

Description:

	The message data field is examined to determine if it matches one
of the defined enumeration label strings.  If so the corresponding
enumeration state is returned otherwise a value of MSG_ENUM_INVALID is
returned.

Notes:

	Upon return the data_p supplied always points past the field
parsed.

See also:  InterfaceBuffer::EnumLabelPut, InterfaceBuffer::EnumCharGet

Arguments:
	state		Reference to enumeration value to set based on buffer data.

	format_p	Pointer to InterfaceFormatEnum data defining extents and labels.

Returns:
	Code		SC_OKAY if a valid state was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
InterfaceBuffer::EnumLabelGet (InterfaceEnum_t & state,
							   const InterfaceFormatEnum * format_p)

{
	Code code = SC_INTERFACE_BAD_DATA;
	InterfaceEnum_t new_state = state;

	for (;;) {
		const char * const * lp;

		/* No data so stop now. */
		if (*data_p == '\0') break;

		/* Scan for matching label. */
		lp = format_p->list_p;
		for (new_state = *format_p->min_p; new_state <= *format_p->max_p; new_state++, lp++)
			{
			if (strcmp (data_p, *lp) == 0) {
				data_p += strlen (*lp);
				code = SC_OKAY;
				break;
				}
			}
		if (code == SC_OKAY) break;

		/* Scan for matching label in alternate list, if supplied. */
		lp = format_p->alt_p;
		if (lp != NULL) {
			for (new_state = *format_p->min_p; new_state <= *format_p->max_p; new_state++, lp++)
				{
				if (strcmp (data_p, *lp) == 0) {
					data_p += strlen (*lp);
					code = SC_OKAY;
					break;
					}
				}
			if (code == SC_OKAY) break;
			}

		/* If no label matchs then report invalid data field value. */
		if (new_state > *format_p->max_p) code = SC_INTERFACE_VALUE_RANGE;

		/* If not found check for numeric value as an alternate form. */
		if (format_p->numeric) {
			char * end_p;
			new_state = strtol (data_p, &end_p, 10);
			if (data_p != end_p) {
				if (new_state >= *format_p->min_p AND new_state <= *format_p->max_p) {
					data_p = end_p;
					code = SC_OKAY;
					break;
					}
				else code = SC_INTERFACE_VALUE_RANGE;
				}
			}

		break;
		}

	/* Set new state value if everything's okay. */
	if (code == SC_OKAY) state = new_state;

	return (code);
}


/*
	InterfaceBuffer::EnumLabelPut:

	Place an enumeration label in the data string.

Description:

	An enumeration label string from the supplied enumeration labels
data is placed in the message data field.

Notes:

	If the enumeration state supplied is not within the range of
available label strings the invalid string supplied is placed in the
message.

See also:  InterfaceBuffer::EnumLabelGet, InterfaceBuffer::EnumCharPut

Arguments:
	state		Enumeration value to use to place label in data buffer.

	format_p	Pointer to InterfaceFormatEnum data defining extents and labels.

Returns:
	size_t		Number of bytes added to buffer data.

*/

		size_t
InterfaceBuffer::EnumLabelPut (InterfaceEnum_t state,
							   const InterfaceFormatEnum * format_p)

{
	size_t length = size;

	if (state >= *format_p->min_p AND state <= *format_p->max_p) {
		AddString (format_p->list_p[state]);
		}

	else if (format_p->invalid != NULL) AddString (format_p->invalid);

	return (size - length);
}


/*
	_get_mode:

	If leading template_p character is a recognized enumeration type record
it and move past it; default to numeric.

Arguments:
	template_p	Pointer to template string.

Returns:
	Boolean		YES if numeric mode;
				NO is character mode.
				
*/

		static Boolean
_get_mode (const char * & template_p)

{
	Boolean numeric;

	numeric = YES;
	switch (*template_p)
		{
	case ENUM_CHARACTER:
		numeric = NO;
	case ENUM_NUMERIC:
		template_p++;
		break;
		}

	return (numeric);
}


