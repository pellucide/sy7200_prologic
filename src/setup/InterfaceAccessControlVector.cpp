#include "InterfaceAccessControlVector.hpp"

InterfaceAccessControlVector::InterfaceAccessControlVector()
{
}

InterfaceAccessControlVector::~InterfaceAccessControlVector()
{
}

void InterfaceAccessControlVector::add(SysAccessBadgeItem assign)
{
    vect.push_back( assign );
}

void InterfaceAccessControlVector::remove(SysAccessBadgeItem item)
{
    
}

int InterfaceAccessControlVector::size()
{
    return vect.size();
}

void InterfaceAccessControlVector::clear()
{
    vect.clear();
}

SysAccessBadgeItem InterfaceAccessControlVector::Get(int index)
{
    SysAccessBadgeItem& assign = *vect[index];
    return assign;
}


