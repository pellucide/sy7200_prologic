#ifndef INTERFACEPROFILEASSIGNVECTOR_HPP
#define INTERFACEPROFILEASSIGNVECTOR_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include "InterfaceProfileAssignHolder.hpp"
#include "TAVector.h"
using namespace std;

class InterfaceProfileAssignVector : public InterfaceProfileAssignHolder{
public:

	InterfaceProfileAssignVector();
	virtual ~InterfaceProfileAssignVector();
	virtual void add(SysProfileAssignItem);
	virtual void remove(SysProfileAssignItem);
	virtual int size();
	virtual void clear();
	virtual SysProfileAssignItem Get(int);

private:
    TAVector<SysProfileAssignItem> vect;
};

#endif // INTERFACEPROFILEASSIGNVECTOR_HPP
