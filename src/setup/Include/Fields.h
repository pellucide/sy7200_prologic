/*
	Fields.h:

	Written by:   Alan J. Luse
	Written on:   31 Aug 2004
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime$
	     $Date: 2005/09/01 22:27:58 $

*/


#if ! defined FIELDS_H
#define FIELDS_H

/* Macro function to set bit masked field to specific value.
	For example, if masks for a 1-bit field and an 8-bit field are
	defined like this:

	#define REGISTER_ABCFIELD   0x0002
	#define REGISTER_XYZFIELD   0xFF00
	
	Then a macro call like this:
		IMMR->REGISTER =
			(field_add (Bit16, REGISTER_ABCFIELD, 0xa3) |
			field_add (Bit16, REGISTER_XYZFIELD, 1)
			);
	should set MYREGISTER to the value 0xa302.

	while:
		field_get (Bit16, REGISTER_ABCFIELD, IMMR->REGISTER)
	should return 0xa3
*/

#define field_add(type, field, value) ((type)((value)*(((type)field)&((~((type)field))+1))))
#define field_set(type, field, value, current) ((type)(((type)(current)&~(type)(field))|((type)(field)&field_add(type, field, value))))
#define field_get(type, field, current) ((type)(((type)(current)&(type)(field))/(((type)field)&((~((type)field))+1))))

#endif

