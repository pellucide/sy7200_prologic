#ifndef INTERFACEVALIDATEENTRYHOLDER_HPP
#define INTERFACEVALIDATEENTRYHOLDER_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include "System.hpp"

class InterfaceValidateEntryHolder{
public:

	InterfaceValidateEntryHolder(){}
	virtual ~InterfaceValidateEntryHolder(){}
	virtual void add(SysValidateEntryItem)=0;
	virtual void remove(SysValidateEntryItem)=0;
	virtual int size()=0;
	virtual void clear()=0;
	virtual SysValidateEntryItem Get(int)=0;
};

#endif // INTERFACEVALIDATEENTRYHOLDER_HPP
