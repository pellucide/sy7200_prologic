#ifndef _VERSION_H_
#define _VERSION_H_

#ifdef TESTBUILD_WITH_TESTS
#warning TESTBUILD with TESTS
#warning Remove tests before Release
#undef RELEASE
#else
#define RELEASE					1
#endif

/**
 * ----------------------------------------------------------------
 * ----------------  SOFTWARE VERSION  ----------------------------
 * ----------------------------------------------------------------
 */
//#define CLIENT_TRADEMARK        "S.SOU"
#define CLIENT_TRADEMARK        "PTS.TEAMS"
#ifdef TESTBUILD_WITH_TESTS
#define MAJOR_VERSION 			"2"
#define MINOR_VERSION 			""
#else
#define MAJOR_VERSION 			"2"
#define MINOR_VERSION 			"0.0.27"
#endif
#define BUILD_NUMBER 			"1.14.06"
			// trunk=1, branch#. build#
			//
			// branch/devtrack = 1 = ssl support
			// branch/devtrack = 2 = offline mode
			// branch/devtrack = 3 = fingerprint mgmt phase I
			// branch/devtrack = 4 = fingerprint mgmt phase IIa
			// branch/devtrack = 5 = fingerprint mgmt phase IIb
			// branch/devtrack = 6 = memory resolutions
			// branch/devtrack = 7 = accept unverified offline transactions
			// branch/devtrack = 8 = Verify By Server if not enrolled
			// branch/devtrack = 9 = PopulateTemplates is now File-based
			// branch/devtrack =10 = ResetAccessCode handling
			// branch/devtrack =11 = Enroll using Image Quality Score
			// 					11.04 = reset fpu after bulk enrollments
			// 					12.08 = reset fpu after enrollment failures. retry
			// 					12.08 = FPU set to Single Template Enroll Mode
			//					12.19 = correction to FPU Reset after Forced PopulateTemplates
			//					12.20 = trial correction to FPU Reset after Scheduled PopulateTemplates
			//					12.26 = trial#2 correction to FPU Reset after Scheduled PopulateTemplates
			//					12.28 = trial#3 FPU Reset, then Clock Reboot, after Scheduled PopulateTemplates
			//					13.02 = use static libcurl.a
			//					13.03 = use static libcurl.a with SSL support
			//					13.04 = more logging to track PopulateTemplates
			//					13.05 = more logging, ignore Scan_Failed on enrollment
			//					13.06 = Set priorities for threads. FPU High, OfflineSender Low;
			//							OfflineSender does not disrupt idle screen display"
			//					13.09 = do not verify SSL Peer or Host if VerifySSL=false (or default)
			//					14.01 = PinTable support - not activated  - for later build release 2.0.0.XX
			//					14.05 = Separated Keypad and Card Reader Badge entry configuration
			//					14.06 = add independent fpv settings to web config interface; clock app test as 22a


#if RELEASE
#define CLOCK_VERSION 			CLIENT_TRADEMARK "-" MAJOR_VERSION "." MINOR_VERSION
#else
#define CLOCK_VERSION 			CLIENT_TRADEMARK "-" MAJOR_VERSION "." MINOR_VERSION "b" BUILD_NUMBER
#endif
#define PRODUCT_NAME 			"SY7000 Series Clock"
#define PRODUCT_VENDOR 			"Synel LTD."
#define PRODUCT_VENDOR_SITE		"www.synel.co.il"

#endif //_VERSION_H_
