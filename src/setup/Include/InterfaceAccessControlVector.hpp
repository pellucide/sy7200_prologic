#ifndef INTERFACEACCESSCONTROLVECTOR_HPP
#define INTERFACEACCESSCONTROLVECTOR_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include "InterfaceAccessControlHolder.hpp"
#include "TAVector.h"

class InterfaceAccessControlVector : public InterfaceAccessControlHolder{
public:

	InterfaceAccessControlVector();
	virtual ~InterfaceAccessControlVector();
	virtual void add(SysAccessBadgeItem);
	virtual void remove(SysAccessBadgeItem);
	virtual int size();
	virtual void clear();
	virtual SysAccessBadgeItem Get(int);

private:
    TAVector<SysAccessBadgeItem> vect;
};

#endif // INTERFACEACCESSCONTROLVECTOR_HPP
