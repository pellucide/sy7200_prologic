#ifndef INTERFACEMESSAGEBADGEVECTOR_HPP
#define INTERFACEMESSAGEBADGEVECTOR_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include "InterfaceMessageBadgeHolder.hpp"
#include "TAVector.h"
using namespace std;

class InterfaceMessageBadgeVector : public InterfaceMessageBadgeHolder{
public:

	InterfaceMessageBadgeVector();
	virtual ~InterfaceMessageBadgeVector();
	virtual void add(SysMessageBadgeItem);
	virtual void remove(SysMessageBadgeItem);
	virtual int size();
	virtual void clear();
	virtual SysMessageBadgeItem Get(int);

private:
    TAVector<SysMessageBadgeItem> vect;
};

#endif // INTERFACEMESSAGEBADGEVECTOR_HPP
