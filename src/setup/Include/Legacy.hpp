/*
	Legacy.hpp

	Data, class and function declarations for Legacy objects.

	Copyright (c) 2004 - Time America, Inc.

	Written by:	  Alan J. Luse
	Written on:   13 May 2004
	 $Revision: 1.6 $
	   $Author: walterm $
	  $Modtime$
	     $Date: 2007/02/16 15:31:34 $

*/


#if ! defined LEGACY_HPP
#define LEGACY_HPP

/* Interface item for data to be parsed. */
#include <time.h>
#include "Interface.hpp"
#include "InterfaceProfileAssignHolder.hpp"
#include "InterfaceMessageBadgeHolder.hpp"
#include "InterfaceBellScheduleHolder.hpp"
#include "InterfaceAccessControlHolder.hpp"

//using namespace std;

/* Status codes for the Legacy subsystem. */
typedef enum {
	SC_LEGACY_GENERIC = SC_BASE_LEGACY,
	SC_LEGACY_NULL,
	SC_LEGACY_EMPTY,
	SC_LEGACY_FORMAT,
	SC_LEGACY_TAG_FORM,
	}
LegacyCode_e;

/* Length of program tags. */
const size_t LegacyTagLength = 2;

/* Badge source flag characters. */
const char LegacySourceFlags [] = "SKW";

/* Predeclare full setup class. */
class LegacySetup;
 
/* Legacy message fild widths, etc. */
const size_t LegacyFieldWidthMessage			= 16;

/* Interface conversion data structures. */
struct LegacyInterfaceMessageBadge {
	const InterfaceFormatBoolean ** reset;
	const InterfaceFormatBoolean ** boolean;
	const char * keys;
};
struct LegacyInterfaceFunctionKey {
	const char * keys;
	const InterfaceFormatBoolean ** boolean;
};
struct LegacyInterfaceFunctionDetail {
	const char * keys;
	const InterfaceFormatFlagsBit8 * source;
};
struct LegacyInterfaceBellSchedule {
	const InterfaceFormatBoolean ** reset;
	const InterfaceFormatDays days;
};
struct LegacyInterfaceProfileMessage {
	const InterfaceFormatIndex * message;
	const InterfaceFormatBoolean ** boolean;
	const char * tone;
};
struct LegacyInterfaceProfileEntry {
	const InterfaceFormatIndex * entry;
	const InterfaceFormatIndex * time;
	const InterfaceFormatIndex * message;
};

struct LegacyInterfaceProfileAssign {
	const InterfaceFormatBoolean ** reset;
	const InterfaceFormatIndex * entry;
};

struct LegacyInterfaceValidateLevel {
	const InterfaceFormatBoolean ** boolean;
};
struct LegacyInterfaceValidateEntry {
	const char * levels;
	const InterfaceFormatBoolean ** reset;
};

struct LegacyFingerTemplate {
	const char * f_index;
	const char * r_threshhold;
	const char * f_priv_level;
};
	

/* Legacy setup object class declaration. */
class LegacySetupItem : public InterfaceBuffer {
	char tag [LegacyTagLength+1];

public:
/*				LegacySetupItem (void)
					: InterfaceBuffer (64) { tag[0] = '\0'; };*/
	LegacySetupItem (void)
					: InterfaceBuffer (512) { tag[0] = '\0'; };
	Boolean		Valid (void) const {return (*tag != '\0'); };
	Code		Parse (const char * line);
	Code		Process (LegacySetup & sys_data);
	size_t      Display (char * buffer_p) const;
	size_t      DisplayError (char * buffer_p) const;
};


/* Legacy terminal data structure. */
class LegacySetup {
public:
				LegacySetup (const char * name = NULL) { Defaults (name); };
	void		Defaults (const char * name = NULL);
	void		List (FILE * stream, Boolean mode = YES) const;

public:
	char name [SysFieldMaxName+1];
	SysAddress id;
	struct {
		Boolean lock;
		Boolean configuration;
		Boolean access_only;
	}
	mode;
	
	/*struct {
		//enum SysFPIDMode mode;
		char mode;
	}
	fpidmode;
	
	struct {
		char clear;
	}
	fptemplate;*/
	
	struct {
		char mode;
		char clear;
		char deletebadge[SysFieldMaxBadgeLength+1];
		char threshhold;
		
		SysFingerTemplateData finger;
		
		/*char templatepart1[128];
		char templatepart2[128];
		char templatepart3[128];
		char templatepart4[128];*/
		char templatestring[600];
	}
	fptemplate;
	
	struct {
		SysBadgeLengths length;
		struct {
			SysSuperPrefix supervisor;
			char security [SysFieldMaxBadgePrefix+1];
			char configuration [SysFieldMaxBadgePrefix+1];
			char facility [SysFieldMaxBadgePrefix+1];
		}
		prefix;
		struct {
			char limit [SysFieldMaxBadgeLength+1];
			char function;
		}
		swipe_n_go;
	}
	badge;
	SysFieldLength pin_length;
	struct {
		SysInputSource initial;
		SysInputSource function;
		SysInputSource supervisor;
		SysInputSource super_employee;
		SysInputSource configuration;
	}
	source;
	struct {
		struct {
			//char idle [SysFieldMaxPrompt+1];
			char idle [SysFieldMaxPrompt+2];
			char invalid_source [SysFieldMaxPrompt+1];
			char invalid_badge [SysFieldMaxPrompt+1];
			char try_again [SysFieldMaxPrompt+1];
			char enter_function [SysFieldMaxPrompt+1];
		}
		prompt;
		struct {
			SysDelayShort clock;
			SysDelayShort idle;
		}
		flip;
		struct {
			SysDelay data;
			SysDelay function;
			SysDelay message;
			SysDelay supervisor;
			SysDelay error;
		}
		timeout;
	}
	user;
	struct {
		struct {
			Boolean date;
			Boolean time;
		}
		use_default;
		struct SysFunctionKeyPair delete_keys;
	}
	supervisor;
	struct {
//		SysMessageBadgeItem * badge;
		InterfaceMessageBadgeHolder * badge;
	}
	message;
	struct {
		SysFunctionKeyItem * key;
		SysFunctionDetailItem * detail;
	}
	function;
	struct {
//		SysBellScheduleItem * schedule;
		InterfaceBellScheduleHolder * schedule;
		Boolean maintenance;
	}
	bell;
	struct {
		enum SysOnOffAuto global;
		struct SysAccessWindowItem window;
		struct {
			char always_on [SysFieldMaxPrompt+1];
			char always_off [SysFieldMaxPrompt+1];
		}
		message;
		SysDelay duration;
		InterfaceAccessControlHolder * badge;
	}
	access;
	struct {
		SysProfileMessageItem * defaults;
		SysProfileMessageItem * message;
		SysProfileEntryItem * entry;
		//vector<SysProfileAssignItem> * assign;
		InterfaceProfileAssignHolder * assign;
		struct {
			Boolean compress; // Not used - legacy purposes only.
			Boolean override;
			Boolean report_fails;
		}
		mode;
	}
	profile;
	SysValidateData validate;
    struct {
		Boolean enable;
		struct {
			char line [SysFieldMaxHeader+1];
		}
		header [SysPrintHeaderMax];
		UBin8 LF_count;
		char reset [SysFieldMaxPrintReset+1];
	}
	print;
	SysFingerData finger;
	struct clock_t {
		Boolean twenty_four_hour;
		Boolean daylight_savings;
	}
	clock;

  class d_s_a_t {
  public:
    d_s_a_t () : forward ( 0 ) , backward ( 0 ){}
    char* operator = ( char* ); //assignment from configuration string
    operator void * () { return ( 0 != forward || 0 != backward ) ? this : 0 ;}
    void as_string ( char* v , size_t l ) const ;

    time_t forward ;
    time_t backward ;
    int skip_min ;

  } daylight_savings_adjustment ;

	struct {
		Boolean lightbox;
	}
	hardware;
	struct {
		struct {
			SysBaudRate baudrate;
		}
		RS232;
		struct {
			SysBaudRate baudrate;
			SysDelay turnaround;
		}
		RS485;
		struct {
			SysBaudRate baudrate;
			struct {
				SysDelayShort ring_delay;
				struct SysTimeWindow time;
			}
			answer;
			Boolean external;
		}
		modem;
	}
	communications;
};

#endif

