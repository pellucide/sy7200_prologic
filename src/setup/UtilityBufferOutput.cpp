/*
	Buffer::Output:  [Class]

	The Buffer class Output module provides functions to output the buffer
data to a stream.

	Copyright (c) 2000-2002 - Alcom Communications
	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   27 Nov 2000
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   19 Apr 2002 14:09:20  $
	     $Date: 2005/09/01 22:27:58 $

Description:

	This module provides a member functions to output a buffer either as
straight data or as a log formatted version.

*/


/* Compilation options for BufferLog. */

/* Include files for BufferLog. */
#include <string.h>
#include <stdio.h>
//#include "Generic.hpp"
#include "Gen_Def.h"
#include "Utility.hpp"

/* Constant parameters for BufferLog. */

/* Type and structure definitions for BufferLog. */

/* Internal static data for BufferLog. */

/* Function prototypes for BufferLog. */


/*
	Buffer::OutputBytes

	Output buffer contents.

Description:

	Send the buffer contents to the supplied stream.

See also:

Arguments:
	stream		Output stream on which to send data.

Returns:
	void

*/

		void
Buffer::OutputBytes (FILE * stream) const

{

	fwrite (data, 1, size, stream);

	return;
}


/*
	Buffer::OutputString

	Output buffer contents as a string.

Description:

	Send the buffer contents to the supplied stream.

See also:

Arguments:
	stream		Output stream on which to send data.

Returns:
	void

*/

		void
Buffer::OutputString (FILE * stream) const

{

	fprintf (stream, "%.*s", size, data);

	return;
}


/*
	Buffer::OutputLog

	Output buffer contents in hexadecimal format.

Description:

	Send the formatted buffer contents to the supplied stream.

See also:

Arguments:
	stream		Output stream on which to send data.

Returns:
	void

*/

		void
Buffer::OutputLog (FILE * stream) const

{

	const Byte * dp = data;
	for (size_t idx = 0; idx < size; idx++) {
		fprintf (stream, " %0.2X", (unsigned int)*dp++);
		}

	return;
}


