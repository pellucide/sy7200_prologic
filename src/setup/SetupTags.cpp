/*
	SetupTags:  [Class]

	The SetupTags module provides tables of the terminal setup parameters.

	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   13 May 2004
	 $Revision: 1.5 $
	   $Author: walterm $
	  $Modtime$
	     $Date: 2007/02/05 22:12:36 $

Description:

This module contains tables for each group of parameters that can occur in
a terminal setup program file.  It contains no executable code.

*/


/* Compilation options for SetupTags. */

/* Include files for SetupTags. */
#include "System.hpp"
#include "Setup.hpp"
#include "Legacy.hpp"
#include "SerialIO.hpp"

/* Constant parameters for SetupTags. */

/* Type and structure definitions for SetupTags. */

/* Internal static data for SetupTags. */
static const LegacySetup * sp = NULL;

/* Function prototypes for SetupTags. */


/** Format parameter data definitions. **/

static const SysIndex _FormatIndexZero = 0;
static const InterfaceFormatBoolean ** _FormatBooleanReset = NULL;
static const InterfaceFormatBoolean * _FormatBooleanYN [] = {
	&InterfaceBooleanLabelYN,
	&InterfaceBooleanLabelTF,
	&InterfaceBooleanLabel10,
	&InterfaceBooleanLabelYesNo,
	&InterfaceBooleanLabelTrueFalse,
	NULL
};
static const InterfaceFormatBoolean _FormatBooleanLabelSpace = {" ", ""};
static const InterfaceFormatBoolean * _FormatBooleanCharsSpace [] = {
	&InterfaceBooleanLabelYN,
	&InterfaceBooleanLabelTF,
	&InterfaceBooleanLabel10,
	&_FormatBooleanLabelSpace,
	NULL
};
static const InterfaceEnum_t _FormatBaudRatesMin = BAUD_75;
static const InterfaceEnum_t _FormatBaudRatesMax = MAX_BAUD-1;
static const InterfaceFormatEnum _FormatEnumBaudRates (
	&_FormatBaudRatesMin, &_FormatBaudRatesMax,
	SioNameBaud, "???", NO, SioNameBaudShort);

static const InterfaceFormatFlagsBit8 _FormatSourceTemplate [] = {
	//InterfaceFormatFlagsBit8 ('S', SysInputSourceMagnetic),
	//attach proxy to Mag swipe. So the Input source 'S' should turn on both Mag and Proxy reader - if available.  
	InterfaceFormatFlagsBit8 ('S', SysInputSourceProxMag),
	InterfaceFormatFlagsBit8 ('K', SysInputSourceKeypad),
	InterfaceFormatFlagsBit8 ('W', SysInputSourceBarcode),
	InterfaceFormatFlagsBit8 ('P', SysInputSourceProximity),
	
	// Alternate flag names.
	InterfaceFormatFlagsBit8 ('M', SysInputSourceMagnetic),
	InterfaceFormatFlagsBit8 ('B', SysInputSourceBarcode),
	// Group flag names.
	InterfaceFormatFlagsBit8 ('D', SysInputSourceDefault),
	InterfaceFormatFlagsBit8 ('A', SysInputSourceAll),
	InterfaceFormatFlagsBit8 (' ', 0x00),
	InterfaceFormatFlagsBit8 ()
};

static const LegacyInterfaceMessageBadge _FormatMessageBadge = {
	_FormatBooleanReset, _FormatBooleanCharsSpace, SysFunctionKeyChars
};
static const LegacyInterfaceFunctionKey _FormatFunctionKey = {
	SysFunctionKeyChars, InterfaceBooleanChars
};
static const LegacyInterfaceFunctionDetail _FormatFunctionDetail = {
	SysFunctionKeyChars, _FormatSourceTemplate
};
static const LegacyInterfaceBellSchedule _FormatBellSchedule = {
	_FormatBooleanReset, InterfaceFormatDays (InterfaceBooleanChars, 8)
};
static const InterfaceFormatIndex _RangeProfileMessage =
	{{0, SysProfileMessageMax-1}, '1'};
static const LegacyInterfaceProfileMessage _FormatProfileMessage = {
	&_RangeProfileMessage, InterfaceBooleanChars, SysProfileFlagToneChars
};


static const InterfaceFormatIndex _RangeProfileDefaults =
	{{0, SysProfileMessageDefaults-1}, '1'};
static const LegacyInterfaceProfileMessage _FormatProfileDefault = {
	&_RangeProfileDefaults, InterfaceBooleanChars, SysProfileFlagToneChars
};
static const InterfaceFormatIndex _RangeProfileEntry =
	{{0, SysProfileEntryMax-1}, '1'};
static const InterfaceFormatIndex _RangeProfileTime =
	{{0, SysProfileTimeMax-1}, '1'};


static const LegacyInterfaceProfileAssign _FormatProfileAssign = {
	_FormatBooleanReset, &_RangeProfileEntry
};



static const LegacyInterfaceProfileEntry _FormatProfileEntry = {
	&_RangeProfileEntry, &_RangeProfileTime, &_RangeProfileMessage,
};
static const LegacyInterfaceValidateLevel _FormatValidateLevel = {
	InterfaceBooleanChars
};
static const LegacyInterfaceValidateEntry _FormatValidateEntry = {
	SysValidateLevelChars, _FormatBooleanReset
};



static const LegacyFingerTemplate _FormatFingerTemplate = {
	SysFingerIndexChars, SysFpRtChars, SysFpPrivilegeLevelChars
};



/** Basic Program data tags list. **/

const SetupElement SetupTagsProgram [] = {
	
	//Fingerprint - Begin
	//what is the significance of the flags SEF_Base etc? get clarified
	
	SetupElement ("FingerIDMode",			    "IM",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_Char , Offset (&sp->fptemplate.mode, sp), 
		SysFpIdModeChars),
		
	SetupElement ("ClearTemplates",				"CT",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		SYSTYPE_ClearTemplates, Offset (&sp->fptemplate.clear, sp),
		SysFpTemplateClearChars),
	
	SetupElement ("DeleteFinger",				"DF",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		SYSTYPE_DeleteFinger, Offset (&sp->fptemplate.deletebadge, sp),
		&SysFieldMaxBadgeLength),
	
	SetupElement ("RejectThreshhold",			"RT",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_Char , Offset (&sp->fptemplate.threshhold, sp), 
		SysFpRtChars),
	
	SetupElement ("FingerTemplateLine1",		"F1",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		SYSTYPE_FingerTemplateLine1 , Offset (&sp->fptemplate.finger, sp),
		&_FormatFingerTemplate), 
	
	SetupElement ("FingerTemplateLine2",		"F2",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		SYSTYPE_FingerTemplateLine2 , Offset (&sp->fptemplate.templatestring, sp)),
		
/*	SetupElement ("FingerTemplateLine2",		"F2",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String , Offset (&sp->fptemplate.templatestring, sp)),*/
		
	/*SetupElement ("FingerTemplateLine2",		"F2",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String , Offset (&sp->fptemplate.templatepart1, sp)), 
	
		
	SetupElement ("FingerTemplateLine3",		"F3",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String , Offset (&sp->fptemplate.templatepart2, sp)), 
		
	SetupElement ("FingerTemplateLine4",		"F4",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String , Offset (&sp->fptemplate.templatepart3, sp)), 
		
	SetupElement ("FingerTemplateLine5",		"F5",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		SYSTYPE_FingerTemplateLine5 , Offset (&sp->fptemplate.templatepart4, sp)), */
	
	//Fingerprint - End
	
	
	SetupElement ("ConfigurationMode",			"CM",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_Boolean, Offset (&sp->mode.configuration, sp),
		_FormatBooleanYN),
				
	SetupElement ("ConfigurationSource",		"CS",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_FlagBit8, Offset (&sp->source.configuration, sp),
		_FormatSourceTemplate),
	SetupElement ("ConfigurationPrefix",		"CP",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->badge.prefix.configuration, sp),
		&SysFieldMaxBadgePrefix),

	SetupElement ("SourceInitial",				"IS",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_FlagBit8, Offset (&sp->source.initial, sp),
		_FormatSourceTemplate),
	#if 0
	SetupElement ("SourceFunction",				"FS",
		SEF_Base|SEF_InterfaceTA_c,
		TYPE_FlagBit8, Offset (&sp->source.function, sp),
		_FormatSourceTemplate),
	#endif

	SetupElement ("PINLength",					"PL",
		SEF_Base|SEF_InterfaceTA_i,
		TYPE_UBin8, Offset (&sp->pin_length, sp)),

	SetupElement ("BadgeMaximum",				"BM",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		SYSTYPE_LegacyBadgeMaxOff, Offset (&sp->badge.length, sp)),
	SetupElement ("BadgeMinimum",				"BN",
		SEF_Base|SEF_InterfaceAcro,
		TYPE_UBin8, Offset (&sp->badge.length.minimum, sp)),
	SetupElement ("BadgeLength",				"BL",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		SYSTYPE_LegacyBadgeLenType, Offset (&sp->badge.length, sp)),
	SetupElement ("BadgeOffset",				"BO",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5,
		TYPE_UBin8, Offset (&sp->badge.length.offset, sp)),
	SetupElement ("BadgeSecurityPrefix",		"BC",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->badge.prefix.security, sp),
		&SysFieldMaxBadgePrefix),
	SetupElement ("FacilityCode",				"FC",
		SEF_Base|SEF_InterfaceAcro,
		TYPE_String, Offset (&sp->badge.prefix.facility, sp),
		&SysFieldMaxBadgePrefix),

	SetupElement ("SwipeNGoBadge",				"SG",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->badge.swipe_n_go.limit, sp),
		&SysFieldMaxBadgeLength),
	SetupElement ("SwipeNGoFunction",			"SC",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_Char, Offset (&sp->badge.swipe_n_go.function, sp),
		SysFunctionKeyChars),

	SetupElement ("IdleTimeDisplay",			"TD",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		SYSTYPE_DelayShort, Offset (&sp->user.flip.clock, sp)),
	SetupElement ("IdleTimeUser",				"TP",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		SYSTYPE_DelayShort, Offset (&sp->user.flip.idle, sp)),

	SetupElement ("IdleUser",					"IP",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->user.prompt.idle, sp),
		&SysFieldMaxPrompt),
	SetupElement ("MessageInvalidSource",		"II",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->user.prompt.invalid_source, sp),
		&SysFieldMaxPrompt),
	SetupElement ("MessageInvalidBadge",		"IB",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->user.prompt.invalid_badge, sp),
		&SysFieldMaxPrompt),
	SetupElement ("MessageTryAgain",			"TA",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->user.prompt.try_again, sp),
		&SysFieldMaxPrompt),
	SetupElement ("MessageEnterFunction",		"EF",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->user.prompt.enter_function, sp),
		&SysFieldMaxPrompt),

	SetupElement ("TimeoutGeneralData",			"T1",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		SYSTYPE_Delay, Offset (&sp->user.timeout.data, sp)),
	SetupElement ("TimeoutFunctionEntry",		"T2",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		SYSTYPE_Delay, Offset (&sp->user.timeout.function, sp)),
	SetupElement ("TimeoutMessage",				"T3",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		SYSTYPE_Delay, Offset (&sp->user.timeout.message, sp)),
	SetupElement ("TimeoutSupervisorInput",		"T4",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		SYSTYPE_Delay, Offset (&sp->user.timeout.supervisor, sp)),
	SetupElement ("TimeoutErrorMessage",		"T5",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		SYSTYPE_Delay, Offset (&sp->user.timeout.error, sp)),

	SetupElement ("Time24Hour",					"MT",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_Boolean, Offset (&sp->clock.twenty_four_hour, sp),
		_FormatBooleanYN),
	SetupElement ("TimeDaylightSavings",		"DL",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_Boolean, Offset (&sp->clock.daylight_savings, sp),
		_FormatBooleanYN),
	SetupElement ("TimeDaylightSavingsAdjust" ,     "DS",
		      SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5,
		      SYSTYPE_Daylight_savings_adjustment , Offset ( & sp ->daylight_savings_adjustment , sp )),


/** Supervisor data tags list. **/

	SetupElement ("SupervisorSource",			"SS",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_FlagBit8, Offset (&sp->source.supervisor, sp),
		_FormatSourceTemplate),
	SetupElement ("SupervisorSourceEmployee",	"SE",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_FlagBit8, Offset (&sp->source.super_employee, sp),
		_FormatSourceTemplate),
	SetupElement ("SupervisorKeys",				"SK",
		SEF_Base|SEF_InterfaceTACWin3,
		SYSTYPE_KeyPair, Offset (&sp->supervisor.delete_keys, sp),
		SysFunctionKeyChars),
	SetupElement ("SupervisorPrefix",			"SP",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		SYSTYPE_SupervisorPrefix, Offset (&sp->badge.prefix.supervisor, sp),
		&SysFieldMaxBadgePrefix),

	SetupElement ("SupervisorDefaultDate",		"DD",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_Boolean, Offset (&sp->supervisor.use_default.date, sp),
		_FormatBooleanYN),
	SetupElement ("SupervisorDefaultTime",		"DT",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_Boolean, Offset (&sp->supervisor.use_default.time, sp),
		_FormatBooleanYN),


/** Function Key data tags list. **/

	SetupElement ("FunctionKey",				"FL",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_s,
		SYSTYPE_LegacyFunctionKey, Offset (&sp->function.key, sp),
		&_FormatFunctionKey),
	SetupElement ("FunctionKeyDetail",			"FP",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_s,
		SYSTYPE_LegacyFunctionDetail, Offset (&sp->function.detail, sp),
		&_FormatFunctionDetail),


/** Program data tags list - Optional modules. **/


	SetupElement ("EmployeeMessage",			"EM",
		SEF_ModuleMessage|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_s,
		SYSTYPE_LegacyMessage, Offset (&sp->message.badge, sp),
		&_FormatMessageBadge, Offset (&sp->badge.length.valid, sp)),

	SetupElement ("BellSchedule",				"BS",
		SEF_ModuleBell|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_s,
		SYSTYPE_LegacyBell, Offset (&sp->bell.schedule, sp),
		&_FormatBellSchedule),
	SetupElement ("BellMaintenance",			"SB",
		SEF_ModuleBell|SEF_InterfaceTA_i,
		TYPE_Boolean, Offset (&sp->bell.maintenance, sp),
		_FormatBooleanYN),

	SetupElement ("AccessOnly",					"AO",
		SEF_ModuleAccess|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_Boolean, Offset (&sp->mode.access_only, sp),
		_FormatBooleanYN),
	SetupElement ("AccessAlwaysGlobal",			"AX",
		SEF_ModuleAccess|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		SYSTYPE_OnOffAuto, Offset (&sp->access.global, sp)),
	SetupElement ("AccessAlwaysMessageOn",		"XN",
		SEF_ModuleAccess|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->access.message.always_on, sp),
		&SysFieldMaxPrompt),
	SetupElement ("AccessAlwaysMessageOff",		"XF",
		SEF_ModuleAccess|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->access.message.always_off, sp),
		&SysFieldMaxPrompt),
	SetupElement ("AccessAlwaysDuration",		"AD",
		SEF_ModuleAccess|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		SYSTYPE_Delay, Offset (&sp->access.duration, sp)),
	SetupElement ("AccessAlwaysWindow",			"AW",
		SEF_ModuleAccess|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		SYSTYPE_AccessWindow, Offset (&sp->access.window, sp)),
	SetupElement ("AccessAlwaysBadges",			"AC",
		SEF_ModuleAccess|SEF_InterfaceComm5|SEF_InterfaceTA_s,
		SYSTYPE_LegacyAccessBadge, Offset (&sp->access.badge, sp),
		NULL, Offset (&sp->badge.length.valid, sp)),

	SetupElement ("ProfileMessage",				"PM",
		SEF_ModuleProfile|SEF_InterfaceComm5|SEF_InterfaceTA_s,
		SYSTYPE_LegacyProfileMessage, Offset (&sp->profile.message, sp),
		&_FormatProfileMessage),
	SetupElement ("ProfileMessageDefault",		"PX",
		SEF_ModuleProfile|SEF_InterfaceComm5|SEF_InterfaceTA_s,
		SYSTYPE_LegacyProfileDefault, Offset (&sp->profile.defaults, sp),
		&_FormatProfileDefault),
	SetupElement ("ProfileEntry",				"PF",
		SEF_ModuleProfile|SEF_InterfaceComm5|SEF_InterfaceTA_s,
		SYSTYPE_LegacyProfileEntry, Offset (&sp->profile.entry, sp),
		&_FormatProfileEntry),
	SetupElement ("ProfileAssignment",			"PE",
		SEF_ModuleProfile|SEF_InterfaceComm5|SEF_InterfaceTA_s,
		SYSTYPE_LegacyProfileAssign, Offset (&sp->profile.assign, sp),
        &_FormatProfileAssign, Offset (&sp->badge.length, sp)),
        
        
        
        
	SetupElement ("ProfileCompress",			"PC",
		SEF_ModuleProfile|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_Boolean, Offset (&sp->profile.mode.compress, sp),
		_FormatBooleanYN),
	SetupElement ("ProfileOverride",			"SO",
		SEF_ModuleProfile|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_Boolean, Offset (&sp->profile.mode.override, sp),
		_FormatBooleanYN),
	SetupElement ("ProfileReportFails",			"RA",
		SEF_ModuleProfile|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_Boolean, Offset (&sp->profile.mode.report_fails, sp),
		_FormatBooleanYN),

	SetupElement ("ValidateLevels",				"LL",
		SEF_ModuleValidate|SEF_InterfaceComm5|SEF_InterfaceTA_s,
		SYSTYPE_LegacyValidLevels, Offset (&sp->validate.level, sp),
		&_FormatValidateLevel),
	SetupElement ("ValidateDescription",		"LD",
		SEF_ModuleValidate|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_Boolean, Offset (&sp->validate.descriptions, sp),
		_FormatBooleanYN),
	SetupElement ("ValidateEntry",				"LE",
		SEF_ModuleValidate|SEF_InterfaceComm5|SEF_InterfaceTA_s,
		SYSTYPE_LegacyValidEntry, Offset (&sp->validate, sp),
		&_FormatValidateEntry, Offset (&sp->badge.length.valid, sp)),
	SetupElement ("ValidateMessage0",			"L0",
		SEF_ModuleValidate|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->validate.level[0].error_message, sp),
		&SysFieldMaxPrompt),
	SetupElement ("ValidateMessage1",			"L1",
		SEF_ModuleValidate|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->validate.level[1].error_message, sp),
		&SysFieldMaxPrompt),
	SetupElement ("ValidateMessage2",			"L2",
		SEF_ModuleValidate|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->validate.level[2].error_message, sp),
		&SysFieldMaxPrompt),
	SetupElement ("ValidateMessage3",			"L3",
		SEF_ModuleValidate|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->validate.level[3].error_message, sp),
		&SysFieldMaxPrompt),

	SetupElement ("PrintTransactions",			"PT",
		SEF_ModulePrint|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_Boolean, Offset (&sp->print.enable, sp),
		_FormatBooleanYN),
	SetupElement ("PrintHeaderLine1",			"H1",
		SEF_ModulePrint|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->print.header[0].line, sp),
		&SysFieldMaxHeader),
	SetupElement ("PrintHeaderLine2",			"H2",
		SEF_ModulePrint|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->print.header[1].line, sp),
		&SysFieldMaxHeader),
	SetupElement ("PrintHeaderLine3",			"H3",
		SEF_ModulePrint|SEF_InterfaceAcro|SEF_NoDisplay,
		SYSTYPE_LegacyHalfHeader, Offset (&sp->print.header[0].line, sp),
		&SysFieldMaxHeaderHalf),
	SetupElement ("PrintHeaderLine4",			"H4",
		SEF_ModulePrint|SEF_InterfaceAcro|SEF_NoDisplay,
		SYSTYPE_LegacyHalfHeader, Offset (&sp->print.header[1].line, sp),
		&SysFieldMaxHeaderHalf),
	SetupElement ("PrintLF",					"LF",
		SEF_ModulePrint|SEF_InterfaceAcro,
		TYPE_UBin8, Offset (&sp->print.LF_count, sp)),
	SetupElement ("PrintResetLine",				"RL",
		SEF_ModulePrint|SEF_InterfaceAcro,
		TYPE_String, Offset (&sp->print.reset, sp),
		&SysFieldMaxPrintReset),

	SetupElement ("FingerEnable",			"FS",
		SEF_ModuleFinger|SEF_InterfaceAcro,
		TYPE_Boolean, Offset (&sp->finger.enable, sp),
		_FormatBooleanYN),
	SetupElement ("FingerLevel",			"UF",
		SEF_ModuleFinger|SEF_InterfaceAcro,
		TYPE_UBin8, Offset (&sp->finger.level, sp)),
	SetupElement ("FingerWaitTime",			"FT",
		SEF_ModuleFinger|SEF_InterfaceAcro,
		SYSTYPE_Delay, Offset (&sp->finger.wait, sp)),
	SetupElement ("FingerSecurity",			"XS",
		SEF_ModuleFinger|SEF_InterfaceAcro,
		TYPE_String, Offset (&sp->finger.security, sp),
		&SysFieldMaxSecurity),
	SetupElement ("FingerTable",			"FZ",
		SEF_ModuleFinger|SEF_InterfaceAcro,
		SYSTYPE_LegacyFingerTable, Offset (&sp->finger.table, sp)),

	SetupElement ()};


/** Configuration data tags list. **/

const SetupElement SetupTagsConfig [] = {
	SetupElement ("SerialNumber",				"SN",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_String, Offset (&sp->name, sp),
		&SysFieldMaxName),

	SetupElement ("TerminalId",					"T#",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_Char, Offset (&sp->id, sp),
               "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ.-"),

	SetupElement ("RS232BaudRate",				"RS",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_EnumLabelUBin8, Offset (&sp->communications.RS232.baudrate, sp),
		&_FormatEnumBaudRates),

	SetupElement ("ModemBaudRate",				"MS",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_EnumLabelUBin8, Offset (&sp->communications.modem.baudrate, sp),
		&_FormatEnumBaudRates),
	SetupElement ("ModemAnswerTimes",			"MA",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_c,
		TYPE_UBin8, Offset (&sp->communications.modem.answer.time, sp)),
	SetupElement ("ModemRingDelay",				"MD",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		SYSTYPE_DelayShort, Offset (&sp->communications.modem.answer.ring_delay, sp)),
	SetupElement ("ModemExternal",				"EX",
		SEF_Base|SEF_InterfaceTA_i,
		TYPE_Boolean, Offset (&sp->communications.modem.external, sp),
		_FormatBooleanYN),

	SetupElement ("LANBaudRate",				"LS",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_EnumLabelUBin8, Offset (&sp->communications.RS485.baudrate, sp),
		&_FormatEnumBaudRates),
	SetupElement ("LANTurnAround",				"LT",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		SYSTYPE_Delay, Offset (&sp->communications.RS485.turnaround, sp)),

	SetupElement ("LightBox",					"LB",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_Boolean, Offset (&sp->hardware.lightbox, sp),
		_FormatBooleanYN),

	SetupElement ()};


/** Terminal function tags list. **/

const SetupElement SetupTagsFunction [] = {
	SetupElement ("ResetComplete",				"CR",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_s),
	SetupElement ("LockClockSwipes",			"ZZ",
		SEF_Base|SEF_InterfaceTACWin3|SEF_InterfaceComm5|SEF_InterfaceTA_i,
		TYPE_Boolean, Offset (&sp->mode.lock, sp),
		_FormatBooleanYN),

	SetupElement ("MemoryPartition",			"MP",
		SEF_Base|SEF_InterfaceComm5|SEF_InterfaceTA_s),

	SetupElement ()};


