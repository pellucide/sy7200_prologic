/*
	str_blank:  [Module]

	The str_blank module provides a function to scan past leading white
space in a supplied string.

	Copyright (c) 1992,1994-1995,2001 - Alcom Communications

	Written by:   Alan J. Luse
	Written on:   18 April 1991
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   21 Feb 2001 11:02:22  $
	     $Date: 2005/09/01 22:27:58 $

*/


/* Include files for str_blank. */
#include <ctype.h>
#include "Str_Def.h"

/* Constant parameters for str_blank. */

/* Type and structure definitions for str_blank. */

/* Internal static data for str_blank. */

/* Function prototypes for str_blank. */



/*
	str_blank:

	Skip over leading white space in a string.

Description:

	Return a string pointer that points past any leading white space
starting from the string pointer supplied.  White space is defined to
be any character for which the isspace function (macro) returns TRUE.

Notes:

	The function str_blank is not an ANSI C function put is commonly
provided by many compiler libraries under this or another name.  It
is implemented here identically to the Lattice C compiler version. 
The function is also used internally within the library to implement
other ANSI C functions.

See also:  strspn

Arguments:
	string		Pointer to input string to be scan for leading white
					space.

Returns:
	char *		Pointer to the first non-white space character found.

*/

		char *
str_blank (const char * string)

{

	while (isspace (*string)) {
		string++;
		}

	return ((char *)string);
}


