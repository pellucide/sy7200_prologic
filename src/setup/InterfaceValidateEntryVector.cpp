#include "InterfaceValidateEntryVector.hpp"

InterfaceValidateEntryVector::InterfaceValidateEntryVector()
{
}

InterfaceValidateEntryVector::~InterfaceValidateEntryVector()
{
}

void InterfaceValidateEntryVector::add(SysValidateEntryItem assign)
{
    vect.push_back( assign );
}

void InterfaceValidateEntryVector::remove(SysValidateEntryItem item)
{
    
}

int InterfaceValidateEntryVector::size()
{
    return vect.size();
}

void InterfaceValidateEntryVector::clear()
{
    vect.clear();
}

SysValidateEntryItem InterfaceValidateEntryVector::Get(int index)
{
    SysValidateEntryItem& assign = *vect[index];
    return assign;
}


