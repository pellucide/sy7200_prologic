#include "logger.h"

// Test Program
int main(int argc, char *argv[])
{
	Logger * logger = Logger::getLogger();
	
	logger->setAppLogLevel(FileAppender::LEVEL_ERROR);
	
	logger->debug("Logging debug messages");
	logger->info("Logging info messages");
	logger->warn("Logging warn messages");
	logger->error("Logging error messages");
	logger->fatal("Logging fatal messages");
	
	delete logger;
	return 0;
}
