/*
	UserBarCode:  [Module]

    The UserBarCode module provides interface routines to configure and
access the barcode decoder chip.

	Copyright (c) 2005
	Alan J. Luse <AlanL@TimeAmerica.com>, TimeAmerica, Inc.

	Written by:   Alan J. Luse
	Written on:   14 Dec 2005
	 $Revision: 1.7 $
	   $Author: mahesh $
	  $Modtime$
	     $Date: 2006/02/02 22:19:12 $

*/


/* Compilation options for UserBarCode. */
#define DEBUG                   0

/* Include files for UserBarCode. */
#include "Gen_Def.h"
#include "User.h"
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <time.h>
#include <errno.h>

/* Debug global defines. */
#if defined (DEBUG) && DEBUG > 0
	#define debug(fmt, args...) printf(fmt, ## args)
	#define debugF(fmt, args...) printf("%s: " fmt, __func__, ## args)
	#define debugX(lvl, fmt, args...) if (DEBUG >= lvl) debug(fmt, ## args);
	#define debugFX(lvl, fmt, args...) if (DEBUG >= lvl) debugF(fmt, ## args);
#else
	#define debug(fmt, args...)
	#define debugF(fmt, args...)
	#define debugX(lvl, fmt, args...)
	#define debugFX(lvl, fmt, args...)
#endif

/* Constant parameters for UserBarCode. */
#define BARCODE_DEVICE		"/dev/tts/1"
#define BARCODE_BAUDRATE	9600
#define STX			0x02
#define ETX			0x03
#define ACK			0x06
#define NAK			0x15
static const Byte BarcodeCmdSend [] = {'S'};
static const Byte BarcodeCmdRecv [] = {'R'};
static const Byte BarcodeSetStdInit [] = {'S', 0x13, 0x01, 0x40, 0x31, 0x02, 0x01, 0x0A};
static const Byte BarcodeSetRestore [] = {'S', 0x16};

/* Type and structure definitions for UserBarCode. */
typedef long TimerInterval_t;
typedef struct {
	int file;
	}
UserBarcode_t;

/* Internal static data for UserBarCode. */
static UserBarcode_t _ub = {
	.file = -1,
	};

/* External global data for UserBarCode. */

/* Function prototypes for UserBarCode. */
static int      _barcode_send (const Byte * buffer, int length);
static int      _barcode_write (const Byte * buffer, int length, Byte * checksum_p);
static Byte     _barcode_checksum (const Byte * buffer, int length, Byte checksum);
static int      _barcode_confirm (TimerInterval_t timeout);

/* Inline functions for UserBarCode. */
#define MS_PER_SEC      1000
		static inline void
_timer_reset (TimerInterval_t * const time_stamp_p, TimerInterval_t milliseconds)

{
	*time_stamp_p = clock () +
		(clock_t)milliseconds * (clock_t)(CLOCKS_PER_SEC / MS_PER_SEC);
	return;
}
		static inline TimerInterval_t
_timer_elapsed (const TimerInterval_t * const time_stamp_p)

{
	TimerInterval_t elapsed;
	clock_t difference;

	difference = clock () - *time_stamp_p;
	elapsed = (TimerInterval_t)(difference /
		(clock_t)(CLOCKS_PER_SEC / MS_PER_SEC));

	return (elapsed);
}
		static inline Boolean
_timer_done (const TimerInterval_t * const time_stamp_p)

{
	return (((clock () - *time_stamp_p) >= 0) ? YES : NO);
}


/*
	user_barcode_setup:

    Setup the barcode decoder chip configuration for normal use.

Description:

    Configure the barcode decoder chip for normal data collection terminal
operation.

See also:  user_init

Arguments:
	void

Returns:
	int		0 for success;

*/

		int
user_barcode_setup (void)

{
	int status = 0;

	for (;;) {

		status = user_barcode_open (NULL, 0);
		if (status < 0) break;

		debugFX (2, "Sending barcode decoder standard settings.\n");
		status = _barcode_send (BarcodeSetStdInit, sizeof BarcodeSetStdInit);
		if (status < 0) break;

		debugFX (2, "Waiting for barcode decoder confirmation response.\n");
		status = _barcode_confirm (200 /* milliseconds */);
		if (status < 0) break;
	
		debugFX (2, "Setup complete.\n");
		break;
	}

	user_barcode_close ();

	debugFX (2, "Returning with status code %d.\n", status);

	return (status);
}


/*
	user_barcode_restore:

    Restore the barcode decoder chip configuration to factory defaults.

Description:

    Instruct the barcode decoder to restore factory default settings.

See also:

Arguments:
	void

Returns:
	int		0 for success;

*/

		int
user_barcode_restore (void)

{
	int status = 0;

	for (;;) {

		status = user_barcode_open (NULL, 0);
		if (status < 0) break;

		debugFX (2, "Request restoration of barcode decoder factory settings.\n");
		status = _barcode_send (BarcodeSetRestore, sizeof BarcodeSetRestore);
		if (status < 0) break;
	
		debugFX (2, "Waiting for barcode decoder confirmation response.\n");
		status = _barcode_confirm (2000 /* milliseconds */);
		if (status < 0) break;
	
		debugFX (2, "Restore complete.\n");
		break;
	}

	user_barcode_close ();

	debugFX (2, "Returning with status code %d.\n", status);

	return (status);
}


/*
	user_barcode_open:

    Open the device file to the barcode decoder.

See also:  user_barcode_setup

Arguments:
	device		Device name for barcode decoder;
			NULL to use default device.
	
	baudrate	Baud rate to use;
			0 to use default baud rate.	

Returns:
	int		0 successful;
			-1 file setup error;
			-5 file open error;

*/

		int
user_barcode_open (const char * device, int baudrate)

{
	int status = 0;

	if (device == NULL || *device == '\0') device = BARCODE_DEVICE;
	if (baudrate == 0) baudrate = BARCODE_BAUDRATE;

	debugFX (2, "Opening barcode decoder device file %s.\n", device);

	for (;;) {
		_ub.file = open (device, O_RDWR|O_NONBLOCK);
		if (_ub.file < 0) {
			status = -5;
			break;
		}
	
		/* Check if file is a terminal; if not skip terminal settings. */
		if (NOT isatty (_ub.file)) break;

		debugFX (3, "Setting TTY flags and baud rate %d.\n", baudrate);

		struct termios tios;
		status = tcgetattr (_ub.file, &tios);
		if (status != 0) break;

		/* Adjust terminal attributes:
			9600-8-N-1
			raw, no echo
			*/
		cfmakeraw (&tios);
		/*
		tios.c_iflag &= ~(INPCK|ISTRIP|IGNCR|ICRNL|IXOFF|IXON);
		tios.c_oflag &= ~(OPOST|ONLCR|OCRNL|ONOCR|ONLRET);
		tios.c_cflag &= ~(CLOCAL|CSTOPB|PARENB|CRTSCTS|CSIZE);
		tios.c_cflag |= CS8;
		tios.c_lflag &= ~(ICANON|ECHO|ISIG);
		*/
		status = cfsetspeed (&tios, baudrate);
		if (status != 0) break;

		status = tcsetattr (_ub.file, TCSANOW, &tios);
		if (status != 0) break;

		/* Flush device stream. */
		tcflush (_ub.file, TCIOFLUSH);

		break;
	}

	return (status);
}


/*
	user_barcode_close:

    Close the device file to the barcode decoder.

See also:  user_barcode_setup

Arguments:
	void

Returns:
	void

*/

		void
user_barcode_close (void)

{

	if (_ub.file >= 0) {
		debugFX (2, "Closing barcode decoder device file.\n");
		close (_ub.file);
		_ub.file = -1;
	}

	return;
}


/*
	_barcode_send:

    Send data message to barcode decoder.

See also:

Arguments:
	buffer		Pointer to storage for data read.
	
	length		Length of data buffer.

Returns:
	int		0 if successful;

*/

		static int
_barcode_send (const Byte * buffer, int length)

{
	int status = 0;
	Byte temp [2];
	int tlen;
	Byte checksum = 0x00;

	debugFX (4, "Send bytes:\n");

	for (;;)
	{
		temp[0] = STX;
		tlen = 1;
		status = _barcode_write (temp, tlen, &checksum);
		if (status < 0) break;

		status = _barcode_write (buffer, length, &checksum);
		if (status < 0) break;

		temp[0] = ETX;
		temp[1] = 0x00;
		tlen = 2;
		temp[1] = checksum = _barcode_checksum (temp, tlen, checksum);
		status = _barcode_write (temp, tlen, &checksum);
		if (status < 0) break;

		status = 0;
		break;
	}

	debugX (4, "\n");

	return (status);
}


/*
	_barcode_write

    Write data to device with checksum calculation.

See also:

Arguments:
	buffer		Pointer to storage for data read.
	
	length		Length of data buffer.

	checksum_p	Pointer to checksum to be updated for data written;
			Initial checksum value typically from previous call;
			0x00 for an initial call.

Returns:
	int		>=0 number of bytes written;
			-2 timeout (too many write tries);
			-5 file system error;

*/

		static int
_barcode_write (const Byte * buffer, int length, Byte * checksum_p)

{
	int status = 0;
	const Byte * bp = buffer;
	TimerInterval_t timer;
	const TimerInterval_t timeout = 100 /* milliseconds */;
	ssize_t bytes;

	*checksum_p = _barcode_checksum (buffer, length, *checksum_p);

	_timer_reset (&timer, timeout);
	while (status == 0) {
		bytes = write (_ub.file, bp, length);
		if (_timer_done (&timer)) {status = -2; break;}
		else if (bytes == 0) continue;
		else if (bytes < 0) {
			if (errno == EAGAIN) continue;
			status = -5;
			break;
		}
		else {
			#if defined (DEBUG) && DEBUG >= 4
				int len = bytes;
				const Byte * xbp = bp;
				while (len--) printf (" %02X", *xbp++);
			#endif
			bp += bytes;
			if (bp - buffer >= length) break;
		}
	}

	if (status == 0) status = bp - buffer;

	return (status);
}


/*
	_barcode_checksum:

    Calculate XOR checksum of a buffer.

Arguments:
	buyffer		Pointer to buffer to checksum.

	int		Length of buffer.

	checksum	Initial checksum value typically from previous call;
			0x00 for an initial call.

Returns:
	Byte		Checksum value.

*/

		static Byte
_barcode_checksum (const Byte * buffer, int length, Byte checksum)

{

	while (length--) checksum ^= *buffer++;

	return (checksum);
}


/*
	_barcode_confirm:

    Read ACK or NAK response from device.

See also:

Arguments:
	timeout		Maximum time to wiat for confirmation reply
			in milliseconds.

Returns:
	int	0 if ACK received;
		-1 NAK reply received;
		-2 timeout (too many read tries);
		-3 unrecognized reply;
		-5 file system error;
*/

		static int
_barcode_confirm (TimerInterval_t timeout)

{
	int status = 1;
	Byte buffer [16];
	TimerInterval_t timer;
	ssize_t bytes;

	_timer_reset (&timer, timeout);
	while (status == 1) {

		bytes = read (_ub.file, buffer, sizeof buffer);
		if (_timer_done (&timer)) {status = -2; break;}
		else if (bytes == 0) continue;
		else if (bytes < 0) {
			if (errno == EAGAIN) continue;
			status = -5;
			break;
		}

		else if (buffer[0] == ACK) status = 0;
		else if (buffer[0] == NAK) status = -1;
		else status = -3;

	}
	debugFX (3, "Spent %ld ms waiting for confirmation.\n",
		 _timer_elapsed (&timer) + timeout);

	return (status);
}


/*
	user_barcode_read_settings:

    Read current settings data from barcode decoder.

Description:

    The complete set of current decoder settings is read and returned.

Notes:

    Due to the non-layered structure of the protocol this routine must
parse the contents of the function blocks and other data in the reply data
in order to properly receive the reply message.

See also:

Arguments:
	buffer		Pointer to storage for data read.
	
	max_length	Maximum length of data buffer.

Returns:
	int		>=0 numbers of bytes of data read;
			-1 NAK reply received;
			-2 timeout (too many read tries);
			-3 STX not found as first message byte;
			-4 checksum error;
			-5 file system error;

*/

		int
user_barcode_read_settings (Byte * buffer, int max_length)

{
	int status = 0;
	enum {
		RM_CHK_ACK,
		RM_GET_STX,		/* Looking for initial STX. */
		RM_BLOCK,		/* Get a block of data of length blk_count. */
		RM_GET_FUNC_CODE,	/* Looking for function code or ETX. */
		RM_GET_FUNC_LEN,	/* Looking for function block length. */
		RM_AT_ETX,		/* Got ETX; get checksum byte. */
		RM_DONE,		/* Message complete. */
	}
	mode = RM_CHK_ACK;

	status = _barcode_send (BarcodeCmdRecv, sizeof BarcodeCmdRecv);

	debugFX (4, "Read bytes:\n");

	Byte checksum = 0x00;
        Byte * bp = buffer;
	TimerInterval_t timer;
	const TimerInterval_t timeout = 100 /* milliseconds */;
	ssize_t blk_count = 0;
	ssize_t bytes = 0;
	int function = 0;
	ssize_t blk_length = 0;
	const Byte * blk_p = bp;

	_timer_reset (&timer, timeout);
	while (mode != RM_DONE && status == 0) {

		if (bytes <= 0) {
			size_t max_read = buffer+max_length-bp;
			/* Read single bytes to allow discard until body of reply. */
			if (mode == RM_CHK_ACK || mode == RM_GET_STX) max_read = 1;
			bytes = read (_ub.file, bp, max_read);
			if (_timer_done (&timer)) {status = -2; break;}
			else if (bytes == 0) continue;
			else if (bytes < 0) {
				if (errno == EAGAIN) continue;
				status = -5;
				break;
			}

			#if defined (DEBUG) && DEBUG >= 3
				if (mode == RM_CHK_ACK) {
					debugFX (3, "Took %ld ms to get first reply data.\n",
						 _timer_elapsed (&timer) + timeout);
				}
			#endif
			#if defined (DEBUG) && DEBUG >= 4
				int len = bytes;
				const Byte * xbp = bp;
				printf("  [%d]", len);
				while (len--) printf (" %02X", *xbp++);
				printf("\n");
				debugX(5, "  bytes = %d, mode = %d, function = %02X, blk_length = %d, blk_count = %d.\n",
				       bytes, mode, function, blk_length, blk_count);
			#endif
		}

		switch (mode)
			{

		case RM_CHK_ACK:
			if (*bp == ACK) mode = RM_GET_STX;
			else if (*bp == NAK) status = -1;
			bytes--;
			break;

		case RM_GET_STX:
			if (*bp == STX) {
				blk_count = blk_length = 2; /* Version length. */
				blk_p = bp;
				bytes--;
				/* Put STX in checksum now since its not kept in buffer. */
				checksum = _barcode_checksum (bp, 1, 0x00);
				mode = RM_BLOCK;
			}
			else status = -3;
			break;

		case RM_BLOCK:
			{
			int chunk = (blk_count > bytes) ? bytes : blk_count;
			bp += chunk;
			bytes -= chunk;
			blk_count -= chunk;
			}
			if (blk_count == 0) {
				if (function == 0x00) {
					debugX (1, "  Version = %d.%d.\n",
						blk_p[0], blk_p[1]);
				}
				else {
					#if defined (DEBUG) && DEBUG >= 1
						printf ("  Function %02X [%d]", function, blk_length);
						int len = blk_length;
						const Byte * xbp = blk_p;
						while (len--) printf (" %02X", *xbp++);
						printf ("\n");
					#endif
				}
				mode = RM_GET_FUNC_CODE;
			}
			break;

		case RM_GET_FUNC_CODE:
			if (*bp == ETX) {
				debugX (4, "  Found ETX.\n");
				bytes--;
				bp++;
				mode = RM_AT_ETX;
			}
			else {
				/* New function block; save function code. */
				function = *bp++;
				bytes--;
				mode = RM_GET_FUNC_LEN;
			}
			break;

		case RM_GET_FUNC_LEN:
			/* Save Block length and block data pointer. */
			blk_count = blk_length = *bp++;
			blk_p = bp;
			bytes--;
			mode = RM_BLOCK;
			break;

		case RM_AT_ETX:
			checksum = _barcode_checksum (buffer, bp - buffer, checksum);
			debugX (4, "  Check byte = %02X, checksum = %02X.\n",
				*bp, checksum);
			if (checksum == *bp) {
				mode = RM_DONE;
				bp--; /* Remove ETX from return length. */
				status = bp - buffer;
			}
			else status = -4;
			break;

		case RM_DONE:
			break;
		}
	}

	/* Return length if no errors. */
	if (status >= 0) status = bp-buffer;
	debugFX (3, "Spent %ld ms waiting for reply message.\n",
		 _timer_elapsed (&timer) + timeout);
	debugFX (2, "Exit status = %d.\n", status);

	return (status);
}


