#include <string.h>
#include <unistd.h>
#include "StdInput.h"

StdInput* StdInput::singleton=NULL;



StdInput::StdInput(){
	lineBuffer=new char[__MAXLINELEN__];
	setBufferMode(_IONBF);
#ifdef __TA7700_TERMINAL__
	if (user_input_init()!=SC_OKAY){
#ifdef DEBUG
		printf ("Failed to initialize user input\n");
#endif
	}
#endif
}

StdInput::~StdInput (){
	if (NULL!=lineBuffer) delete[] lineBuffer;
#ifdef __TA7700_TERMINAL__
	user_input_close();
#endif
}

StdInput* StdInput::getInputDevice (){
	if (NULL==singleton){
		singleton=new StdInput();
	}
	return singleton;
}

char* StdInput::gets(void){
	int		lastRead=0;
	int		bytesInBuffer=0;
	char	*inBuffer=lineBuffer;

	setBufferMode(_IONBF);
	memset(lineBuffer,0x00,__MAXLINELEN__);

	while (((lastRead = get()) != '\n')&&bytesInBuffer<__MAXLINELEN__-1){
		if (lastRead!=EOF){
			*(inBuffer++)=lastRead;
			bytesInBuffer++;
		}
	}
	return lineBuffer;
}

int StdInput::get(void){
	int retVal=EOF;
#ifdef __TA7700_TERMINAL__
	if (user_input_ready()){
		retVal=(int)user_input_get();
		printf ("%c",(char)retVal);
		fflush(stdout);
	}
	else usleep(10000);
#else
	retVal=getchar();
#endif

	return retVal;
}

void StdInput::setBufferMode (int mode){
#ifndef __TA7700_TERMINAL__
	if (this->mode!=mode){
		setvbuf (stdin,NULL,mode,0);
	}
#endif
}

