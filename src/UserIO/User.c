/*
	User:

    Interface functions to communicate with the NE7000 user interface
board.

	Copyright (c) 2005
	Alan J. Luse <AlanL@TimeAmerica.com>, TimeAmerica, Inc.

	Written by:   Alan J. Luse
	Written on:   25 Jan 2005
	 $Revision: 1.5 $
	   $Author: walterm $
	  $Modtime$
	     $Date: 2006/01/06 00:58:09 $

*/


/* Compilation optins for User. */
#define DEBUG                   0

/* Configuration include file for User. */

#if defined CONFIG_NE7000
#define OPT_U_BOOT              1       /* Use U-BOOT direct call I2C driver. */
#else
#define OPT_U_BOOT              0       /* Use Linux I2C device driver. */
#endif

#if ! OPT_U_BOOT || defined CONFIG_NE7000_USER || defined CONFIG_SPLASH_TEXT

/* Include files for User. */
#include "User.h"
#if OPT_U_BOOT
#include "I2C.h"
#else /* Kernel driver */
#include <StdIO.h>
#include <String.h>
#include <FCntl.h>
#include <UniStd.h>
#include <Linux/i2c-dev.h>
#endif /* ! OPT_U_BOOT */

/* Debug global defines. */
#if defined (DEBUG) && DEBUG > 0
	#define debugF(fmt, args...) printf("%s: " fmt, __func__, ## args)
	#define debugFX(lvl, fmt, args...) if (DEBUG >= lvl) debugF(fmt, ## args);
#else
	#define debugF(fmt, args...)
	#define debugFX(lvl, fmt, args...)
#endif
#if ! OPT_U_BOOT
	//#define printf printk
#endif

/* Constant parameters for User. */
#define NAME "User"
static const int I2C_ADAPTOR = 0;
static const Byte I2C_ID_UI7000 = 0x1C;
#define STATUS_KEYDOWN          0x01    /* Key currently held down. */
#define STATUS_NEW              0x02    /* New, single key press. */
#define STATUS_MULTI            0x04    /* Multiple key press. */
#define STATUS_SIGNATURE        0x50    /* Status byte high nibble constant. */
#define STATUS_SIGN_MASK        0xF0    /* Status byte high nibble. */
typedef enum {
		CMD_LCD_INIT    = 0x10,
		CMD_LCD_CLEAR   = 0x11,
		CMD_LCD_WRITE   = 0x12,
		CMD_LCD_CURSOR  = 0x13,
		CMD_LCD_CONTRAST= 0x20,

}
UserCmd_e;
const size_t HEADER_LEN_WRITE = 3;

/* Keycode to ASCII character translation table. */

/* Type and structure definitions for User. */
typedef struct {
	struct {
		Boolean ready;
		char next;
		}
	input;
	int file;
	}
UserData_t;
typedef union {
	Byte buffer [2];
	struct {
		Byte status;
		Byte keycode;
	} data;
}
InputData_t;

/* Internal static data for User. */
static UserData_t _ud = {{NO, '\0'}, -1};

/* Function prototypes for User. */
static Boolean  _input_poll (void);
static Code     _output_send (const Byte * buffer, size_t length);


/*
	user_init:

    Initialize the User module.

Description:

    Initialize User module.

Arguments:
	void

Returns:
	Code    System status code of SC_OKAY (0) for successful completion;
		SC_USER_MISSING_ADAPTOR if bus adaptor driver not found;
		SC_USER_DEVICE_ADDRESS if user interface device not;

*/

		Code
user_init (void)

{
	Code code = SC_OKAY;

	_ud.input.ready = NO;
	_ud.input.next = '\0';

	for (;;) {

		#if OPT_U_BOOT

		i2c_init (0, 0);    /* Current implementation ignores both parameters. */

		/* See if user interface board is present and talking. */
		if (i2c_probe (I2C_ID_UI7000)) code = SC_FAILURE;

		#else /* Use kernel driver */
		char filename [20];
		int status;

		sprintf (filename, "/dev/i2c/%d", I2C_ADAPTOR);
		debugFX (1, "Opening I2C adaptor \"%s\".\n", filename);
		_ud.file = open (filename, O_RDWR);
		if (_ud.file < 0) {
			code = SC_USER_MISSING_ADAPTOR;
			break;
		}

		status = ioctl (_ud.file, I2C_SLAVE, I2C_ID_UI7000);
		if (status < 0) {
			code = SC_USER_DEVICE_ADDRESS;
			break;
		}

		user_barcode_setup ();

		#endif /* OPT_U_BOOT */

		user_output_initialize ();

		break;
	}

	return (code);
}


/*
	user_close:

    Close the User module.

Description:

    Close User module data.

Arguments:
	void

Returns:
	Code    System status code of SC_OKAY for successful completion;

*/

		Code
user_close (void)

{
	Code code = SC_OKAY;

	_ud.input.ready = NO;
	_ud.input.next = '\0';

	#if OPT_U_BOOT
	#else /* Kernel driver */

	#endif /* OPT_U_BOOT */

	return (code);
}


/*
	user_input_ready:

    Check if an input character is available.

Description:

See also:  user_input_get

Arguments:
	void

Returns:
	Boolean YES if an input character is available;
		NO otherwise.

*/

		Boolean
user_input_ready (void)

{

	if (NOT _ud.input.ready) 
	{
		_input_poll ();
	}

	return (_ud.input.ready);
}


/*
	user_input_get:

    Get an input character if one is available.

Description:

    If a character has previously been read return it.  Otherwise check
board to see if one is available.

See also:  user_input_ready

Arguments:
	void

Returns:
	char    Input character if one is available;
		Null character '\0' if not.

*/

		char
user_input_get (void)

{
	char input;

	if (NOT _ud.input.ready) _input_poll ();

	input = _ud.input.ready ? _ud.input.next : '\0';
	_ud.input.ready = NO;

	return (input);
}


/*
	_input_poll:

    Poll user interface for new input characters.

Notes:

    Do not call this polling routine if a character is already in local
data; it may get overwritten.

Arguments:
	void

Returns:
	Boolean YES if a new character was received;
		No if not.

*/

		static Boolean
_input_poll (void)

{
	InputData_t reply;
	int status;

	for (;;) {

		#if OPT_U_BOOT
		status = i2c_read (I2C_ID_UI7000, 0, 0, reply.buffer, sizeof reply.buffer);
		if (status) break;
		#else /* Use kernel driver */
		//reply.buffer[1] = 'C';
		//printf("befor poll read reply.buffer = %c\n",reply.buffer[1]);
		status = read (_ud.file, reply.buffer, sizeof reply.buffer);
		//printf("after poll read reply.buffer = %c\n",reply.buffer[1]);
		if (status != sizeof reply.buffer) {
			debugFX (1, "I2C read returned %d bytes instead of %zu.\n",
				   status, sizeof reply.buffer);
			break;
		}
		#endif /* ! OPT_U_BOOT */

		debugFX (2, "I2C read returned status %.2Xh & character %.2Xh.\n",
			   reply.data.status, reply.data.keycode);

		debugFX (2, "Checking for proper status values.\n");
		if ((reply.data.status & STATUS_SIGN_MASK) == STATUS_SIGNATURE) {
			debugFX (2, "I2C read found proper status.\n");
			if (reply.data.status & STATUS_NEW) {
				_ud.input.next = reply.data.keycode;
				_ud.input.ready = YES;
				debugFX (1, "I2C read of input character %.2X.\n", _ud.input.next);
			}
		}

		break;
	}

	return (_ud.input.ready);
}


/*
	user_output_initialize:

    Initialize or reinitialize the LCD display.

Description:

    The display is initialized, cleared and the cursor position set to row
1, column 1.

Arguments:
	void

Returns:
	Code    System status code of SC_OKAY for successful completion;

*/

		Code
user_output_initialize (void)

{
	Code code = SC_OKAY;
	const Byte command [] = {CMD_LCD_INIT};

	code = _output_send (command, sizeof command);

	return (code);
}


/*
	user_output_clear:

    Clear the LCD display.

Description:

    The display is cleared and the cursor position set to row 1, column 1.

Arguments:
	void

Returns:
	Code    System status code of SC_OKAY for successful completion;

*/

		Code
user_output_clear (void)

{
	Code code = SC_OKAY;
	const Byte command [] = {CMD_LCD_CLEAR};

	code = _output_send (command, sizeof command);

	return (code);
}


/*
	user_output_write:

    Write a string to the LCD display.

Description:

    Write the supplied string to the LCD display.  Printable ASCII
characters are displayed; certain control characters cause their normal
display actions (see Notes below).

Notes:

    The LCD Write command optionally positions the cursor then writes data
to successive display positions.  Its data consists of a pair of cursor
position bytes followed, optionally, by data to write to the display.  The
first position byte specifies the LCD character row while the second
position byte specifies the LCD character column.  The rows and columns
are numbered starting with 1.  A row or column position of 0 indicates the
respective value does not change instead the current cursor row and/or
column is used.

    Any bytes following the two positioning bytes are written to the
display as ASCII data starting at the present cursor position as defined
by the positioning bytes.  If encountered, any of the 32 character codes
in the range 00h to 1Fh are not sent to the display.  Some of the values
in this range perform their standard ASCII functions as follows:

	NewLine         '\n' [0Ah]   Position to column 1 of the next row.
	Carriage Return '\r' [0Dh]   Position to column 1 of the current row.
	Form Feed       '\f' [0Ch]   Position to row 1, column 1.

    Non-ASCII characters codes in the range 80h to FFh are sent to the
display without further processing and are assumed to be displayable
characters.  The resulting display is hardware dependent and left to the
Master Controller developer and driver for its specifics.

Arguments:
	row     Position to the specified row before writing;
		0 use current row setting.

	column  Position to the specified column before writing;
		0 use current column setting.

	display String to write to display (see Notes).

Returns:
	Code    System status code of SC_OKAY for successful completion;

*/

		Code
user_output_write (UBin8 row, UBin8 column, const char * display)

{
	Code code = SC_OKAY;
	Byte buffer [strlen (display) + HEADER_LEN_WRITE + 1];
	Byte * bp = buffer;
	const size_t max_len = sizeof buffer - HEADER_LEN_WRITE - 1;
	size_t length;

	*bp++ = CMD_LCD_WRITE;
	*bp++ = row;
	*bp++ = column;
	length = strlen (display);
	length = length < max_len ? length : max_len;
	*bp = '\0';
	debugFX (1, "Copy %d display characters to buffer.\n", length);
	strncat (bp, display, length);
	length += HEADER_LEN_WRITE;

	code = _output_send (buffer, length);

	return (code);
}


/*
	user_output_cursor:

    Set cursor mode for LCD display.

Description:

    The cursor mode for the display is set to the specified mode.  Cursor
can be off, blink character at cursor or display underline cursor.

Arguments:
	mode    Bit values from USER_CURSOR_* defines.

Returns:
	Code    System status code of SC_OKAY for successful completion;

*/

		Code
user_output_cursor (Bit8 mode)

{
	Code code = SC_OKAY;
	Byte command [2] = {CMD_LCD_CURSOR};

	command[1] = mode;

	code = _output_send (command, sizeof command);

	return (code);
}

/*
	user_output_test:

    Test LCD display.

Description:

    Perform a display test by filling the display region requested with a
test character.

Arguments:
	rows		Number of display rows to fill.
	
	columns		Number of display columns to fill.

Returns:
	void

*/

		Code
user_output_test (UBin8 rows, UBin8 columns)

{
	Code status = SC_OKAY;
	char buffer [columns+1];
	static const int test_char = 0xFF;

	buffer [columns] = '\0';
	memset (buffer, test_char, columns);

	user_output_write (1, 1, "");
	UBin8 ridx;
	for (ridx = 1; ridx <= rows; ridx++) {
		user_output_write (ridx, 1, buffer);
	}
	user_output_write (1, 1, "");

	return (status);
}

/*
	_output_send:

    Send (write) output to I2C interface.

Arguments:
	buffer  Buffer to write out to interface.

	length  Number of bytes to write.

Returns:
	Code    System status code of SC_OKAY for successful completion;

*/

		Code
_output_send (const Byte * buffer, size_t length)

{
	Code code = SC_OKAY;
	int status;

	switch (*buffer)
	{
	case CMD_LCD_INIT:
	case CMD_LCD_CLEAR:
		debugFX (1, "Write cmd %.02X to display.\n", *buffer);
		break;
	case CMD_LCD_WRITE:
		debugFX (1, "Write cmd %.02Xh to display at %d,%d \"%.*s\".\n",
				*buffer, *(buffer+1), *(buffer+2), length-HEADER_LEN_WRITE,
				buffer+HEADER_LEN_WRITE);
		break;
	}

	#if OPT_U_BOOT
	status = i2c_write (I2C_ID_UI7000, 0, 0, buffer, length);
	if (status > 0) {
		debugFX (1, "I2C write had %d failures for %u bytes.\n",
			   status, length);
		code = SC_FAILURE;
	}
	#else /* Use kernel driver */
	status = write (_ud.file, buffer, length);
	if (status != length) {
		debugFX (1, "I2C write sent %d bytes instead of %u.\n",
			   status, length);
		code = SC_FAILURE;
	}
	#endif /* ! OPT_U_BOOT */

	return (code);
}

/*
	user_output_contrast:

    Set LCD contrast.

Description:

    Set bias voltage to character LCD to adjust contrast of display.

Notes:

    Only the high 10 bits of the contrast setting are significant in the
actual hardware setting.

Arguments:
	contrast	Unsigned 16 bit contrast setting.

Returns:
	Code		System status code of SC_OKAY for successful completion;

*/

		Code
user_output_contrast (UBin16 contrast)

{
	Code code = SC_OKAY;
	Byte buffer [3];

	buffer[0] = CMD_LCD_CONTRAST;
	buffer[1] = (contrast >> 8) & 0xFF;
	buffer[2] = contrast & 0xFF;

	code = _output_send (buffer, sizeof buffer);

	return (code);
}

#endif /* ! OPT_U_BOOT || defined CONFIG_NE7000_USER || defined CONFIG_SPLASH_TEXT */

