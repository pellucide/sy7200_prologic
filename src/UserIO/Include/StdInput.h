
#ifndef __STDINPUT_H__

#include <termios.h>
#include <stdio.h>
#ifdef __TA7700_TERMINAL__
extern "C"{
	#include "User.h"
}
#endif



#define __MAXLINELEN__ 128
#define BLANK_LINE "                    "
#define CURSOR ">"
#define BLANKSPACE " "

//StdInput is written as a singleton (i.e there is only one object ever)
//this single object is retrieved with a call to getInputDevice()
//Currently there isn't a mutext to guard the object so it isn't thread safe
//So make certain that only one thread ever has access to the object.

class StdInput{

private:
	char *lineBuffer;
	int	 mode;
	static StdInput *singleton;
private: 
	StdInput();
protected:
	void setBufferMode(int mode);
public:
	virtual ~StdInput();
	static	StdInput *getInputDevice(void);
	virtual char	 *gets(void);
	virtual int		 get(void);
};




#define __STDINPUT_H__
#endif
