#ifndef _LCDHANDLER_H_
#define _LCDHANDLER_H_

#include "StdInput.h"
#include "Operation.hpp"
#include "Trans.hpp"

enum LCD_READ_STATE
{
	READ_TIMEDOUT = 0,
	READ_CLEAR,
	READ_SUCCESS
};

static char alpha_numeric_table[10][3];
class LCDHandler
{
public:
	LCDHandler();
	virtual ~LCDHandler();
	LCD_READ_STATE ReadDate(int row, int col, char * buffer);
	LCD_READ_STATE ReadTime(int row, int col, char * buffer);
	LCD_READ_STATE ReadData(SysInputType_e type, int row, int col, int timeout, int maxChars, int min, char * buffer,char* defaultData = NULL);
	//LCD_READ_STATE ReadLevelData(SysInputType_e type, int row, int col, int timeout, int maxChars, int min, char * buffer);
	LCD_READ_STATE ReadLevelData(SysInputType_e type, SysInputSource level_src, int row, int col, int timeout, int maxChars, int min, char * buffer , char* defaultData = NULL);
	LCD_READ_STATE ReadDecimal(int row, int col, int timeout, int maxChars, int min, int decimal, char * buffer,char* defaultData = NULL);

	// Reads the IP Address in the format 000.000.000.000
	LCD_READ_STATE ReadIPAddress(int row, int col, int timeout, char * ipaddr);

	// Used to Read WEP Keys
	LCD_READ_STATE ReadHex(int row, int col, int timeout, int maxChars, char * buffer);

	// Can accept alphanumeric or special characters.
	// Can handle a max of 20 chars only
	LCD_READ_STATE ReadSSID(int row, int col, int timeout, int maxChars, char * buffer);

	// Read the Terminal ID
	LCD_READ_STATE ReadTerminalId(int row, int col, int timeout, char & ch);

	// Used for supervisor view during transaction edit
	LCD_READ_STATE DisplayTransaction(Trans * trans, int timeout, char * key);

	LCD_READ_STATE DisplayTransactionToEmployee(Trans * trans, int timeout, char * key);

	// Reads a 64 Bit WEP Key
	LCD_READ_STATE Get64BitWEPKey(int row, int col, int timeout, char * buffer);

	// Reads a 128 Bit WEP key
	LCD_READ_STATE Get128BitWEPKey(int row, int col, int timeout, char * buffer);

	LCD_READ_STATE ShowConfirmationDialog(const char * msg, bool & confirm);

private:
	Operation oper;

	char GetNextAlphaNumericChar(char ch);
	char GetPrevAlphaNumericChar(char ch);
	char GetNextAlphaChar(char ch);
	char GetPrevAlphaChar(char ch);

	/**
	 * SSID can have values [0-9] OR [a-z] OR [_-]
	 */
	char GetNextSSIDChar(char ch);
	char GetPrevSSIDChar(char ch);

	/**
	 * Can have values [0-9] OR [A-F]
	 */
	char GetNextHexChar(char ch);
	char GetPrevHexChar(char ch);

	/**
	 * Can have values [0-9] OR [A-Z]
	 */
	char GetNextTId(char ch);
	char GetPrevTId(char ch);
};

#endif //_LCDHANDLER_H_
