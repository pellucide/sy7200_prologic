/*
	Gen_Def.h:

	Generally useful, system independent definitions and types for C.

	Copyright (c) 1991-1993,1995-1999,2001-2002,2004 - Alcom Communications

	Written by:   Alan J. Luse
	Written on:   04 Sep 1989
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   31 Jan 2004 13:39:38  $
	     $Date: 2005/09/01 22:27:58 $

*/


#if ! defined _GEN_DEF
#define _GEN_DEF

/* Boolean type and values. */
/* Use a byte value for Boolean on 8-bit processors. */
#if defined __CC11__ || defined __CC12__ || defined __CX6805__
typedef unsigned char Boolean;
#else
#if defined __CC68__
/* Use short integer for Boolean on 16 bit embedded processors. */
typedef short Boolean;
#else
/* Use integer for Boolean on all others. */
typedef int Boolean;
#endif
#endif
#define TRUE	1
#define FALSE	0
#define YES		TRUE
#define NO		FALSE
#define ON		TRUE
#define OFF		FALSE

/* Standard string terminator. */
#define NULL_CHAR '\0'

/* General type definitions. */
typedef unsigned int Bit;
typedef signed char SChar;
typedef unsigned char UChar;
typedef unsigned char Byte;
typedef unsigned char Bit8;
typedef unsigned short Bit16;
typedef unsigned long Bit32;
typedef unsigned short Count;

/* Numeric type definitions. */
typedef signed char SBin7;
typedef unsigned char UBin8;
typedef signed short SBin15;
typedef unsigned short UBin16;
typedef signed long SBin31;
typedef unsigned long UBin32;

/* Named logical operators. */
#define NOT	!
#define AND	&&
#define OR	||

/* Array size (# elements) macro. */
#define array_size(array) (sizeof array / sizeof array[0])

/* Define ANSI standard exit function values if not previously defined. */
#if ! defined (EXIT_SUCCESS)
#define EXIT_SUCCESS 0
#endif
#if ! defined (EXIT_FAILURE)
#define EXIT_FAILURE 1
#endif

/* Define additional exit values. */
#define EXIT_ABORT	2			/* Program abort. */			
#define EXIT_INTR	3			/* User interrupt (^C). */			

/* Anonymous handle type and various Null definitions. */
#define NULL_PTR ((void *) NULL)
typedef void * Handle;			/* Abstract data type handle. */
#define NULL_HANDLE ((Handle) NULL)
typedef void (* Entry) (void);	/* General pointer to function. */
#define NULL_ENTRY ((Entry) NULL)

/* Null reference macro to satisfy different compilers. */
#if defined __CX6805__
#define NULL_REF(parm) 
#else
#define NULL_REF(parm) if (parm)
#endif

/* Set standard C mode and rename qualifiers for Cosmic C compiler. */
#if defined __CX6805__
#define __interrupt @interrupt
#define __protected
#define __nosave @nosvf
#define __near @near
#define __eeprom @eeprom @near
#define __cdecl
#endif

/* Rename storage and function qualifier for Introl-C compilers. */
#if defined __CC11__ || defined __CC12__ || defined __CC68__
#define __no_init __mod1__
#else
#define __no_init
#endif
#if defined __CC11__
#define __protected __protected __cdecl
#endif
#if defined __CC12__
#define __protected
#endif
#if defined __CC68__
#define __protected __protected
#endif

/* Defaults if not otherwise defined. */
#if !defined (__near)
#define __near
#endif
#if !defined (__protected)
#define __protected
#endif
#if !defined (__nosave)
#define __nosave
#endif
#if !defined (__eeprom)
#define __eeprom
#endif

/* Fixed point & integer operations with rounding. */
#define round_div(num, div) (((((num)*2)/(div))+1)>>1) 
#define round_divs(num, scale, div) (((((num)*(scale*2))/(div))+1)>>1) 
#define round_divr(type, num, div) (((type)(((num)*2)/(div))+1)>>1) 
#define round_divrs(type, num, scale, div) (((type)(((num)*(scale*2))/(div))+1)>>1) 
#define round_percent(num, percent) (((((num)*(percent))/50)+1)>>1) 
#define round_percentr(type, num, percent) (((type)(((num)*(percent))/50)+1)>>1) 
#define round_to(num, base) (round_div (num, base) * base);

/* Cross-Sectional Array referencing macros. */
#define csa_step(array, span) (void *)((char *)array + span)
#define csa_index(array, index, span) (void *)((char *)array + (index)*span)
#define csa_bound(array, index, lbound, span) (void *)((char *)array + ((index)-(lbound))*span)

/* System status code type definition. */
typedef SBin15 Code;
enum status_code {
	SC_FAILURE		=      -1,
	SC_OKAY			=       0,
	SC_ABORT		=    -512,
	SC_ASSERT,
	SC_MEMORY,
	SC_MEMORY_TAG,
	SC_OBJECT_NULL,
	SC_OBJECT_DEAD,
	SC_OBJECT_BAD,

	SC_LIB_BASE		= -0x6000,
	SC_FIELD_BASE	= -0x6100,
	SC_WINDOW_BASE	= -0x6200,
	SC_DIR_BASE		= -0x6300,
	SC_CONTROL_BASE	= -0x6400,
	SC_FILE_BASE	= -0x6500,
	SC_PRT_BASE		= -0x6600,
	SC_EIO_BASE		= -0x6700,
	MAX_SC
};


/* Object tag definitions. */
typedef enum {
	OBJECT_NULL =			0x0000,
	OBJECT_GENERIC =		0x0001,
	OBJECT_LIB_BASE =		0x3000,
	OBJECT_USER_BASE =		0x4000,
	OBJECT_DEAD =			0x7FFF
	}
Object_Type;

/* Program and data signature values. */
#define SIGN_PROG			0xAA55
#define SIGN_DATA			0x55AA

#endif

