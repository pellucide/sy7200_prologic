//Copyright (C) 2005 TimeAmerica, Inc. All Rights Reserved
//20051109 wjm
//$Id: UserInput.hpp,v 1.7 2006/06/23 23:26:06 deepali Exp $

#ifndef USERINPUT_HPP
#define USERINPUT_HPP

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

//***********************************************************************
/** combine various ways the user can input data
    keyboard, prox reader, barcode, ...
*/
class UserInput {

public:
  enum status_t {
    OK=0,
    FAIL=1,
    TIMEOUT=2
  };
  
  enum source_t {
  	KEYBOARD=0,
  	PROX=1,
  	MAG=2,
  	BARCODE=3,
  	OTHER=4,
	FPRINT=5
  };

  static void TimeOut ( const unsigned int time_us ){
    timeout_us = time_us ;
  }

  static status_t GetChar ( char& c,  UserInput ::source_t & source );
  static void Flush ();
  static void ClearBuffer();
  static bool TemplateScanReady;
  static bool PartialScanReady;
  static bool ScanFailed;
  static bool pause_reading;
  static char* getBuffer(){return buffer;}
private:
  static void InitFds ();
  static int PrepSet ();
  static int DoSelect ( int mx );
  static int ReadFile ( int fd ); //success: return is > 0
  static int ReadBuffer ( char& r );//success: return is > 0
  static void ReleaseFds();
  static int* in_fd ;
  static unsigned int timeout_us ;

  //implementation of data buffer, since some devices will 
  //not buffer their data, but scrap whatever is not read
  static char buffer [ 100 ];//could probably be smaller...
  static char* bufEnd ;
  static char* bufP ;

  static fd_set set ;
};

//***********************************************************************
#endif //USERINPUT_HPP
