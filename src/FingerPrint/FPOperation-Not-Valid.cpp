#include "FPConfig.hpp"
#include "SystemGlobals.h"
#include "logger.h" 

pthread_mutex_t FPOperation::finger_print_mutex = PTHREAD_MUTEX_INITIALIZER;

FPConfig fpCfg;


extern Logger * logger;

namespace FPmutex {
  class FPguard {
  public:
    FPguard (){
      pthread_mutex_lock ( & _ );
    }

    ~FPguard (){
      pthread_mutex_unlock ( & _ );
    }

    static pthread_mutex_t _ ;
  };

  //********************************************************************
  pthread_mutex_t FPguard ::_ = PTHREAD_MUTEX_INITIALIZER ;

};

using namespace FPmutex;

FPOperation::FPOperation()
{
  char logstr[50];
  int mBaudRate;
  char mDeviceName[256];
  int mAsciiPacket =0;
  int mNetworkMode =0;
  int mModuleID = 1;
  int mTimeout =5000;   //timeout = 5000 ms
  fpResource =  From_Idle_Display;
  fpMode = Idle_Display_Mode;
  FpUserID = 0;
  FpScanValid = false;
  ScanResult = UF_ERR_READ_SERIAL_TIMEOUT;
  ScanCount = 0 ;
  mBaudRate = UF_BAUD115200;
  strcpy(mDeviceName, "/dev/tts/3");
  UF_RET_CODE result = uf_initialize( mDeviceName, (UF_BAUDRATE)mBaudRate, (UF_BOOL)mAsciiPacket, (UF_BOOL)mNetworkMode, mModuleID, mTimeout );
  if( result == UF_RET_SUCCESS || result == 0)
  {
     //printf("uf_initialize success - making fpUnitEnabled true\n");
  }
  else
  {
      //printf("uf_initialize failed - making fpUnitEnabled false\n");
  }
}




OperationStatusCode FPOperation::Enroll(unsigned long int UserId)
{
  
  FPguard g ;
  UF_UINT32 user_id=0;
  //printf("please place you finger\n");

   //printf("%d %d %d %d %d %d %d %d %d %d %d %d\n",UF_RET_SUCCESS,UF_ERR_SCAN_FAILED,UF_ERR_TIMEOUT,UF_ERR_TRY_AGAIN,UF_ERR_MEM_FULL,UF_ERR_FINGER_LIMIT,UF_ERR_INVALID_ID,UF_ERR_EXIST_ID,UF_ERR_NOT_MATCH,UF_ERR_UNSUPPORTED,UF_ERR_BUSY,UF_ERR_CANCELED );
    
  uf_cancel();
  uf_set_read_timeout( 5000 );
  UF_RET_CODE resultEn =  uf_enroll_by_scan( (UF_UINT32)UserId,( UF_ENROLL_OPTION)UF_ENROLL_ADD_NEW, &user_id );
  //printf("uf_enroll_by_scan returned %d new_user = %d TIME_OUT = %d\n",resultEn,user_id,UF_ERR_TIMEOUT);
  int status;
  UF_RET_CODE ret;
  if( resultEn == UF_RET_SUCCESS || resultEn == 0)
  {
      uf_cancel();
	  //printf("uf_enroll_by_scan success\n ");
	  return SC_Success;
  }
  else
  {
      uf_cancel();
	  //printf("uf_enroll_by_scan failed\n ");
	  return SC_Fail;
  }
}

OperationStatusCode FPOperation::Verification(unsigned long int UserId ,UF_RET_CODE* Result )
{
  FPguard g ;
  UF_UINT32 sub_id=0;
 // printf("FPConfig::Verification\n");
 // printf("befor uf_verify_by_scan\n");
  uf_cancel();
  uf_set_read_timeout( 5000 );
  *Result =  uf_verify_by_scan( (UF_UINT32)UserId,&sub_id );
  //printf("uf_verify_by_scan returned %d sub_id = %d TIME_OUT = %d\n",*Result,sub_id,UF_ERR_TIMEOUT);
  
  if( *Result == UF_RET_SUCCESS || *Result == 0)
  {
      uf_cancel();
	  //printf("uf_verify_by_scan success\n ");
	  return SC_Success;
  }
  else
  {
      uf_cancel();
	  //printf("uf_verify_by_scan failed\n ");
	  return SC_Fail;
  }
}

OperationStatusCode FPOperation::Identification(unsigned long int* UserId,UF_RET_CODE* Result,FingerPrintResource Resource)
{
  
  FPguard g ;
  UF_UINT32 sub_id=0;
  //printf("FPConfig::Identification\n");

  //printf("befor uf_identify_by_scan\n");
  *UserId = 0 ;
  uf_cancel();
  uf_set_read_timeout( 5000 );
  *Result =  uf_identify_by_scan( (UF_UINT32*)UserId,&sub_id );
  //pthread_mutex_unlock(&scan_finger_mutex);
  //printf("uf_identify_by_scan returned %d userID = %d TIME_OUT = %d\n",*Result,*UserId,UF_ERR_TIMEOUT);
  
  if( *Result == UF_RET_SUCCESS || *Result == 0)
  {
      uf_cancel();
	  if(Resource == From_Idle_Display)
	  {
		FpUserID = *UserId;
		//FpScanValid = true;
		ScanResult = *Result;
		ScanCount ++ ;
	    //printf("uf_identify_by_scan success\n ");
	  }
	return SC_Success;
  }
  else
  {
      uf_cancel();
	  if(Resource == From_Idle_Display)
	  {
		FpUserID = *UserId;
		//if(*Result != UF_ERR_READ_SERIAL_TIMEOUT)FpScanValid = true;
		ScanResult = *Result;
		ScanCount ++ ;
		//printf("uf_identify_by_scan failed\n ");
	  }
	  return SC_Fail;
  }
}

OperationStatusCode ScanTemplate(UF_UINT32* templ_size,UF_BYTE* templ_data , UF_RET_CODE* Result)
{

  *Result = uf_scan_template( templ_size );
  if( *Result == UF_RET_SUCCESS )
  {
	UF_BYTE* temp_templ_data = (UF_BYTE*)malloc( templ_size );
	uf_get_raw_data( temp_templ_data, templ_size );
	if(templ_data)
		strcpy(templ_data,temp_templ_data);
	free( temp_templ_data );
   }
}
