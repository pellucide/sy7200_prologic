#ifndef _FP_H_
#define _FP_H_

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <typeinfo>
#include <errno.h>
#include <time.h>

#include "Str_Def.h"
#include "StdInput.h"
#include "asm/TA7000_IO.h"
#include "Operation.hpp"
#include "LCDHandler.h"
extern "C" {
#include "uf_api.h"
#include "uf_serial.h"
#include "uf_module_log.h"
#include "uf_log.h"
#include "uf_packet.h"
#include "uf_command.h"
}

enum FingerPrintResource
{
	From_Idle_Display=0,
	From_Technical_Mode,
	From_Idle_Verification
};

enum FingerPrintMode
{
	Technical_Mode=0,
    Idle_Display_Mode,
	Idle_Verification_Mode

};

class FPOperation
{
public:
	FPOperation();
	virtual ~FPOperation(){};
    LCDHandler lcdHandler;
	Operation oper;
	FingerPrintResource fpResource;
	FingerPrintMode fpMode;
	int FpUserID;
	bool FpScanValid ;
	bool EnableFpUnit;
	UF_RET_CODE ScanResult;
	int ScanCount;

	static pthread_mutex_t finger_print_mutex;

	OperationStatusCode Enroll(unsigned long int UserId);
    OperationStatusCode ScanTemplate(UF_UINT32* templ_size,UF_BYTE* templ_data , UF_RET_CODE* Result);
    OperationStatusCode Verification(unsigned long int UserId ,UF_RET_CODE* Result );
    OperationStatusCode Identification(unsigned long int* UserId,UF_RET_CODE* Result,FingerPrintResource Resource = From_Technical_Mode);

	OperationStatusCode VerifyByTemplate(UF_BYTE* templ_data, UF_UINT32* templ_size, UF_UINT32 UserId ,int timeOut, UF_RET_CODE* Result);

};

#endif //_FP_H_
