# Project: SY7000-SDK : Prologic SY-7200 with SSL support



# client id and conditional define
CLIENT_CONDITIONAL = -DCLIENT_PROLOGIC -Dclient_prologic
FEATURE_FLAGS = -DSUPPORT_SSL -DADD_LOCALTIME_REQUEST

#DEBUG_FLAGS = -DTESTBUILD_WITH_TESTS -O2
#DEBUG_FLAGS = -DTESTBUILD_WITH_TESTS -O2 -DDEBUG  -DDEBUGPRINT
#DEBUG_FLAGS = -DTESTBUILD_WITH_TESTS -O2 -DDEBUGPRINT -DUSE_MCHECK -DDEBUG
#DEBUG_FLAGS = -DTESTBUILD_WITH_TESTS -O2 -DDEBUGPRINT -DDEBUG
#DEBUG_FLAGS = -O2
#DEBUG_FLAGS = -O2 -DUSE_MCHECK -DDEBUGSEGV -DDEBUGPRINT -DDDEBUG
#DEBUG_FLAGS = -O2 -DDEBUGPRINT -DDEBUGSEGV -DUSE_MCHECK
# with mcheck DEBUG_FLAGS = -O2 -DDEBUGPRINT -DDEBUGSEGVMT -DUSE_MCHECK -DUSE_GETACT_SINGLETHREAD_RUN
# no mcheck DEBUG_FLAGS = -O2 -DDEBUGPRINT -DDEBUGSEGVMT -DUSE_GETACT_SINGLETHREAD_RUN
#DEBUG_FLAGS = -O2 -DDEBUGPRINT -DDEBUGSEGV -DUSE_GETACT_FUNCTION -DUSE_GETACT_SINGLETHREAD_RUN



#DEBUG_FLAGS = -DDEBUGPRINT -DUSE_GETACT_FUNCTION -DUSE_GETACT_SINGLETHREAD_RUN -DUSE_GETPIN_SINGLETHREAD_RUN
DEBUG_FLAGS = -DUSE_GETACT_FUNCTION -DUSE_GETACT_SINGLETHREAD_RUN -DUSE_GETPIN_SINGLETHREAD_RUN
#DEBUG_FLAGS += -DUSE_PIN_SUPPORT
DEBUG_FLAGS += -DUSE_SELECTIVE_FPV


# if testbuild_with_tests is defined, also modify BIN

CPP  = /tools/powerpc-88x-linux-gnu/bin/g++ -D__TA7700_TERMINAL__ -D__CYGWIN__ $(CLIENT_CONDITIONAL) $(FEATURE_FLAGS) $(DEBUG_FLAGS)
#CPP  = g++ -D__DEBUG__
CC   = /tools/powerpc-88x-linux-gnu/bin/gcc -D__TA7700_TERMINAL__ -D__CYGWIN__ $(CLIENT_CONDITIONAL) $(FEATURE_FLAGS) $(DEBUG_FLAGS)
#CC   = gcc -D__DEBUG__
STRIP = /tools/powerpc-88x-linux-gnu/bin/strip
#STRIP = strip
WINDRES = windres.exe
RES  =
#OBJ  = debug/SY7000Demo.o debug/common.o debug/ServerDataBaseConnection.o debug/FPRemoteManagement.o debug/FPOperation.o debug/sqlitedb.o debug/ActivitiesTable.o debug/ProjectTasksTable.o debug/OffLineTranTable.o debug/HttpClient.o debug/SoapMessageGen.o debug/logger.o debug/fileappender.o debug/Base64.o debug/UserBarcode.o debug/UserInput.o debug/TAString.o debug/User.o debug/UserAudible.o debug/UserVisual.o debug/UserDigital.o debug/StdInput.o debug/KeyboardReader.o debug/LCDHandler.o debug/LCDUtil.o debug/LCDWEPHandler.o debug/LegacyTrans.o debug/TransFIFO.o debug/UtilityBufferOutput.o debug/ErrorSystem.o debug/InterfaceAccessControlVector.o debug/InterfaceBellScheduleVector.o debug/InterfaceBufferDays.o debug/InterfaceBufferEnum.o debug/InterfaceBufferFlags.o debug/InterfaceBufferGeneric.o debug/InterfaceElement.o debug/InterfaceElementBase.o debug/InterfaceElementExtend.o debug/InterfaceElementSystem.o debug/InterfaceMessageBadgeVector.o debug/InterfaceProfileAssignVector.o debug/InterfaceValidateEntryVector.o debug/LegacySetup.o debug/SerialIOName.o debug/SetupItem.o debug/SetupTags.o debug/StrBlank.o debug/StrClip.o debug/TA7000Init.o debug/UtilityBuffer.o debug/Trans.o debug/TransUtil.o debug/Operation.o debug/OperationValidation.o debug/OperationMessage.o debug/OperationHelper.o debug/OperationUtil.o debug/OperationIODriver.o debug/DateTimeConfig.o debug/TCPIPConfig.o debug/AccessConfig.o debug/UtilConfig.o debug/ReaderConfig.o debug/ConfigMgr.o debug/TagHandler.o debug/XMLProperties.o $(RES)
OBJ  = debug/SY7000Demo.o debug/FPOperation.o debug/sqlitedb.o debug/ActivitiesTable.o debug/ProjectTasksTable.o debug/OffLineTranTable.o debug/HttpClient.o debug/SoapMessageGen.o debug/logger.o debug/fileappender.o debug/Base64.o debug/UserBarcode.o debug/UserInput.o debug/TAString.o debug/User.o debug/UserAudible.o debug/UserVisual.o debug/UserDigital.o debug/StdInput.o debug/KeyboardReader.o debug/LCDHandler.o debug/LCDUtil.o debug/LCDWEPHandler.o debug/LegacyTrans.o debug/TransFIFO.o debug/UtilityBufferOutput.o debug/ErrorSystem.o debug/InterfaceAccessControlVector.o debug/InterfaceBellScheduleVector.o debug/InterfaceBufferDays.o debug/InterfaceBufferEnum.o debug/InterfaceBufferFlags.o debug/InterfaceBufferGeneric.o debug/InterfaceElement.o debug/InterfaceElementBase.o debug/InterfaceElementExtend.o debug/InterfaceElementSystem.o debug/InterfaceMessageBadgeVector.o debug/InterfaceProfileAssignVector.o debug/InterfaceValidateEntryVector.o debug/LegacySetup.o debug/SerialIOName.o debug/SetupItem.o debug/SetupTags.o debug/StrBlank.o debug/StrClip.o debug/TA7000Init.o debug/UtilityBuffer.o debug/Trans.o debug/TransUtil.o debug/Operation.o debug/OperationValidation.o debug/OperationMessage.o debug/OperationHelper.o debug/OperationUtil.o debug/OperationIODriver.o debug/DateTimeConfig.o debug/TCPIPConfig.o debug/AccessConfig.o debug/UtilConfig.o debug/ReaderConfig.o debug/ConfigMgr.o debug/TagHandler.o debug/XMLProperties.o $(RES)

OBJ2 = debug/stringutil.o

ifdef USE_PIN_SUPPORT
OBJ_PIN = debug/pintable.o
else
OBJ_PIN =
endif

#LINKOBJ  = debug/SY7000Demo.o debug/common.o debug/ServerDataBaseConnection.o debug/FPRemoteManagement.o debug/FPOperation.o debug/sqlitedb.o debug/ActivitiesTable.o debug/ProjectTasksTable.o debug/OffLineTranTable.o debug/HttpClient.o debug/SoapMessageGen.o debug/logger.o debug/fileappender.o debug/Base64.o debug/UserBarcode.o debug/UserInput.o debug/TAString.o debug/User.o debug/UserAudible.o debug/UserVisual.o debug/UserDigital.o debug/StdInput.o debug/KeyboardReader.o debug/LCDHandler.o debug/LCDUtil.o debug/LCDWEPHandler.o debug/LegacyTrans.o debug/TransFIFO.o debug/UtilityBufferOutput.o debug/ErrorSystem.o debug/InterfaceAccessControlVector.o debug/InterfaceBellScheduleVector.o debug/InterfaceBufferDays.o debug/InterfaceBufferEnum.o debug/InterfaceBufferFlags.o debug/InterfaceBufferGeneric.o debug/InterfaceElement.o debug/InterfaceElementBase.o debug/InterfaceElementExtend.o debug/InterfaceElementSystem.o debug/InterfaceMessageBadgeVector.o debug/InterfaceProfileAssignVector.o debug/InterfaceValidateEntryVector.o debug/LegacySetup.o debug/SerialIOName.o debug/SetupItem.o debug/SetupTags.o debug/StrBlank.o debug/StrClip.o debug/TA7000Init.o debug/UtilityBuffer.o debug/Trans.o debug/TransUtil.o debug/Operation.o debug/OperationValidation.o debug/OperationMessage.o debug/OperationHelper.o debug/OperationUtil.o debug/OperationIODriver.o debug/DateTimeConfig.o debug/TCPIPConfig.o debug/AccessConfig.o debug/UtilConfig.o debug/ReaderConfig.o debug/ConfigMgr.o  debug/TagHandler.o debug/XMLProperties.o $(RES)
LINKOBJ  = debug/SY7000Demo.o debug/FPOperation.o debug/sqlitedb.o debug/ActivitiesTable.o debug/ProjectTasksTable.o debug/OffLineTranTable.o debug/HttpClient.o debug/SoapMessageGen.o debug/logger.o debug/fileappender.o debug/Base64.o debug/UserBarcode.o debug/UserInput.o debug/TAString.o debug/User.o debug/UserAudible.o debug/UserVisual.o debug/UserDigital.o debug/StdInput.o debug/KeyboardReader.o debug/LCDHandler.o debug/LCDUtil.o debug/LCDWEPHandler.o debug/LegacyTrans.o debug/TransFIFO.o debug/UtilityBufferOutput.o debug/ErrorSystem.o debug/InterfaceAccessControlVector.o debug/InterfaceBellScheduleVector.o debug/InterfaceBufferDays.o debug/InterfaceBufferEnum.o debug/InterfaceBufferFlags.o debug/InterfaceBufferGeneric.o debug/InterfaceElement.o debug/InterfaceElementBase.o debug/InterfaceElementExtend.o debug/InterfaceElementSystem.o debug/InterfaceMessageBadgeVector.o debug/InterfaceProfileAssignVector.o debug/InterfaceValidateEntryVector.o debug/LegacySetup.o debug/SerialIOName.o debug/SetupItem.o debug/SetupTags.o debug/StrBlank.o debug/StrClip.o debug/TA7000Init.o debug/UtilityBuffer.o debug/Trans.o debug/TransUtil.o debug/Operation.o debug/OperationValidation.o debug/OperationMessage.o debug/OperationHelper.o debug/OperationUtil.o debug/OperationIODriver.o debug/DateTimeConfig.o debug/TCPIPConfig.o debug/AccessConfig.o debug/UtilConfig.o debug/ReaderConfig.o debug/ConfigMgr.o  debug/TagHandler.o debug/XMLProperties.o $(RES)

#LIBS =  -lmcheck -L"/tools/powerpc-8xx-linux-gnu/usr/lib" -L"/cygdrive/c/cygwin/tools/powerpc-88x-linux-gnu/lib" -L"/cygdrive/c/cygwin/usr/local/lib" -L"lib" lib/libsqlite3.a lib/libexpat.a lib/libwep.a lib/libuf.a lib/libcurl.so lib/libct.so -lpthread -ggdb
#LIBS =  -L"/cygdrive/c/cygwin/tools/powerpc-88x-linux-gnu/lib" -L"/cygdrive/c/cygwin/usr/local/lib" -L"lib" lib/libsqlite3.a lib/libexpat.a lib/libwep.a lib/libuf.a lib/libcurl.so lib/libct.so -lpthread -ggdb
#LIBS = -lmcheck -L"/cygdrive/c/cygwin/tools/powerpc-88x-linux-gnu/lib" -L"/cygdrive/c/cygwin/usr/local/lib" -L"lib" lib/libsqlite3.a lib/libexpat.a lib/libwep.a lib/libuf.a lib/libcurl.so lib/libct.so -lpthread -ggdb
#LIBS = -L"C:/cygwin/lib" -lpthread -lstdc++ -static -ggdb
# with mcheck LIBS =  -lmcheck -L"/tools/powerpc-8xx-linux-gnu/usr/lib" -L"/cygdrive/c/cygwin/tools/powerpc-88x-linux-gnu/lib" -L"/cygdrive/c/cygwin/usr/local/lib" -L"lib" lib/libsqlite3.a lib/libexpat.a lib/libwep.a lib/libuf.a lib/libcurl.so -lpthread -ggdb
# no mcheck   LIBS =  -L"/tools/powerpc-8xx-linux-gnu/usr/lib" -L"/cygdrive/c/cygwin/tools/powerpc-88x-linux-gnu/lib" -L"/cygdrive/c/cygwin/usr/local/lib" -L"lib" lib/libsqlite3.a lib/libexpat.a lib/libwep.a lib/libuf.a lib/libcurl.so -lpthread -ggdb
#LIBS =  -L"/tools/powerpc-8xx-linux-gnu/usr/lib" -L"/cygdrive/c/cygwin/tools/powerpc-88x-linux-gnu/lib" -L"/cygdrive/c/cygwin/usr/local/lib" -L"lib" lib/libsqlite3.a lib/libexpat.a lib/libwep.a lib/libuf.a lib/libcurl.so -lpthread -ggdb
LIBS =  -L"/tools/powerpc-8xx-linux-gnu/usr/lib" -L"/cygdrive/c/cygwin/tools/powerpc-88x-linux-gnu/lib" -L"/cygdrive/c/cygwin/usr/local/lib" -L"lib" lib/libsqlite3.a lib/libexpat.a lib/libwep.a lib/libuf.a lib/libcurl.a lib/libssl.a -lpthread -ggdb

INCS =  -I"usr/include" -I"/cygdrive/c/cygwin/tools/powerpc-88x-linux-gnu/include" -I"/cygdrive/c/cygwin/usr/local/include"  -I"src/Include"  -I"src/logger/include" -I"src/Database/Include" -I"src/UserIO/Include" -I"src/setup/Include"  -I"src/Trans/Include"  -I"src/Operation/Include" -I"src/Util" -I"include" -I"include/ufp" -I"include/freetds" -I"src/Database/Include"
#INCS =  -I"/cygdrive/c/cygwin/tools/powerpc-88x-linux-gnu/include" -I"/cygdrive/c/cygwin/usr/local/include"  -I"src/Include"  -I"src/logger/include" -I"src/Database/Include" -I"src/UserIO/Include" -I"src/setup/Include"  -I"src/Trans/Include"  -I"src/Operation/Include" -I"src/Util" -I"include" -I"include/ufp" -I"include/freetds" -I"src/Database/Include"
#CXXINCS = -I"/cygdrive/c/cygwin/tools/powerpc-88x-linux-gnu/include" -I"/cygdrive/c/cygwin/usr/local/include"  -I"src/Include"  -I"src/logger/include" -I"src/Database/Include" -I"src/UserIO/Include" -I"src/setup/Include"  -I"src/Trans/Include"  -I"src/Operation/Include" -I"src/Util" -I"include" -I"include/ufp" -I"include/freetds" -I"src/Database/Include"
CXXINCS = -I"usr/include" -I"/cygdrive/c/cygwin/tools/powerpc-88x-linux-gnu/include" -I"/cygdrive/c/cygwin/usr/local/include"  -I"src/Include"  -I"src/logger/include" -I"src/Database/Include" -I"src/UserIO/Include" -I"src/setup/Include"  -I"src/Trans/Include"  -I"src/Operation/Include" -I"src/Util" -I"include" -I"include/ufp" -I"include/freetds" -I"src/Database/Include"

BIN  = SY7000Demo

#CXXFLAGS = $(CXXINCS) -pthread -D_REENTRANT -fomit-frame-pointer -D_GNU_SOURCE -Wall -ggdb -fstack-check
CXXFLAGS = $(CXXINCS) -D_REENTRANT -fomit-frame-pointer -D_GNU_SOURCE -Wall -ggdb -Os
#CXXFLAGS = $(CXXINCS) -pthread -D_REENTRANT -fomit-frame-pointer -D_GNU_SOURCE -Wall -ggdb -ffunction-sections -fdata-sections -fvtable-gc
#CFLAGS = $(INCS) -lcurl -pthread -D_REENTRANT -fomit-frame-pointer -D_GNU_SOURCE -Wall -ggdb -fstack-check
CFLAGS = $(INCS) -D_REENTRANT -fomit-frame-pointer -D_GNU_SOURCE -Wall -ggdb -Os
#CFLAGS = $(INCS) -lcurl -pthread -D_REENTRANT -fomit-frame-pointer -D_GNU_SOURCE -Wall -ggdb  -ffunction-sections -fdata-sections -fvtable-gc

.PHONY: all all-before all-after clean clean-custom

all: all-before strip all-after


clean: clean-custom
	rm -f $(OBJ) $(OBJ2) $(OBJ_PIN) $(BIN)

strip: $(BIN)
#	echo $(BIN)
	$(STRIP) $(BIN)

$(BIN): $(OBJ) $(OBJ2)
#	echo "******** TEST BUILD ***********"
#	$(CPP) $(LINKOBJ) -o $(BIN) $(LIBS) -lrt -rdynamic -Wl,-Map=SY7000Demo.map
#	$(CPP) $(LINKOBJ) -o $(BIN) $(LIBS) -rdynamic -Wl,-Map=SY7000Demo.map
#	echo "******** REMOVE TESTS *********"
#	echo "******** REMOVE DEBUG/TEST LINKAGE COMMANDS ********"
#	$(CPP) $(LINKOBJ) $(OBJ2) -o $(BIN) $(LIBS)  -lrt -Wl,-Map=SY7000Demo.map
#	$(CPP) $(LINKOBJ) $(OBJ2) -o $(BIN) $(LIBS)  -rdynamic -lrt -Wl,-Map=SY7000Demo.map
#	$(CPP) $(LINKOBJ) $(OBJ2) -o $(BIN) $(LIBS)  -Wl,-Map=SY7000Demo.map
#	$(CPP) $(LINKOBJ) $(OBJ2) -o $(BIN) $(LIBS)  -rdynamic -lrt -Wl,-Map=SY7000Demo.map -Wl,--gc-sections
	$(CPP) $(LINKOBJ) $(OBJ2) -o $(BIN) $(LIBS)  -lrt -Wl,-Map=SY7000Demo.map

debug/Base64.o: src/Util/Base64.cpp
	$(CPP) -c src/Util/Base64.cpp -o debug/Base64.o $(CXXFLAGS)

debug/TAString.o: src/Util/TAString.cpp
	$(CPP) -c src/Util/TAString.cpp -o debug/TAString.o $(CXXFLAGS)

debug/logger.o : src/logger/logger.cpp
	$(CPP) -c src/logger/logger.cpp -o debug/logger.o $(CXXFLAGS)

debug/fileappender.o : src/logger/fileappender.cpp
	$(CPP) -c src/logger/fileappender.cpp -o debug/fileappender.o $(CXXFLAGS)

debug/UserInput.o: src/UserIO/UserInput.cpp
	$(CPP) -c src/UserIO/UserInput.cpp -o debug/UserInput.o $(CXXFLAGS)

debug/TCPIPConfig.o: src/Operation/TCPIPConfig.cpp
	$(CPP) -c src/Operation/TCPIPConfig.cpp -o debug/TCPIPConfig.o $(CXXFLAGS)

debug/User.o: src/UserIO/User.c
	$(CC) -c src/UserIO/User.c -o debug/User.o $(CXXFLAGS)

debug/UserAudible.o: src/UserIO/UserAudible.c
	$(CC) -c src/UserIO/UserAudible.c -o debug/UserAudible.o $(CXXFLAGS)

debug/UserBarcode.o: src/UserIO/UserBarcode.c
	$(CC) -c src/UserIO/UserBarcode.c -o debug/UserBarcode.o $(CXXFLAGS)

debug/UserVisual.o: src/UserIO/UserVisual.c
	$(CC) -c src/UserIO/UserVisual.c -o debug/UserVisual.o $(CXXFLAGS)

debug/UserDigital.o: src/UserIO/UserDigital.c
	$(CC) -c src/UserIO/UserDigital.c -o debug/UserDigital.o $(CXXFLAGS)

debug/StdInput.o: src/UserIO/StdInput.cpp
	$(CPP) -c src/UserIO/StdInput.cpp -o debug/StdInput.o $(CXXFLAGS)

debug/KeyboardReader.o: src/UserIO/KeyboardReader.cpp
	$(CPP) -c src/UserIO/KeyboardReader.cpp -o debug/KeyboardReader.o $(CXXFLAGS)

debug/LegacyTrans.o: src/Trans/LegacyTrans.cpp
	$(CPP) -c src/Trans/LegacyTrans.cpp -o debug/LegacyTrans.o $(CXXFLAGS)

debug/TransFIFO.o: src/Trans/TransFIFO.cpp
	$(CPP) -c src/Trans/TransFIFO.cpp -o debug/TransFIFO.o $(CXXFLAGS)

debug/UtilityBufferOutput.o: src/setup/UtilityBufferOutput.cpp
	$(CPP) -c src/setup/UtilityBufferOutput.cpp -o debug/UtilityBufferOutput.o $(CXXFLAGS)

debug/ErrorSystem.o: src/setup/ErrorSystem.cpp
	$(CPP) -c src/setup/ErrorSystem.cpp -o debug/ErrorSystem.o $(CXXFLAGS)

debug/InterfaceAccessControlVector.o: src/setup/InterfaceAccessControlVector.cpp
	$(CPP) -c src/setup/InterfaceAccessControlVector.cpp -o debug/InterfaceAccessControlVector.o $(CXXFLAGS)

debug/InterfaceBellScheduleVector.o: src/setup/InterfaceBellScheduleVector.cpp
	$(CPP) -c src/setup/InterfaceBellScheduleVector.cpp -o debug/InterfaceBellScheduleVector.o $(CXXFLAGS)

debug/InterfaceBufferDays.o: src/setup/InterfaceBufferDays.cpp
	$(CPP) -c src/setup/InterfaceBufferDays.cpp -o debug/InterfaceBufferDays.o $(CXXFLAGS)

debug/InterfaceBufferEnum.o: src/setup/InterfaceBufferEnum.cpp
	$(CPP) -c src/setup/InterfaceBufferEnum.cpp -o debug/InterfaceBufferEnum.o $(CXXFLAGS)

debug/InterfaceBufferFlags.o: src/setup/InterfaceBufferFlags.cpp
	$(CPP) -c src/setup/InterfaceBufferFlags.cpp -o debug/InterfaceBufferFlags.o $(CXXFLAGS)

debug/InterfaceBufferGeneric.o: src/setup/InterfaceBufferGeneric.cpp
	$(CPP) -c src/setup/InterfaceBufferGeneric.cpp -o debug/InterfaceBufferGeneric.o $(CXXFLAGS)

debug/InterfaceElement.o: src/setup/InterfaceElement.cpp
	$(CPP) -c src/setup/InterfaceElement.cpp -o debug/InterfaceElement.o $(CXXFLAGS)

debug/InterfaceElementBase.o: src/setup/InterfaceElementBase.cpp
	$(CPP) -c src/setup/InterfaceElementBase.cpp -o debug/InterfaceElementBase.o $(CXXFLAGS)

debug/InterfaceElementExtend.o: src/setup/InterfaceElementExtend.cpp
	$(CPP) -c src/setup/InterfaceElementExtend.cpp -o debug/InterfaceElementExtend.o $(CXXFLAGS)

debug/InterfaceElementSystem.o: src/setup/InterfaceElementSystem.cpp
	$(CPP) -c src/setup/InterfaceElementSystem.cpp -o debug/InterfaceElementSystem.o $(CXXFLAGS)

debug/InterfaceMessageBadgeVector.o: src/setup/InterfaceMessageBadgeVector.cpp
	$(CPP) -c src/setup/InterfaceMessageBadgeVector.cpp -o debug/InterfaceMessageBadgeVector.o $(CXXFLAGS)

debug/InterfaceProfileAssignVector.o: src/setup/InterfaceProfileAssignVector.cpp
	$(CPP) -c src/setup/InterfaceProfileAssignVector.cpp -o debug/InterfaceProfileAssignVector.o $(CXXFLAGS)

debug/InterfaceValidateEntryVector.o: src/setup/InterfaceValidateEntryVector.cpp
	$(CPP) -c src/setup/InterfaceValidateEntryVector.cpp -o debug/InterfaceValidateEntryVector.o $(CXXFLAGS)

debug/LegacySetup.o: src/setup/LegacySetup.cpp
	$(CPP) -c src/setup/LegacySetup.cpp -o debug/LegacySetup.o $(CXXFLAGS)

debug/SerialIOName.o: src/setup/SerialIOName.cpp
	$(CPP) -c src/setup/SerialIOName.cpp -o debug/SerialIOName.o $(CXXFLAGS)

debug/SetupItem.o: src/setup/SetupItem.cpp
	$(CPP) -c src/setup/SetupItem.cpp -o debug/SetupItem.o $(CXXFLAGS)

debug/SetupTags.o: src/setup/SetupTags.cpp
	$(CPP) -c src/setup/SetupTags.cpp -o debug/SetupTags.o $(CXXFLAGS)

debug/StrBlank.o: src/setup/StrBlank.c
	$(CPP) -c src/setup/StrBlank.c -o debug/StrBlank.o $(CXXFLAGS)

debug/StrClip.o: src/setup/StrClip.c
	$(CPP) -c src/setup/StrClip.c -o debug/StrClip.o $(CXXFLAGS)

debug/SY7000Demo.o: debug/XMLProperties.o SY7000Demo.cpp SY7000Demo.h src/Util/debugprint.h
	$(CPP) -c SY7000Demo.cpp -o debug/SY7000Demo.o $(CXXFLAGS)

debug/UtilityBuffer.o: src/setup/UtilityBuffer.cpp
	$(CPP) -c src/setup/UtilityBuffer.cpp -o debug/UtilityBuffer.o $(CXXFLAGS)

debug/Trans.o: src/Trans/Trans.cpp
	$(CPP) -c src/Trans/Trans.cpp -o debug/Trans.o $(CXXFLAGS)

debug/TransUtil.o: src/Trans/TransUtil.cpp
	$(CPP) -c src/Trans/TransUtil.cpp -o debug/TransUtil.o $(CXXFLAGS)

debug/Operation.o: src/Operation/Operation.cpp
	$(CPP) -c src/Operation/Operation.cpp -o debug/Operation.o $(CXXFLAGS)

debug/OperationValidation.o: src/Operation/OperationValidation.cpp
	$(CPP) -c src/Operation/OperationValidation.cpp -o debug/OperationValidation.o $(CXXFLAGS)

debug/OperationMessage.o: src/Operation/OperationMessage.cpp
	$(CPP) -c src/Operation/OperationMessage.cpp -o debug/OperationMessage.o $(CXXFLAGS)

debug/OperationHelper.o: src/Operation/OperationHelper.cpp
	$(CPP) -c src/Operation/OperationHelper.cpp -o debug/OperationHelper.o $(CXXFLAGS)

debug/OperationUtil.o: src/Operation/OperationUtil.cpp
	$(CPP) -c src/Operation/OperationUtil.cpp -o debug/OperationUtil.o $(CXXFLAGS)

debug/OperationIODriver.o: src/Operation/OperationIODriver.cpp src/Operation/Include/OperationDriver.hpp
	$(CPP) -c src/Operation/OperationIODriver.cpp -o debug/OperationIODriver.o $(CXXFLAGS)

debug/DateTimeConfig.o : src/Operation/DateTimeConfig.cpp
	$(CPP) -c src/Operation/DateTimeConfig.cpp -o debug/DateTimeConfig.o  $(CXXFLAGS)

debug/AccessConfig.o : src/Operation/AccessConfig.cpp
	$(CPP) -c src/Operation/AccessConfig.cpp -o debug/AccessConfig.o  $(CXXFLAGS)

debug/UtilConfig.o : src/Operation/UtilConfig.cpp src/Operation/Include/Config.h
	$(CPP) -c src/Operation/UtilConfig.cpp -o debug/UtilConfig.o  $(CXXFLAGS)

debug/ReaderConfig.o : src/Operation/ReaderConfig.cpp
	$(CPP) -c src/Operation/ReaderConfig.cpp -o debug/ReaderConfig.o  $(CXXFLAGS)

debug/ConfigMgr.o : src/Operation/ConfigMgr.cpp src/setup/Include/version.h
	$(CPP) -c src/Operation/ConfigMgr.cpp -o debug/ConfigMgr.o  $(CXXFLAGS)

debug/TagHandler.o : src/Operation/TagHandler.cpp
	$(CPP) -c src/Operation/TagHandler.cpp -o debug/TagHandler.o $(CXXFLAGS)

debug/TA7000Init.o : src/Operation/TA7000Init.cpp
	$(CPP) -c src/Operation/TA7000Init.cpp -o debug/TA7000Init.o $(CXXFLAGS)

debug/FPOperation.o : src/Operation/FPOperation.cpp
	$(CPP) -c src/Operation/FPOperation.cpp -o debug/FPOperation.o $(CXXFLAGS)

debug/LCDHandler.o : src/UserIO/LCDHandler.cpp
	$(CPP) -c src/UserIO/LCDHandler.cpp -o debug/LCDHandler.o $(CXXFLAGS)

debug/LCDWEPHandler.o : src/UserIO/LCDWEPHandler.cpp
	$(CPP) -c src/UserIO/LCDWEPHandler.cpp -o debug/LCDWEPHandler.o $(CXXFLAGS)

debug/LCDUtil.o : src/UserIO/LCDUtil.cpp
	$(CPP) -c src/UserIO/LCDUtil.cpp -o debug/LCDUtil.o $(CXXFLAGS)

debug/XMLProperties.o : src/Util/XMLProperties.cpp
	$(CPP) -c src/Util/XMLProperties.cpp -o debug/XMLProperties.o $(CXXFLAGS)

debug/FPRemoteManagement.o : src/Operation/FPRemoteManagement.cpp
	$(CPP) -c src/Operation/FPRemoteManagement.cpp -o debug/FPRemoteManagement.o $(CXXFLAGS)

debug/SoapMessageGen.o : src/Util/SoapMessageGen.cpp src/Util/SoapMessageGen.h
	$(CPP) -c src/Util/SoapMessageGen.cpp -o debug/SoapMessageGen.o $(CXXFLAGS)

debug/HttpClient.o : debug/XMLProperties.o src/Util/HttpClient.cpp
	$(CPP) -c src/Util/HttpClient.cpp -o debug/HttpClient.o $(CXXFLAGS)

debug/sqlitedb.o : src/Database/sqlitedb.cpp
	$(CPP) -c src/Database/sqlitedb.cpp -o debug/sqlitedb.o $(CXXFLAGS)

debug/ProjectTasksTable.o : src/Database/ProjectTasksTable.cpp
	$(CPP) -c src/Database/ProjectTasksTable.cpp -o debug/ProjectTasksTable.o $(CXXFLAGS)

debug/ActivitiesTable.o : src/Database/ActivitiesTable.cpp
	$(CPP) -c src/Database/ActivitiesTable.cpp -o debug/ActivitiesTable.o $(CXXFLAGS)

debug/OffLineTranTable.o : src/Database/OffLineTranTable.cpp
	$(CPP) -c src/Database/OffLineTranTable.cpp -o debug/OffLineTranTable.o $(CXXFLAGS)

debug/common.o: src/Database/common.c
	$(CC) -c src/Database/common.c -o debug/common.o $(CXXFLAGS)

debug/ServerDataBaseConnection.o: src/Database/ServerDataBaseConnection.cpp
	$(CC) -c src/Database/ServerDataBaseConnection.cpp -o debug/ServerDataBaseConnection.o $(CXXFLAGS)

#######

ifdef USE_PIN_SUPPORT
debug/pintable.o: src/Database/pintable.cpp
	$(CC) -c src/Database/pintable.cpp -o debug/pintable.o $(CXXFLAGS)
endif

debug/stringutil.o: src/Util/stringutil.cpp
	$(CC) -c src/Util/stringutil.cpp -o debug/stringutil.o $(CXXFLAGS)


# DO NOT DELETE
