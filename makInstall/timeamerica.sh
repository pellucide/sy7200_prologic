#!/bin/sh
#
#install the DEMO packages and files
#
#
su -p root
# kill known clockapps
killall -9 TA7000
killall -9 SY7000DEMO
killall -9 SY7000Demo
killall -9 SY7000IPPS
killall -9 SY7000
#

# it is assumed that ta already exists
# since the Enabler transferred the files
#
cd /home/terminal/avalanche/ta
#

chmod 777 *
chmod 777 *.*

if [ -e "Spieler" ]; then
./Spieler &
fi

if [ -e "SY7000Demo.tar" ]; then
cd /home/terminal/bin
if [ -e "TA7000" ]; then rm TA7000; fi
if [ -e "SY7000DEMO" ]; then rm SY7000DEMO; fi
if [ -e "SY7000Demo" ]; then rm SY7000Demo; fi
if [ -e "SY7000" ]; then rm SY7000; fi
if [ -e "SY7000IPPS" ]; then rm SY7000IPPS; fi
cd /home/terminal/avalanche/ta
mv SY7000Demo.tar /home/terminal/bin/SY7000Demo.tar
cd /home/terminal/bin
tar -z -x -f SY7000Demo.tar
chmod u+x /home/terminal/bin/SY7000Demo
rm SY7000Demo.tar
cd /home/terminal/avalanche/ta
fi

if [ -e "demowebsapp.tar" ]; then
killall -9 webs
if [ -e "/home/terminal/bin/LINUX/webs" ]; then rm /home/terminal/bin/LINUX/webs; fi
mv demowebsapp.tar /home/terminal/bin/demowebsapp.tar
cd /home/terminal/bin
tar -z -x -f demowebsapp.tar
chmod u+x /home/terminal/bin/LINUX/webs
rm demowebsapp.tar
cd /home/terminal/avalanche/ta
fi

if [ -e "demoweb.tar" ]; then
killall -9 webs
mv demoweb.tar /home/terminal/bin/demoweb.tar
cd /home/terminal/bin
rm -r web
mkdir web
tar -z -x -f demoweb.tar
rm demoweb.tar
cd /home/terminal/avalanche/ta
fi

if [ -e "demowebdocs.tar" ]; then
mv demowebdocs.tar /home/terminal/bin/demowebdocs.tar
cd /home/terminal/bin
tar -z -x -f demowebdocs.tar
rm demowebdocs.tar
cd /home/terminal/avalanche/ta
fi

if [ -e "applstart" ]; then
mv applstart /home/terminal/applstart.sh
chmod u+x /home/terminal/applstart.sh
fi

if [ -e "ResetApplication" ]; then
mv ResetApplication /home/terminal/bin/ResetApplication.sh
chmod u+x /home/terminal/bin/ResetApplication.sh
fi

if [ -e "properties.xml" ]; then
mv properties.xml /home/terminal/bin/properties.xml
fi

if [ -e "netconfig" ]; then
chmod 777 /home/terminal/network/interfaces
mv netconfig /home/terminal/network/interfaces
chmod u+x /home/terminal/network/interfaces
fi

if [ -e "dnsconfig" ]; then
chmod 777 /home/terminal/network/resolv.conf
mv dnsconfig /home/terminal/network/resolv.conf
chmod u+x /home/terminal/network/resolv.conf
fi

#make sure webs is  executable
cd /home/terminal/bin/LINUX
chmod u+x webs


#
reboot
#
