#ifndef SY_DEMO
#define SY_DEMO


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <resolv.h>
#include <netdb.h>

#include "System.hpp"
#include "Legacy.hpp"
#include "InterfaceValidateEntryVector.hpp"
#include "InterfaceMessageBadgeVector.hpp"
#include "UserInput.hpp"
#include "OperationDriver.hpp"
#include "XMLProperties.h"
#include "HttpClient.h"
#include "sqlitedb.h"


// not used: #include "FPRemoteManagement.hpp"
#include "ServerDataBaseConnection.h"
#include "FPOperation.hpp"
#include "Base64.h"
extern "C" {
#include "User.h"
}



using namespace std;

// extern the LegactSetup object that implement the setup data structure this defined in Operation.cpp file.
 extern LegacySetup SystemSetup;
 extern sqlite3 *db;
 extern FPOperation fpOpr;


#ifdef client_prologic
// keypad badge entry has different parameters than card reader badge
	extern LegacySetup KeypadSetup;
#endif


enum savetranstype_enum {
	otSwipePunch=0,
	otInPunch,
	otOutPunch,
	otClockIn,
	otGetProjectTasks
};

class SY7000Demo
{

  public:
	 SY7000Demo(){EnableFpVerifyMode = false;};
	~SY7000Demo(){};

	static SY7000Demo * staticPointer;
    static char LinkWeb[100];
    static char Account[30];
    static char ClockId[30];

	// special wait states for thread startups
	static bool ok_for_this_thread;
	static bool ok_for_next_thread;
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    char useProxy[8];
	char proxy[500];
	char proxyPort[10];
	char useProxyAuth[8];
	char proxyUserName[50];
	char proxyPassword[50];
	char timeOut[6];
	bool useProxyBool;
	bool useProxyAuthBool;
	bool useSoapActionHeader;
	bool EnableFpVerifyMode;
	bool UseSqlServer;
	char soapActionHeader[200];
	static int heartbeatFrequency;

	static bool DontSendPunches;

#ifdef client_prologic

#ifdef USE_SELECTIVE_FPV
	static bool useKeypadFPV;
	static bool useCardFPV;
#endif

#endif

	static int getActivitiesFrequency;
	static int getRunServerTimeFrequency;
	static int TemplatesAvailableFrequency;
    static HttpClient* httpS;
	static HttpClient* httpOffline;
	ConfigMgr cfgMgr;

	//static bool communication;
	void initSetupSystemDataStructure(void);
	void SetupSystem(void);
	void SetInPunchFunctionKey(int FunctionKey);
	void SetOutPunchFunctionKey(int FunctionKey);
	void SetClockInFunctionKey(int FunctionKey);
	void SetSwipePunchFunctionKey(int FunctionKey);
	void ResetFunctionKeys();
	void initHttpClient();
    void initFpuConfiguration();
	//void initServerDBTables(ServerDataBaseConnection* SDBConnection);
	//void SendTransaction(OperationIODriver * oper,ServerDataBaseConnection* SDBConnection);
	void SendTransaction(OperationIODriver * oper,char* SDBConnection);
    void ParseAndDisplayResponse(OperationIODriver * oper,char* response,char* defaultMessage);

	void initOfflineMode(OperationIODriver * oper);

	void SYClock_GetProps();

	void initDB();
	void RunHeartBeatThread(); // create the heartbeat thread
	static void * RunHeartBeatFunction(void* id);

	// create the thread that's responsible to get the Activities and update the DB table
	void RunGetActivitiesThread();
	static void * RunGetActivitiesFunction(void* id);
	void RunFingerPrintThread(OperationIODriver * oper);
    static void * FingerPrintFunction(void* id);
	bool FpVerifyUser(OperationIODriver *oper);
	static void DispalyFpPromptError(OperationIODriver * oper,OperationStatusCode code);
	static void DispalyIdentifyFpuError(OperationIODriver * oper,UF_BYTE Error);

private:

    static int UpdateActivitiesInDB(const char* activitiesXmlList);
	static void fromActivitiesListToRecord( list<ElmData>& activitiesList,list<ElmData>::iterator& i,ActivityRecord& R);
    bool checkActivityValidity(OperationIODriver * oper);
	//bool checkUserInServerDB(char* UserName ,OperationIODriver * oper,ServerDataBaseConnection* SDBConnection);

	static int SaveTransaction(OffLineTranRecord* R,SaveReason sr = OFFLINE_MODE);
	int SaveOfflineTransaction(OperationIODriver * oper, char * utcTime, SaveReason sr = OFFLINE_MODE, savetranstype_enum savetranstype=otSwipePunch);

public:
	//void SendSwipePunch(OperationIODriver * oper,ServerDataBaseConnection* SDBConnection);
	void SendSwipePunch(OperationIODriver * oper,char* SDBConnection);
	//void SendSwipePunchWithActivityCode(OperationIODriver * oper,ServerDataBaseConnection* SDBConnection);
	void SendSwipePunchWithActivityCode(OperationIODriver * oper,char* SDBConnection);
	bool ValidateUser(OperationIODriver* oper);
	void SetSwipePunchWithActivityCodeFunctionKey(int FunctionKey);
	void initSwipeAndGo();
	static void * RunOfflineSenderFunction(void * id);
	void RunOfflineSenderThread();
	//
	void GetServerTimeFunction();
	//
	void initCfgIODriver(OperationIODriver * oper);
	//
	bool ValidatePinEntry(OperationIODriver* oper);
	//
	// fingerprint management - Phase I
	//
	// threaded:
	void 			RunPopulateTemplatesThread();
	static void * 	RunPopulateTemplatesFunction(void* id);
	// no thread:
	static void *   PopulateTemplates();
	//
    static int 		UpdateTemplatesInFPU(const char* activitiesXmlList, bool displayprogress);
	static int 		UpdateTemplatesInFPUFromFile( FILE *xmlTemplatesFile, bool displayprogress );

	static void		RunTemplatesAvailableThread();
	static void *	RunTemplatesAvailableFunction(void * id);

	static void		DoGetActivitiesFunction();


#ifdef USE_PIN_SUPPORT
public:
	//void RunOnceGetPinTableThread();
	//static void * RunOnceGetPinTableFunction(void* id);
	void DoGetPinTableFunction();					// <---- not static !!!
#endif

	//
public:
	int VerifyUserFromServerTemplates( const int UserBadge, const char *xmlUserTemplateList, UF_BYTE *liveTemplate );
	bool FpVerifyUserDisplay(OperationIODriver * oper);
	OperationStatusCode FpVerifyUserLocally(OperationIODriver * oper, unsigned long int UserBadge, UF_BYTE * rawtemplate, UF_RET_CODE* pResult );
	OperationStatusCode FpVerifyUserFromServer(OperationIODriver * oper, unsigned long int UserBadge, UF_BYTE * rawtemplate, UF_RET_CODE* pResult );
	bool UserNeedsFPV(OperationIODriver * oper);

};

#endif
