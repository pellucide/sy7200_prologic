/**
 *  	Unifinger module initialization and configuration 
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */


/*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */

 #ifndef	__UNIFINGERMODULE_H__
 #define __UNIFINGERMODULE_H__

#include "uf_type.h"
#include "uf_sys_parameter.h"
#include "uf_enroll.h"
#include "uf_sensor.h"
#include "uf_image.h"


typedef enum {
	UF_SYS_ALIVE			= 0x30,
	UF_SYS_WAIT 			= 0x31,
	UF_SYS_FAIL				= 0x32,
	UF_SYS_SENSOR_FAIL		= 0x33,
	UF_SYS_BUSY				= 0x34,
} UF_SYSTEM_STAUTS;


typedef struct {
	int	module_id;
	char version[4];
	char build_no[4];
	int	serial_no;

	UF_ENROLL_MODE	enroll_mode;
	UF_BOOL			encrypt;
	UF_SENSOR_TYPE	sensor_type;
	UF_IMAGE_TYPE	image_type;
	int				baudrate;
	UF_BOOL			send_scan_success;
	UF_BOOL			ascii_packet;
	UF_BOOL			network_mode;
	int				packet_len;
	int				read_timeout;
 } uf_system_config_t;

#endif
