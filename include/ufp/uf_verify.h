/**
 *  	Definitions of Verifyl related constants
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */


/*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */
 
 #ifndef __UNIFINGERVERIFY_H__
#define __UNIFINGERVERIFY_H__

#define UF_MAX_VH_TEMPLATE	10

#endif
