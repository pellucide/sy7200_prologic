#include	"wsIntrn.h"

#define		NONE_OPTION		T("<NONE>")
#define		MSG_START		T("<body><h2>")
#define		MSG_END			T("</h2></body>")

static void		formReaderConf(webs_t wp, char_t *path, char_t *query);
static void		websMsgStart(webs_t wp);
static void		websMsgEnd(webs_t wp);

void formDefineReaderConf(void)
{

    websFormDefine(T("SetReaderConf"), formReaderConf);
}


static void		formReaderConf(webs_t wp, char_t *path, char_t *query)
{
    char_t	*readerType, *badgeLen, *pin ;
    a_assert(wp);
    //printf("we are here\n");
	readerType = websGetVar(wp, T("READER"), T("")); 
	badgeLen = websGetVar(wp, T("BL"), T("")); 
	pin = websGetVar(wp, T("PIN"), T("")); 
	
	websHeader(wp);
	websMsgStart(wp);
    
    printf("reader=%s badgeLen=%s pin=%s\n",readerType,badgeLen,pin);
    websWrite(wp, T("reader=%s badgeLen=%s pin=%s"),readerType,badgeLen,pin);

    websMsgEnd(wp);
	websFooter(wp);
	websDone(wp, 200);
}

static void	websMsgStart(webs_t wp)
{
	websWrite(wp, MSG_START);
}

static void	websMsgEnd(webs_t wp)
{
	websWrite(wp, MSG_END);
}