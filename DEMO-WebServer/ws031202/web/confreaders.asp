<html>
<!- Copyright (c) Go Ahead Software Inc., 2000-2000. All Rights Reserved. ->
<head>
<title>Set Date And Time</title>
<meta http-equiv="Pragma" content="no-cache">
<link rel="stylesheet" href="style/normal_ws.css" type="text/css">
<% language=javascript %>
</head>

<body>
<h1>Configure Reader</h1>
<form name="reader" action=/goform/SetReaderConf onsubmit="return checkInput()" method=POST>
<script language="JavaScript">
function checkInput()
{
   if(20 < reader.BL.value ||  reader.BL.value < 1)
    {
        alert('Badge Length must be between 1 to 20');
        reader.BL.focus();
        return false;
  	}
    return true;
}

</script>

<table>
<tr>
    <td>
	  <fieldset>
	  <legend>
	    Reader:
	  </legend> 
	Reader Type:
	<select name="READER" title="Month">
	        <option value ="BR">Bar Code</option>
            <option value ="PRO">Proximity</option>
            <option value ="MAG">Magnetic</option>
            <option value ="FP">Finger Print</option>
    </select>
      &nbspBadge Length:
	<input id="BL" type="text" maxlength=2  name="BL" title="Badge Length" size=1 value="01"/>
      &nbspPIN:
     <INPUT TYPE=checkbox CHECKED name="PIN" title="PIN">	 
       </fieldset>
     </td>
</tr>
<tr>
   <td>
     <input type="submit" value="SET" />	
   </td> 
</tr>
</table>
</form>

</body>
</html>