<html>
<!- Copyright (c) Go Ahead Software Inc., 2000-2000. All Rights Reserved. ->
<head>
<title>Proxy Server Settings</title>
<meta http-equiv="Pragma" content="no-cache">
<link rel="stylesheet" href="style/normal_ws.css" type="text/css">
<% language=javascript %>
<script language="JavaScript">
function EnableProxy()
{
  document.forms[0].proxy.disabled = !document.forms[0].proxy.disabled;
  document.forms[0].port.disabled = !document.forms[0].port.disabled;
}

function EnableAuthentication()
{
  document.forms[0].userName.disabled = !document.forms[0].userName.disabled;
  document.forms[0].password.disabled = !document.forms[0].password.disabled;
}

function EnableToSend()
{
  document.forms[0].proxy.disabled = false;
  document.forms[0].port.disabled = false;
}

</script>
</head>

<body>
<h1>Proxy Server Settings</h1>
<form name="ProxySettings" action=/goform/SetProxy method=POST>
<table>
<tr>
    <td>
	 <fieldset>
	 <legend>
	 Proxy Server Settings:
	 </legend>
	 <input id="CHECKBOX1" type="CHECKBOX" name="enableProxy" onclick="EnableProxy()" value="true">Use Proxy Server For Connection<br>
	 Proxy Server:
	 <input id="ps" type="text" name="proxy" disabled="true" size=30>
	 Port:
     <input id="po" type="text" name="port" disabled="true" size=4>
     </fieldset>
    </td>
</tr>
<tr>
    <td>
	 <fieldset>
	 <legend>
	 Proxy Server Authentication Settings:
	 </legend>
	 <input id="CHECKBOX2" type="CHECKBOX" name="enableAuth" onclick="EnableAuthentication()" value="true">Use Proxy Server Authentication<br>
	 User Name:
	  <left><input id="un" type="text" name="userName" disabled="true" size=15></left>
	 Password:
     <input id="pa" type="password" name="password" disabled="true" size=15>
     </fieldset>
    </td>
</tr>
<tr>
   <td>
     <input type="submit" onclick="EnableToSend()" value="SET" />	
   </td> 
</tr>
</table>
</form>

</body>
</html>