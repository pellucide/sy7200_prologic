<html>
<!- Copyright (c) Go Ahead Software Inc., 1994-2000. All Rights Reserved. ->

<head>
<title>Contents for Tech Ref Menu Applet</title>
</head>
<body>
<BLOCKQUOTE>
  <pre>

{0}	{SY-7 0 0 0 Manual}	{SY7000.htm}

	{1}	{English Manual}	{docs/SY7000.pdf} 
	
{0}	{SY-7 0 0 0 Configuration}		{SY7000Conf.htm} 
 
	{1}	{Set Time}	{setdatetime.asp}
	
{0}	{Web Service Configuration}		{SY7000Conf.htm}

    {1}	{Web Service Account}	{confreaders.asp}	
    {1}	{Clock ID}	{confreaders.asp}
    
</pre></BLOCKQUOTE>
</body>
</html>