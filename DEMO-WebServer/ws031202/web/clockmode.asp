<html>
<!- Copyright (c) Synel –Present Perfect  ., 2000-2000. All Rights Reserved. ->
<head>
<title>Set Clock Mode</title>
<meta http-equiv="Pragma" content="no-cache">
<link rel="stylesheet" href="style/normal_ws.css" type="text/css">
<% language=javascript %>


</head>
<body>
<h1>Set Terminal Operation Mode</h1>
<form name="clockmode" action=/goform/SetClockMode onsubmit="return checkClock()" method=POST>
<script language="JavaScript">
function PlayBuzzer()
{

  window.location = "/goform/PalyBuzzer"
}
function StopBuzzer()
{
  window.location = "/goform/StopBuzzer"
}
function SetBuzzerVolume(level)
{
  window.location = "/goform/SetBuzzerVolume?buzzer="+level
}


function SetContrast(level)
{
  window.location = "/goform/SetContrastLevel?contrast="+level
}

function checkClock()
{
    if(60 < clockmode.min.value || clockmode.min.value < 0)
    {
        alert('Please Insert legal minutes');
        clockmode.min.focus();
        return false;
  	}
  	if (clockmode.min.value==null||clockmode.min.value=="")
    {
        alert('Please Insert minutes');clockmode.min.focus();return false;
    }

    if(23 < clockmode.hour.value || clockmode.hour.value < 0)
    {
	   alert('Please Insert legal houres');
	   clockmode.hour.focus();
       return false;
    }
    if (clockmode.hour.value==null||clockmode.hour.value=="")
    {
        alert('Please Insert houres');clockmode.hour.focus();return false;
    }

    if(60 < clockmode.sec.value || clockmode.sec.value < 0)
    {
	  alert('Please Insert legal seconds');
	   clockmode.sec.focus();
      return false;
    }
    if (clockmode.sec.value==null||clockmode.sec.value=="")
    {
        alert('Please Insert seconds');clockmode.sec.focus();return false;
    }
//    if(clockmode.ClockIn.value == clockmode.InPunch.value || clockmode.ClockIn.value == clockmode.OutPunch.value
//        || clockmode.ClockIn.value == clockmode.SwipePunch.value || clockmode.ClockIn.value == clockmode.OutPunch.value )
//    {
//        alert('Multiply Functions To One Key Please Remove This State Before Proceeding');clockmode.ClockIn.focus();return false;
//    }
//    if(clockmode.InPunch.value == clockmode.OutPunch.value || clockmode.InPunch.value == clockmode.SwipePunch.value)
//    {
//        alert('Multiply Functions To One Key Please Remove This State Before Proceeding');clockmode.InPunch.focus();return false;
//    }
//    if(clockmode.OutPunch.value == clockmode.SwipePunch.value)
//    {
//        alert('Multiply Functions To One Key Please Remove This State Before Proceeding');clockmode.OutPunch.focus();return false;
//    }
//    if(clockmode.ClockInLev1.value != "NONE" &&(clockmode.ClockInLev1.value == clockmode.ClockInLev2.value || clockmode.ClockInLev1.value == clockmode.ClockInLev3.value
//        || clockmode.ClockInLev1.value == clockmode.ClockInLev4.value))
//    {
//        alert('Multiply ClockIn Data Type  Please Remove This State Before Proceeding');clockmode.ClockInLev1.focus();return false;
//    }
//    if(clockmode.ClockInLev2.value != "NONE" && (clockmode.ClockInLev2.value == clockmode.ClockInLev3.value || clockmode.ClockInLev2.value == clockmode.ClockInLev4.value))
//    {
//        alert('Multiply ClockIn Data Type  Please Remove This State Before Proceeding');clockmode.ClockInLev2.focus();return false;
//    }
//    if(clockmode.ClockInLev3.value != "NONE" && (clockmode.ClockInLev3.value == clockmode.ClockInLev4.value))
//    {
//        alert('Multiply ClockIn Data Type  Please Remove This State Before Proceeding');clockmode.ClockInLev3.focus();return false;
//    }
//    if(clockmode.ClockInLev2.value != "NONE")
//    {
//      if(clockmode.ClockInLev1.value == "NONE")
//       {alert('ClockIn Levels Not Sequential Please Remove This State Before Proceeding');clockmode.ClockInLev1.focus();return false;}
//    }
//    if(clockmode.ClockInLev3.value != "NONE")
//    {
//      if(clockmode.ClockInLev1.value == "NONE")
//       {alert('ClockIn Levels Not Sequential Please Remove This State Before Proceeding');clockmode.ClockInLev1.focus();return false;}
//      if(clockmode.ClockInLev2.value == "NONE")
//       {alert('ClockIn Levels Not Sequential Please Remove This State Before Proceeding');clockmode.ClockInLev2.focus();return false;}
//    }
//    if(clockmode.ClockInLev4.value != "NONE")
//    {
//      if(clockmode.ClockInLev1.value == "NONE")
//       {alert('ClockIn Levels Not Sequential Please Remove This State Before Proceeding');clockmode.ClockInLev1.focus();return false;}
//      if(clockmode.ClockInLev2.value == "NONE")
//       {alert('ClockIn Levels Not Sequential Please Remove This State Before Proceeding');clockmode.ClockInLev2.focus();return false;}
//      if(clockmode.ClockInLev3.value == "NONE")
//       {alert('ClockIn Levels Not Sequential Please Remove This State Before Proceeding');clockmode.ClockInLev3.focus();return false;}
//    }


    return true;
}
</script>
<table COLS="2" WIDTH="50%">
<tr>
     <td>
      <fieldset>
       <legend>
	   Turn On/Off Swipe And Go Mode :
       </legend>
       <%GetSwipeAndGoStatus();%>Turn On Swipe And Go Mode<br>
	   <input id="sah" type="hidden" name="SwipeAndGoForm" size=40>
       </fieldset>
     </td>
		 <TD>
			<fieldset>
				<legend>
					Title Display
				</legend>
			<input  id="TitleDisplay" maxlength=18  name="TitleDisplay" TITLE="TCode" SIZE="18" value="<%GetTitleDisplay();%>" >
			</fieldset>
		 </TD>
</tr>
<tr>
     <td>
      <fieldset>
       <legend>
	   		Finger Print Unit :
       </legend>
       <%GetFpUnitStatus();%>Enable Finger Print Unit<br><%GetFpVerificationStatus();%>FPU Verification
       </fieldset>
     </td>
		 <TD>
			<fieldset>
				<legend>
					Templates Available
				</legend>
			Check every  <input  id="TAvail" maxlength=10  type="text" name="TemplatesAvailableTimer" TITLE="TemplatesAvailableTimer" size=7 value=<%GetCurrentTemplatesAvailableTimer();%>> seconds.<br><br>
			</fieldset>
		 </TD>
</tr>
<tr>
     <td>
      <fieldset>
       <legend>
	   Terminal Time And Date Synchronization  :
       </legend>
       Time And Date Synchronization Clock :
	    <%GetClockUpdateTime();%>
	  </fieldset>
     </td>
		 <TD>
			<fieldset>
				<legend>
					Offline Mode :
				</legend>
				<%GetOfflineModeStatus();%>Enable Offline Mode<br>
			</fieldset>
		 </TD>
</tr>

</table>
<table>
<tr>
     <td>
      <fieldset>
       <legend>

	   Badge Configuration  :
       </legend>
	   Source :
	    <%GetReadersStatus();%><br><br>
       Badge Length :
	     <!-- <input type=text maxlength=2  name="badgeLen" id="Text1" title="length" size=1 value=<%GetBadgeLength();%>>&nbsp&nbsp -->
			 <input type=text maxlength=2  name="badgeLen" id="Text1" title="length" size=1 value="14">&nbsp&nbsp
		 Badge Offset  :
			<!--<input type=text maxlength=2 name="BadgeOffset" id="Text2" title="offset" size=1 value=<%GetBadgeOffset();%>>&nbsp&nbsp-->
			<input type=text maxlength=2 name="BadgeOffset" id="Text2" title="offset" size=1 value="0">&nbsp&nbsp
		 Badge Valid   :
			<!--<input TYPE="text" NAME="BadgeValid" SIZE="1" MAXLENGTH="2" TITLE="valid" ID="Text3" value=<%GetBadgeValid();%>>&nbsp&nbsp-->
			<input TYPE="text" NAME="BadgeValid" SIZE="1" MAXLENGTH="2" TITLE="valid" ID="Text3" value="14">&nbsp&nbsp
		 Keypad Length   :
			<input TYPE="text" NAME="KeypadLength" SIZE="1" MAXLENGTH="2" TITLE="keypad" ID="Text4" value=<%GetKeypadLength();%>>&nbsp&nbsp
			<!--<input TYPE="text" NAME="KeypadLength" SIZE="1" MAXLENGTH="2" TITLE="keypad" ID="Text4" value="5">&nbsp&nbsp-->
		<br>
      </fieldset>
     </td>
</tr>
<tr>
     <td>
      <fieldset>
       <legend>
	   Badge Verification  : (if FPU Verification is Checked)
       </legend>
       <%GetKeypadNeedsFPV();%>Enable Keypad Verify        <%GetCardNeedsFPV();%>Enable Card Reader Verify
       </fieldset>
     </td>
</tr>
<tr>
  <td>
      <fieldset>
       <legend>
	   Technical Mode  :
       </legend>
	   Technical Mode Code........:<input  id="Password1" type=password maxlength=20  name="code1" title="Code" size=13 >
	   Confirm Technical Mode Code........:<input  id="Password2" type=password maxlength=20  name="code2" title="CCode" size=13 ><br>
	   Reset Only Mode Code.....:<input  id="Password3" type=password maxlength=20  name="code3" title="RCode" size=13 >
	   Confirm Reset Only Mode Code.....:<input  id="Password4" type=password maxlength=20  name="code4" title="RCCode" size=13 >
	  </fieldset>
     </td>
</tr>
<!--<tr>-->
<!--  <td>-->
<!--    <fieldset>-->
<!--      <legend>-->
<!--	  Function Keys  :-->
<!--      </legend>-->
<!--    <table>-->
<!--        <tr>-->
<!--            <td>-->
<!--	          Clock In :<br>-->
<!--	          In Punch :<br>-->
<!--	          Out Punch :<br>-->
<!--	          Swipe Punch :-->
<!--	        </td>-->
<!--            <td>-->
<!--                <select name="ClockIn"  title="ClockIn">-->
<!--	            <%IsSelectedFK("ClockIn","F1"); %><%IsSelectedFK("ClockIn","F2"); %><%IsSelectedFK("ClockIn","F3"); %>-->
<!--                <%IsSelectedFK("ClockIn","F4"); %><%IsSelectedFK("ClockIn","F5"); %><%IsSelectedFK("ClockIn","F6"); %>-->
<!--                <%IsSelectedFK("ClockIn","F7"); %><%IsSelectedFK("ClockIn","F8"); %><%IsSelectedFK("ClockIn","F9"); %>-->
<!--                <%IsSelectedFK("ClockIn","F10"); %><%IsSelectedFK("ClockIn","F11"); %><%IsSelectedFK("ClockIn","F12"); %>-->
<!--                </select>-->
<!--                Levels :-->
<!--                <select name="ClockInLev1"  title="ClockInLev1">-->
<!--                <%IsSelectedClockInLevels("ClockInLev1","NONE"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev1","PS"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev1","PC"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev1","TC"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev1","AC"); %>-->
<!--                </select>-->
<!--                <select name="ClockInLev2"  title="ClockInLev2">-->
<!--                <%IsSelectedClockInLevels("ClockInLev2","NONE"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev2","PS"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev2","PC"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev2","TC"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev2","AC"); %>-->
<!--                </select>-->
<!--                <select name="ClockInLev3"  title="ClockInLev3">-->
<!--                <%IsSelectedClockInLevels("ClockInLev3","NONE"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev3","PS"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev3","PC"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev3","TC"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev3","AC"); %>-->
<!--                </select>-->
<!--                <select name="ClockInLev4"  title="ClockInLev4">-->
<!--                <%IsSelectedClockInLevels("ClockInLev4","NONE"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev4","PS"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev4","PC"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev4","TC"); %>-->
<!--                <%IsSelectedClockInLevels("ClockInLev4","AC"); %>-->
<!--                </select><br>-->
<!--                <select name="InPunch" title="InPunch">-->
<!--	            <%IsSelectedFK("InPunch","F1"); %><%IsSelectedFK("InPunch","F2"); %><%IsSelectedFK("InPunch","F3"); %>-->
<!--                <%IsSelectedFK("InPunch","F4"); %><%IsSelectedFK("InPunch","F5"); %><%IsSelectedFK("InPunch","F6"); %>-->
<!--                <%IsSelectedFK("InPunch","F7"); %><%IsSelectedFK("InPunch","F8"); %><%IsSelectedFK("InPunch","F9"); %>-->
<!--                <%IsSelectedFK("InPunch","F10"); %><%IsSelectedFK("InPunch","F11"); %><%IsSelectedFK("InPunch","F12"); %>-->
<!--                </select><br>-->
<!--                <select name="OutPunch" title="OutPunch">-->
<!--	            <%IsSelectedFK("OutPunch","F1"); %><%IsSelectedFK("OutPunch","F2"); %><%IsSelectedFK("OutPunch","F3"); %>-->
<!--                <%IsSelectedFK("OutPunch","F4"); %><%IsSelectedFK("OutPunch","F5"); %><%IsSelectedFK("OutPunch","F6"); %>-->
<!--                <%IsSelectedFK("OutPunch","F7"); %><%IsSelectedFK("OutPunch","F8"); %><%IsSelectedFK("OutPunch","F9"); %>-->
<!--                <%IsSelectedFK("OutPunch","F10"); %><%IsSelectedFK("OutPunch","F11"); %><%IsSelectedFK("OutPunch","F12"); %>-->
<!--                </select><br>-->
<!--                <select name="SwipePunch" title="SwipePunch">-->
<!--	            <%IsSelectedFK("SwipePunch","F1"); %><%IsSelectedFK("SwipePunch","F2"); %><%IsSelectedFK("SwipePunch","F3"); %>-->
<!--                <%IsSelectedFK("SwipePunch","F4"); %><%IsSelectedFK("SwipePunch","F5"); %><%IsSelectedFK("SwipePunch","F6"); %>-->
<!--                <%IsSelectedFK("SwipePunch","F7"); %><%IsSelectedFK("SwipePunch","F8"); %><%IsSelectedFK("SwipePunch","F9"); %>-->
<!--                <%IsSelectedFK("SwipePunch","F10"); %><%IsSelectedFK("SwipePunch","F11"); %><%IsSelectedFK("SwipePunch","F12"); %>-->
<!--                </select>-->
<!--            </td>-->
<!--      </tr>-->
<!--    </table>-->
<!--    </fieldset>-->
<!--  </td>-->
<!--</tr>-->
<tr>
   <td>
   <fieldset>
       <legend>
	   Set Buzzer And Contrast  :
       </legend>
     <table>
        <tr>
         <td>
            Buzzer:<br>
            Contrast:
         </td>
         <td>
            <font size="2" face="Verdana" color=blue>
            Low&nbsp</font>
             <%SelectedBuzzerVolume("BuzzerVolume");%>
             <font size="2" face="Verdana" color=blue> Hight</font>
             <button onclick="PlayBuzzer()" type = button>Play</button><button onclick="StopBuzzer()" type = button>Stop</button>
             <br>
             <font size="2" face="Verdana" color=blue>
             Low&nbsp</font>
             <%SelectedContrastLevel("ContrastLevel");%>
             <font size="2" face="Verdana" color=blue> Hight</font><br>
         </td>
        </tr>
       </table>

   </fieldset>
   </td>
</tr>
<tr>
   <td>
     <input type="submit" value="SET" />
   </td>
</tr>
</table>
</form>

</body>
</html>
