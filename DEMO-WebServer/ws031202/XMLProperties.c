/**
 * Simple XML Properties object.
 * It can read properties from xml and and write properties to an xml file.
 * This supports only one level of xml, meaning, the root's
 * children are all leaves. It cannot handle multiple levels
 */

#include "XMLProperties.h"
#define DEBUG	1

void XMLPropertiesInit(char * file)
{
	// Registry File
	filename = file;


	// Create the parser
	parser = XML_ParserCreate(NULL);
	if (! parser)
	{
		#ifdef DEBUG
		printf("Couldn't allocate memory for parser\n");
		#endif
	}
	buffern = NULL;
}


void XMLPropertiesFree()
{
	if(parser)
	{
		XML_ParserFree(parser);
	}
	if(buffern) {free(buffern);buffern = NULL;}
}

char * GetProperty(const char * key)
{
	FILE * fp ;
	int hd ;
	struct stat file_info;
	static char value[128];
	PROP_P prop;

	hd = open(filename, O_RDONLY) ;
    fstat(hd, &file_info);
    close(hd);

	fp = fopen(filename, "r");
	if( !fp )
	{
		#ifdef DEBUG
		printf("Couldn't open the Properties file\n");
		#endif
		return NULL;;
	}

	// Reset and reuse the parser
	XML_ParserReset(parser, NULL);
	XML_SetElementHandler(parser, startElementHandler, endElementHandler);
	XML_SetCharacterDataHandler(parser, characterDataHandler);


	value[0] = '\0';

	prop = (PROP_P) malloc( sizeof(PROP) );
	prop->key = (char *)(key);
	prop->value = value;
	prop->command = FINDKEY;
	prop->found = NO0;

	XML_SetUserData(parser, prop);


	buffern =(char*)malloc(file_info.st_size + 1);
#ifdef WIN32
	memset(buffern, 0, file_info.st_size+1);
#else
	bzero(buffern, file_info.st_size+1);
#endif
	for (;;)
	{
		int done;
		int len;

		fread(buffern, 1, file_info.st_size+1, fp);

		if (ferror(fp))
		{
			#ifdef DEBUG
			printf("File Read error\n");
			#endif
            if(prop)free( prop );
			if(buffern){free(buffern);buffern = NULL;}
			return NULL;
    	}
    	len = strlen(buffern);
    	done = feof(fp);
    	if (XML_Parse(parser, buffern, len, done) == XML_STATUS_ERROR)
		{
			#ifdef DEBUG
			printf("Parse error at line %d:\n%s\n",
					XML_GetCurrentLineNumber(parser), XML_ErrorString(XML_GetErrorCode(parser)));
			#endif
			if(buffern){free(buffern);buffern = NULL;}
			if(prop)free( prop );
			return NULL;
		}
    	if(done) break;
	}

	// Cleanup
	if(fp)
	{
		fclose(fp);
	}

	if(prop)free( prop );

	if( strlen(value) != 0 ) {if(buffern)free(buffern);buffern = NULL; return value;}
	if(buffern){free(buffern);buffern = NULL;}
	return NULL;
}





bool SetProperty(const char * key, const char * value)
{
	FILE * fp ;
	FILE * fw;
	int hd ;
	struct stat file_info;
	const char * tmpfile = "propertiestmp.xml";
	PROP_P prop;

	hd = open(filename, O_RDONLY) ;
    fstat(hd, &file_info);
    close(hd);

	fp = fopen(filename, "r");
	//printf("filename = %s\n",filename);
	if( !fp )
	{
		#ifdef DEBUG
		printf("Couldn't open the Properties file\n");
		#endif
		return false;
	}



	fw = fopen(tmpfile, "w");
	if( !fw )
	{
		#ifdef DEBUG
		printf("Couldn't open the file for write\n");
		#endif
		if( fp ) fclose( fp );
		return false;
	}

	if( fputs(XML_HEADER, fw) == EOF )
	{
		if( fp ) fclose( fp );
		if( fw ) fclose( fw );
		return false;
	}

	if( fputs("\n", fw) == EOF )
	{
		if( fp ) fclose( fp );
		if( fw ) fclose( fw );
		return false;
	}

	// Reset and reuse the parser
	XML_ParserReset(parser, NULL);
	XML_SetElementHandler(parser, startElementHandler, endElementHandler);
	XML_SetCharacterDataHandler(parser, characterDataHandler);

	prop = (PROP_P) malloc( sizeof(PROP) );
	prop->key = (char *)(key);
	prop->value = (char *)(value);
	prop->command = SETKEY;
	prop->found = NO0;
	prop->fw = fw;
	prop->status = CMD_STATUS_DEFAULT;

	XML_SetUserData(parser, prop);

	//bzero(buffer, BUFFERSIZE);
	buffern =(char*)malloc(file_info.st_size + 1);
#ifdef WIN32
	memset(buffern, 0, file_info.st_size+1);
#else
	bzero(buffern, file_info.st_size+1);
#endif
	for (;;)
	{
		int done;
		int len;

		fread(buffern, 1, file_info.st_size + 1, fp);

		if (ferror(fp))
		{
			#ifdef DEBUG
			printf("File Read error\n");
			#endif
			prop->status = CMD_STATUS_DEFAULT;
			break;
    	}
    	len = strlen(buffern);
    	done = feof(fp);
    	if (XML_Parse(parser, buffern, len, done) == XML_STATUS_ERROR)
		{
			#ifdef DEBUG
			printf("Parse error at line %d:\n%s\n",
					XML_GetCurrentLineNumber(parser), XML_ErrorString(XML_GetErrorCode(parser)));
			#endif
			prop->status = CMD_STATUS_DEFAULT;
			break;
		}
		if(done) break;
	}

	// Cleanup
	if(fp)
	{
		fclose(fp);
	}
	if(fw)
	{
		fclose(fw);
	}

	// Rename the File
	if( prop->status == CMD_SETKEY_SUCCESS )
	{
#ifdef WIN32
		unlink(filename); // win32 rename only works if 2nd arg file does not exist
#endif
		rename(tmpfile, filename);
		if(prop)free( prop );
		if(buffern){free(buffern);buffern = NULL;}
		return true;
	}
	if(prop)free( prop );
	if(buffern){free(buffern);buffern = NULL;}
	return false;
}

/**
 * Start Element Handler
 */
void XMLCALL startElementHandler(void * userData, const char * name, const char ** atts)
{
	//int i;
	if( userData != NULL )
	{
		PROP_P prop = (PROP_P)userData;
		(prop->level)++; // Increase The Level
		if( strcmp( name, prop->key ) == 0 )
		{
			prop->found = YES1;
		}
		if( prop->command == SETKEY )
		{
			if( prop->status == CMD_FILE_WRITE_FAIL ) return;

			/*for(i = 1; i < prop->level; i++)
			{
				if( fputs("\t", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
			}*/
			if( fputs("<", prop->fw) == EOF )
			{
				prop->status = CMD_FILE_WRITE_FAIL;
				return;
			}
			if( fputs(name, prop->fw) == EOF )
			{
				prop->status = CMD_FILE_WRITE_FAIL;
				return;
			}
			if( prop->level == 1 )
			{
				if( fputs(">\n", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
			}
			else
			{
				if( fputs(">", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
			}
		}
	}
}

/**
 * End Element Handler
 */
void XMLCALL endElementHandler(void * userData, const char * name)
{
	if( userData != NULL )
	{
		PROP_P prop = (PROP_P)userData;
		(prop->level)--;
		if( prop->command == FINDKEY )
		{
			if( strcmp( name, prop->key ) == 0 )
			{
				prop->found = NO0;
			}
		}
		if( prop->command == SETKEY )
		{
			if( prop->status == CMD_FILE_WRITE_FAIL ) return;

			// Handle the case where in the key is not present in the xml file
			if( prop->level == 0 && prop->status != CMD_SETKEY_SUCCESS )
			{
				if( fputs("\t<", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs(prop->key, prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs(">", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs(prop->value, prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs("</", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs(prop->key, prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs(">\n", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				prop->status = CMD_SETKEY_SUCCESS;
			}

			// Handle the case where in the element is empty
			if( prop->found == YES1 )
			{
				if( prop->status != CMD_SETKEY_SUCCESS )
				{
					if( fputs(prop->value, prop->fw) == EOF )
					{
						prop->status = CMD_FILE_WRITE_FAIL;
						prop->found = NO0;
						return;
					}
					prop->status = CMD_SETKEY_SUCCESS;
				}
				prop->found = NO0;
			}

			if( fputs("</", prop->fw) == EOF )
			{
				prop->status = CMD_FILE_WRITE_FAIL;
				return;
			}
			if( fputs(name, prop->fw) == EOF )
			{
				prop->status = CMD_FILE_WRITE_FAIL;
				return;
			}
			if( fputs(">\n", prop->fw) == EOF )
			{
				prop->status = CMD_FILE_WRITE_FAIL;
				return;
			}
		}
	}
}

/**
 * Character Data Handler
 */
void XMLCALL characterDataHandler(void * userData, const XML_Char * s, int len)
{
	char *data;
	data = (char*)malloc(len+1);
	if( userData != NULL )
	{
		PROP_P prop = (PROP_P)userData;
		if( prop->command == FINDKEY )
		{
			if( prop->found == YES1 )
			{
				strncpy(prop->value, s, len);
				prop->value[len] = '\0';
			}
		}
		else if( prop->command == SETKEY )
		{
			if( prop->status == CMD_FILE_WRITE_FAIL ) return;
			// ignore tabs and newlines
			if(*s != '\n' && *s != '\t')
			{
				if( prop->found == YES1 )
				{
					if( fputs(prop->value, prop->fw) == EOF )
					{
						prop->status = CMD_FILE_WRITE_FAIL;
						return;
					}
					prop->status = CMD_SETKEY_SUCCESS;
				}
				else
				{
					//char data[len + 1];
					strncpy(data, s, len);
					data[len] = '\0';
					if( fputs(data, prop->fw) == EOF )
					{
						prop->status = CMD_FILE_WRITE_FAIL;
					}
				}
			}
		}
	}
	free(data);
}


