#ifndef _XMLPROPERTIES_H_
#define _XMLPROPERTIES_H_

/**
 * Simple XML Properties object.
 * It can read properties from xml and and write properties to an xml file.
 * This supports only one level of xml, meaning, the root's
 * children are all leaves. It cannot handle multiple levels
 */
#include <stdio.h>
#include <string.h>
#include <expat.h>
#include <sys/stat.h>
#include <fcntl.h>
#ifdef WIN32
#include <stdlib.h>
#include <io.h>
#else
#include <unistd.h>
#endif

#ifndef YES1
#define YES1 		0x01
#endif
#ifndef NO0
#define NO0	 		0x00
#endif

#define XML_HEADER  "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
#define BUFFERSIZE 1024
typedef enum COMMAND
{
	FINDKEY = 0x01,
	SETKEY = 0x02
}COMMAND;

typedef enum CMD_STATUS
{	
	CMD_STATUS_DEFAULT = 0x00,
	CMD_FINDKEY_SUCCESS = 0x01,
	CMD_SETKEY_SUCCESS = 0x02,
	// Errors
	CMD_FILE_WRITE_FAIL = 0x03
}CMD_STATUS;

typedef enum bool
{	
	false = 0,
	true = 1
}bool;

typedef struct prop
{
	char * key;
	char * value;
	COMMAND command;
	unsigned char found;
	unsigned char level;
	FILE * fw;
	CMD_STATUS status;
} PROP, *PROP_P;

// XML Handlers
void XMLCALL startElementHandler(void * userData, const char * name, const char ** atts);
void XMLCALL endElementHandler(void * userData, const char * name);
void XMLCALL characterDataHandler(void * userData, const XML_Char * s, int len);

void XMLPropertiesInit(char * filename);
void XMLPropertiesFree();
	
char * GetProperty(const char * key);
bool SetProperty(const char * key, const char * value);

static char* filename;
static XML_Parser parser;
static char* buffern;
static char buffer[BUFFERSIZE];

#endif //_XMLPROPERTIES_H_
