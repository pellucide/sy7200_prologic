/*
	Fields.h:

	Written by:   Alan J. Luse
	Written on:   31 Aug 2004
	 $Revision: 1.1.1.1 $
	   $Author: AlanL $
	  $Modtime$
	     $Date: 2006/12/01 18:43:11 $

*/


#if ! defined FIELDS_H
#define FIELDS_H

/***
Macro functions to manipulate bit masked fields:
	For example, if masks for a 1-bit field and an 8-bit field are
	defined like this:

	#define REGISTER_ABCFIELD   0x0002
	#define REGISTER_XYZFIELD   0xFF00
	
	Then a macro call like this:
		imm_p->REGISTER =
			(field_add (Bit16, REGISTER_ABCFIELD, 0xa3) |
			field_add (Bit16, REGISTER_XYZFIELD, 1)
			);
	should set REGISTER to the value 0xa302.

	while:
		field_get (Bit16, REGISTER_XYZFIELD, IMMR->REGISTER)
	should return 0xa3
***/
/* Add a field value to a presumably blank value. */
#define field_add(type, field, value) ((type)((value)*(((type)field)&((~((type)field))+1))))
/* Clear the field and set a new value. */
#define field_set(type, field, value, current) ((type)(((type)(current)&~(type)(field))|((type)(field)&field_add(type, field, value))))
/* Extract a field's value. */
#define field_get(type, field, current) ((type)(((type)(current)&(type)(field))/(((type)field)&((~((type)field))+1))))

/***
Generate field masks from field bit length and (optionally) bit position;
	Bit position is 0 for highest order bit, etc.
***/
/* Field mask of length low order bits. */
#define field_mask_low(type, length) ((type)(((type)0x1 << (length))-1))
/* Field mask of length bits starting position bits before lowest order bit. */
#define field_mask_lsb(type, length, position) ((type)(field_mask_low(type, length) << (position)))
/* Field mask of length bits starting position bits after highest order bit. */
#define field_mask_msb(type, length, position) ((type)(field_mask_low(type, length) << ((sizeof(type)*8) - (position) - (length))))
/* Field mask of length bits starting with highest order bit. */
#define field_mask_high(type, length) field_mask(type, length, 0)
/* Default field mask is from MSB. */
#define field_mask field_mask_msb

/* Get field of length bits starting position bits before lowest order bit. */
#define field_mask_get_lsb(type, length, position, value) (((type)(value) >> (position)) & (field_mask_low(type, length)))

#endif /* ! FIELDS_H */

