/*
	common.h
	
	Dummy U-Boot common.h file.
	
*/

/* This flag says we are compiling for library. */		
#define CONFIG_LIBNE		1

/* These defines enable shared U-Boot code segments. */
#if ! defined LIBNE_CYGWIN
#define CONFIG_SPLASH_TEXT
#define CONFIG_NE7000_USER
#endif

