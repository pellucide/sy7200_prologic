/**
 *  	Module log 
 *
 *  	@author 	sjlee@suprema.co.kr
 *  	@see    	uf_config.h
 */


/*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */

 #ifndef	__UNIFINGERMODULELOG_H__
 #define __UNIFINGERMODULELOG_H__

#define UF_MODULE_LOG_SIZE	16

typedef enum {
	UF_LOG_SOURCE_HOST_PORT		= 0x01,
	UF_LOG_SOURCE_AUX_PORT		= 0x02,
	UF_LOG_SOURCE_WIEGAND_INPUT	= 0x03,
	UF_LOG_SOURCE_IN0				= 0x04,
	UF_LOG_SOURCE_IN1				= 0x05,
	UF_LOG_SOURCE_IN2				= 0x06,
	UF_LOG_SOURCE_FREESCAN		= 0x07,
 } UF_LOG_SOURCE;

#endif
