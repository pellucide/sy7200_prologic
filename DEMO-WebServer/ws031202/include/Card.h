/*
    Card.h

    Definitions for proximity, magnetic and barcode card reader modules.

	Copyright (c) 2005 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   09 Jun 2005
	 $Revision: 1.1.1.1 $
	   $Author: AlanL $
	  $Modtime$
	     $Date: 2006/12/01 18:43:11 $

*/


#ifndef _PPC_CARD_H
#define _PPC_CARD_H

/* Include standard and bit buffer module definitions. */
#include <StdDef.h>
#include "BitBuffer.h"

#if ! defined _GEN_DEF
typedef unsigned char UChar;
typedef signed char SBin7;
typedef unsigned char UBin8;
typedef unsigned long int Bit32;
#endif
typedef unsigned long long Bit64;

/* Parity setting. */
typedef SBin7 CardParity_t;
typedef enum {
	CardParityOff = -1,
	CardParityEven = 0,
	CardParityOdd = 1,
}
CardParity_e;


/*** Magnetic card types & definitions. ***/

/* Magnetic card character encoding constants. */
#define CardMagEncode5BitMax            ((0x1<<5) - 1)
#define CardMagEncode7BitMax            ((0x1<<7) - 1)
#define CardMagEncodeFlag               0x80
#define CardMagEncodeErrorParity        0xFF
#define CardMagEncodeErrorInvalid       0xFE
#define CardMagEncodeNational           0xB0
#define CardMagEncodeGraphics           0x80

/* Encoding tables. */
extern const UChar CardMagEncode5Bit [CardMagEncode5BitMax+1];
extern const UChar CardMagEncode7Bit [CardMagEncode7BitMax+1];

/* Check if decoded character is valid. */
#define CardMagDecodeValid(byte)        (((byte)&CardMagEncodeFlag)?0:1)

/* Check if decoded character is valid. */
#define CardMagParityInvalid(byte)         ((byte) == CardMagEncodeErrorParity)

/* Magnetic card track setup data. */
typedef struct CardMagTrackSetup_s {
	unsigned int char_width;        /* Including parity bit. */
	const UChar * encode_table;
	CardParity_t parity;
	struct {
		UChar start;
		UChar end;
	}
	sentinel;
	struct {
		unsigned int chars;
		unsigned int bits;
	}
	length;
	unsigned int bpi;
}
CardMagTrackSetup_t;

/* Standard magnetic card track setups. */
extern const CardMagTrackSetup_t CardMagTrack1;
extern const CardMagTrackSetup_t CardMagTrack2;
extern const CardMagTrackSetup_t CardMagTrack3;

/* External function prototypes. */
int		card_mag_convert (const CardMagTrackSetup_t * const setup_p,
			BitBuffer_t * const bb_p, UChar * data_p, size_t max_length);

void		card_mag_track (const CardMagTrackSetup_t * const setup_p,
			BitBuffer_t * const bb_p, const UChar * data_p,
			BitIndex_t leading, BitIndex_t trailing);
void            card_mag_track_reverse (const CardMagTrackSetup_t * const setup_p,
			BitBuffer_t * const bb_p, const UChar * data_p,
			BitIndex_t leading, BitIndex_t trailing);


/*** Proximity card types & definitions. ***/

/* Proximity card data types. */
typedef Bit32 CardProxFacility_t;

/* Proximity card format definition data structures. */
typedef struct {
	CardParity_t type;
	UBin8 position;
	UBin8 start;
	UBin8 length;
}
CardProxFieldParity_t;
typedef struct {
	UBin8 start;
	UBin8 length;
	char delimiter;
	UBin8 width;
}
CardProxFieldBadge_t;
typedef struct CardProxFormat_s {
	char name [16];
	unsigned int bit_length;
	CardProxFieldParity_t parity [2];
	CardProxFieldBadge_t field [3];
}
CardProxFormat_t;

/* Field value names arrays. */
extern const char * CardNamesParity [3];
extern const char * CardNamesBadgeFields [3];

#endif

