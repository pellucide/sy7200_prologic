#ifndef _TAVector_H_
#define _TAVector_H_

#include <TAAlgorithm.h>

#include <stddef.h>

const int DEF_VECTOR_SIZE = 25;
//20060105 wjm. Vector will always be terminated with a 0 value. This might be problematic in the long run.
//for now it allows to use the vector template for zero-terminated strings.
template <typename Val>
class TAVector
{
public:
  typedef Val value_type ;
  typedef Val* iterator_t ;
  typedef const Val* const_iterator_t ;

	TAVector();
	TAVector(int);

	virtual ~TAVector();

	TAVector& operator = ( const Val* r ){ //copy array until null value. Useful for char*.
	  set ( r , TAFind ( r , r - 1 , 0 ));
	  return * this ;
	}

	void resize ( const unsigned int size );
	void pop_back();
	void clear();
	Val * back();
	Val * front();
	iterator_t begin () { return valPtr ;}
	iterator_t end () { return valPtr + sizeofvect ;}

	const_iterator_t begin () const { return valPtr ;}
	const_iterator_t end () const { return valPtr + sizeofvect ;}

	void push_back(const Val&);
	bool erase(int index);

	void set ( const Val* b , const Val* e );
	void insert ( const int index , const Val v );
	void append ( const Val v ){ insert ( end () - begin () , v );}

	int size () const ;
	Val* operator[](int index);

private:
	TAVector ( const TAVector& );
	TAVector& operator = ( const TAVector &);

	int sizeofvect;
	int maxsize;
	int offset;
	Val* valPtr;
};

template <typename Val>
TAVector<Val>::TAVector()
{
	valPtr = new Val[DEF_VECTOR_SIZE];
	maxsize = DEF_VECTOR_SIZE;
	sizeofvect = 0;
  resize ( 0 );
}

template <typename Val>
TAVector<Val>::TAVector(int s)
{
	if(s > 0)
	{
		valPtr = new Val[s];
		maxsize = s;
		sizeofvect = 0;	
		resize ( s );
	}
	else
	{
		this();
	}
}

template <typename Val>
TAVector<Val>::~TAVector()
{
	delete[] valPtr;
}

template <typename Val>
void TAVector<Val>::pop_back()
{
	//valPtr[--sizeofvect] = NULL;
	sizeofvect--;
}

template <typename Val>
void TAVector<Val>::push_back(const Val& val)
{
  resize ( sizeofvect + 1 );
  valPtr[sizeofvect - 1 ] = val;
}

template < typename Val>
void TAVector<Val> ::set ( const Val* b , const Val* e ){
  resize ( e - b );
  Val* d = valPtr ;
  while ( b != e ){
    *d = *b ;
    b ++ ;
    d ++ ;
  }
}

template <typename Val>
void TAVector<Val>::resize(const unsigned int size )
{
  if((int)(size) + 1   > maxsize)
	{
		maxsize = size + DEF_VECTOR_SIZE + 1 ;
		Val * tmp = new Val[maxsize];
		for(int i=0; i<sizeofvect ; i++)
		{
			tmp[i] = valPtr[i];
		}
		delete[] valPtr;
		valPtr = tmp;
		tmp = NULL;
	}		
	sizeofvect = size ;
	//	valPtr [ sizeofvect ] = 0 ;
}

template <typename Val>
int TAVector<Val>::size() const 
{
	return sizeofvect;
}

template <typename Val>
void TAVector<Val>::clear()
{
	delete[] valPtr;
	valPtr = new Val[DEF_VECTOR_SIZE];
	maxsize = DEF_VECTOR_SIZE;
	sizeofvect = 0;
}

template <typename Val>
Val* TAVector<Val>::back()
{
	if(sizeofvect == 0) return NULL;
	return &valPtr[sizeofvect-1];
}

template <typename Val>
Val* TAVector<Val>::front()
{
	if(sizeofvect == 0) return NULL;
	return &valPtr[0];
}

template <typename Val>
bool TAVector<Val>::erase(int index)
{
	if(index < 0 || index >= sizeofvect) return false;
	for(int i = index; i<sizeofvect-1; i++)
	{
		valPtr[i] = valPtr[i + 1];
	}
	//valPtr[sizeofvect - 1] = NULL;
	sizeofvect--; 
	return true;
}

template <typename Val>
Val* TAVector<Val>::operator[](int index)
{
	if(index < 0 || index >= sizeofvect) return NULL;
	return &valPtr[index];
}

template <typename Val>
void TAVector<Val>::insert ( const int index , const Val v ){
  resize ( size () + 1 );
  for ( int i = size () - 2 ; i >= index ; i -- ){
    valPtr [ i + 1 ] = valPtr [ i ] ;
  }
  valPtr [ index ] = v ;
}

#endif //_TAVector_H_
