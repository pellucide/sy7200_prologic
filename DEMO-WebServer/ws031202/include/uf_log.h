/**
 *  	SDK log 
 *	- To turn on log, define UF_LOG_ENABLE
 *
 *  	@author 	sjlee@suprema.co.kr
 *  	@see    	uf_config.h
 */


/*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */

 #ifndef	__UNIFINGERLOG_H__
 #define __UNIFINGERLOG_H__

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include "uf_config.h"

#ifdef UF_USING_CPP
extern "C" 
{
#endif

typedef enum {
	ERR_CH		= 0x01,
	API_CH		= 0x02,
	COM_CH		= 0x04,
	DATA_CH		= 0x08,
	USER_CH		= 0x10,
} UF_LOG_CHANNEL;

typedef enum {
	LOG_MSG_COMMAND					= 0x01,
	LOG_MSG_SYS_STATUS				= 0x02,

	LOG_MSG_SYS_PARAMETER			= 0x10,
	LOG_MSG_BAUDRATE				= 0x11,

	LOG_MSG_ENROLL_OPTION			= 0x20,

	LOG_MSG_GPIO_MODE				= 0x30,
	LOG_MSG_GPIO_INPUT_FUNC			= 0x31,
	LOG_MSG_GPIO_INPUT_ACTIVATION 	= 0x32,
	LOG_MSG_GPIO_OUTPUT_FUNC 		= 0x33,
	LOG_MSG_GPIO_OUTPUT_LEVEL		= 0x34,

	LOG_MSG_WIEGAND_FUNCTION		= 0x40,
	LOG_MSG_WIEGAND_PORT			= 0x41,
	LOG_MSG_WIEGAND_GET_INPUT_OPTION	= 0x42,

	LOG_MSG_INPUT_FUNC				= 0x50,
	LOG_MSG_INPUT_PORT				= 0x51,
	LOG_MSG_INPUT_OPTION			= 0x52,
	LOG_MSG_INPUT_STATUS			= 0x53,

	LOG_MSG_OUTPUT_EVENT			= 0x60,
	LOG_MSG_OUTPUT_PORT 			= 0x61,
	LOG_MSG_OUTPUT_SETTING			= 0x62,

	LOG_MSG_LOG_SOURCE				= 0x63,

	LOG_MSG_RAW_DATA				= 0x64,

	LOG_MSG_ADMIN_LEVEL				= 0x65,
} UF_LOG_MSG_TYPE;


char* uf_get_log_msg( UF_LOG_MSG_TYPE msg_type, int msg_code );
int uf_get_log_msg_code( UF_LOG_MSG_TYPE msg_type, const char* msg );
void uf_init_log_msg();

void uf_log( UF_LOG_CHANNEL channel, char* fmt, ... );
void uf_log_cont( UF_LOG_CHANNEL channel, char* fmt, ... );
void uf_trace( char* fmt, ... );
void uf_log_on( UF_LOG_CHANNEL channel );
void uf_log_off( UF_LOG_CHANNEL channel );

#ifdef UF_LOG_ENABLE
#define	UF_LOG 			uf_log
#define 	UF_LOG_CONT	uf_log_cont
#if defined(UF_SDK_WIN32)
#define 	UF_TRACE		fprintf(stderr, "[%s(%d)] ", strrchr( __FILE__, '\\') + 1, __LINE__ ), uf_trace
#else
#define 	UF_TRACE		fprintf(stderr, "[%s(%d)] ", __FILE__, __LINE__) , uf_trace
#endif
#define	UF_TRACE_CONT	uf_trace
#define	UF_LOG_ON		uf_log_on
#define	UF_LOG_OFF		uf_log_off
#else
#define	UF_LOG			if(1) {} else uf_log
#define	UF_LOG_CONT	if(1) {} else uf_log_cont
#define	UF_TRACE		if(1) {} else uf_trace
#define	UF_TRACE_CONT	if(1) {} else uf_trace
#define	UF_LOG_ON		if(1) {} else uf_log_on
#define 	UF_LOG_OFF		if(1) {} else uf_log_off
#endif

#ifdef UF_USING_CPP
}
#endif


 #endif

