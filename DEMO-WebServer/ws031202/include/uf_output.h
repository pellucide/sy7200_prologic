/**
 *  	Definitions of Output port related constants
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */


/*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */
 
#ifndef __UNIFINGEROUTPUT_H__
#define __UNIFINGEROUTPUT_H__

typedef enum {
	UF_OUTPUT_CLEAR_ALL_EVENTS		= 0x00,
	UF_OUTPUT_ENROLL_WAIT_WIEGAND	= 0x13,
	UF_OUTPUT_ENROLL_WAIT_FINGER	= 0x14,
	UF_OUTPUT_ENROLL_PROCESSING		= 0x15,
	UF_OUTPUT_ENROLL_BAD_FINGER		= 0x16,
	UF_OUTPUT_ENROLL_SUCCESS		= 0x17,
	UF_OUTPUT_ENROLL_FAIL			= 0x18,
	UF_OUTPUT_VERIFY_WAIT_WIEGAND	= 0x23,
	UF_OUTPUT_VERIFY_WAIT_FINGER	= 0x24,
	UF_OUTPUT_VERIFY_PROCESSING		= 0x25,
	UF_OUTPUT_VERIFY_BAD_FINGER		= 0x26,
	UF_OUTPUT_VERIFY_SUCCESS			= 0x27,
	UF_OUTPUT_VERIFY_FAIL			= 0x28,
	UF_OUTPUT_IDENTIFY_WAIT_FINGER	= 0x34,
	UF_OUTPUT_IDENTIFY_PROCESSING	= 0x35,
	UF_OUTPUT_IDENTIFY_BAD_FINGER	= 0x36,
	UF_OUTPUT_IDENTIFY_SUCCESS		= 0x37,
	UF_OUTPUT_IDENTIFY_FAIL			= 0x38,
	UF_OUTPUT_DELETE_WAIT_WIEGAND	= 0x43,
	UF_OUTPUT_DELETE_WAIT_FINGER	= 0x44,
	UF_OUTPUT_DELETE_PROCESSING		= 0x45,
	UF_OUTPUT_DELETE_BAD_FINGER		= 0x46,
	UF_OUTPUT_DELETE_SUCCESS		= 0x47,
	UF_OUTPUT_DELETE_FAIL			= 0x48,
	UF_OUTPUT_DETECT_INPUT0			= 0x54,
	UF_OUTPUT_DETECT_INPUT1			= 0x55,
	UF_OUTPUT_DETECT_INPUT2			= 0x56,	
	UF_OUTPUT_DETECT_WIEGAND		= 0x57,
	UF_OUTPUT_DETECT_FINGER			= 0x58,
	UF_OUTPUT_END_PROCESSING		= 0x59
} UF_OUTPUT_EVENT;


typedef enum {
	UF_OUTPUT_PORT0	= 0x00,
	UF_OUTPUT_PORT1	= 0x01,
	UF_OUTPUT_PORT2	= 0x02,
	UF_OUTPUT_LED0		= 0x03,
	UF_OUTPUT_LED1		= 0x04,
	UF_OUTPUT_LED2		= 0x05,
} UF_OUTPUT_PORT;


typedef enum {
	UF_OUTPUT_CLEAR	= 0x00,
	UF_OUTPUT_SET		= 0x01
} UF_OUTPUT_SETTING;

#endif
