/**
 *  	Constants for managing templates
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */


/*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */

 #ifndef __UNIFINGERTEMPLATE_H__
 #define __UNIFINGERTEMPLATE_H__

typedef enum {
	UF_ADMIN_NONE	 	= 0x0,
	UF_ADMIN_ENROLL		= 0x1,
	UF_ADMIN_DELETE		= 0x2,
	UF_ADMIN_ALL		= 0x3,
} UF_ADMIN_LEVEL;

#endif

