//Copyright (C) 2005 TimeAmerica, Inc All Rights Reserved
//$Id: EventManager.h,v 1.5 2006/01/27 23:26:39 walterm Exp $

#ifndef _EVENTMANAGER_H_
#define _EVENTMANAGER_H_

#include <pthread.h>

#include "Event.h"
#include "TAVector.h"
//*****************************************************************
class EventManager
{
 public:
  virtual ~EventManager();
	
  /**
   * Registering an Event
   */
  void RegisterForEvent(Event eventId, EventCallback callback , void* userdata );
  void UnRegisterForEvent(Event eventId, EventCallback callback);

  //InstallNetworkServer should not be called more than once!
  int InstallNetworkServer ( const unsigned int port , const char* ifc = "eth0" );

  int createLowResTimer (long timeoutSeconds);
  void killTimer (int timerId);

  int EventLoop ();

  static EventManager * GetInstance();
	
 private:
  EventManager();
  void WatchEvents ();
  void HandleEvent( Event id , const char* buf , const unsigned int size );

  static EventManager * instance;

  //***************************************************************
  //event source. Data associated with an event source
  class evtSrc {
  public:
    int fd ;
    Event id ;
  };

  class callbackRecord {
  public:
    callbackRecord ( Event iid = EVENT_NOOP , EventCallback ccb = 0 , void* userdata = 0 ) : id ( iid ) , cb ( ccb ) {}
    Event id ;
    EventCallback cb ;
    void* userdata ;
  };

  TAVector < evtSrc > sources ;
  typedef TAVector < evtSrc > vecsrc_t ;

  typedef TAVector < callbackRecord > cbr_t ;
  cbr_t callbacks ;

  void AddSocketSrc (); //add socket to the list of event sources
  vecsrc_t ::iterator_t FindById ( const Event id );
  int maxFd ();

  int timerIds;
  int listenSocket ;
  int* rcvSocket ;
};

#endif //_EVENTMANAGER_H_
