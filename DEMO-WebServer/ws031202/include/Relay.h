#ifndef _RELAY_H_
#define _RELAY_H_

class Relay
{
public:
	Relay();
	virtual ~Relay();
	
	void Set(int index, int state);
};

#endif //_RELAY_H_
