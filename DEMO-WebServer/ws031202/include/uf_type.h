/**
 *  	Misc type definition
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */


/*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */

#ifndef __UNIFINGERTYPES_H__
#define __UNIFINGERTYPES_H__

typedef unsigned char 	UF_UINT8;
typedef unsigned short	UF_UINT16;
typedef	unsigned long	UF_UINT32;
typedef	unsigned char	UF_BYTE;
typedef char			UF_INT8;
typedef short			UF_INT16;
typedef int				UF_INT32;
typedef enum {
	UF_FALSE 	= 0,
	UF_TRUE		= 1,
} UF_BOOL;

#endif

