/*
	time_def.h

	External definitions and function prototypes for the timer module.

	Copyright (c) 1995,1997-1998 - Alcom Communications

	Written by:	  Alan J. Luse
	Written on:	  16 May 1990
	 $Revision: 1.1.1.1 $
	   $Author: AlanL $
	  $Modtime:   01 Mar 2000 17:29:16  $
	     $Date: 2006/12/01 18:43:11 $

*/


#if ! defined _TIME_DEF

/* Include compiler timer definitions for clock_t data type. */
#include <time.h>

/* Data type for timer intervals. */
typedef long TimerInterval_t;
typedef TimerInterval_t Timer_Interval;

/* Time interval conversion constants. */
#define MS_PER_SEC	((TimerInterval_t)1000)

/* Type and structure definitions for timers. */
typedef struct {
	clock_t date_time;
	}
Timer_t, * Timer_Hdl;

/* Map timer handles to __near memory if requested. */
#if defined TIMER_NEAR
#define THANDLE __near Timer_t *
#else
#define THANDLE Timer_t *
#endif

/* External function prototypes for timer module. */
Handle
		timer_instance (TimerInterval_t milliseconds);
void *	timer_release (Handle timer_hdl);
void  	timer_reset (THANDLE timer_hdl, TimerInterval_t milliseconds);
TimerInterval_t
		timer_elapsed (THANDLE timer_hdl);
TimerInterval_t
		timer_left (THANDLE timer_hdl);
Boolean	timer_done (THANDLE timer_hdl);
void	timer_wait (TimerInterval_t milliseconds);

#define _TIME_DEF
#endif

