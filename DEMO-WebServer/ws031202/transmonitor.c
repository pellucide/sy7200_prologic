#include	"wsIntrn.h"
#include <ctpublic.h>
#include "common.h"

#define		NONE_OPTION		T("<NONE>")
#define		MSG_START		T("<body><h2>")
#define		MSG_END			T("</h2></body>")

static int		aspMakeTransTable(int eid, webs_t wp,int argc, char_t **argv);
static int		formAddUser(webs_t wp, char_t *path, char_t *query);
static void		websMsgStart(webs_t wp);
static void		websMsgEnd(webs_t wp);

void aspDefineTransMonitor(void)
{

    websAspDefine(T("MakeTransTable"), aspMakeTransTable);
    websFormDefine(T("AddUserToDB"),formAddUser);
}


static int		aspMakeTransTable(int eid, webs_t wp,int argc, char_t **argv)
{
    a_assert(wp);
    
    CS_CONTEXT *ctx;
	CS_CONNECTION *conn;
	CS_COMMAND *cmd;
	
	CS_INT num_cols;
	CS_INT count, row_count = 0;
	CS_INT result_type;
	CS_RETCODE ret;
	CS_RETCODE results_ret;
	int i, is_return_status=0;
	int is_status_result=0;
	
	enum {maxcol=10, colsize=260};
	
	struct _col { 
		CS_DATAFMT datafmt;
		CS_INT datalength;
		CS_SMALLINT ind;
		CS_CHAR data[colsize];
	} col[maxcol];
	
	int verbose = 0;
	
	ret = try_ctlogin(&ctx, &conn, &cmd, verbose);
	if (ret != CS_SUCCEED) {
		websWrite(wp, T("<tr>Connedt To The Data Base Failed</tr>"));
		fprintf(stderr, "SQL Login failed\n");
		return -1;
	}
	
	ret = ct_command(cmd, CS_LANG_CMD, " select Users.Name,Badges.Badge,Badges.Date,Fun from Users RIGHT OUTER JOIN Badges ON (Badges.Badge=Users.Badge);", CS_NULLTERM, CS_UNUSED);
	if (ret != CS_SUCCEED) {
		websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
		fprintf(stderr, "ct_command() failed\n");
		return -1;
	}
	
	ret = ct_send(cmd);
	if (ret != CS_SUCCEED) {
		websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
		fprintf(stderr, "ct_send: \"select Users.Name,Badges.Badge,Badges.Date\" failed with %d\n", ret);
		return -1;
	}
	
	while ((results_ret = ct_results(cmd, &result_type)) == CS_SUCCEED) 
	{
	 is_status_result = 0;
	 switch ((int) result_type) {
	    case CS_CMD_SUCCEED:
			break;
		case CS_CMD_DONE:
			break;
		case CS_CMD_FAIL:
			websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
			fprintf(stderr, "ct_results() result_type CS_CMD_FAIL.\n");
			return -1;
		case CS_STATUS_RESULT:
			websWrite(wp, T("<tr><td>ct_results: CS_STATUS_RESULT detected for sp_who</td></tr>"));
			fprintf(stdout, "ct_results: CS_STATUS_RESULT detected for sp_who\n");
			is_status_result = 1;
			/* fall through */
		case CS_ROW_RESULT:
			ret = ct_res_info(cmd, CS_NUMDATA, &num_cols, CS_UNUSED, NULL);
			printf("num_cols=%d\n",num_cols);
			if (ret != CS_SUCCEED || num_cols > maxcol) {
				websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
				fprintf(stderr, "ct_res_info() failed\n");
				return -1;
			}

			if (num_cols <= 0) {
				websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
				fprintf(stderr, "ct_res_info() return strange values\n");
				return -1;
			}

			if (is_status_result && num_cols != 1) {
				websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
				fprintf(stderr, "CS_STATUS_RESULT return more than 1 column\n");
				return -1;
			}
			
			for (i=0; i < num_cols; i++) 
			{

				/* here we can finally test for the return status column */
				ret = ct_describe(cmd, i+1, &col[i].datafmt);
				
				if (ret != CS_SUCCEED) {
					websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
					fprintf(stderr, "ct_describe() failed for column %d\n", i);
					return -1;
				}

				if (col[i].datafmt.status & CS_RETURN) {
					websWrite(wp, T("<tr>ct_describe() indicates a return code in column %d for sp_who</tr>"),i);
					fprintf(stdout, "ct_describe() indicates a return code in column %d for sp_who\n", i);
					is_return_status = i+1;
					
					/*
					 * other possible values:
					 * CS_CANBENULL
					 * CS_HIDDEN
					 * CS_IDENTITY
					 * CS_KEY
					 * CS_VERSION_KEY
					 * CS_TIMESTAMP
					 * CS_UPDATABLE
					 * CS_UPDATECOL
					 */
				}
				
				col[i].datafmt.datatype = CS_CHAR_TYPE;
				col[i].datafmt.format = CS_FMT_NULLTERM;
				col[i].datafmt.maxlength = colsize;
				col[i].datafmt.count = 1;
				col[i].datafmt.locale = NULL;

				ret = ct_bind(cmd, i+1, &col[i].datafmt, &col[i].data, &col[i].datalength, &col[i].ind);
				if (ret != CS_SUCCEED) 
				{
					websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
					fprintf(stderr, "ct_bind() failed\n");
					return -1;
				}
				
			}	
 	        row_count = 0;
			while ((ret = ct_fetch(cmd, CS_UNUSED, CS_UNUSED, CS_UNUSED, &count)) == CS_SUCCEED) 
			{
				if( row_count == 0) 
				{ /* titles */
					websWrite(wp,T("<tr>"));
					for (i=0; i < num_cols; i++) 
					{
						char fmt[40];
						//printf("col%d = %s",i,col[i].datafmt.namelen);
						//websWrite(wp,T("<td>XXXXX</td>"));
                        if (col[i].datafmt.namelen == 0) 
						{
						  //  printf("unnamed%d ",i+1);
							websWrite(wp,T("</tr>"));
							continue;
						}
                        websWrite(wp,T("<th>%s</th>"),col[i].datafmt.name);
					}
					websWrite(wp,T("</tr>"));
				}
				//printf("num_cols=%d\n",num_cols);
				for (i=0; i < num_cols; i++) { /* data */
					char fmt[40];
					if (col[i].ind && (strcmp(col[i].datafmt.name,"Name") == 0))
					{	
						websWrite(wp,T("<td>"));
						websWrite(wp,T("##Unoccupied##"));
					    websWrite(wp,T(" </td>\n"));
						continue;
					}
					if(i==0)
					   websWrite(wp, T("\n<tr>")); 
					sprintf(fmt, "%%-%d.%ds  ", col[i].datalength, col[i].datafmt.maxlength);
					//printf(fmt, col[i].data);
					websWrite(wp,T(" <td>"));
					//websWrite(wp,T(fmt),col[i].data);
					websWrite(wp,T(col[i].data));
					websWrite(wp,T(" </td>\n"));
					if (is_status_result && strcmp(col[i].data,"0")) {
						websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
						fprintf(stderr, "CS_STATUS_RESULT should return 0 as result\n");
						return -1;
					}
					if(i == (num_cols-1))
					   websWrite(wp,T("</tr>\n")); 
				}	
				
				row_count += count;
			}	
				
            if (is_status_result && row_count != 1) {
				websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
				fprintf(stderr, "CS_STATUS_RESULT should return a row\n");
				return -1;
            }
            
           switch ((int) ret) 
		   {
			 case CS_END_DATA:
				fprintf(stdout, "ct_results fetched %d rows.\n", row_count);
				break;
			 case CS_ROW_FAIL:
				websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
				fprintf(stderr, "ct_fetch() CS_ROW_FAIL on row %d.\n", row_count);
				return ret;
			 case CS_FAIL:
				websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
				fprintf(stderr, "ct_fetch() returned CS_FAIL.\n");
				return ret;
			 default:
				websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
				fprintf(stderr, "ct_fetch() unexpected return.\n");
				return ret;
		   }
			break;
		    case CS_COMPUTE_RESULT:
			 websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
			 fprintf(stderr, "ct_results() unexpected CS_COMPUTE_RESULT.\n");
			 return -1;
		    default:
			 websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
			 fprintf(stderr, "ct_results() unexpected result_type.\n");
			 return -1;
		}
	  }	
    switch ((int) results_ret) {
	  case CS_END_RESULTS:
		break;
	  case CS_FAIL:
		websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
		fprintf(stderr, "ct_results() failed.\n");
		return -1;
		break;
	 default:
		websWrite(wp, T("<tr>Perform Data Base Query Failed</tr>"));
		fprintf(stderr, "ct_results() unexpected return.\n");
		return -1;
	}
	
	ret = try_ctlogout(ctx, conn, cmd, verbose);
    if (ret != CS_SUCCEED) {
		websWrite(wp, T("<tr>DisConnedt To The Data Base Failed</tr>"));
		fprintf(stderr, "Logout failed\n");
		return -2;
    }
    
    return 0;
}

static int		formAddUser(webs_t wp, char_t *path, char_t *query)
{

    char_t	*userName, *badge,*ok;
	int	nCheck;
    char query[1024]= "INSERT Users (Name, Badge) VALUES (";
    
	websHeader(wp);
	websMsgStart(wp);

	//printf("IN formAddUser\n");
	a_assert(wp);
    
	CS_CONTEXT *ctx;
    CS_CONNECTION *conn;
    CS_COMMAND *cmd;
	int verbose = 0 ,ret;
	
	//printf("try_ctlogin\n");
	ret = try_ctlogin(&ctx, &conn, &cmd, verbose);
	if (ret != CS_SUCCEED) {
		websWrite(wp, T("SQL Login failed"));
		fprintf(stderr, "SQL Login failed\n");
		return -1;
	}
    
    //printf("get user name and badge\n");
	userName = websGetVar(wp, T("user"), T("")); 
	badge = websGetVar(wp, T("badge"), T("")); 
	ok = websGetVar(wp, T("ok"), T("")); 
    
	strcat(query,"'");
	strcat(query,userName);
	strcat(query,"',");
	strcat(query,badge);
    strcat(query,")");

	 //printf("run command insert the user\n");
	ret = run_command(cmd,query);
	if (ret != CS_SUCCEED)
	{ 
	   websWrite(wp, T("Add User To the SQL Failed")); 
	   printf("Add User To the SQL Failed  \n");
	   websMsgEnd(wp);
		websFooter(wp);
		websDone(wp, 200);  
	    return -1;
	}
    
	ret = try_ctlogout(ctx, conn, cmd, verbose);
    if (ret != CS_SUCCEED) {
		websWrite(wp, T("SQL Server Logout Failed"));
		fprintf(stderr, "Logout failed\n");
		websMsgEnd(wp);
	    websFooter(wp);
	    websDone(wp, 200);
		return -1;
    }
    
	//printf("END the formAddUser ret = %d CS_SUCCEED = %d\n",ret,CS_SUCCEED);
    websWrite(wp, T("User, \"%s\" was successfully added."),userName);
    
	websMsgEnd(wp);
	websFooter(wp);
	websDone(wp, 200);
    return 0 ;

}


static void	websMsgStart(webs_t wp)
{
	websWrite(wp, MSG_START);
}

static void	websMsgEnd(webs_t wp)
{
	websWrite(wp, MSG_END);
}